-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.27-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table nababan_client_ziptango_kua_api.dashboard_account
CREATE TABLE IF NOT EXISTS `dashboard_account` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `account_username` varchar(128) NOT NULL,
  `account_password` tinytext NOT NULL,
  `account_fullname` varchar(128) NOT NULL,
  `account_level` smallint(6) NOT NULL DEFAULT '0',
  `account_datetime_create` datetime NOT NULL,
  `account_datetime_update` datetime NOT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nababan_client_ziptango_kua_api.dashboard_account: ~2 rows (approximately)
/*!40000 ALTER TABLE `dashboard_account` DISABLE KEYS */;
INSERT INTO `dashboard_account` (`seq`, `account_username`, `account_password`, `account_fullname`, `account_level`, `account_datetime_create`, `account_datetime_update`) VALUES
	(1, 'admin', 'n8e+eyY41U6cKj4addsVIuOPpi1wG19O75mj1KjhvSY3vd8OR0OxRrEyPKouNxQwDVy2OuwaTotibGe7wQxnGA==', 'Administrator', 1, '2019-11-26 00:53:51', '2020-03-05 16:07:53'),
	(2, 'dev', 'koTwAyD4YJJMlffRXcsn0dwFfAchP620RshCjFpVFLpBDJAIjSci1GM0ezAZAqRB+PeJ4fDhg7D27Jh7fCrdMQ==', 'Developer', 1, '2019-11-26 00:53:51', '2020-03-05 16:08:24');
/*!40000 ALTER TABLE `dashboard_account` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
