-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.27-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table nababan_client_ziptango_kua_core.kua_vendor
CREATE TABLE IF NOT EXISTS `kua_vendor` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_code` varchar(32) NOT NULL,
  `vendor_name` varchar(32) NOT NULL,
  `vendor_datetime_insert` datetime DEFAULT NULL,
  `vendor_datetime_update` datetime DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nababan_client_ziptango_kua_core.kua_vendor: ~2 rows (approximately)
/*!40000 ALTER TABLE `kua_vendor` DISABLE KEYS */;
INSERT INTO `kua_vendor` (`seq`, `vendor_code`, `vendor_name`, `vendor_datetime_insert`, `vendor_datetime_update`) VALUES
	(1, 'chinese-astrology', 'Chinese Astrology', '2020-03-05 16:44:15', '2020-03-05 17:39:51'),
	(2, 'gregorian-calendar', 'Gregorian Calendar', '2020-03-05 17:37:29', '2020-03-05 17:37:29');
/*!40000 ALTER TABLE `kua_vendor` ENABLE KEYS */;

-- Dumping structure for table nababan_client_ziptango_kua_core.kua_zodiac
CREATE TABLE IF NOT EXISTS `kua_zodiac` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_seq` int(11) NOT NULL,
  `zodiac_year` int(11) NOT NULL,
  `zodiac_type` enum('astrology','element','shio') NOT NULL DEFAULT 'shio',
  `zodiac_name_english` varchar(128) NOT NULL,
  `zodiac_name_indonesia` varchar(128) NOT NULL,
  `zodiac_insert_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nababan_client_ziptango_kua_core.kua_zodiac: ~1 rows (approximately)
/*!40000 ALTER TABLE `kua_zodiac` DISABLE KEYS */;
INSERT INTO `kua_zodiac` (`seq`, `vendor_seq`, `zodiac_year`, `zodiac_type`, `zodiac_name_english`, `zodiac_name_indonesia`, `zodiac_insert_datetime`) VALUES
	(1, 1, 1924, 'shio', 'Rat', 'Tikus', NULL),
	(2, 1, 1920, 'shio', 'Monkey', 'Monyet', '2020-03-05 18:35:57');
/*!40000 ALTER TABLE `kua_zodiac` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
