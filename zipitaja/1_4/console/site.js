/**
 * Created by Riad on 21-Jul-16.
 */
function appErrorDialogS(message) {
    appErrorDialog("dialog-container", message);
}

function appErrorDialog(placementId, message) {
    appModalDialog(
        placementId,
        "Oops, Kesalahan!",
        '<div class="i-circle danger"><i class="fa fa-times"></i></div>',
        message,
        null,
        '<button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>'
    );

}

function appInfoDialogS(message) {
    appInfoDialog("dialog-container", message);

}

function appInfoDialog(placementId, message) {
    appModalDialog(
        placementId,
        "Informasi",
        '<div class="i-circle success"><i class="fa fa-check"></i></div>',
        message,
        null,
        '<button type="button" class="btn btn-success" data-dismiss="modal">OK</button>'
    );

}

function appConfirmDialogS(message, posCallback, negCallback) {
    appConfirmDialog("dialog-container", message, posCallback, negCallback);
}

function appConfirmDialog(placementId, message, posCallback, negCallback) {
    appModalDialog(
        placementId,
        "Konfirmasi",
        '<div class="i-circle primary"><i class="fa fa-check"></i></div>',
        message,
        '<button id="neg" type="button" class="btn btn-default" >Batal</button>',
        '<button id="pos" type="button" class="btn btn-primary" >Ya</button>'
    );

    $("#neg").click(function () {
        $("#app-dlg-id4353").modal("hide");
        if (negCallback)negCallback();
    });
    $("#pos").click(function () {
        $("#app-dlg-id4353").modal("hide");
        if (posCallback)posCallback();
    });

}

function appModalDialog(placementId, heading, icon, formContent, btnNegative, btnPositive) {
    var html;
    html = '<div id="app-dlg-id4353" class="modal" tabindex="-1" role="dialog">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<div class="text-center">';
    html += icon;
    html += '<h4>' + heading + '</h4>';
    html += '<p>' + formContent + '</p>';
    html += '</div>';
    html += '</div>';
    html += '<div class="modal-footer">';
    if (btnNegative)
        html += btnNegative;
    if (btnPositive)
        html += btnPositive;
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    $("#" + placementId).html(html);
    $("#app-dlg-id4353").modal("show");
}

function appFormModal(id, container, formId, title) {
    var frm1 = $('#' + formId);
    var content = frm1.html();
    frm1.remove();

    var html = '';
    html += '<div id="' + id + '" class="modal colored-header md-effect-10" tabindex="-1" role="dialog">';
    html += '<div class="modal-dialog form-modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="md-content">';
    html += '<div class="modal-header">';
    html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '<span aria-hidden="true">&times;</span>';
    html += '</button>';
    html += '<h3 class="modal-title">' + title + '</h3>';
    html += '</div>';
    html += '<div class="modal-body form-modal-body">';
    html += '<div id="' + formId + '-info"></div>';
    html += '<form id="' + formId + '" method="post" enctype="multipart/form-data" novalidate></form>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<button type="button" class="btn btn-default md-close" data-dismiss="modal">Tutup</button>';
    html += '<button id="' + formId + '-submit" type="submit" class="btn btn-primary" data-loading-text="<i class=\'fa fa-circle-o-notch fa-spin\'></i> Menyimpan" form="' + formId + '"><i class="fa fa-save"></i> Simpan';
    html += '</button>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#" + container).html(html);
    $('#' + formId).html(content);
    $('#' + formId).parsley();
    $('#' + id).on('hidden.bs.modal', function () {
        $('#' + formId).parsley().reset();
    })

}

function showForceModal(id) {
    $("#" + id).modal({backdrop: 'static', keyboard: false});

}

function appProgressDialogShow() {
    var html;
    html = '<div id="3453-progress-dialog" class="modal colored-header md-effect-10" tabindex="-1" role="dialog">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="md-content">';
    html += '<div class="modal-header">';
    html += '<h3 class="text-center">Mohon Tunggu</h3>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<div class="progress progress-striped active">';
    html += '<div class="progress-bar progress-bar-info" style="width: 100%"></div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';

    $("#dialog-container").html(html);
    $("#3453-progress-dialog").modal({backdrop: 'static', keyboard: false});
}

function appProgressDialogHide() {
    $("#3453-progress-dialog").modal('hide');

}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function createAlert(class_name, message) {
    var id = guid();
    return '<div id="' + id + '" class="alert ' + class_name + ' fade in text-center">' +
        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
        message +
        '</div><script>$("#' + id + '").fadeTo(4000, 500).slideUp(500, ' +
        'function(){$("#success-alert").alert(\'close\');});</script>';
}

function appInlineAjaxPOSTVar2(url, formData, form_id, re0001Callback, re0004Callback, re0005Callback) {
    appInlineAjaxInit(url, 'POST', formData, form_id, re0001Callback, null, null, re0004Callback, re0005Callback, null);

}

function appInlineAjaxInit(url, method, formData, form_id, re0001Callback, re0002Callback, re0003Callback, re0004Callback, re0005Callback, re0006Callback) {
    var form_submit = $('#' + form_id + '-submit');

    appAjaxInit(url, method, formData, function () {
        (form_submit.button('loading'));

    }, function () {
        (form_submit.button('reset'));

    }, re0001Callback, re0002Callback, re0003Callback, re0004Callback, re0005Callback, re0006Callback);

}

function appAjaxPOSTVar1(url, formData, re0001Callback, re0002Callback, re0003Callback, re0005Callback) {
    appNormalAjaxInit(url, 'POST', formData, re0001Callback, re0002Callback, re0003Callback, null, re0005Callback, null);
}

function appAjaxPOSTVar2(url, formData, re0001Callback, re0004Callback, re0005Callback) {
    appNormalAjaxInit(url, 'POST', formData, re0001Callback, null, null, re0004Callback, re0005Callback, null);
}

function appAjaxGETVar1(url, re0001Callback, re0002Callback, re0003Callback, re0005Callback) {
    appNormalAjaxInit(url, 'GET', null, re0001Callback, re0002Callback, re0003Callback, null, re0005Callback, null);
}

function appAjaxGETVar2(url, formData, re0001Callback, re0004Callback, re0005Callback) {
    appNormalAjaxInit(url, 'GET', formData, re0001Callback, null, null, re0004Callback, re0005Callback, null);
}

function appNormalAjaxInit(url, method, formData, re0001Callback, re0002Callback, re0003Callback, re0004Callback, re0005Callback, re0006Callback) {
    appAjaxInit(url, method, formData, function () {
        appProgressDialogShow();

    }, function () {
        appProgressDialogHide();

    }, re0001Callback, re0002Callback, re0003Callback, re0004Callback, re0005Callback, re0006Callback);
}

function appAjaxInit(url, method, formData, beforeSendCallback, completeCallback, re0001Callback, re0002Callback, re0003Callback, re0004Callback, re0005Callback, re0006Callback) {
    $.ajax({
        url: url,
        type: method,
        data: formData,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        beforeSend: function () {
            if (beforeSendCallback)
                beforeSendCallback();

        },
        success: function (data) {
            if (completeCallback)
                completeCallback();

            console.log(data);
            if (!data) {
                if (re0001Callback)
                    re0001Callback('Respon server tidak dapat diurai');
                return;
            }
            try {
                var obj = JSON.parse(data);
                switch (obj.Code) {
                    case 'RE0001':
                        if (re0001Callback)
                            re0001Callback(obj.Response);
                        break;
                    case 'RE0002':
                        if (re0002Callback)
                            re0002Callback(obj.Response);
                        break;
                    case 'RE0003':
                        if (re0003Callback)
                            re0003Callback(obj.Response);
                        break;
                    case 'RE0004':
                        if (re0004Callback)
                            re0004Callback(obj.Response);
                        break;
                    case 'RE0005':
                        if (re0005Callback)
                            re0005Callback(obj.Response);
                        break;
                    case 'RE0006':
                        if (re0006Callback)
                            re0006Callback(obj.Response);
                        break;
                }
            }
            catch (e) {
                if (re0001Callback)
                    re0001Callback('Gagal mengurai respon<br/>' + e.message);
            }


        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (completeCallback)
                completeCallback();

            if (re0001Callback)
                re0001Callback('Request Error : ' + errorThrown);

        }
    });
}

function fetchSelectOptions(selectId, fLabel, fValue, apiUrl, defaultValue, extraLabel, extraValue) {
    var sel = $('#' + selectId);
    sel.html('<option value="-1">Memuat Data...</option>');

    appAjaxGETVar1(apiUrl, function (response) {
            sel.html('<option value="-1">Gagal Memuat Data</option>');
            console.log(response);

        }
        , function (data) {
            sel.html('');
            if (extraLabel != null && extraValue != null)
                sel.append($('<option>', {
                    value: extraValue,
                    text: extraLabel
                }));

            for (var i = 0; i < data.length; i++)
                sel.append($('<option>', {
                    value: data[i][fValue],
                    text: data[i][fLabel]
                }));

            if (defaultValue != null)
                sel.val(defaultValue);

        }
        , function (response) {
            sel.html('<option value="-1">Tidak Ada Data</option>');

        }
        , null
    )
}