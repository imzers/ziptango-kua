<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 12:32 AM
 */

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/PushNotificationManager.php";

$engine = new Engine();

header("Content-Type: text/plain");

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'user' => true,
        'summary' => true,
        'image' => false
    )
);

$user = $engine->getPOSTField('user');
$summary = $engine->getPOSTField('summary');

$image = uniqid() . '.png';
if (file_exists($_FILES['image']['tmp_name']))
    move_uploaded_file($_FILES['image']['tmp_name'], 'image/' . $image);
else
    $image = "";

// if (!$image) die($engine->createResponse(Engine::codError, 'Image field is required.'));

$usr_cond = "";
if ($user == 2)
    $usr_cond = " WHERE mobile = 1";
else if ($user == 3)
    $usr_cond = " WHERE mobile = 2";
else
    $usr_cond = "";

//$query = $engine->executePrepared("SELECT * FROM gcm_users", array());

$query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users" . $usr_cond, array());
$ids = array();
while ($row = $query->fetch(PDO::FETCH_ASSOC))
    $ids[] = array($row["gcm_regid"], $row["mobile"]);

//die($engine->createResponse(Engine::codError, json_encode($ids, JSON_PRETTY_PRINT)));

//Send notification
$gcm = new PushNotificationManager();
$gcm->sendPromoNotification($ids, $summary, $image ? API_PATH . '/console/image/' . $image : '');

$engine->setSingleLineResponse(1);
$engine->sendResponse();