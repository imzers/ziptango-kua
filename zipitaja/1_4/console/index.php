<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ziptango Notification Console</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="favicon.png">

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="fileinput/css/fileinput.min.css"/>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <script type="application/javascript" src="jquery.min.js"></script>
    <script type="application/javascript" src="fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script type="application/javascript" src="fileinput/js/plugins/sortable.min.js"></script>
    <script type="application/javascript" src="fileinput/js/plugins/purify.min.js"></script>
    <script type="application/javascript" src="fileinput/js/fileinput.min.js"></script>
    <script type="application/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="application/javascript" src="fileinput/themes/fa/theme.js"></script>
    <script type="application/javascript" src="fileinput/js/locales/id.js"></script>
    <script type="application/javascript" src="site.js"></script>

    <style>
        .form-control {
            max-width: 400px;
        }

        .input-group {
            width: 100%;
            max-width: 400px;
        }
    </style>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-inverse" style="margin-top: 20px">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Ziptango Notification Console</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <div class="well">

        <form id="form-input" method="post" enctype="multipart/form-data" novalidate>
            <div class="form-group">
                <label>User</label>
                <select id="user" name="user" class="form-control">
                    <option value="1">All User</option>
                    <option value="2">Android User Only</option>
                    <option value="3">iOS User Only</option>
                </select>
            </div>

            <div class="form-group">
                <label>Summary Text</label>
                <textarea id="summary" name="summary" class="form-control" rows="5"></textarea>
            </div>

            <div class="form-group">
                <label>Image</label>
                <div class="input-group">
                    <input id="image" name="image" type="file" class="file" data-show-upload="false">
                </div>
            </div>

            <hr/>

            <input type="submit" class="btn btn-primary" value="Submit"/>
        </form>

    </div>
</div>

<div id="dialog-container"></div>

<script>
    $('#form-input').submit(function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);
            appAjaxPOSTVar2('submit.php', formData, function (response) {
                appErrorDialogS(response);

            }, function (response) {
                appInfoDialogS("Successfully sent notification.");
                //$('#form-input')[0].reset();

            }, null);

        }
    );
</script>

</body>
</html>