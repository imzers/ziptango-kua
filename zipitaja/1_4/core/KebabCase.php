<?php

class KebabCase
{
    public function __construct()
    {
    }

    public function kebabify($text)
    {
        return preg_replace('/-$/', '', preg_replace('/\W+/', '-', strtolower($text)));
    }
}