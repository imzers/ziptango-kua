<?php

class PushNotificationManager
{

    //put your code here
    // constructor
    const MSG_NEW_LITTLE = "MSG_NEW_LITTLE";
    const MSG_NEW_BIG = "MSG_NEW_BIG";
    const MSG_READ = "MSG_READ";

    function __construct()
    {
    }

    public function sendMessageNotification($tipe, $arrayIds, $id, $id_service, $id_user_send, $id_user_get, $time_send, $text, $img)
    {
        $mode = PushNotificationManager::MSG_NEW_LITTLE;
        $msg_to_send = $text;
        if (strlen($text) > 750) {
            $mode = PushNotificationManager::MSG_NEW_BIG;
            $msg_to_send = "";
        }
        $message = array(
            "tipe" => $tipe,
            "mode" => $mode,
            "id" => $id,
            "id_service" => $id_service,
            "id_user_send" => $id_user_send,
            "id_user_get" => $id_user_get,
            "time_send" => $time_send,
            "text" => $msg_to_send,
            "img" => $img
        );

        $i_aps = array(
            'alert' => 'Anda memiliki pesan baru',
            'sound' => 'default',
            'link_url' => '',
        );

        $this->send_notification(self::PROV_GCM, $arrayIds, $message);
        $this->send_notification(self::PROV_APNS, $arrayIds, $message, $i_aps);

    }

    public function sendReadNotification($tipe, $arrayIds, $id, $id_service, $me)
    {
        $message = array(
            "tipe" => $tipe,
            "id" => $id,
            "id_service" => $id_service,
            "id_user" => $me
        );

        $i_aps = array(
            'content-available' => 1,
            'link_url' => ''
        );

        $this->send_notification(self::PROV_GCM, $arrayIds, $message);
        $this->send_notification(self::PROV_APNS, $arrayIds, $message, $i_aps);

    }

    public function sendSoldNotification($tipe, $arrayIds)
    {
        $message = array(
            "tipe" => $tipe
        );

        $i_aps = array(
            'alert' => 'Selamat! Barang anda terjual. Silakan cek order anda.',
            'sound' => 'default',
            'link_url' => '',
        );

        $this->send_notification(self::PROV_GCM, $arrayIds, $message);
        $this->send_notification(self::PROV_APNS, $arrayIds, $message, $i_aps);

    }

    public function sendPromoNotification($arrayIds, $summary, $img_url)
    {
        $message = array(
            'tipe' => 4,
            'summary' => $summary,
            'image_url' => $img_url
        );
        $i_aps = array(
            'alert' => $summary,
            'sound' => 'default',
            'link_url' => '',
        );

        $this->send_notification(self::PROV_GCM, $arrayIds, $message);
        $this->send_notification(self::PROV_APNS, $arrayIds, $message, $i_aps);

    }

    const PROV_GCM = 1;
    const PROV_APNS = 2;

    /**
     * Sending Push Notification
     *
     * @param $provider
     * @param $registration_ids
     * @param $message
     * @param array $i_aps
     */
    public function send_notification($provider, $registration_ids, $message, $i_aps = [])
    {
        require_once "Consts.php";

        if ($provider == self::PROV_GCM) {
            $reg_id = array();
            $ct = 0;
            for ($i = 0; $i < count($registration_ids); $i++) {
                if ($registration_ids[$i][1] == 1) {
                    $reg_id[$ct] = $registration_ids[$i][0];
                    $ct++;
                }
            }

            // Set POST variables
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array('registration_ids' => $reg_id, 'data' => $message,);
            $headers = array('Authorization: key=' . GCM_KEY, 'Content-Type: application/json');

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);

            // if ($result === FALSE) {
            //    echo 'Curl failed: ' . curl_error($ch);
            // }
            // echo $result;

        } else if ($provider == self::PROV_APNS) {
            // Adjust to your timezone
            date_default_timezone_set('Asia/Jakarta');

            require_once '../vendor/autoload.php';

            // Instanciate a new ApnsPHP_Push object
            $push = new ApnsPHP_Push(
                ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
                __DIR__ . "/apns.pem"
            );

            // Set the Root Certificate Autority to verify the Apple remote peer
            $push->setRootCertificationAuthority(__DIR__ . '/apns_root_ca.pem');

            // Increase write interval to 100ms (default value is 10ms).
            // This is an example value, the 10ms default value is OK in most cases.
            // To speed up the sending operations, use Zero as parameter but
            // some messages may be lost.
            $push->setWriteInterval(100 * 1000);

            // Connect to the Apple Push Notification Service
            ob_start();
            $push->connect();

            $reg_id = array();
            $ct = 0;
            for ($i = 0; $i < count($registration_ids); $i++) {
                if ($registration_ids[$i][1] == 2) {
                    $reg_id[$ct] = $registration_ids[$i][0];
                    $ct++;
                }
            }
            for ($i = 1; $i <= count($reg_id); $i++) {
                $deviceToken = $reg_id[$i];
                if ($deviceToken && $deviceToken != '-') {

                    try {
                        $iMsg = new ApnsPHP_Message($deviceToken);
                        $iMsg->setText($i_aps['alert']);
                        $iMsg->setSound($i_aps['sound']);
                        $iMsg->setContentAvailable($i_aps['content-available'] ? true : false);
                        $iMsg->setCustomProperty('back', $message);
                        $push->add($iMsg);
                    } catch (ApnsPHP_Message_Exception $e) {
                        // Ignore
                    }
                }
            }

            // Send all messages in the message queue
            try {
                $push->send();
            } catch (ApnsPHP_Push_Exception $e){
                // Ignore
            }
            // Disconnect from the Apple Push Notification Service
            $push->disconnect();
            ob_end_clean();

            // Examine the error message container
            // $aErrorQueue = $push->getErrors();
            // if (!empty($aErrorQueue)) {
            //     var_dump($aErrorQueue);
            // }
        }
    }


}