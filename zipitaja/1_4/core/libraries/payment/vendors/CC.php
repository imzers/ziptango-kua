<?php
/**
 * @param $tran_id
 * @param $cust_name
 * @param $cust_email
 * @param $amount
 * @param CustAddress $address
 * @param $product
 * @param $jsonProduct
 * @return string
 */
function generateCCForm($tran_id, $cust_name, $cust_email, $amount, $address, $product, $jsonProduct) {
    require_once "../core/Consts.php";
    require_once "../core/MCrypt.php";

    $merchant_id = CC_MERCHANT_ID;
    $trans_pwd = CC_TRANS_PWD;

    $crypt = new MCrypt();
    $param = $crypt->encrypt($jsonProduct);
    $param1 = substr($param, 0, 194);
    $param2 = substr($param, 194, 398);

    $amount = $amount + ceil($amount * 2.8 / 100);

    $signaturecc = sha1('##' . strtoupper($merchant_id) . '##' . strtoupper($trans_pwd) . '##' . strtoupper($tran_id) . "##$amount.00##" . '0' . '##');
    $post = array(
        "LANG" => 'en',
        "MERCHANTID" => $merchant_id,
        "PAYMENT_METHOD" => '1',
        "TXN_PASSWORD" => $trans_pwd,
        "MERCHANT_TRANID" => $tran_id,
        "CURRENCYCODE" => 'IDR',
        "AMOUNT" => "$amount.00",
        "CUSTNAME" => $cust_name,
        "CUSTEMAIL" => $cust_email,
        "DESCRIPTION" => '-',
        "RETURN_URL" => APP_CC_CALLBACK,
        "SIGNATURE" => $signaturecc,
        "BILLING_ADDRESS" => $address->address,
        "BILLING_ADDRESS_CITY" => $address->city,
        "BILLING_ADDRESS_REGION" => $address->country,
        "BILLING_ADDRESS_STATE" => $address->state,
        "BILLING_ADDRESS_POSCODE" => '',
        "BILLING_ADDRESS_COUNTRY_CODE" => '',
        "RECEIVER_NAME_FOR_SHIPPING" => '',
        "SHIPPING_ADDRESS" => $address->address,
        "SHIPPING_ADDRESS_CITY" => $address->city,
        "SHIPPING_ADDRESS_REGION" => $address->country,
        "SHIPPING_ADDRESS_STATE" => $address->state,
        "SHIPPING_ADDRESS_POSCODE" => '',
        "SHIPPING_ADDRESS_COUNTRY_CODE" => '',
        "SHIPPINGCOST" => '0.00',
        "PHONE_NO" => $address->phone,
        "MREF1" => isset($product[0]) ? $product[0] : '',
        "MREF2" => isset($product[1]) ? $product[1] : '',
        "MREF3" => isset($product[2]) ? $product[2] : '',
        "MREF4" => isset($product[3]) ? $product[3] : '',
        "MREF5" => isset($product[4]) ? $product[4] : '',
        "MREF6" => isset($product[5]) ? $product[5] : '',
        "MREF7" => isset($product[6]) ? $product[6] : '',
        "MREF8" => isset($product[7]) ? $product[7] : '',
        "MREF9" => isset($product[8]) ? $product[8] : '',
        "MREF10" => isset($product[9]) ? $product[9] : '',
        "MPARAM1" => $param1,
        "MPARAM2" => $param2,
        "CUSTOMER_REF" => '',
        "PYMT_IND" => 'card_range_ind',
        "PYMT_CRITERIA" => 'card_bin_3D_Indo',
        //"PYMT_IND" => '',
        //"PYMT_CRITERIA" => '',
        "FRISK1" => '',
        "FRISK2" => '',
        "DOMICILE_ADDRESS" => $address->address,
        "DOMICILE_ADDRESS_CITY" => $address->city,
        "DOMICILE_ADDRESS_REGION" => $address->country,
        "DOMICILE_ADDRESS_STATE" => $address->state,
        "DOMICILE_ADDRESS_POSCODE" => '',
        "DOMICILE_ADDRESS_COUNTRY_CODE" => '',
        "DOMICILE_PHONE_NO" => $address->phone,
        "handshake_url" => '',
        "handshake_param" => '',
    );

    // Yang diubah URL nya ke prod / dev
    $string = "<form method='post' name='form' action='https://uc.faspay.co.id/payment/PaymentWindow.jsp'>";
    if ($post != null) {
        foreach ($post as $name => $value) {
            $string .= "<input type='hidden' name='{$name}' value='{$value}'>";
        }
    }

    $string .= "</form>";
    $string .= "<script> document.form.submit();</script>";

    return $string;
}

class CustAddress {
    public $address;
    public $city;
    public $state;
    public $country;
    public $phone;
    public $lat;
    public $lng;
    public $unit_apt;
    public $address_notes;

    /**
     * CustAddress constructor.
     * @param $address
     * @param $city
     * @param $state
     * @param $country
     * @param $phone
     * @param $lat
     * @param $lng
     * @param $unit_apt
     * @param $address_notes
     */
    public function __construct($address, $city, $state, $country, $phone, $lat, $lng, $unit_apt, $address_notes)
    {
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->phone = $phone;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->unit_apt = $unit_apt;
        $this->address_notes = $address_notes;
    }
}
