<?php


class Faspay {
	protected $faspay_config = array();
	private $faspay_headers;
	protected $faspay_endpoint = array(
		'dev'		=> 'https://dev.faspay.co.id/pws/300011/183xx00010100000',
		'prod'		=> 'https://web.faspay.co.id/pws/300011/383xx00010100000',
	);
	public $error = FALSE, $error_msg = array();
	private $faspay_redirect = array(
		'dev'					=> 'https://dev.faspay.co.id/pws/100003/0830000010100000/##SIGNATURE##?',
		'prod'					=> 'https://web.faspay.co.id/pws/100003/2830000010100000/##SIGNATURE##?',
	);
	protected $url_redirect = 'https://dev.faspay.co.id/pws/100003/0830000010100000/##SIGNATURE##?';
	protected $url_faspay;
	function __construct($faspay_config) {
		$this->faspay_config = $faspay_config;
		$this->faspay_headers = array(
			'Content-Type'		=> 'application/xml',
			'Accept'			=> 'application/xml,*/*;q=0.1',
		);
		$this->faspay_headers = $this->create_curl_headers($this->faspay_headers);
		$this->faspay_config['faspay_merchant_name'] = (isset($this->faspay_config['faspay_merchant_name']) ? $this->faspay_config['faspay_merchant_name'] : '');
		$this->faspay_config['faspay_merchant_id'] = (isset($this->faspay_config['faspay_merchant_id']) ? $this->faspay_config['faspay_merchant_id'] : '');
		$this->faspay_config['faspay_merchant_userid'] = (isset($this->faspay_config['faspay_merchant_userid']) ? $this->faspay_config['faspay_merchant_userid'] : '');
		$this->faspay_config['faspay_merchant_password'] = (isset($this->faspay_config['faspay_merchant_password']) ? $this->faspay_config['faspay_merchant_password'] : '');
		
		if (isset($this->faspay_config['faspay_environment']) && is_string($this->faspay_config['faspay_environment'])) {
			if (strtolower($this->faspay_config['faspay_environment']) === strtolower('prod')) {
				$this->url_redirect = $this->faspay_redirect['prod'];
				$this->url_faspay = $this->faspay_endpoint['prod'];
			} else {
				$this->url_redirect = $this->faspay_redirect['dev'];
				$this->url_faspay = $this->faspay_endpoint['dev'];
			}
		}
	}
	public function get_faspay_config() {
		return $this->faspay_config;
	}
	//------------------------------------------------------------------------------------
	function xmltoarray($contents, $get_attributes=1) {
		if(!$contents) return array();
		if(!function_exists('xml_parser_create')) {
			return array();
			}
		$parser = xml_parser_create();
		xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
		xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
		xml_parse_into_struct( $parser, $contents, $xml_values );
		xml_parser_free( $parser );
		if(!$xml_values) return;
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();
		$current = &$xml_array;
		foreach($xml_values as $data) {
			unset($attributes,$value);
			extract($data);
			$result = '';
			if($get_attributes) {
				$result = array();
				if(isset($value)) $result['value'] = $value;
				if(isset($attributes)) {
					foreach($attributes as $attr => $val) {
						if($get_attributes == 1) $result['attr'][$attr] = $val;
						}
					}
				} elseif(isset($value)) {
				$result = $value;
				}
			if($type == "open") {
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) {
					$current[$tag] = $result;
					$current = &$current[$tag];
					} else {
					if(isset($current[$tag][0])) {
						array_push($current[$tag], $result);
						} else {
						$current[$tag] = array($current[$tag],$result);
						}
					$last = count($current[$tag]) - 1;
					$current = &$current[$tag][$last];
					}
				} elseif($type == "complete") {
				if(!isset($current[$tag])) {
					$current[$tag] = $result;
					} else {
					if((is_array($current[$tag]) and $get_attributes == 0)
					or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
						array_push($current[$tag],$result);
						} else {
						$current[$tag] = array($current[$tag],$result);
						}
					}
				} elseif($type == 'close') {
				$current = &$parent[$level-1];
				}
		}
		return($xml_array);
	}
	function create_curl_request($action, $url, $UA, $headers = null, $params = array(), $timeout = 30) {
		$cookie_file = (dirname(__FILE__).'/cookies.txt');
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) );
		$ch = curl_init();
		switch (strtolower($action)) {
			case 'get':
				if ((is_array($params)) && (count($params) > 0)) {
					$Querystring = http_build_query($params);
					$url .= "?";
					$url .= $Querystring;
				}
			break;
			case 'post':
			default:
				$url .= "";
			break;
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		if ($headers != null) {
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		} else {
			curl_setopt($ch, CURLOPT_HEADER, false);
		}
		curl_setopt($ch, CURLOPT_USERAGENT, $UA);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_COOKIE, $cookie_file);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		$post_fields = NULL;
		switch (strtolower($action)) {
			case 'get':
				curl_setopt($ch, CURLOPT_POST, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, null);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			break;
			case 'post':
			default:
				if ((is_array($params)) && (count($params) > 0) && (is_array($headers) && count($headers) > 0)) {
					foreach ($headers as $heval) {
						$getContentType = explode(":", $heval);
						if (strtolower($getContentType[0]) !== 'content-type') {
							continue;
						}
						switch (strtolower(trim($getContentType[0]))) {
							case 'content-type':
								if (isset($getContentType[1])) {
									switch (strtolower(trim($getContentType[1]))) {
										case 'application/xml':
											$post_fields = $post_fields;
										break;
										case 'application/json':
											$post_fields = json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
										break;
										case 'application/x-www-form-urlencoded':
											$post_fields = http_build_query($params);
										break;
										default:
											$post_fields = http_build_query($params);
										break;
									}
								}
							break;
							default:
								$post_fields = http_build_query($params);
							break;
						}
					}
				} else if ((!empty($params)) || ($params != '')) {
					$post_fields = $params;
				}
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			break;
		}
		// Get Response
		$response = curl_exec($ch);
		$mixed_info = curl_getinfo($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header_string = substr($response, 0, $header_size);
		$header_content = $this->get_headers_from_curl_response($header_string);
		$header_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if (count($header_content) > 1) {
			$header_content = end($header_content);
		}
		$body = substr($response, $header_size);
		curl_close ($ch);
		$return = array(
			'request'		=> array(
				'method'			=> $action,
				'host'				=> $url,
				'header'			=> $headers,
				'body'				=> $post_fields,
			),
			'response'		=> array(),
		);
		if (!empty($response) || $response != '') {
			$return['response']['code'] = (int)$header_code;
			$return['response']['header'] = array(
				'size' => $header_size, 
				'string' => $header_string,
				'content' => $header_content,
			);
			$return['response']['body'] = $body;
			return $return;
		}
		return false;
	}
	
	##########################################################
	# Faspay
	##########################################################
	function faspay_create_signature_merchant($user_id, $user_pass) {
		return sha1(md5("{$user_id}{$user_pass}"));
	}
	function faspay_create_signature_order($user_id, $user_pass, $order_id) {
		return sha1(md5("{$user_id}{$user_pass}{$order_id}"));
	}
	function custom_arraytoxml($array, &$xml){
		foreach ($array as $key => $value) {
			if(is_array($value)){
				if (is_numeric($key)) {
					$key = "e";
				}
				
				$label = $xml->addChild($key);
				$this->custom_arraytoxml($value, $label);
			} else {
				$xml->addChild($key, $value);
			}
		}
	}
	function create_faspay_payment_structure($input_params, $payment_channel, $collectData, $co) {
		$date_created = new DateTime();
		$date_tomorrow = date('Y-m-d H:i:s', (strtotime($date_created->format('Y-m-d H:i:s')) + 86400));
		
		$bill_desc = '';
		$bill_descs = $co->getServicesName();
		if (is_array($bill_descs) && (count($bill_descs) > 0)) {
			$for_i = 0;
			foreach ($bill_descs as $desc) {
				if ($for_i > 0) {
					$bill_desc .= sprintf("- %s", $desc);
				} else {
					$bill_desc .= sprintf("%s", $desc);
				}
				$for_i++;
			}
		}
		$faspay_structure = array(
			"request" 						=>  'Payment For Jajan Dijalan',
			"merchant_id" 					=>  $this->faspay_config['faspay_merchant_id'],
			"merchant" 						=>  $this->faspay_config['faspay_merchant_name'],
			"bill_no" 						=>  $co->getUniqIdFromToken($collectData['collect']['payment_token']),
			"bill_reff" 					=>  $co->getUniqIdFromToken($collectData['collect']['payment_token']),
			"bill_date" 					=>  $date_created->format('Y-m-d H:i:s'),
			"bill_expired" 					=>  $date_tomorrow,
			"bill_desc" 					=>  $bill_desc,
			"bill_currency" 				=>  "IDR",
			//"bill_gross" =>  "900000", // Without tax
			//"bill_tax" =>  "9000", // Tax price (normally 10%)
			//"bill_miscfee" =>  "10000", // Fee for payment channel, etc
			"bill_total" 					=>  $co->getGrandTotal(),
			"cust_no" 						=>  $feed['id'],
			"cust_name" 					=>  sprintf("%s %s", (isset($collectData['collect']['payment_userinfo']['firstname']) ? $collectData['collect']['payment_userinfo']['firstname'] : ''), (isset($collectData['collect']['payment_userinfo']['lastname']) ? $collectData['collect']['payment_userinfo']['lastname'] : '')),
			"payment_channel" 				=>  $payment_channel,
			"pay_type" 						=>  "1", // 1: Full settlement
			"bank_userid" 					=>  $this->faspay_config['faspay_merchant_userid'],
			"msisdn" 						=>  (isset($collectData['collect']['payment_userinfo']['telephone']) ? $collectData['collect']['payment_userinfo']['telephone'] : $input_params['param_hp']), // This is mandatory for customer phone number
			"email" 						=>  (isset($collectData['collect']['payment_userinfo']['email']) ? $collectData['collect']['payment_userinfo']['email'] : ''),
			"terminal" 						=>  "10", // 10. Web, 21. MobApp Android, details: https://faspay.co.id/docs/#xml-request-parameter-post-data
			"billing_address" 				=>  $input_params['param_address'],
			"billing_address_city" 			=>  $input_params['param_city'],
			"billing_address_region" 		=>  $input_params['param_state'],
			"billing_address_state" 		=>  $input_params['param_country'],
			"billing_address_poscode" 		=>  '',
			"billing_address_country_code" 	=>  "ID",
			"receiver_name_for_shipping" 	=>  sprintf("%s %s", (isset($collectData['collect']['payment_userinfo']['firstname']) ? $collectData['collect']['payment_userinfo']['firstname'] : ''), (isset($collectData['collect']['payment_userinfo']['lastname']) ? $collectData['collect']['payment_userinfo']['lastname'] : '')),
			"shipping_address" 				=>  $input_params['param_address'],
			"shipping_address_city" 		=>  $input_params['param_city'],
			"shipping_address_region" 		=>  $input_params['param_state'],
			"shipping_address_state" 		=>  $input_params['param_state'],
			"shipping_address_poscode" 		=>  '',
			"item" 							=>  array(),
		);
		if (isset($collectData['collect']['payment_data']['items'])) {
			if (is_array($collectData['collect']['payment_data']['items']) && (count($collectData['collect']['payment_data']['items']) > 0)) {
				$for_i = 1;
				foreach ($collectData['collect']['payment_data']['items'] as $item) {
					$faspay_structure['item'][] = array(
						'id'				=> sprintf("%d%d", $item['seller_id'], $for_i),
						'amount'			=> $item['price'],
						'qty'				=> 1,
						'product'			=> $item['name'],
						'payment_plan'		=> '01', // 01: full, 02: Installment
						'tenor'				=> "00", // 00: Full payment, 03: 3 months
					);
					$for_i++;
				}
			}
		}
		// number formatting bill_total
		$faspay_structure['bill_total'] = number_format($faspay_structure['bill_total'], 2);
		$faspay_structure['bill_total'] = str_replace(',', '', $faspay_structure['bill_total']);
		// make signature
		$faspay_structure['signature'] = $this->faspay_create_signature_order($this->faspay_config['faspay_merchant_userid'], $this->faspay_config['faspay_merchant_password'], $faspay_structure['bill_no']);
		
		
		############################################
		# Create Faspay XML
		$faspay_xml = new SimpleXMLElement('<faspay/>');
		$this->custom_arraytoxml($faspay_structure, $faspay_xml);
		
		# Make curl request
		try {
			$faspay_curl = $this->create_curl_request('POST', $this->url_faspay, 'JajanDiJalan/Api.Context', $this->faspay_headers, $faspay_xml->asXML(), 30);
		} catch (Exception $ex) {
			$this->error = TRUE;
			$this->error_msg[] = ('ERROR: ' . $ex->getMessage());
		}
		
		
		##############################################
		# Passing validation
		##############################################
		if (!$this->error) {
			if (isset($faspay_curl['response']['body'])) {
				$faspay_payment_response = $this->xmltoarray($faspay_curl['response']['body']);
				if (isset($faspay_payment_response['faspay']['response_code']['value'])) {
					if ($faspay_payment_response['faspay']['response_code']['value'] != '00') {
						$this->error = TRUE;
						$this->error_msg[] = (isset($faspay_payment_response['faspay']['response_desc']['value']) ? $faspay_payment_response['faspay']['response_desc']['value'] : 'Not get 00 as response payment code.');
					}
				} else {
					$this->error = TRUE;
					$this->error_msg[] = "We not get response_code of faspay payment.";
				}
			}
		}
		
		
		
		##############################################
		if (!$this->error) {
			if (!isset($faspay_payment_response['faspay']['trx_id']['value']) && (!isset($faspay_payment_response['faspay']['merchant_id']['value'])) && (!isset($faspay_payment_response['faspay']['bill_no']['value']))) {
				$this->error = TRUE;
				$this->error_msg[] = "There is no-trx_id, no-merchant_id, and no-bill_no.";
			}
		}
		if (!$this->error) {
			$faspay_query_string = array(
				'trx_id'				=> $faspay_payment_response['faspay']['trx_id']['value'],
				'merchant_id'			=> $faspay_payment_response['faspay']['merchant_id']['value'],
				'bill_no'				=> $faspay_payment_response['faspay']['bill_no']['value'],
			);
			try {
				$faspay_signature = $this->faspay_create_signature_order($this->faspay_config['faspay_merchant_userid'], $this->faspay_config['faspay_merchant_password'], $faspay_query_string['bill_no']);
				$this->url_redirect = str_replace('##SIGNATURE##', $faspay_signature, $this->url_redirect);
				$this->url_redirect .= http_build_query($faspay_query_string);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot string replace signature.";
			}
		}
		
		####################################
		# Debug Response from Faspay:
			
		/*
		if (!$this->error) {
			echo "<pre>";
			echo $url_faspay_redirect;
			echo "<hr/>";
			print_r($faspay_curl['request']);
			echo "<hr/>";
			print_r($faspay_payment_response);
			exit;
		} else {
			print_r($this->error_msg);
			echo "<hr/>";
			print_r($faspay_curl);
			exit;
		}
		*/
		####################################
		
		if (!$this->error) {
			return array(
				'status'		=> true,
				'url'			=> $this->url_redirect,
				'error'			=> false,
				'response'		=> $faspay_payment_response,
			);
		} else {
			return array(
				'status'		=> false,
				'url'			=> '',
				'error'			=> $this->error_msg,
				'response'		=> false,
			);
		}
		####################################################
		
	}
		
	//----------------------------------------------------------------------------------------------------------------------
	// UTILS
	private static function get_headers_from_curl_response($headerContent) {
		$headers = array();
		// Split the string on every "double" new line.
		$arrRequests = explode("\r\n\r\n", $headerContent);
		
		// Loop of response headers. The "count($arrRequests) - 1" is to 
		// avoid an empty row for the extra line break before the body of the response.
		for ($index = 0; $index < (count($arrRequests) - 1); $index++) {
			foreach (explode("\r\n", $arrRequests[$index]) as $i => $line) {
				if ($i === 0) {
					$headers[$index]['http_code'] = $line;
				} else {
					list ($key, $value) = explode(': ', $line);
					$headers[$index][$key] = $value;
				}
			}
		}
		return $headers;
	}
	private function create_curl_headers($headers = array()) {
		$curlheaders = array();
		foreach ($headers as $ke => $val) {
			$curlheaders[] = "{$ke}: {$val}";
		}
		return $curlheaders;
	}
	public static function get_apache_headers() {
		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
			$out = array();
			foreach ($headers AS $key => $value) {
				$key = str_replace(" ", "-", ucwords(strtolower(str_replace("-", " ", $key))));
				$out[$key] = $value;
			}
		} else {
			$out = array();
			if	(isset($_SERVER['CONTENT_TYPE'])) {
				$out['Content-Type'] = $_SERVER['CONTENT_TYPE'];
			}
			if (isset($_ENV['CONTENT_TYPE'])) {
				$out['Content-Type'] = $_ENV['CONTENT_TYPE'];
				foreach ($_SERVER as $key => $value) {
					if (substr($key, 0, 5) == "HTTP_") {
						$key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
						$out[$key] = $value;
					}
				}
			}
		}
		return $out;
	}
}

















