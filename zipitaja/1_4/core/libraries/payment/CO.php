<?php
// Mengambil object checkout dari tabel temporary sesuai token
function getCoByTemporaryToken($token)
{
    $engine = new Engine();
    $query = $engine->executePrepared('SELECT * FROM tb_tmp_checkout WHERE token = :token', ['token'=> $token]);
    $row = $query->fetch(PDO::FETCH_ASSOC);

    if ($row) {
        $address = new CustAddress(
            $row['address'],
            $row['city'],
            $row['state'],
            $row['country'],
            $row['hp'],
            $row['lat'],
            $row['long'],
            $row['unit_apt'],
            $row['address_notes']
        );
        return new CO($row['payment_type'], $row['usr_id'], $row['services'], $row['use_voucher'], $row['coupon'], $row['platform'], $address, $token);
    } else {
        return null;
    }
}

class CO {
    // Untuk membuat token (base64 tahap 2)
    const SALT = 'salt:892341-2-2394-23:salt';

    const PAYMENT_BT = 'bt';
    const PAYMENT_CC = 'cc';
    const PAYMENT_KP = 'kp';
	const PAYMENT_GOPAY = 'gopay';

    private $payment_type;
    private $user_id;
    private $services;
    private $use_voucher;
    private $coupon_code;
    private $mobile_code;
    private $address;
    private $token;

    /**
     * CO constructor.
     * @param $payment_type
     * @param $user_id // User ID bisa null agar bisa dipakai oleh callback Credit Card (jika null akan diisei dengan id yang tersimpan di session)
     * @param $services
     * @param $use_voucher
     * @param $coupon_code
     * @param $mobile_code
     * @param CustAddress $address
     */
    public function __construct($payment_type, $user_id, $services, $use_voucher, $coupon_code, $mobile_code, $address, $token = null) {
        $this->payment_type = $payment_type;
        $this->user_id = $user_id;
        $this->services = preg_replace("/\;;;$/", "", $services); // antisipasi client request dengan delimiter (;;;) di akhir
        $this->use_voucher = $use_voucher;
        $this->coupon_code = $coupon_code;
        $this->mobile_code = $mobile_code;
        $this->address = $address;
        $this->token = $token;
    }

    // Engine
    public function ce()
    {
        require_once "../core/Engine.php";
        return new Engine();
    }

    public function currentUserId()
    {
        return ($this->user_id != null) ? $this->user_id : $_SESSION['id'];
    }

    /**
     * @return array [firstname, lastname, email]
     */
    public function currentUserInfo()
    {
        $query_user = $this->ce()->executePrepared("SELECT id, firstname, lastname, concat(firstname, lastname) as fullname, email, telephone FROM users WHERE id = :id", array(
            'id' => $this->currentUserId()
        ));
        return $query_user->fetch(PDO::FETCH_ASSOC);
    }

    public function IsNullOrEmptyString($question)
    {
        return (!isset($question) || trim($question)==='');
    }

    // Menyatukan service dengan delimiter koma,
    // untuk kebutuhan select in di mysql e.g in (2134, 2423)
    public function commaServices()
    {
        $services = explode(';;;', $this->services);
        for ($i = 0; $i < count($services); $i++) {
            if (!$this->IsNullOrEmptyString($services[$i])) {
                if ($i > 0) {
                    $s = $s . ', ';
                }
                $s = $s . $services[$i];
            }
        }

        return "($s)";
    }

    public function serviceCount()
    {
        $services = explode(';;;', $this->services);
        return count($services);
    }

    public function isServicesAvailable()
    {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT COUNT(id) as count FROM payment_list WHERE id_service IN $s", array());
        return $query->fetch(PDO::FETCH_ASSOC)['count'] <= 0;
    }

    public function getSubTotal()
    {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT SUM(price) as subtotal FROM service WHERE id IN $s", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row['subtotal'];
    }

    public function getServices()
    {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT u.id as seller_id, s.name, s.category_id, CONCAT('bt,cc', IF(KP_QUALIFY(s.brand_id, s.bagtype_id, s.conditions, CAST(s.price AS DECIMAL), s.category_id, s.id_user, s.payment_kp_accepted) = 1, ',kp', '')) as accepted_payment FROM service s, users u WHERE s.id_user = u.id AND s.id IN $s", array());
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCoupon(&$error)
    {
        $error = '';
        $coupon_code = $this->coupon_code;
        $mobile_code = $this->mobile_code;
        $subtotal = $this->getSubTotal();

        if (!$coupon_code) {
            return 0;
        }

        // Ambil Data Kupon
        $query = $this->ce()->executePrepared("SELECT ( IF(Unix_timestamp() BETWEEN start AND end, '', 'Kupon yang anda masukkan hanya bisa digunakan dalam waktu tertentu')) AS msg, id_user as user_id, type,  value, min, max, `limit`, firsttimebuyer, mobile, (SELECT Count(id) FROM   payment_list WHERE  status IN ( 0, 3, 4, 5 ) AND id_buyer = :me) AS is_firstime_buy FROM   coupon WHERE  active = 0 AND name = :name", array(
            'name' => $coupon_code,
            'me' => $this->currentUserId()
        ));
        $coupon = $query->fetch(PDO::FETCH_ASSOC);

        // Temporary perhitungan
        $tmp = 0;

        // Memulai Validasi Kupon
        // ====================================
        if (!empty($coupon['msg'])) {
            // Check apakah ada pesan error
            $error = $coupon['msg'];
            return 0;
        } else {
            $type = $coupon['type'];
            $value = $coupon['value'];
            if ($type == 1) {
                $tmp = $value / 100 * $subtotal;
            } else {
                $tmp = $value;
            }
        }

        // Cek kupon untuk seller tertentu
        $service = $this->getServices();
        for ($i = 0; $i < count($service) - 1; $i++) {
            if ($service[$i]['seller_id'] != $coupon['user_id']) {
                $error = 'Kode kupon hanya berlaku untuk seller tertentu!';
            }
        }

        //Check Limit
        $limit = $coupon['limit'];
        if ($limit > 0 && $subtotal < $limit) {
            $error = "Kode kupon tidak bisa digunakan untuk subtotal pembelian kurang dari Rp. $limit";
        }

        //Check First Time Buy
        $firstTimeBuy = $coupon['firsttimebuyer'];
        $isFirstTimeBuy = $coupon['is_firstime_buy'];
        if ($firstTimeBuy == 1 && $isFirstTimeBuy > 0) {
            $error = "Coupon ini khusus untuk pembelian pertama";
        }

        //Check Platform
        $mobileCode = $coupon['mobile'];

        if ($mobileCode != 4) {
            if ($mobile_code != $mobileCode) {
                if ($mobileCode == 3) {
                    if ($mobile_code != 1 && $mobile_code != 2) {
                        switch ($mobileCode) {
                            case 0:
                                $error = "Coupon berlaku hanya untuk pembelian di Web";
                                return 0;
                            case 1:
                                $error = "Coupon berlaku hanya untuk pembelian di Android";
                                return 0;
                            case 2:
                                $error = "Coupon berlaku hanya untuk pembelian di iOS";
                                return 0;
                            case 3:
                                $error = "Coupon berlaku hanya untuk pembelian di aplikasi mobile";
                                return 0;
                        }
                    }
                }
            }
        }

        //Validate min & max
        $c_min = $coupon['min'];
        $c_max = $coupon['max'];
        if ($c_min > 0 && $tmp < $c_min) {
            $tmp = $c_min;
        }
        if ($c_max > 0 && $tmp > $c_max) {
            $tmp = $c_max;
        }

        return $tmp ? $tmp : 0;
    }

    public function getVoucher()
    {
        if (!$this->use_voucher) {
            return 0;
        }

        $err = '';
        $coupon = $this->getCoupon($err);
        $subtotal = $this->getSubTotal();

        // Ambil total voucher yang dimiliki
        $query = $this->ce()->executePrepared("SELECT IFNULL(SUM(value), 0) AS credit FROM zip_credits WHERE deleted = 0 AND assigned_to = :assigned_to", array(
            'assigned_to' => $this->currentUserId()
        ));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $voucher = $row['credit'];
        if ($subtotal - $coupon <= $voucher) {
            $voucher = $subtotal - $coupon;
        }

        return $voucher;
    }

    public function getBuyerCommission()
    {
        $subtotal = $this->getSubTotal();

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'buyer_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $komisi_buyer = $row["value_2"] / 100 * $subtotal;

        return $komisi_buyer;
    }

    public function getSellerCommission()
    {
        $subtotal = $this->getSubTotal();

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $komisi_seller = $row['value_2'] / 100 * $subtotal;

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_min_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $min_komisi_seller = $row['value_2'];

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_max_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $max_komisi_seller = $row['value_2'];

        if ($komisi_seller < $min_komisi_seller) {
            $komisi_seller = $min_komisi_seller;
        }

        if ($max_komisi_seller > 0) {
            if ($max_komisi_seller < $komisi_seller) {
                $komisi_seller = $max_komisi_seller;
            }
        }

        return $komisi_seller;
    }

    public function isPhoneExists()
    {
        $query = $this->ce()->executePrepared("SELECT count(id) AS result FROM users u WHERE u.telephone = :telephone AND u.id != :id", array(
            'telephone' => $this->address->phone,
            'id' => $this->currentUserId()
        ));
        $res = $query->fetch(PDO::FETCH_ASSOC);

        return $res['result'] >= 1;
    }

    public function updateAddress()
    {
        $query = $this->ce()->executePrepared(
            "
            UPDATE users SET
                telephone = :telephone,
                la = :la,
                lo = :lo,
                address = :address,
                city = :city,
                state = :state,
                unit_apt = :unit_apt,
                address_notes = :address_notes
            WHERE id = :id",
            [
                'telephone' => $this->address->phone,
                'la' => $this->address->lat,
                'lo' => $this->address->lng,
                'address' => $this->address->address,
                'city' => $this->address->city,
                'state' => $this->address->state,
                'unit_apt' => $this->address->unit_apt,
                'address_notes' => $this->address->address_notes,
                'id' => $this->currentUserId()
            ]
        );
        return $query;
    }

    public function getGrandTotal()
    {
        $grandTotal = 0;

        $services = explode(';;;', $this->services);
        $voucher = $this->getVoucher();
        $err = '';
        $coupon = $this->getCoupon($err);
        $komisi_buyer = $this->getBuyerCommission();

        $serviceCount = $this->serviceCount();
        //Checkout
        for ($i = 0; $i < $serviceCount; $i++) {
            //Get Service Info
            $query = $this->ce()->executePrepared("SELECT * FROM service WHERE id = :id", array(
                'id' => $services[$i]
            ));
            $row = $query->fetch(PDO::FETCH_ASSOC);

            $s_price = $row['price'];
            $s_total = ($s_price - ($voucher / $serviceCount) - ($coupon / $serviceCount)) + $komisi_buyer;
            $grandTotal += $s_total;
        }

        return $grandTotal;
    }

    public function getServicesName()
    {
        $services_detail = array();
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT `name` FROM service s WHERE s.id IN $s", array());
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        for ($i = 0; $i < count($rows); $i++) {
            $services_detail[$i] = $rows[$i]['name'];
        }

        return $services_detail;
    }

    public function validateOrder(&$error)
    {
        if (!$this->isServicesAvailable()) {
            $error = "Semua atau sebagian produk yang anda pilih telah terjual.";
            return false;
        }

        $this->getCoupon($error);
        if ($error) {
            return false;
        }

        if ($this->isPhoneExists()) {
            $error = "Nomor HP sudah terdaftar!";
            return false;
        };

        return true;
    }

    public function getGUID()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid, 12, 4).$hyphen
                .substr($charid, 16, 4).$hyphen
                .substr($charid, 20, 12)
                .chr(125);// "}"
            return $uuid;
        }
    }

    public function generateToken()
    {
        $a = strtoupper(uniqid()) . '|' . $this->getGUID() . '|'. time();
        $b = base64_encode($a);
        $c = base64_encode(Self::SALT.$b.Self::SALT);
        return $c;
    }

    public function getUniqIdFromToken($token)
    {
        $a = base64_decode($token);
        $b = base64_decode(str_replace(Self::SALT, '', $a));
        return explode('|', $b)[0];
    }

    public function placeAsTemporary()
    {
        $token = $this->generateToken();
        $this->ce()->executePrepared("INSERT INTO tb_tmp_checkout (`usr_id`, `payment_type`, `token`, `services`, 
                `use_voucher`, `coupon`, `hp`, `lat`, `long`, `address`, `city`, `state`, `country`, `unit_apt`, `address_notes`,
                `platform`, `create_time`, `deleted`
            ) 
            VALUES (:usr_id, :payment_type, :token, :services, :use_voucher, :coupon, :hp, :lat, :long, :address,
                :city, :state, :country, :unit_apt, :address_notes, :platform, :create_time, :deleted
            )", ['usr_id' =>$this->currentUserId(),
                    'payment_type' => $this->payment_type,
                    'token' => $token,
                    'services' =>$this->services,
                    'use_voucher' => $this->use_voucher,
                    'coupon' =>$this->coupon_code,
                    'hp' => $this->address->phone,
                    'lat' =>$this->address->lat,
                    'long' =>$this->address->lng,
                    'address' =>$this->address->address,
                    'city' =>$this->address->city,
                    'state' =>$this->address->state,
                    'country' =>$this->address->country,
                    'unit_apt' =>$this->address->unit_apt,
                    'address_notes' =>$this->address->address_notes,
                    'platform'=> $this->mobile_code,
                    'create_time' => time(),
                    'deleted' => 0
    ]);

        return $token;
    }

    public function deleteFromTemporary($token)
    {
        $this->ce()->executePrepared('DELETE FROM tb_tmp_checkout WHERE token = :token', ['token' => $token]);
    }

    public function placeOrder($cc_id)
    {
        require_once "../core/SMTPMail.php";
        require_once "../core/PushNotificationManager.php";

        $engine = $this->ce();

        $services = explode(';;;', $this->services);
        $serviceCount = $this->serviceCount();

        $voucher = $this->getVoucher();
        $err = '';
        $coupon = $this->getCoupon($err);
        $komisi_buyer = $this->getBuyerCommission();
        $komisi_seller = $this->getSellerCommission();
        $grand_total = $this->getGrandTotal();

        $res_user = $this->currentUserInfo();
        $full_name = $res_user['firstname'] . " " . $res_user['lastname'];
        $email = $res_user['email'];

        //Checkout
        for ($i = 0; $i < $serviceCount; $i++) {
            //Get Service Info
            $query = $engine->executePrepared("SELECT * FROM service WHERE id = :id", array(
                'id' => $services[$i]
            ));
            $row = $query->fetch(PDO::FETCH_ASSOC);

            $s_price = $row['price'];
            $s_total = ($s_price - ($voucher / $serviceCount) - ($coupon / $serviceCount)) + $komisi_buyer;
            $s_seller = $row['id_user'];

            //Insert checkout and get order id
            $engine->executePrepared(
                "
        INSERT INTO payment_list (id_buyer, id_service, total, price, status, payment_type, date, paid, zip_credit, mobile, ccid, qty, date_app)
        VALUES (:id_buyer, :id_service, :total, :price, :status, :payment_type, UNIX_TIMESTAMP(), :paid, :zip_credit, :mobile, :ccid, 1, :date_app)",
                array(
                    'id_buyer' => $this->currentUserId(),
                    'id_service' => $services[$i],
                    'total' => $s_total,
                    'price' => $s_price,
                    'status' => '0',
                    'payment_type' => '2',
                    'paid' => '0',
                    'zip_credit' => $voucher / $serviceCount,
                    'mobile' => $this->mobile_code,
                    'ccid' => $cc_id,
					'date_app' => time()
                )
            );
            //Last id
            $last_id = $engine->getLastID('id');

            $engine->executePrepared("UPDATE service SET qty = qty-1 WHERE id = :id AND qty > 0", array('id' => $services[$i]));

            //Update End Date F
            $engine->executePrepared("UPDATE service SET enddatef = UNIX_TIMESTAMP(DATE_ADD(CURDATE(),INTERVAL 15 DAY)) WHERE id = :id", array(
                'id' => $services[$i]
            ));

            //Insert Commission
            $engine->executePrepared(
                "INSERT INTO site_commission (id_buyer, id_seller, id_service, id_order, total, price, com_s, com_b, date_c, status) VALUES (:id_buyer, :id_seller, :id_service, :id_order, :total, :price, :com_s, :com_b, UNIX_TIMESTAMP(), status);",
                array(
                    'id_buyer' => $this->currentUserId(),
                    'id_seller' => $s_seller,
                    'id_service' => $services[$i],
                    'id_order' => $last_id,
                    'total' => $s_total,
                    'price' => $s_price,
                    'com_s' => $komisi_seller / $serviceCount,
                    'com_b' => $komisi_buyer / $serviceCount
                )
            );

            //Insert coupon log
            if (!empty($this->coupon_code)) {
                $engine->executePrepared(
                    " INSERT INTO coupon_log (id_buyer, id_order, price, coupon_sum, data, name) VALUES (:id_buyer, :id_order, :price, :coupon_sum, UNIX_TIMESTAMP(), :name);",
                    array(
                        'id_buyer' => $this->currentUserId(),
                        'id_order' => $last_id,
                        'price' => $s_price,
                        'coupon_sum' => $coupon,
                        'name' => $this->coupon_code
                    )
                );
            }

            //insert used zip voucher
            if ($voucher > 0) {
                $engine->executePrepared(
                    "INSERT INTO zip_credits (value, details, created, assigned_to) VALUES (:value, :details, UNIX_TIMESTAMP(), :assigned_to)",
                    array(
                        'value' => -($voucher / $serviceCount),
                        'details' => 'Voucher for order #' . $last_id,
                        'assigned_to' => $this->currentUserId()
                    )
                );
            }

            // Update alamat
            $this->updateAddress();

            //Send Mail per Item
            $mailC = getMailMessage('orderplacementmanual');

            $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
            $mailC->message = str_replace("#idlisting#", $services[$i], $mailC->message); //Listing
            $mailC->message = str_replace("#buyerpayment#", number_format($s_total, 2, ",", "."), $mailC->message); //Total
            sendMail($email, $mailC->subject, $mailC->message);
        }

        // Insert KP Data
        if ($this->payment_type == Self::PAYMENT_KP) {
            $engine->executePrepared('INSERT INTO kp_payment_data (`order_id`, `updated_at`) VALUES (:order_id, :updated_at)', [
                'order_id' => $this->getUniqIdFromToken($this->token),
                'updated_at' => time()
            ]);
        }

        if ($this->serviceCount() > 1) {
            $mailC = getMailMessage("orderplacementtotal");

            $mail_listing = "";
            for ($i = 0; $i < count($services) - 1; $i++) {
                if ($i > 0) {
                    $mail_listing = $mail_listing . ", ";
                }
                $mail_listing = $mail_listing . "<a href='" . APP_SERVER_ROOT . "service/viewlisting/" . $services[$i] . "'>Listing " . $services[$i] . "</a>";
            }

            $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
            $mailC->message = str_replace("#totallisting#", $mail_listing, $mailC->message); //Listing
            $mailC->message = str_replace("#totalprice#", "Rp. " . number_format($grand_total, 2, ",", "."), $mailC->message); //Total
            sendMail($email, $mailC->subject, $mailC->message);
        }

        /*
         * Send Mail to Seller
         */
        for ($i = 0; $i < $serviceCount; $i++) {
            //== Get Seller Profile
            $query = $engine->executePrepared("SELECT u.id, u.firstname, u.lastname, u.email, s.price FROM users u, service s WHERE s.id_user = u.id AND s.id = :id", array(
                "id" => $services[$i]
            ));
            $res = $query->fetchAll(PDO::FETCH_ASSOC);

            $id = $res[0]["id"];
            $full_name = $res[0]["firstname"] . " " . $res[0]["lastname"];
            $email = $res[0]["email"];
            $price = $res[0]["price"];

            $mailC = getMailMessage("orderapprovalmanual");
            $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
            $mailC->message = str_replace("#idlisting#", $services[$i], $mailC->message); //ID Listing
            $mailC->message = str_replace("#price#", number_format($price, 2, ",", "."), $mailC->message); //Price Total
            sendMail($email, $mailC->subject, $mailC->message);

            /*
             * SEND Notification
             */
            $gcm = new PushNotificationManager();
            //Get list registered id
            $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id", array(
                "user_id" => $id
            ));
            $ids = array();
            $ct = 0;
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $ids[$ct] = array($row['gcm_regid'], $row['mobile']);
                $ct++;
            }
            
            //Send notifikasi barang terjual ke seller
            $gcm->sendSoldNotification(3, $ids);
        }
    }
}
