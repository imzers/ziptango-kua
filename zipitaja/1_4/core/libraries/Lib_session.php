<?php
if (!defined('LIB_PATH')) { exit('No direct script access allowed'); }
class Lib_Session implements SessionHandlerInterface {
    public $life_time;
    public $db = FALSE;
	public $db_config;
	public $table;
	public $DateObject;
	protected $ip_address;
    public function __construct($db_config) {
        // Read the maxlifetime setting from PHP
        $this->life_time = (isset($db_config['max_lifetime']) ? $db_config['max_lifetime'] : 3600);
		$this->table = (isset($db_config['db_table_session']) ? $db_config['db_table_session'] : 'weblog_sessions');
		$this->db_config = array(
			'db_host'			=> (isset($db_config['db_host']) ? $db_config['db_host'] : ''),
			'db_user'			=> (isset($db_config['db_user']) ? $db_config['db_user'] : ''),
			'db_pass'			=> (isset($db_config['db_pass']) ? $db_config['db_pass'] : ''),
			'db_name'			=> (isset($db_config['db_name']) ? $db_config['db_name'] : ''),
		);
		$this->db_config['db_timezone'] = (isset($db_config['db_timezone']) ? $db_config['db_timezone'] : 'Asia/Bangkok');
		$this->DateObject = new DateTime(date('Y-m-d H:i:s'));
		$this->DateObject->setTimezone(new DateTimeZone($this->db_config['db_timezone']));
		try {
			$this->db = new mysqli($this->db_config['db_host'], $this->db_config['db_user'], $this->db_config['db_pass'], $this->db_config['db_name']);
		} catch (Exception $ex) {
			exit("Cannot connect to database session: {$ex->getMessage()}.");
		}
		$this->ip_address = $this->get_ip_address();
        // Register this object as the session handler
		session_set_save_handler(
            [$this, "open"],
            [$this, "close"],
            [$this, "read"],
            [$this, "write"],
            [$this, "destroy"],
            [$this, "gc"]
        );
    }
	public function open($save_path = '', $session_name = 'PHPSESSION') {
		if ($this->db->connect_error) {
			return false;
		} else {
			return true;
		}
    }
    public function close() {
        if ($this->db) {
			$this->db->close();
		}
		return true;
    }
    public function read($id) {
		$sql = sprintf("SELECT session_data FROM %s WHERE session_id = '%s' AND session_expires > '%s'",
			$this->table,
			$id,
			$this->DateObject->format('Y-m-d H:i:s')
		);
		try {
			$sql_query = $this->db->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		while ($result = $sql_query->fetch_object()) {
            return $result->session_data;
        }
		return false;
    }
    public function write($id, $data) {
        $datetime_expires = new DateTime($this->DateObject->format('Y-m-d H:i:s'));
		$datetime_expires->add(new DateInterval("PT{$this->life_time}S"));
		$sql = sprintf("REPLACE %s(session_id, session_ip, session_data, session_expires) VALUES('%s', '%s', '%s', '%s')",
			$this->table,
			$id,
			$this->ip_address,
			$data,
			$datetime_expires->format('Y-m-d H:i:s')
		);
		return $this->db->query($sql);
    }
    public function destroy($id) {
		$sql = sprintf("DELETE FROM %s WHERE session_id = '%s'",
			$this->table,
			$id
		);
		$sql_query = $this->db->query($sql);
        if ($sql_query) {
			return true;
		} else {
			return false;
		}
    }
    public function gc($maxlifetime) {
		$sql = sprintf("DELETE FROM %s WHERE session_expires < '%s'",
			$this->table,
			$this->DateObject->format('Y-m-d H:i:s')
		);
        return $this->db->query($sql);
    }
	private function get_ip_address() {
		$ip_address = (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] :
							(isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] :
								(getenv('HTTP_X_FORWARDED_FOR') ? getenv('HTTP_X_FORWARDED_FOR') :
									(isset($_ENV['HTTP_X_FORWARDED_FOR']) ? $_ENV['HTTP_X_FORWARDED_FOR'] :
										(getenv('HTTP_CLIENT_IP') ? getenv('HTTP_CLIENT_IP') :
											(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] :
												(getenv('REMOTE_ADDR') ? getenv('REMOTE_ADDR') :
													(isset($_ENV['REMOTE_ADDR']) ? $_ENV['REMOTE_ADDR'] :
														'0.0.0.0'))))))));
		return $this->set_client_ip($ip_address);
	}
	private function set_client_ip($data = null) {
		// Get user IP address
		$ip = (isset($data)) ? $data : '0.0.0.0';
		if(strpos($ip, ',')) {
			$ip2 = explode(',', $ip);
			$ip = $ip2[0];
			if(strpos($ip, '192.168.') !== false && isset($ip2[1])) {
				$ip = $ip2[1];
			} elseif(strpos($ip, '10.') !== false && isset($ip2[1])) {
				$ip = $ip2[1];
			} elseif(strpos($ip, '172.16.') !== false && isset($ip2[1])) {
				$ip = $ip2[1];
			}
		}
		$ip = filter_var($ip, FILTER_VALIDATE_IP);
		$ip = ($ip === false) ? '0.0.0.0' : $ip;
		return $ip;
	}
	

	

}