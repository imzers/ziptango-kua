<?php

/**
 * MCrypt short summary.
 *
 * MCrypt description.
 *
 * @version 1.0
 * @author Riad
 */
class MCrypt
{
    private $iv = '_*j3HE83(#/?.do@';
    private $key = '#xPSm3$jN++={skD';


    function __construct()
    {
    }

    function encrypt($str)
    {
        return $this->encryptC($str, $this->key);

    }

    function encryptC($str, $custKey)
    {
        $iv = $this->iv;

        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

        mcrypt_generic_init($td, $custKey, $iv);
        $encrypted = mcrypt_generic($td, $str);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return bin2hex($encrypted);
    }

    function decrypt($code)
    {
        $code = $this->hex2bin($code);
        $iv = $this->iv;

        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);

        mcrypt_generic_init($td, $this->key, $iv);
        $decrypted = mdecrypt_generic($td, $code);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return utf8_encode(trim($decrypted));
    }

    protected function hex2bin($hexdata)
    {
        $bindata = '';

        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }

        return $bindata;
    }

}
