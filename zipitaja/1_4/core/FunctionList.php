<?php

define("KEYPASS", "!i3JDmj-_=3:le;$");

function getFormattedTimeNow()
{
    return date('d-m-Y H:i:s.u');

}

function generateUniqueID($length, $type = 1)
{
    $char1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz12345678900987654321zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA9876543210";
    $char2 = "0123456789123456789009876543219876543210";
    $char3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzzyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA";

    if ($type == 2)
        $usedChar = $char2;
    else if ($type == 3)
        $usedChar = $char3;
    else
        $usedChar = $char1;

    $uid = "";
    for ($i = 0; $i < $length; $i++)
        $uid = $uid . substr($usedChar, rand(0, strlen($usedChar) - 1), 1);

    return $uid;
}

function serverRoot()
{
    return $_SERVER['DOCUMENT_ROOT'] . "/";

}

function handleUploadFile($file_param_name, $path, $with_thumb, $append_path_to_return)
{
    require_once "class.upload.php";

    $image = new Upload($_FILES[$file_param_name]);
    if ($image->uploaded) {
        $newName = generateUniqueID(30);
        $newExt = "png";

        // Save Original
        $image->file_new_name_body = $newName;
        $image->file_new_name_ext = $newExt;
        $image->process(serverRoot() . $path);

        /*
        $image->image_resize = false;
        $image->file_new_name_body = $newName;
        $image->file_new_name_ext = $newExt;
        $image->image_convert = 'png';
        $image->image_x = $image->image_src_x;
        $image->image_ratio_y = true;
        $image->process($path);
        */

        // Save Resized
        if ($with_thumb)
            generateThumbnail(serverRoot() . $path . $newName . $newExt, $path);

        $image->clean();

        return ($append_path_to_return ? $path : "") . $newName . "." . $newExt;
    } else
        return "";

}

function generateThumbnail($source_path, $path)
{
    //die('{"Code":"RE0001", "Response":"' .  "$source_path" . '"}');

    require_once "class.upload.php";
    require_once "Consts.php";

    $name = getFileName($source_path);

    $image = new Upload($source_path);
    $image->file_new_name_body = $name;
    $image->file_new_name_ext = getFileExt($source_path) . thumb_prefix();
    $image->image_convert = "jpg";
    $image->image_resize = true;
    $image->file_auto_rename = false;
    $image->file_overwrite = true;
    if ($image->image_src_x > $image->image_src_y) {
        $image->image_x = 280;
        $image->image_ratio_y = true;
    } else {
        $image->image_y = 280;
        $image->image_ratio_x = true;
    }
    $image->jpeg_quality = 75;
    $image->png_compression = 9;
    $image->process(serverRoot() . $path);

    //if (!$image->processed)
    //    die('{"Code":"RE0001", "Response":"' . $image->error . "\n" . $source_path . '"}');

    if (!$image->processed) {
        $image = new Upload($source_path);
        $image->file_new_name_body = $name;
        $image->file_new_name_ext = getFileExt($source_path) . thumb_prefix();
        $image->process(serverRoot() . $path);
    }

}

function generateThumbnails($source_paths, $path)
{
    require_once "Consts.php";
    for ($i = 0; $i < count($source_paths); $i++)
        if (!file_exists($source_paths[$i] . thumb_prefix()))
            generateThumbnail($source_paths[$i], $path);

}

function getFileName($file_path)
{
    $path_parts = pathinfo($file_path);
    return $path_parts['filename'];

}

function getFileExt($file_path)
{
    $path_parts = pathinfo($file_path);
    return $path_parts['extension'];

}

function isUsernameValid($s)
{
    preg_match('/([a-zA-Z0-9\-]+$)/', $s, $result);

    if (count($result) > 0)
        if ($result[0] == $s)
            return true;

    return false;

}

function createUsernameSlug($s)
{
    return strtolower(str_replace(" ", "_", $s));

}

function utf8ize($data)
{
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string($data)) {
        return utf8_encode($data);
    }
    return $data;
}