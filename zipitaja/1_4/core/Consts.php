<?php
// == GENERAL
// ------------------------------------------------------------------------------------------------------------
# Google Key
define("GCM_KEY", "AAAA8RUQFLQ:APA91bFWLbeht53_CrM782xYNJoghYHNXZrOihbSFVwKpt3jM2nqd8HplH4B3uZtKomgfTpEjFbQkzr7C1aZ7E8x6Ew7kY6TNoklXG5CqSpLOp5nrqK3H-V7W5BkA1rvad00_YBhhORy");
define("GCM_KEY_LEGACY", "AIzaSyCRRyZLdeDTR1RRIqfPMWIH7-hCP0V9Xn8");
define("GOOGLE_CLIENT_ID", "1035440493748-4l82pb2r31v9j9emcu0c83lu9vveuneo.apps.googleusercontent.com");

# FB Key
define("FB_APP_ID", "450431345084697");
define("FB_SECRET", "1eac2f2dfd23632a4ea1ae5821c8374c");

# Server Path Variable
define("APP_IMG_SERVICE_PATH", "assets/service/");
define("APP_IMG_SERVICE_THUMB_PATH", "https://static.ziptango.com/assets/service/mobile/");
define("APP_IMG_USER_PATH", "https://static.ziptango.com/assets/usericons/");
define("APP_IMG_BANNER_PATH", "assets/img/frontend/slider/");
define("APP_IMG_CATEGORY_PATH", "https://static.ziptango.com/assets/icons/category/");
define("APP_IMG_MESSAGE_PATH", "https://static.ziptango.com/assets/inboximages/");
define("APP_IMG_BUGS_PATH", "https://static.ziptango.com/assets/bugs/");

function thumb_prefix()
{
    return "";
}

// == DEVELOPMENT
// ------------------------------------------------------------------------------------------------------------
// # Kredit Plus
// define('KP_API_URL', 'http://182.23.26.233:8094/kpservices/kreditplus.asmx/');
// define("KP_SUPPLIER_KEY", "hSxHFAdr0syPBUmInaqfPyCeOscNl2ZH");

// # FasPay
// define('CC_MERCHANT_ID', 'bri_ziptango');
// define('CC_TRANS_PWD', 'rgmvb');

// # Server URL
// define("APP_SERVER_ROOT", "http://o782x389o168v1.ziptango.com/");
// define("API_PATH", "http://o782x389o168v1.ziptango.com/ZiptangoAPI/");
// define("APP_CC_CALLBACK", "http://o782x389o168v1.ziptango.com/ZiptangoAPI/private/svcCCCallback.php");
// define("APP_KP_CALLBACK", "http://o782x389o168v1.ziptango.com/ZiptangoAPI/private/svcKpCallback.php");


// == PRODUCTION
// ------------------------------------------------------------------------------------------------------------
# Kredit Plus
define('KP_API_URL', 'http://182.23.26.233:8092/kpservices/kreditplus.asmx/');
define("KP_SUPPLIER_KEY", "FOtXeMBQFUyNUbYben5vXbHTLBfxRQFL");

# FasPay
define('CC_MERCHANT_ID', 'bri_ziptango');
define('CC_TRANS_PWD', 'ugnxusp');

// # Server URL
define("APP_SERVER_ROOT", "https://sku.jajandijalan.com/");
define("API_PATH", "https://api.jajandijalan.com/zipitaja/1_4/");
define("APP_CC_CALLBACK", "https://api.jajandijalan.com/zipitaja/1_4/private/svcCCCallback.php");
define("APP_KP_CALLBACK", "https://api.jajandijalan.com/zipitaja/1_4/private/svcKpCallback.php");
define("APP_PIC_ROOT", "https://sku.jajandijalan.com/");


class Payment_configs {
	const TIMEZONE = 'Asia/Bangkok';
	public static $MIDTRANS_APP_MID;
	public static $MIDTRANS_APP_CLIENT;
	public static $MIDTRANS_APP_SERVER;
	
	public static $FASPAY_APP = array();
	protected $payment_environment;
	function __construct($payment_environment = 'dev') {
		$this->payment_environment = $payment_environment;
		
	}
	public function set_payment_environment($payment_environment = 'dev') {
		$this->payment_environment = $payment_environment;
		if ($this->payment_environment === 'live') {
			// Midtrans
			self::$MIDTRANS_APP_MID = 'G112265910';
			self::$MIDTRANS_APP_CLIENT = 'Mid-client-v7GcoJWRjRBqg8UQ';
			self::$MIDTRANS_APP_SERVER = 'Mid-server-WUL89ACsF4hEb408BJMwYf-3';
			// Faspay
			self::$FASPAY_APP['faspay_merchant_name'] = '';
			self::$FASPAY_APP['faspay_merchant_id'] = '';
			self::$FASPAY_APP['faspay_merchant_userid'] = '';
			self::$FASPAY_APP['faspay_merchant_password'] = '';
			self::$FASPAY_APP['faspay_environment'] = 'prod';
		} else {
			// Midtrans
			self::$MIDTRANS_APP_MID = 'G112265910';
			self::$MIDTRANS_APP_CLIENT = 'SB-Mid-client--nt_haGlPwFm3clQ';
			self::$MIDTRANS_APP_SERVER = 'SB-Mid-server-nmc2Djfmf3WNxBSqKEZxQMmo';
			// Faspay
			self::$FASPAY_APP['faspay_merchant_name'] = 'Ziptango';
			self::$FASPAY_APP['faspay_merchant_id'] = '31583';
			self::$FASPAY_APP['faspay_merchant_userid'] = 'bot31583';
			self::$FASPAY_APP['faspay_merchant_password'] = 'p@ssw0rd';
			self::$FASPAY_APP['faspay_environment'] = 'dev';
		}
	}
	public function get_payment_environment() {
		return $this->payment_environment;
	}
}











