<?php
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField(
    array(
        "param_service" => true,
        "param_id" => true,
        "api_key" => true
    )
);
$service = $engine->getPOSTField("param_service");
$param_id = $engine->getPOSTField("param_id");

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
//Update Views Number
$query = $engine->executePrepared(
    "UPDATE nviews SET num = num + 1 WHERE id_service = :id",
    array(
        "id" => $service
    )
);
if ($query->rowCount() <= 0) {
    $query = $engine->executePrepared(
        "INSERT INTO nviews (id_service, num) VALUES (:id, 1)",
        array(
            "id" => $service
        )
    );
}

//Get User Active Status
$query = $engine->executePrepared(
    "SELECT * FROM users WHERE id = :id",
    array(
        "id" => $param_id
    )
);
$row = $query->fetch(PDO::FETCH_ASSOC);
$active = $row["active"];

//Get Data
$icon_path = APP_IMG_USER_PATH;

$query = $engine->executePrepared(
    "
SELECT u.*,
       IF(v.num IS NULL, 0, v.num) AS number_views,
       '$active' as me_active
FROM   (SELECT t.*,
               IF(p.id_service IS NULL AND t.force_sold = 0, 0, 1) AS sold
        FROM   (SELECT b.id,
                       b.category_id,
                       c.name     category_name,
                       b.brand_id,
                       IFNULL(bt.name, 'Not Assigned')  as    brand_name,
                       b.color_id,
                       IFNULL(ct.name, 'Not Assigned')    as color_name,
                       b.bagtype_id,
                       IFNULL(tt.name, 'Not Assigned') as    type_name,
                       b.name,
                       b.id       AS listing,
                       b.datepost,
                       b.startdatef,
                       b.conditions,
                       IFNULL(cot.name, 'Not Assigned') as condition_name,
                       b.pictures,
                       b.street_1,
                       b.city_1,
                       b.lat_1,
                       b.lng_1,
                       s.id       AS id_seller,
                       CONCAT('$icon_path', s.icon) as icon,
                       s.username AS seller,
                       b.price,
                       b.details,
                       b.payment_kp_accepted,
                       b.force_sold,
                       CONCAT('bt,cc', IF(KP_QUALIFY(b.brand_id, b.bagtype_id, b.conditions, CAST(b.price AS DECIMAL), b.category_id, b.id_user, b.payment_kp_accepted) = 1, ',kp', '')) as accepted_payment
                FROM   service b
                       INNER JOIN users s ON (b.id_user = s.id)
                       INNER JOIN service_categories c ON (b.category_id = c.id)
                       LEFT JOIN brandtable bt ON (b.brand_id = bt.id)
                       LEFT JOIN colortable ct ON (b.color_id = ct.id)
                       LEFT JOIN conditionstable cot ON (b.conditions = cot.id)
                       LEFT JOIN bagtypetable tt ON (b.bagtype_id = tt.id)
                WHERE  
                       b.id = :id
                       -- AND b.activate = 1
                       ) t
               LEFT JOIN payment_list p
                      ON ( p.id_service = t.id )) u
       LEFT JOIN nviews v
              ON ( v.id_service = u.id ) ",
    array(
        "id" => $service
    )
);

$rows = array();
$ct = 0;
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["pictures"]);
    for ($i = 0; $i < count($pic); $i++) {
        if (!file_exists(serverRoot() . $pic[$i] . thumb_prefix())) {
            generateThumbnail(serverRoot() . $pic[$i], APP_IMG_SERVICE_PATH);
        }
    }

    for ($i = 0; $i < count($pic); $i++) {
        $pic[$i] = APP_SERVER_ROOT . $pic[$i] . thumb_prefix();
    }

    $rows[$ct] = array(
        "id" => $row["id"],
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "brand_id" => $row["brand_id"],
        "brand_name" => $row["brand_name"],
        "color_id" => $row["color_id"],
        "color_name" => $row["color_name"],
        "bagtype_id" => $row["bagtype_id"],
        "type_name" => $row["type_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "datepost" => $row["datepost"],
        "startdatef" => $row["startdatef"],
        "conditions" => $row["conditions"],
        "condition_name" => $row["condition_name"],
        "pictures" => $pic,
        "street_1" => $row["street_1"],
        "city_1" => $row["city_1"],
        "lat_1" => $row["lat_1"],
        "lng_1" => $row["lng_1"],
        "id_seller" => $row["id_seller"],
        "seller" => $row["seller"],
        "icon" => $row["icon"],
        "price" => $row["price"],
        "details" => utf8_encode($row["details"]),
        "sold" => $row["sold"],
        "number_views" => $row["number_views"],
        "me_active" => $row["me_active"],
        "payment_kp_accepted" => $row['payment_kp_accepted'],
        "accepted_payment" => $row["accepted_payment"]
    );
    $ct++;
}
$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
