<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField(
    array(
        "param_service" => true,
        "param_id" => true,
        "api_key" => true
    )
);
$service = $engine->getPOSTField("param_service");
$param_id = $engine->getPOSTField("param_id");

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
//Update Views Number
$query = $engine->executePrepared(
    "UPDATE nviews SET num = num + 1 WHERE id_service = :id",
    array(
        "id" => $service
    )
);
if ($query->rowCount() <= 0) {
    $query = $engine->executePrepared(
        "INSERT INTO nviews (id_service, num) VALUES (:id, 1)",
        array(
            "id" => $service
        )
    );
}

//Get User Active Status
$query = $engine->executePrepared(
    "SELECT * FROM users WHERE id = :id",
    array(
        "id" => $param_id
    )
);
$row = $query->fetch(PDO::FETCH_ASSOC);
$active = $row["active"];

//Get Data
$icon_path = APP_IMG_USER_PATH;

$base_sql = <<<SQLSTRING
SELECT u.*,
	IF(v.num IS NULL, 0, v.num) AS number_views,
	'$active' AS me_active
FROM (SELECT b.*,
		IF(p.qty > 0 AND b.force_sold = 0, 0, 1) AS sold,
		REPLACE(b.icon, '#ICON_PATH#', '$icon_path') AS icon_path_image
	FROM view_public_svcgetproductdetail AS b
		LEFT JOIN payment_list p ON p.id_service = b.id
	WHERE b.id = :id) AS u
LEFT JOIN nviews AS v ON v.id_service = u.id
SQLSTRING;
$base_param = array(
	'id'		=> $service,
);
//------------------------------
/*
echo $base_sql;
exit;
*/
//------------------------------
$pdoObj = $engine->getConn();
$sql_query = $pdoObj->prepare($base_sql);
$sql_query->bindParam(':id', $base_param['id']);
$sql_query->execute();
//------------------------------
$rows = array();
$ct = 0;
while ($row = $sql_query->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["pictures"]);
    for ($i = 0; $i < count($pic); $i++) {
        if (!file_exists(serverRoot() . $pic[$i] . thumb_prefix())) {
            generateThumbnail(serverRoot() . $pic[$i], APP_IMG_SERVICE_PATH);
        }
    }

    for ($i = 0; $i < count($pic); $i++) {
        $pic[$i] = APP_PIC_ROOT . $pic[$i] . thumb_prefix();
    }

    $rows[$ct] = array(
        "id" => $row["id"],
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "brand_id" => $row["brand_id"],
        "brand_name" => $row["brand_name"],
        "color_id" => $row["color_id"],
        "color_name" => $row["color_name"],
        "bagtype_id" => $row["bagtype_id"],
        "type_name" => $row["type_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "datepost" => $row["datepost"],
        "startdatef" => $row["startdatef"],
        "conditions" => $row["conditions"],
        "condition_name" => $row["condition_name"],
        "pictures" => $pic,
        "street_1" => $row["street_1"],
        "city_1" => $row["city_1"],
        "lat_1" => $row["lat_1"],
        "lng_1" => $row["lng_1"],
        "id_seller" => $row["id_seller"],
        "seller" => $row["seller"],
        //"icon" => $row["icon"],
		'icon'	=> $row['icon_path_image'],
        "price" => $row["price"],
        "details" => utf8_encode($row["details"]),
        "sold" => $row["sold"],
        "number_views" => $row["number_views"],
        "me_active" => $row["me_active"],
        "payment_kp_accepted" => $row['payment_kp_accepted'],
        "accepted_payment" => $row["accepted_payment"]
    );
	
	$rows[$ct]['recomendation'] = array(
		'brand'		=> array('id' => $row['id'], 'category_id'	=> $row["category_id"], 'brand_id' => $row["brand_id"]),
		'type'		=> array('id' => $row['id'], 'category_id'	=> $row["category_id"], 'bagtype_id' => $row["bagtype_id"]),
	);
	
    $ct += 1;
}
// Make rating data (Product)
if (is_array($rows) && (count($rows) > 0)) {
	foreach ($rows as &$prodval) {
		$prodval['reputation_product'] = 0;
		if (isset($prodval['id'])) {
			$sql = sprintf("SELECT COALESCE(SUM(vf.rep), 0) AS sum_reputaion, COUNT(vf.id) AS count_reputation FROM view_public_svcgetuserfeedback AS vf WHERE vf.id_service = '%d'", $prodval['id']);
			$sql_rate = $pdoObj->prepare($sql);
			$sql_rate->execute();
			$rate_row = $sql_rate->fetch(PDO::FETCH_ASSOC);
			if (isset($rate_row['sum_reputaion']) && isset($rate_row['count_reputation'])) {
				if (($rate_row['sum_reputaion'] > 0) && ($rate_row['count_reputation'] > 0)) {
					$prodval['reputation_product'] = ceil($rate_row['sum_reputaion']/$rate_row['count_reputation']);
				}
			}	
		}
	}
}
// Make $my_id
$my_id = 0;
// Make rating data (Seller)
if (is_array($rows) && (count($rows) > 0)) {
	foreach ($rows as &$rowval) {
		$rowval['reputation_seller'] = 0;
		if (isset($rowval['id_seller'])) {
			$my_id = (int)$rowval['id_seller'];
			$sql = sprintf("SELECT COALESCE(SUM(vf.rep), 0) AS sum_reputaion, COUNT(vf.id) AS count_reputation FROM view_public_svcgetuserfeedback AS vf WHERE vf.id_seller = '%d'", $rowval['id_seller']);
			$sql_rate = $pdoObj->prepare($sql);
			$sql_rate->execute();
			$rate_row = $sql_rate->fetch(PDO::FETCH_ASSOC);
			if (isset($rate_row['sum_reputaion']) && isset($rate_row['count_reputation'])) {
				if (($rate_row['sum_reputaion'] > 0) && ($rate_row['count_reputation'] > 0)) {
					$rowval['reputation_seller'] = ceil($rate_row['sum_reputaion']/$rate_row['count_reputation']);
				}
			}	
		}
	}
}

///////////////////////////////////////////////////////
/////////////// BEGIN - sellers rating ////////////////
///////////////////////////////////////////////////////
$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCompleteOrder = $result['value'];

$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCanceledOrder = $result['value'];

// Number of complete orders for this year
$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
$noOfCompleteOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

// Number of canceled orders for this year
$noOfCanceledOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

$rating = ($noOfCompleteOrdersThisYear * $pointsPerCompleteOrder) + $noOfCanceledOrdersThisYear*$pointsPerCanceledOrder;

// Star rating
$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
$starRatingLastYear = $engine->executePrepared("
                SELECT last_year_star_rating
                FROM users
                WHERE id = :id",
                array(
                    'id' => $my_id
                )
            );
$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
$starRatingLastYear = $result['last_year_star_rating'];

if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
$starRatingThisYear = 'Bronze';
if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
if ($rating > 400) $starRatingThisYear = 'Diamond';

if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) $starRating = $starRatingThisYear;
else $starRating = $starRatingLastYear;
///////////////////////////////////////////////////////
///////////////////////// END /////////////////////////
///////////////////////////////////////////////////////
// Push Rating and medalRating
if (is_array($rows) && (count($rows) > 0)) {
	foreach ($rows as &$rateVal) {
		$rateVal['rating'] = $rating;
		$rateVal['medalRating'] = $starRating;
	}
}


// Make recomendation data
foreach ($rows as &$recomendationrow) {
	$recomendationrow['recomendation']['brand_data'] = array();
	$recomendationrow['recomendation']['type_data'] = array();
	if (isset($recomendationrow['recomendation']['brand'])) {
		$sql = sprintf("SELECT prodet.* FROM view_public_svcgetproductlist AS prodet WHERE (prodet.id != '%d') AND (prodet.category_id = '%d' AND prodet.brand_id = '%d') AND (prodet.conditions IN(0,1))",
			$recomendationrow['recomendation']['brand']['id'],
			$recomendationrow['recomendation']['brand']['category_id'],
			$recomendationrow['recomendation']['brand']['brand_id']
		);
		$recomendation_brand = $pdoObj->prepare($sql);
		$recomendation_brand->execute();
		while ($row = $recomendation_brand->fetch(PDO::FETCH_ASSOC)) {
			$recomendationrow['recomendation']['brand_data'][] = $row;
		}
	}
	if (isset($recomendationrow['recomendation']['type'])) {
		$sql = sprintf("SELECT prodet.* FROM view_public_svcgetproductlist AS prodet WHERE (prodet.id != '%d') AND (prodet.category_id = '%d' AND prodet.bagtype_id = '%d') AND (prodet.conditions IN(0,1))",
			$recomendationrow['recomendation']['type']['id'],
			$recomendationrow['recomendation']['type']['category_id'],
			$recomendationrow['recomendation']['type']['bagtype_id']
		);
		$recomendation_type = $pdoObj->prepare($sql);
		$recomendation_type->execute();
		while ($row = $recomendation_type->fetch(PDO::FETCH_ASSOC)) {
			$recomendationrow['recomendation']['type_data'][] = $row;
		}
	}
}



$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
