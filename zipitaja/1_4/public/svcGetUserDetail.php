<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
$engine = new Engine();
//Get POST Value
$engine->setPostField(
    array(
        "param_user" => true,
        "api_key" => true
    )
);

$user = $engine->getPOSTField("param_user");

//Check API key
$engine->checkAPIKeyPair();

$query = $engine->executePrepared("SELECT count(id) AS 'count' FROM service WHERE id_user = :id_user", array("id_user" => $user));
$rows = $query->fetchAll(PDO::FETCH_ASSOC);
$serviceCount = $rows[0]["count"];

$query = $engine->executePrepared("SELECT count(p.id) AS 'count' FROM payment_list p, service s WHERE p.id_service = s.id AND p.status = 4 AND id_user = :id_user", array("id_user" => $user));
$rows = $query->fetchAll(PDO::FETCH_ASSOC);
$soldCount = $rows[0]["count"];

/*
 * Database Opration
 */
/* $icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH; */
$icon_path = APP_IMG_USER_PATH;
$query = $engine->executePrepared("
    SELECT
	    u.id,
        u.username,
        CONCAT('$icon_path', u.icon) as icon,
        u.about,
        u.username_slug,
        100 as 'rating_seller',
        100 as 'rating_buyer',
        $serviceCount as 'jumlah_produk',
        $soldCount as 'produk_terjual'
    FROM
	    users u
    WHERE
	    u.id = :id",
    array("id" => $user)
);
$rows = $query->fetchAll(PDO::FETCH_ASSOC);
$my_id = 0;
// Make rating data
if (is_array($rows) && (count($rows) > 0)) {
	foreach ($rows as &$row) {
		if (isset($row['id'])) {
			$my_id = (int)$row['id'];
			$row['reputation_seller'] = 0;
			$row['reputation_buyer'] = 0;
			$sql = "SELECT COALESCE(SUM(vf.rep), 0) AS sum_reputation, COUNT(vf.id) AS count_reputation FROM view_public_svcgetuserfeedback AS vf WHERE vf.id_seller = :id_seller";
			$sql_params = array('id_seller' => $row['id']);
			$sql_query = $engine->executePrepared($sql, $sql_params);
			$rate_row = $sql_query->fetch(PDO::FETCH_ASSOC);
			if (isset($rate_row['sum_reputation']) && isset($rate_row['count_reputation'])) {
				if (($rate_row['sum_reputation'] > 0) && ($rate_row['count_reputation'] > 0)) {
					$row['reputation_seller'] = ceil($rate_row['sum_reputation']/$rate_row['count_reputation']);
				}
			}
		}
	}
}
///////////////////////////////////////////////////////
/////////////// BEGIN - sellers rating ////////////////
///////////////////////////////////////////////////////
$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCompleteOrder = $result['value'];

$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCanceledOrder = $result['value'];

// Number of complete orders for this year
$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
$noOfCompleteOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

// Number of canceled orders for this year
$noOfCanceledOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

$rating = ($noOfCompleteOrdersThisYear * $pointsPerCompleteOrder) + $noOfCanceledOrdersThisYear*$pointsPerCanceledOrder;

// Star rating
$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
$starRatingLastYear = $engine->executePrepared("
                SELECT last_year_star_rating
                FROM users
                WHERE id = :id",
                array(
                    'id' => $my_id
                )
            );
$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
$starRatingLastYear = $result['last_year_star_rating'];

if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
$starRatingThisYear = 'Bronze';
if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
if ($rating > 400) $starRatingThisYear = 'Diamond';

if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) $starRating = $starRatingThisYear;
else $starRating = $starRatingLastYear;
///////////////////////////////////////////////////////
///////////////////////// END /////////////////////////
///////////////////////////////////////////////////////
// Push Rating and medalRating
if (is_array($rows) && (count($rows) > 0)) {
	foreach ($rows as &$rateVal) {
		$rateVal['rating'] = $rating;
		$rateVal['medalRating'] = $starRating;
	}
}


$engine->setTranslateDataResponse($rows);

/*
 * Send Response
 */
$engine->sendResponse();
