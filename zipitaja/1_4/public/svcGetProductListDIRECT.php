<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/FunctionList.php";

$engine = new Engine();
//Get POST Value
$engine->setPostField(
    array(
        "param_category" => true,
        "param_user" => true,
        "param_search_key" => false,
        "param_brand" => false, // Value From Database
        "param_color" => false, // Value From Database
        "param_type" => false, // Value From Database
        "param_condition" => false, // 0 = New, 1 = Pre-Loved
        "param_price" => false, // 1 (< 5jt), 2 (< 10jt), 3 (< 25jt), 4 (< 50jt), 5 (< 100jt)
        "param_payment" => false, // 0 all, 1 bt, 2 cc, 3 kp
        "param_sort_mode" => true, //0 - 7, 0 tanpa sort
        "param_page" => true,
        "api_key" => true
    )
);
//Check API key
$engine->checkAPIKeyPair();

//Database Opration
$category = $engine->getPOSTField("param_category");
if ($category == -1) {
    $category = "";
} else {
    $category = " AND b.category_id = $category";
}

$brand = $engine->getPOSTField("param_brand");
if ($brand) {
    $brand = " AND b.brand_id = '$brand'";
} else {
    $brand = "";
}

$color = $engine->getPOSTField("param_color");
if ($color) {
    $color = " AND b.color_id = '$color'";
} else {
    $color = "";
}

$type = $engine->getPOSTField("param_type");
if ($type) {
    $type = " AND b.bagtype_id = '$type'";
} else {
    $type = "";
}

$condition = $engine->getPOSTField("param_condition");
if ($condition) {
    $condition = " AND b.conditions = '$condition'";
} else {
    $condition = "";
}

$payment = $engine->getPOSTField("param_payment");
if ($payment) {
    $t = '';
    if ($payment == 1) {
        $t = 'bt';
    } elseif ($payment == 2) {
        $t = 'cc';
    } elseif ($payment == 3) {
        $t='kp';
    }
    $payment = " AND LOCATE('$t', CONCAT('bt,cc', IF(KP_QUALIFY(b.brand_id, b.bagtype_id, b.conditions, CAST(b.price AS DECIMAL), b.category_id, b.id_user, b.payment_kp_accepted) = 1, ',kp', ''))) != 0";
} else {
    $payment = "";
}

$price = intval($engine->getPOSTField("param_price"));
if ($price) {
    switch ($price) {
        case 1:
            $price = " AND  CAST(b.price as DECIMAL) <= 5000000";
            break;
        case 2:
            $price = " AND  CAST(b.price as DECIMAL) <= 10000000";
            break;
        case 3:
            $price = " AND  CAST(b.price as DECIMAL) <= 25000000";
            break;
        case 4:
            $price = " AND  CAST(b.price as DECIMAL) <= 50000000";
            break;
        case 5:
            $price = " AND  CAST(b.price as DECIMAL) <= 100000000";
            break;
        default:
            $price = '';
    }
} else {
    $price = "";
}

$user = $engine->getPOSTField("param_user");
if ($user == -1) {
    $user = "";
} else {
    $user = " AND b.id_user = $user";
}

$search = $engine->getPOSTField("param_search_key");
if ($search == null) {
    $search = "";
}

$page = $engine->getPOSTField("param_page");
$offset = $page * 10;

$base_sql = "
SELECT * FROM (SELECT   
          t.*,
          -- IF(p.id_service IS NULL AND t.force_sold = 0, 0, 1) AS sold,
          0 as sold,
          IF(nv.id_service IS NULL, 0, nv.num) AS views
FROM      (
                 SELECT b.id,
                        b.category_id,
                        c.name                                      category_name,
                        b.brand_id,
                        b.color_id,
                        b.bagtype_id,
                        b.name,
                        b.id            AS listing,
                        b.pictures,
                        s.username      AS seller,
                        CAST(b.price as DECIMAL) as price,
                        b.conditions,
                        b.can_buy,
                        b.force_sold,
                        CONCAT('bt,cc', IF(KP_QUALIFY(b.brand_id, b.bagtype_id, b.conditions, CAST(b.price AS DECIMAL), b.category_id, b.id_user, b.payment_kp_accepted) = 1, ',kp', '')) as accepted_payment
                 from   service b,
                        users s,
                        service_categories c
                 WHERE  b.id_user = s.id 
                        AND b.category_id = c.id
                        AND CAST(b.startdatef AS UNSIGNED) >= UNIX_TIMESTAMP()
                        AND qty > 0
                 $brand
                 $color
                 $type
                 $condition
                 $price
                 $payment
                 -- AND    b.activate = 1
                 AND    (
                               b.name LIKE :search OR b.id LIKE :search OR s.username LIKE :search) $category $user) t
-- LEFT JOIN payment_list p
-- ON        (
--                    p.id_service = t.id )
LEFT JOIN nviews nv
ON        (
                    nv.id_service = t.id )) t 
";

$base_param = array(
    "search" => "%" . $search . "%"
);

$sort = $engine->getPOSTField("param_sort_mode");
$sort_text = "";
if ($sort == "0") {
    $sort_text = "ORDER BY id DESC";
} elseif ($sort == "1") {
    $sort_text = "ORDER BY views DESC";
} elseif ($sort == "2") {
    $sort_text = "ORDER BY id DESC";
} elseif ($sort == "3") {
    $sort_text = "ORDER BY id ASC";
} elseif ($sort == "4") {
    $sort_text = "ORDER BY price DESC";
} elseif ($sort == "5") {
    $sort_text = "ORDER BY price ASC";
} elseif ($sort == "6") {
    $sort_text = "ORDER BY name ASC";
} elseif ($sort == "7") {
    $sort_text = "ORDER BY name DESC";
}

//die($engine->createResponse(Engine::codError, $base_sql . " $sort_text LIMIT 10 OFFSET $offset", $base_param));
$query = $engine->executePrepared($base_sql . " $sort_text LIMIT 10 OFFSET $offset", $base_param);
$rows = array();
$ct = 0;
clearstatcache();
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["pictures"])[0];
    if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
        generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
    }

    $rowC = $engine->executePrepared("SELECT COUNT(id) FROM ($base_sql) a", $base_param)->fetchColumn();

    $cid = $row['id'];
    $stock = '/homepages/10/d492278480/htdocs/rushedo/assets/service/stock/' . $cid . '.jpg';
    $rows[$ct] = array(
        "id" => $cid,
        "has_stock" => file_exists($stock) ? 1 : 0,
        "count" => $rowC,
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "picture" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "seller" => $row["seller"],
        "price" => $row["price"],
        "can_buy" => $row["can_buy"],
        "sold" => $row["sold"],
        "conditions" => $row["conditions"],
        "accepted_payment" => $row["accepted_payment"]
    );
    $ct++;
}
$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
