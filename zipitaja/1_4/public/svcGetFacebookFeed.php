<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/12/2016
 * Time: 6:44 PM
 */
require_once "../core/Consts.php";
require_once "../core/Engine.php";
require_once "rss_php.php";

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField([]);

// Authenticate
$url = "https://graph.facebook.com/oauth/access_token?client_id=450431345084697&client_secret=" . FB_SECRET . "&grant_type=client_credentials";
$options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120              // time-out on response
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$result = curl_exec($ch);
$result = json_decode($result);
curl_close($ch);

$access_token = $result->access_token;
if (!$access_token) {
    $engine->setSingleLineResponse(json_encode(['data' => []]));
    $engine->sendResponse();
    return;
}

// Get Facebook Feed
$url = "https://graph.facebook.com/457040171049599/feed?fields=id,from,message,picture,full_picture,link,name,icon,type,status_type,created_time&access_token=$access_token&limit=250";
$options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120              // time-out on response
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$result = curl_exec($ch);
curl_close($ch);

$engine->setSingleLineResponse($result);

/*
 * Send Response
 */
$engine->sendResponse();
