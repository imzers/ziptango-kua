<?php

class SessionDatabaseHandler
{

    /**
     * PDO
     */
    private $db;

    public function __construct($db)
    {
        // Assign Database object
        $this->db = $db;

        //Set Session Path
        session_save_path(__DIR__ . "/session");

        // Set handler to overide SESSION
        session_set_save_handler(
            array($this, "_open"),
            array($this, "_close"),
            array($this, "_read"),
            array($this, "_write"),
            array($this, "_destroy"),
            array($this, "_gc")
        );

        // Start the session
        session_start();
    }

    /**
     * Open
     */
    public function _open()
    {
        // If successful
        if ($this->db) {
            // Return True
            return true;
        }
        // Return False
        return false;
    }

    /**
     * Close
     */
    public function _close()
    {
        //Close Connection
        $this->db = null;
        // Return False
        return false;
    }

    /**
     * Read
     * @param $id
     * @return string
     */
    public function _read($id)
    {
        // Set query
        $query = $this->db->prepare("SELECT s.data FROM tb_session s WHERE s.id = :id");

        // Attempt execution
        // If successful
        if ($query->execute(array("id" => $id))) {
            // Save returned row
            $row = $query->fetch(PDO::FETCH_ASSOC);
            // Return the data
            return $row["data"];
        } else {
            // Return an empty string
            return "";
        }
    }

    /**
     * Write
     * @param $id
     * @param $data
     * @return bool
     */
    public function _write($id, $data)
    {
        // Create time stamp
        $access = time();

        // Set query
        $query = $this->db->prepare("REPLACE INTO tb_session VALUES (:id, :access, :data)");

        // Attempt Execution
        // If successful
        if ($query->execute(array(
            "id" => $id,
            "access" => $access,
            "data" => $data,
        ))
        ) {
            // Return True
            return true;
        }

        // Return False
        return false;
    }

    /**
     * Destroy
     * @param $id
     * @return bool
     */
    public function _destroy($id)
    {
        // Set query
        $query = $this->db->prepare("DELETE FROM tb_session WHERE id = :id");

        // Attempt execution
        // If successful
        if ($query->execute(array("id" => $id))) {
            // Return True
            return true;
        }

        // Return False
        return false;
    }

    /**
     * Garbage Collection
     * @param $max
     * @return bool
     */
    public function _gc($max)
    {
        // Calculate what is to be deemed old
        $old = time() - $max;

        // Set query
        $query = $this->db->prepare("DELETE * FROM tb_session WHERE access < :old");

        // Attempt execution
        if ($query->execute(array("old" => $old))) {
            // Return True
            return true;
        }

        // Return False
        return false;
    }

}