<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/12/2016
 * Time: 6:44 PM
 */
require_once "../core/Consts.php";
require_once "../core/Engine.php";

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField([]);

$page = isset($_GET['page']) ? $_GET['page'] : 0;
$page = is_numeric($page) ? $page : 0;
if ($page > 0) $page--;
$limit = 25;

// Ambil data feed
$query = $engine->executePrepared("SELECT * FROM `feed_list` ORDER BY create_time DESC LIMIT $limit OFFSET " . $limit * $page, []);
$result = $query->fetchAll(PDO::FETCH_ASSOC);
$engine->setTranslateDataResponse($result);

/*
 * Send Response
 */
$engine->sendResponse();
