<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 6/26/2016
 * Time: 5:31 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";

$engine = new Engine();

//Get POST Value
$engine->setPostField(
    array(
        "param_mode" => true, //1 = contact us, 2 = terms and service, 3 = faq, 4 = how buy, 5 = how sell, 6 = vip
        "api_key" => true
    )
);

$mode = $engine->getPOSTField("param_mode");

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Database Opration
 */
//Get Term and Service
$alias = "";
if ($mode == 1)
    $alias = "contact";
else if ($mode == 2)
    $alias = "terms-of-service";
else if ($mode == 3)
    $alias = "faq";
else if ($mode == 4)
    $alias = "how-shop";
else if ($mode == 5)
    $alias = "how-sell-1";
else if ($mode == 6)
    $alias = "vip";

$query = $engine->executePrepared("
    SELECT
        *
    FROM
        cms
    WHERE
	    alias = '$alias'",
    array()
);
$rows = $query->fetchAll(PDO::FETCH_ASSOC);

$server = APP_SERVER_ROOT;

$bd = $rows[0]["content"];
if ($mode == 1)
    $bd .=  "<br /><br /><button class='btn btn-default center-block' type='button' onclick='window.JSIface.showFormContact();'>Kontak Formulir</button>";

$content = utf8_encode("<div class='container'>" . $bd . "</div>");
$content = str_replace("#DYNAMIC_CMS_CONTACT_FORM#", "", $content);
$content = str_replace("/assets/", $server . "assets/", $content);
$content = str_replace("href=\"/", "href=\"$server/", $content);
$content = str_replace("<img", "<img width='100%' ", $content);

if ($mode == 6)
    $content = "<div class='container'>VIP Service kami sediakan secara GRATIS untuk pengguna yang mempunyai lebih dari 8 produk untuk dijual. Kami akan mengirimkan photographer professional beserta staff ke tempat Anda.<br /><br />Hubungi kami melalui metode berikut untuk menjadwalkan VIP Service<br />WA/TEL 0859 2056 6936<br />LINE @ziptango<br />BBM 79910171<br /><br /></div>" . $content;

$result = array(
    "content" => $content,
    "browser_title" => $rows[0]["title"]
);

$engine->setTranslateDataResponse(array($result));

//Send Response
$engine->sendResponse();