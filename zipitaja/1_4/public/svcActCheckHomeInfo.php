<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField(
    array(
        'gcm_id' => false,
        'mobile' => true,
        'api_key' => true
    ));

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Database Opration
 */
$result = array();

$video_banner = array();

$file = serverRoot() . APP_IMG_BANNER_PATH . "youtube.jpg";
if (file_exists($file)) {
    $video_banner[0]["id"] = 0;
    $video_banner[0]["link"] = APP_SERVER_ROOT . APP_IMG_BANNER_PATH . "youtube.jpg";
    $video_banner[0]["video_url"] = "https://youtu.be/2JN8alHUge4";
    $video_banner[0]["name"] = "youtube.jpg";
    $video_banner[0]["updated"] = filemtime($file); // UNIX TIME STAMP
}

//Banner
/*
$banner = array();
$ct = 0;
for ($i = 0; $i < 10; $i++) {
    $file = serverRoot() . APP_IMG_BANNER_PATH . ($i + 1) . ".jpg";

    //die($engine->createResponse(Engine::codError, $file));

    if (file_exists($file)) {
        $banner[$ct]["id"] = $ct + 1;
        $banner[$ct]["link"] = APP_SERVER_ROOT . APP_IMG_BANNER_PATH . ($i + 1) . ".jpg";
        $banner[$ct]["name"] = $i . ".jpg";
        $banner[$ct]["updated"] = filemtime($file); // UNIX TIME STAMP
        $ct++;
    }
}
*/
$banner = array();
$path_file = (serverRoot() . 'banner' . DIRECTORY_SEPARATOR . 'banner.txt');
$file_i = 0;
if (file_exists($path_file)) {
	// Open the log file for read
	if ((!$stream_fileopen = fopen($path_file, 'r'))) {
		for ($i = 0; $i < 10; $i++) {
			$file = (serverRoot() . 'banner' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . ($i + 1) . '.jpg');
			if (file_exists($file)) {
				$banner[$file_i]["id"] = ($file_i + 1);
				$banner[$file_i]["link"] = APP_SERVER_ROOT . 'banner/' . ($i + 1) . ".jpg";
				$banner[$file_i]["name"] = $i . ".jpg";
				$banner[$file_i]["updated"] = filemtime($file); // UNIX TIME STAMP
				$file_i += 1;
			}
		}
	} else {
		try {
			$file_data = fread($stream_fileopen, filesize($path_file));
			fclose($stream_fileopen);
		} catch (Exception $ex) {
			throw $ex;
			$file_data = '';
		}
		try {
			$banner_streams = preg_split('/$\R?^/m', $file_data);
		} catch (Exception $ex) {
			throw $ex;
			$banner_streams = FALSE;
		}
		if ($banner_streams != FALSE) {
			if (is_array($banner_streams) && (count($banner_streams) > 0)) {
				$banner_i = 1;
				foreach ($banner_streams as $val) {
					$bannerval = explode('|', $val);
					$bannerfile = (serverRoot() . 'banner' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . (isset($bannerval[0]) ? $bannerval[0] : "{$banner_i}.jpg"));
					$banner[] = array(
						'id'		=> $banner_i,
						'link'		=> (APP_SERVER_ROOT . 'banner/img/' . (isset($bannerval[0]) ? $bannerval[0] : "{$banner_i}.jpg")),
						'url'		=> (isset($bannerval[1]) ? $bannerval[1] : '#'),
						'name'		=> (isset($bannerval[0]) ? $bannerval[0] : "{$banner_i}.jpg"),
						'updated'	=> filemtime($bannerfile),
					);
					
					$banner_i += 1;
				}
			}
		}
	}
	
}



//Kategori
$img_path = APP_SERVER_ROOT;

$query = $engine->executePrepared("SELECT id, name, icons, CONCAT('$img_path', icons) as link  FROM service_categories s WHERE s.enabled = 1 AND s.id != 6", null);
$result = $query->fetchAll(PDO::FETCH_ASSOC);

$category = array();
for ($i = 0; $i < count($result); $i++) {
    $fileC = serverRoot() . $result[$i]["icons"];

    //die($engine->createResponse(Engine::codError, $fileC));

    $category[$i]["id"] = $result[$i]["id"];
    $category[$i]["name"] = $result[$i]["name"];
    $category[$i]["link"] = $result[$i]["link"];
    if (file_exists($fileC))
        $category[$i]["updated"] = filemtime($fileC);
    else
        $category[$i]["updated"] = -1;
}

$engine->setSingleLineResponse(
    array(
        "video_banner"		=> $video_banner,
        "banner"			=> $banner,
        "category"			=> $category,
        "logged"			=> $engine->isLogged() ? "1" : "0"
    )
);

$gcm_id = $engine->getPOSTField('gcm_id');
$mobile = $engine->getPOSTField('mobile');

// Push Notification ID
if (!$engine->isLogged())
    $engine->executePrepared("REPLACE INTO gcm_users (gcm_regid, user_id, mobile) VALUES (:gcm_regid, 0, :mobile)", array(
        "gcm_regid" => $gcm_id,
        "mobile" => $mobile
    ));

/*
 * Send Response
 */
$engine->sendResponse();
