<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/MCrypt.php');


$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "id_user" => true,
        "api_key" => true
    )
);
$user_id = $engine->getPOSTField("id_user");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

//Core
$query = $engine->executePrepared("SELECT * FROM view_public_svcgetuserfeedback AS t WHERE t.seller_id = :user_id ORDER BY t.id DESC",
    array(
        "user_id" => $user_id
    )
);
$as_seller = $query->fetchAll(PDO::FETCH_ASSOC);
if ($as_seller != FALSE) {
	$reputation_sum = 0;
	$reputation_count = count($as_seller);
	if (is_array($as_seller) && ($reputation_count > 0)) {
		$row_i = 0;
		foreach ($as_seller as $row) {
			$reputation_sum += (int)$row['rep'];
			$row_i += 1;
		}
	}
	// Make reputation
	if (is_array($as_seller) && ($reputation_count > 0)) {
		foreach ($as_seller as &$keval) {
			$keval['reputation_calculate'] = array(
				'sum'		=> $reputation_sum,
				'count'		=> $reputation_count,
			);
			$keval['reputation'] = ceil($reputation_sum/$reputation_count);
		}
	}
}





$engine->setTranslateDataResponse($as_seller);

/*
 * Send Response
 */
$engine->sendResponse();

