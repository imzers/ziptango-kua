<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 6/27/2016
 * Time: 8:44 PM
 */
require_once "../core/Engine.php";
require_once "../core/MCrypt.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true, // fb, g+, other
        "param_access" => true,
        "param_password" => true,
        "param_gcm_id" => true,
        "param_mobile" => true,
        "api_key" => true
    )
);
$param_mode = $engine->getPOSTField("param_mode");
$param_access = $engine->getPOSTField("param_access");
$param_password = $engine->getPOSTField("param_password");
$param_gcm_id = $engine->getPOSTField("param_gcm_id");
$param_mobile = $engine->getPOSTField("param_mobile");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
if ($param_mode == "fb") {
    //LOGIN BY FB
    //Validate Facebook Login
    $appsecret_proof = hash_hmac('sha256', $param_access, FB_SECRET);
    $url = "https://graph.facebook.com/me?fields=id,email,name,about,first_name,last_name&access_token=$param_access&appsecret_proof=$appsecret_proof";
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER => false,            // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
        CURLOPT_AUTOREFERER => true,        // set referrer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
        CURLOPT_TIMEOUT => 120              // time-out on response
    );
    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    curl_close($ch);
    //Decode Result
    $res = json_decode($content);
    $fb_id = $res->id;
    $fb_email = $res->email;
    $fb_name = $res->name;
    $fb_first_name = $res->first_name;
    $fb_last_name = $res->last_name;
    //Check Email is Exists
    //If email not exist, register as new user
    $is_new_user = false;
    $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email", array("email" => $fb_email));
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] <= 0) {
        //Check username is exists
        //Change username if exists
        $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.username = :username", array("username" => $fb_first_name));
        $res = $query->fetch(PDO::FETCH_ASSOC);
        if ($res["result"] >= 1) {
            $fb_first_name = $fb_first_name . generateUniqueID(3);
        }
        //Register new user
        $query = $engine->executePrepared("
           INSERT INTO users
                (id_admin, id_users, firstname, lastname, username, icon, email, about, telephone, password, permanent_link_code, activate_code, active, mobile, data_reg)
            VALUES
                (0, 0, :firstname, '', :username, 'no_user_icons.png', :email, '-', :telephone, :password, :permanent_link_code, :activate_code, 1, :mobile, UNIX_TIMESTAMP())",
            array(
                "firstname" => $fb_name,
                "username" => $fb_first_name,
                "email" => $fb_email,
                "telephone" => "-",
                "permanent_link_code" => generateUniqueID(16),
                "password" => "FB",
                "activate_code" => "-",
                "mobile" => $engine->getPOSTField("param_mobile")
            )
        );

        $mailC = getMailMessage("congratulation");
        $mailC->message = str_replace("#fullname#", $fb_name, $mailC->message);
        sendMail($fb_email, $mailC->subject, $mailC->message);

        $is_new_user = true;
    }

    //Get User Data
    $icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH;
    $query = $engine->executePrepared("SELECT id, email, firstname, lastname, username, CONCAT('$icon_path', icon) as 'icon', permanent_link_code as permalink FROM users u WHERE u.email = :email",
        array(
            "email" => $fb_email
        )
    );
    $res = $query->fetchAll(PDO::FETCH_ASSOC);
    $res[0]["is_new_user"] = $is_new_user ? 1 : 0;
    $engine->setTranslateDataResponse($res);
    $engine->setInitSession($res[0]["id"], $res[0]["email"], $res[0]["firstname"] . " " . $res[0]["lastname"], $res[0]["permalink"]);
} elseif ($param_mode == "g+") {
    $url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=$param_access";
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER => false,            // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
        CURLOPT_AUTOREFERER => true,        // set referrer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
        CURLOPT_TIMEOUT => 120              // time-out on response
    );
    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $payload = json_decode(curl_exec($ch), true);
    curl_close($ch);

    if ($payload) {
          $g_userid = $payload['sub'];
          $g_email = $payload['email'];
          $g_username = $payload['name'];
          $g_firstname = $payload['name'];
          
        $is_new_user = false;
        $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email", array("email" => $g_email));
        $res = $query->fetch(PDO::FETCH_ASSOC);
        if ($res["result"] <= 0) {
            //Check username is exists
            //Change username if exists
            $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.username = :username", array("username" => $g_username));
            $res = $query->fetch(PDO::FETCH_ASSOC);
            if ($res["result"] >= 1) {
                $g_firstname = $g_firstname . generateUniqueID(3);
            }
            //Register new user
            $query = $engine->executePrepared("
           INSERT INTO users
                (id_admin, id_users, firstname, lastname, username, icon, email, about, telephone, password, permanent_link_code, activate_code, active, mobile, data_reg)
            VALUES
                (0, 0, :firstname, '', :username, 'no_user_icons.png', :email, '-', :telephone, :password, :permanent_link_code, :activate_code, 1, :mobile, UNIX_TIMESTAMP())",
            array(
                "firstname" => $g_username,
                "username" => $g_firstname,
                "email" => $g_email,
                "telephone" => "-",
                "permanent_link_code" => generateUniqueID(16),
                "password" => "G+",
                "activate_code" => "-",
                "mobile" => $engine->getPOSTField("param_mobile")
            )
                );

                $mailC = getMailMessage("congratulation");
                $mailC->message = str_replace("#fullname#", $g_username, $mailC->message);
                sendMail($g_email, $mailC->subject, $mailC->message);

                $is_new_user = true;
        }

        //Get User Data
        $icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH;
        $query = $engine->executePrepared("SELECT id, email, firstname, lastname, username, CONCAT('$icon_path', icon) as 'icon', permanent_link_code as permalink FROM users u WHERE u.email = :email",
        array(
            "email" => $g_email
        )
        );
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        $res[0]["is_new_user"] = $is_new_user ? 1 : 0;
        $engine->setTranslateDataResponse($res);
        $engine->setInitSession($res[0]["id"], $res[0]["email"], $res[0]["firstname"] . " " . $res[0]["lastname"], $res[0]["permalink"]);
    } else {
          $engine->setErrorResponse("Token tidak valid!");
    }
} else {
    //LOGIN BY NORMAL FORM
    $crypt = new MCrypt();
    $pass = $param_mobile == 2 ? $param_password : $crypt->decrypt($param_password);
    $icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH;
    $query = $engine->executePrepared("SELECT id, email, firstname, lastname, username, CONCAT('$icon_path', icon) as 'icon', permanent_link_code as permalink FROM users u WHERE u.email = :email AND u.password = :password",
        array(
            "email" => $param_access,
            "password" => md5($pass) //$crypt->encrypt($pass, KEYPASS)
        )
    );
    $res = $query->fetchAll(PDO::FETCH_ASSOC);
    $engine->setTranslateDataResponse($res);
    $engine->setInitSession($res[0]["id"], $res[0]["email"], $res[0]["firstname"] . " " . $res[0]["lastname"], $res[0]["permalink"]);
}

//Change last activity
$engine->executePrepared("UPDATE users SET last_activity = UNIX_TIMESTAMP() WHERE id = :id", array("id" => $_SESSION["id"]));

// Push Notification ID
$engine->executePrepared("REPLACE INTO gcm_users (gcm_regid, user_id, mobile) VALUES (:gcm_regid, :user_id, :mobile)", array(
        "gcm_regid" => $param_gcm_id,
        "user_id" => $_SESSION["id"],
        "mobile" => $param_mobile
    )
);
//Send Result
$engine->sendResponse();
