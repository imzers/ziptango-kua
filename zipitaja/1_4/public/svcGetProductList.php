<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');


// Created by imzers@gmail.com
// Permalink string
if (!function_exists('base_permalink')) {
	function base_permalink($url) {
		$url = strtolower($url);
		$url = preg_replace('/&.+?;/', '', $url);
		$url = preg_replace('/\s+/', '_', $url);
		$url = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '_', $url);
		$url = preg_replace('|%|', '_', $url);
		$url = preg_replace('/&#?[a-z0-9]+;/i', '', $url);
		$url = preg_replace('/[^%A-Za-z0-9 \_\-]/', '_', $url);
		$url = preg_replace('|_+|', '-', $url);
		$url = preg_replace('|-+|', '-', $url);
		$url = trim($url, '-');
		$url = (strlen($url) > 128) ? substr($url, 0, 128) : $url;
		return $url;
	}
}

$engine = new Engine();
//Get POST Value
$engine->setPostField(
    array(
        "param_category" => true,
        "param_user" => true,
        "param_search_key" => false,
        "param_brand" => false, // Value From Database
        "param_color" => false, // Value From Database
        "param_type" => false, // Value From Database
        "param_condition" => false, // 0 = New, 1 = Pre-Loved
        "param_price" => false, // 1 (< 5jt), 2 (< 10jt), 3 (< 25jt), 4 (< 50jt), 5 (< 100jt)
        "param_payment" => false, // 0 all, 1 bt, 2 cc, 3 kp
        "param_sort_mode" => true, //0 - 7, 0 tanpa sort
        "param_page" => true,
        "api_key" => true
    )
);

//Check API key
$engine->checkAPIKeyPair();


//Database Opration
$category = $engine->getPOSTField("param_category");
if ($category == -1) {
    $category = "";
} else {
    $category = " AND b.category_id = $category";
}


$brand = $engine->getPOSTField("param_brand");
if ($brand) {
    $brand = " AND b.brand_id = '$brand'";
} else {
    $brand = "";
}

$color = $engine->getPOSTField("param_color");
if ($color) {
    $color = " AND b.color_id = '$color'";
} else {
    $color = "";
}

$type = $engine->getPOSTField("param_type");
if ($type) {
    $type = " AND b.bagtype_id = '$type'";
} else {
    $type = "";
}

$condition = $engine->getPOSTField("param_condition");
if ($condition) {
    $condition = " AND b.conditions = '$condition'";
} else {
    $condition = "";
}

$payment = $engine->getPOSTField("param_payment");
if ($payment) {
    $t = '';
    if ($payment == 1) {
        $t = 'bt';
    } elseif ($payment == 2) {
        $t = 'cc';
    } elseif ($payment == 3) {
        $t='kp';
    }
    $payment = " AND LOCATE('$t', CONCAT('bt,cc', IF(KP_QUALIFY(b.brand_id, b.bagtype_id, b.conditions, CAST(b.price AS DECIMAL), b.category_id, b.id_user, b.payment_kp_accepted) = 1, ',kp', ''))) != 0";
} else {
    $payment = "";
}

$price = intval($engine->getPOSTField("param_price"));
if ($price) {
    switch ($price) {
        case 1:
            $price = " AND  CAST(b.price as DECIMAL) <= 5000000";
            break;
        case 2:
            $price = " AND  CAST(b.price as DECIMAL) <= 10000000";
            break;
        case 3:
            $price = " AND  CAST(b.price as DECIMAL) <= 25000000";
            break;
        case 4:
            $price = " AND  CAST(b.price as DECIMAL) <= 50000000";
            break;
        case 5:
            $price = " AND  CAST(b.price as DECIMAL) <= 100000000";
            break;
        default:
            $price = '';
    }
} else {
    $price = "";
}

$user = $engine->getPOSTField("param_user");
if ($user > 0) {
	$user = " AND b.id_user = '$user'";
} else {
    $user = "";
}

$page = $engine->getPOSTField("param_page");
$offset = ($page * 10);

/*
$base_sql = <<<SQL
SELECT b.*, 0 as sold,
	IF (nv.id_service IS NULL, 0, nv.num) AS views
FROM view_public_svcgetproductlist AS b
	LEFT JOIN nviews AS nv ON nv.id_service = b.id
WHERE (1 = 1)
	$brand
	$color
	$type
	$condition
	$price
	$payment
	AND ((CONCAT('', b.name, '') LIKE :search) OR (CONCAT('', b.id, '') LIKE :search) OR (CONCAT('', b.seller, '') LIKE :search))
	$category 
	$user
SQL;
*/

$base_sql = <<<SQL
SELECT b.*, 0 as sold,
	IF (nv.id_service IS NULL, 0, nv.num) AS views
FROM view_public_svcgetproductlist AS b
	LEFT JOIN nviews AS nv ON nv.id_service = b.id
WHERE (1 = 1)
	$brand
	$color
	$type
	$condition
	$price
	$payment
	$category 
	$user
SQL;
// Fixing search method
$sql_search = "";
$search = $engine->getPOSTField("param_search_key");
if ($search != FALSE || ($seach != null)) {
	$search = (is_string($search) || is_numeric($search)) ? sprintf("%s", $search) : '';
	if (strlen($search) > 0) {
		$search_string = base_permalink($search);
		$search_array = explode("-", $search_string);
		if (count($search_array) > 0) {
			$sql_search .= " AND (";
			$search_i = 0;
			foreach ($search_array as $searchVal) {
				if ($search_i > 0) {
					$sql_search .= sprintf(" AND ((CONCAT('', b.name, '') LIKE '%%%s%%') OR (CONCAT('', b.id, '') LIKE '%%%s%%') OR (CONCAT('', b.seller, '') LIKE '%%%s%%'))",
						$searchVal, $searchVal, $searchVal);
				} else {
					$sql_search .= sprintf("((CONCAT('', b.name, '') LIKE '%%%s%%') OR (CONCAT('', b.id, '') LIKE '%%%s%%') OR (CONCAT('', b.seller, '') LIKE '%%%s%%'))",
						$searchVal, $searchVal, $searchVal);
				}
				$search_i++;
			}
			$sql_search .= ")";
		}
	}
	$base_sql .= $sql_search;
}

$sort = $engine->getPOSTField("param_sort_mode");
$sort_text = "";
if ($sort == "0") {
    $sort_text = "ORDER BY id DESC";
} elseif ($sort == "1") {
    $sort_text = "ORDER BY views DESC";
} elseif ($sort == "2") {
    $sort_text = "ORDER BY id DESC";
} elseif ($sort == "3") {
    $sort_text = "ORDER BY id ASC";
} elseif ($sort == "4") {
    $sort_text = "ORDER BY price DESC";
} elseif ($sort == "5") {
    $sort_text = "ORDER BY price ASC";
} elseif ($sort == "6") {
    $sort_text = "ORDER BY name ASC";
} elseif ($sort == "7") {
    $sort_text = "ORDER BY name DESC";
}
//-------------
$rowC = $engine->executePrepared("SELECT COUNT(id) AS rowC FROM ({$base_sql}) a", $base_param)->fetch(PDO::FETCH_ASSOC)['rowC'];
$sql_string =  sprintf("%s %s LIMIT 10 OFFSET %d",
	$base_sql,
	$sort_text,
	$offset
);

//die($engine->createResponse(Engine::codError, $base_sql . " $sort_text LIMIT 10 OFFSET $offset", $base_param));
/*
$pdoObj = $engine->getConn();
$sql_query = $pdoObj->prepare($sql_string);
$sql_query->execute();
*/
$sql_query = $engine->executePrepared($sql_string);
// DEBUG
//-----------
/*
$sql_string = str_replace(':search', "'%{$search}%'", $sql_string);
echo $sql_string;
exit;
*/
$rows = array();
$ct = 0;
clearstatcache();
while ($row = $sql_query->fetch(PDO::FETCH_ASSOC)) {
	if (isset($row["pictures"])) {
		$pic = unserialize($row["pictures"])[0];
	} else {
		$pic = '';
	}
	if (strlen($pic) > 0) {
		if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
			generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
		}
	}
    $cid = $row['id'];
    $stock = '/home/www/domains/jajan-domains/domains/sku/html/assets/service/stock/' . $cid . '.jpg';
    $rows[$ct] = array(
        "id" => $cid,
        "has_stock" => $row['qty'],
        "count" => $rowC,
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "picture" => APP_PIC_ROOT . $pic . thumb_prefix(),
        "seller" => $row["seller"],
        "price" => $row["price"],
        "can_buy" => $row["can_buy"],
        "sold" => $row["sold"],
        "conditions" => $row["conditions"],
        "accepted_payment" => $row["accepted_payment"]
    );
    $ct++;
}

$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
