<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'MCrypt.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'payment' . DIRECTORY_SEPARATOR . 'CC.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'payment' . DIRECTORY_SEPARATOR . 'CO.php');


$engine = new Engine();
# Post Parameters
$engine->setPostField(
    array(
        'param_services' 				=> true,
        'param_use_voucher' 			=> true,
        'param_coupon' 					=> false,
        'param_hp' 						=> true,
        'param_lat' 					=> true,
        'param_long' 					=> true,
        'param_address' 				=> true,
        'param_city' 					=> true,
        'param_state' 					=> true,
        'param_country' 				=> true,
        'param_unit_apt' 				=> true,
        'param_address_notes' 			=> true,
        'param_mobile' 					=> true,
        'api_key' 						=> true
    )
);
$engine->checkAPIKeyPair();
$engine->checkCurrentSession();
$input_params = array();
$input_params['param_services'] = $engine->getPOSTField('param_services');
$input_params['param_use_voucher'] = $engine->getPOSTField('param_use_voucher');
$input_params['param_coupon'] = $engine->getPOSTField('param_coupon');
$input_params['param_mobile'] = $engine->getPOSTField('param_mobile');

$input_params['param_hp'] = $engine->getPOSTField('param_hp');
$input_params['param_lat'] = $engine->getPOSTField('param_lat');
$input_params['param_long'] = $engine->getPOSTField('param_long');
$input_params['param_address'] = $engine->getPOSTField('param_address');
$input_params['param_city'] = $engine->getPOSTField('param_city');
$input_params['param_state'] = $engine->getPOSTField('param_state');
$input_params['param_country'] = $engine->getPOSTField('param_country');
$input_params['param_unit_apt'] = $engine->getPOSTField('param_unit_apt');
$input_params['param_address_notes'] = $engine->getPOSTField('param_address_notes');

// Hanya untuk memvalidasi pesanan
// pesanan akan disimpan di callback credit card
$customer_address = new CustAddress(
    $input_params['param_address'],
    $input_params['param_city'],
    $input_params['param_state'],
    $input_params['param_country'],
    $input_params['param_hp'],
    $input_params['param_lat'],
    $input_params['param_long'],
    $input_params['param_unit_apt'],
    $input_params['param_address_notes']
);
$co = new CO(CO::PAYMENT_CC, null, $input_params['param_services'], $input_params['param_use_voucher'], $input_params['param_coupon'], $input_params['param_mobile'], $customer_address);

print_r($co);
exit;

// Validasi Order
$error = '';
if (!$co->validateOrder($error)) {
    die($engine->createResponse(Engine::codError, $error));
}

// Update alamat
$co->updateAddress();

// Parameter CC
$data = json_encode(array(
    'usr_id' => $co->currentUserId(),
    'services' => str_replace(';;;', ';', $input_params['param_services']),
    'use_voucher' => $input_params['param_use_voucher'],
    'coupon' => $input_params['param_coupon'],
    'mobile' => $input_params['param_mobile'],
    'ex_adr' => array(
        $input_params['param_lat'],
        $input_params['param_long'],
        $input_params['param_country'],
        $input_params['param_unit_apt'],
        $input_params['param_address_notes'],
        substr($input_params['param_hp'], 0, 12)
    )
));





// Membuat eform cc
$engine->setSingleLineResponse(
    generateCCForm(
        uniqid(),
        $_SESSION['fullname'],
        $_SESSION['email'],
        $co->getGrandTotal(),
        new CustAddress($param_address, $param_city, $param_state, $param_country, $param_hp, $param_lat, $param_long, $param_unit_apt, $param_address_notes),
        $co->getServicesName(),
        $data
    )
);
$engine->sendResponse();





if ($_POST['ERR_CODE'] == 0) {
    $crypt = new MCrypt();
    $param = $_POST['MPARAM1'] . $_POST['MPARAM2'];
    $param = $crypt->decrypt($param);
    $param = json_decode($param, true);

    $trans_id = $_POST['TRANSACTIONID'];

    $current_id = $param['usr_id'];
    $param_services = $param['services'];
    $param_use_voucher = $param['use_voucher'];
    $param_coupon = $param['coupon'];
    $param_mobile = $param['mobile'];

    //Change Address
    $param_hp = $param['ex_adr'][5];
    $param_lat = $param['ex_adr'][0];
    $param_long = $param['ex_adr'][1];
    $param_address = $_POST['SHIPPING_ADDRESS'];
    $param_city = $_POST['SHIPPING_ADDRESS_CITY'];
    $param_state = $_POST['SHIPPING_ADDRESS_STATE'];
    $param_country = $param['ex_adr'][2];
    $param_unit_apt = $param['ex_adr'][3];
    $param_address_notes = $param['ex_adr'][4];

    $co = new CO(CO::PAYMENT_CC, $current_id, str_replace(';', ';;;', $param_services), $param_use_voucher, $param_coupon, $param_mobile, new CustAddress(
        $param_address,
        $param_city,
        $param_state,
        $param_country,
        $param_hp,
        $param_lat,
        $param_long,
        $param_unit_apt,
        $param_address_notes
    ));

    // Menyimpan pesanan
    $co->placeOrder($trans_id);

    header("Content-Type: text/html"); ?>

	<!DOCTYPE html>
	<html>

	<head>
	</head>

	<body>
		<script>
			window.JSIface.sayThanks();
		</script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
	</body>

	</html>

	<?php
} else {
        ?>

		<!DOCTYPE html>
		<html>

		<head>
			<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7'
			 crossorigin='anonymous'>
			<title>Pembayaran Gagal</title>
		</head>

		<body>

			<div class="container">
				<h3 class="text-center text-danger">PEMBAYARAN GAGAL</h3>
				<p class="text-center">Pembayarang gagal karena beberapa alasan.<br/><br/>Error Code :
					<?php echo $_POST['ERR_CODE']; ?><br/>Error Description :
					<?php echo $_POST['ERR_DESC']; ?>
				</p>
			</div>

			<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
		</body>

		</html>

		<?php
}
