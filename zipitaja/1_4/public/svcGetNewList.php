<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();
//Get POST Value
$engine->setPostField([]);

$page = isset($_GET['page']) ? $_GET['page'] : 0;
$page = is_numeric($page) ? $page : 0;
if ($page > 0) {
    $page--;
}
$limit = 25;

// Ambil 8 hari ke belakang
$prevWeek = time() - (8 * 24 * 60 * 60);

$base_sql = <<<SQLSTRING
SELECT b.*, 
	IF (p.id_service IS NULL AND b.force_sold = 0, 0, 1) AS sold,
	IF (nv.id_service IS NULL, 0, nv.num) AS views
FROM view_public_svcgetnewlist AS b
	LEFT JOIN payment_list AS p ON p.id_service = b.id
	LEFT JOIN nviews AS nv ON nv.id_service = b.id
WHERE b.datepost >= $prevWeek
SQLSTRING;
//-------------
$sql_string =  sprintf("%s ORDER BY b.datepost DESC LIMIT %d OFFSET %d",
	$base_sql,
	$limit,
	($limit * $page)
);
$base_param = array();
//------------------------------
$rowC = $engine->executePrepared("SELECT COUNT(id) AS rowC FROM ({$base_sql}) a", $base_param)->fetch(PDO::FETCH_ASSOC)['rowC'];
$pdoObj = $engine->getConn();
$sql_query = $pdoObj->prepare($sql_string);
//$sql_query->bindParam(':search', $base_param['search']);
$sql_query->execute();
$rows = array();
$ct = 0;
clearstatcache();
while ($row = $sql_query->fetch(PDO::FETCH_ASSOC)) {
    if (isset($row["pictures"])) {
		$pic = unserialize($row["pictures"])[0];
	} else {
		$pic = '';
	}
	if (strlen($pic) > 0) {
		if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
			generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
		}
	}
    $cid = $row['id'];
    $stock = '/homepages/10/d492278480/htdocs/rushedo/assets/service/stock/' . $cid . '.jpg';
    $rows[$ct] = array(
        "id" => $cid,
        "has_stock" => file_exists($stock) ? 1 : 0,
        "count" => $rowC,
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "picture" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "seller" => $row["seller"],
        "price" => $row["price"],
        "can_buy" => $row["can_buy"],
        "sold" => $row["sold"],
        "conditions" => $row["conditions"],
        "accepted_payment" => $row["accepted_payment"]
    );
    $ct++;
}

$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
