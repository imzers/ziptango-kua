<?php

function writeProgress($string)
{
    echo '[' . date('d-m-Y H:i:s') . '] ' . $string . "\n";
}

function findImg($source)
{
    $result = [];
    preg_match_all('/<img[^>]+>/i', $source, $result);

    if (count($result) <= 0) {
        return null;
    }
    if (count($result[0]) <= 0) {
        return null;
    }

    $img_tag = $result[0][0];
    $doc = new DOMDocument();
    $doc->loadHTML($img_tag);
    $xpath = new DOMXPath($doc);
    return $xpath->evaluate("string(//img/@src)");
}

function processCurlFB($starting_url)
{
    $sql = new Engine();

    writeProgress('Mengambil data feed facebook dari url ' . $starting_url);

    $options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120              // time-out on response
    );
    $ch = curl_init($starting_url);

    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    curl_close($ch);

    // Save data ke database, jika ada
    $feeds = json_decode($result, true);

    if (array_key_exists('error', $feeds)) {
        echo 'Ada kesalahan saat mengambil data dari facebook : ' . $feeds['error']['message'] . "\n";
        return;
    }

    if ($feeds['data']) {
        if (count($feeds['data']) > 0) {
            // Simpan data yang didapatkan
            foreach ($feeds['data'] as $item) {
                $query = $sql->executePrepared("INSERT INTO `feed_list` (`source`, `post_id`, `link`, `title`, `body`, `image`, `create_time`) VALUES (:source, :post_id, :link, :title, :body, :image, :create_time);",
                    array(
                        'source' => 'fb',
                        'post_id' => $item['id'],
                        'link' => $item['link'],
                        'title' => $item['name'] ,
                        'body' => $item['message'],
                        'image' => $item['full_picture'],
                        'create_time' => strtotime($item['created_time'])
                    )
                );
            }

            // Load next url, jika ada
            if ($feeds['paging']) {
                if ($feeds['paging']['next']) {
                    processCurlFB($feeds['paging']['next']);
                }
            }
        }
    } else {
        writeProgress('Data kosong, mungkin ini halaman terakhir');
    }

    // Tulis flag fetch fb terakhir
    $query = $sql->executePrepared("UPDATE `feed_flag` SET  `last_fb_fetch` = :last_fb_fetch WHERE 1 = 1;",
        array(
            'last_fb_fetch' => time()
        )
    );
}
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'rss_php.php');

$engine = new Engine();
$engine->setPostField([]);
header("Content-Type: text/plain");

$regenerate = $_GET['regenerate'];

// Drop tabel feed jika feed harus regenerate
if ($regenerate) {
    writeProgress("Menghapus semua data feed...");

    $engine->executePrepared("DROP TABLE IF EXISTS `feed_list`;", []);
    $engine->executePrepared("DROP TABLE IF EXISTS `feed_flag`;", []);
}

    writeProgress("Membuat tabel feed jika belum ada...");
// Buat tabel feed jika belum ada
$engine->executePrepared("
CREATE TABLE IF NOT EXISTS `feed_list` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `source` VARCHAR(45) NULL,
    `post_id` VARCHAR(100) NULL,
    `link` TEXT NULL,
    `title` TEXT NULL,
    `body` TEXT NULL,
    `image` TEXT NULL,
    `create_time` BIGINT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
);
", []);
$engine->executePrepared("
CREATE TABLE IF NOT EXISTS `feed_flag` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `last_fb_fetch` BIGINT NULL DEFAULT 0,
    `last_rss_fetch` BIGINT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
);
", []);

if ($regenerate) {
    writeProgress("Inisialisasi flag data feed...");

    // Insert inisial flag
    $engine->executePrepared(" INSERT INTO `feed_flag` (`last_fb_fetch`, `last_rss_fetch`) VALUES (0, 0);", []);
}

// ==========================================
// Ambil data feed dari Facebook
// ==========================================
// Authenticate Facebook
writeProgress("Otentikasi dengan Facebook...");

$url = "https://graph.facebook.com/oauth/access_token?client_id=450431345084697&client_secret=" . FB_SECRET . "&grant_type=client_credentials";
$options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120              // time-out on response
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$result = curl_exec($ch);
$result = json_decode($result);
curl_close($ch);

$access_token = $result->access_token;
if (!$access_token) {
    writeProgress("Otentikasi dengan facebook gagal");
    writeProgress("Proses dihentikan");
    return;
}
writeProgress("Otentikasi dengan facebook berhasil");

// Mengambil flah fetch data
$query = $engine->executePrepared("SELECT * FROM feed_flag", []);
$result = $query->fetch(PDO::FETCH_ASSOC);

$last_fb_fetch = 0;
$last_rss_fetch = 0;
if ($result) {
    $last_fb_fetch = $result['last_fb_fetch'];
    $last_rss_fetch = $result['last_rss_fetch'];
}

// Get Facebook Feed
writeProgress("Mengambil data feed dari facebook");
processCurlFB("https://graph.facebook.com/457040171049599/feed?fields=id,from,message,picture,full_picture,link,name,icon,type,status_type,created_time&access_token=$access_token&limit=100&since=$last_fb_fetch");
writeProgress('Pengambilan data feed dari facebook selesai');

// RSS
writeProgress('Mengambil data feed dari RSS...');

$RSS_PHP = new rss_php;
$RSS_PHP->load('https://ziptango.com/blog/feed');
$rss_items = $RSS_PHP->getItems();

writeProgress(count ($rss_items). " data feed dari RSS ditemukan");
foreach ($rss_items as $item) {
    if (strtotime($item['pubDate']) < $last_rss_fetch) {
        writeProgress('Data feed dari RSS sudah up-to-date');
        writeProgress('Proses dihentikan...');
        break;
    }
    $query = $engine->executePrepared("INSERT INTO `feed_list` (`source`, `post_id`, `link`, `title`, `body`, `image`, `create_time`) VALUES (:source, :post_id, :link, :title, :body, :image, :create_time);",
        array(
            'source' => 'rss',
            'post_id' => $item['post-id'],
            'link' => $item['link'],
            'title' => $item['title'] ,
            'body' => $item['description'],
            'image' => findImg($item['content:encoded']),
            'create_time' => strtotime($item['pubDate'])
        )
    );
}

// Tulis flag fetch fb terakhir
$query = $engine->executePrepared("UPDATE `feed_flag` SET  `last_rss_fetch` = :last_rss_fetch WHERE 1 = 1;",
    array(
        'last_rss_fetch' => time()
    )
);

writeProgress('Fetch data feed selesai');
