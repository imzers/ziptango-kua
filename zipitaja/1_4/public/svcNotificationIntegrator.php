<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 23-Feb-17
 * Time: 12:14 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/PushNotificationManager.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

$engine = new Engine();

$engine->setPostField(array(
    'type' => true // 1 = Read, 2 = Send
));
$type = $engine->getPOSTField('type');

if ($type == 1) {
    $engine->setPostField(array(
        'recipient_id' => true,
        'service_id' => true,
        'last_read_id' => true,
        'my_id' => true
    ));
    $recipient_id = $engine->getPOSTField('recipient_id');
    $service_id = $engine->getPOSTField('service_id');
    $last_read_id = $engine->getPOSTField('last_read_id');
    $my_id = $engine->getPOSTField('my_id');

    $gcm = new PushNotificationManager();
    //Get list registered id
    $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id",
        array(
            "user_id" => $recipient_id
        ));
    $ids = array();
    while ($row = $query->fetch(PDO::FETCH_ASSOC))
        $ids[] = array($row["gcm_regid"], $row["mobile"]);

    //Send notification
    $gcm->sendReadNotification(2, $ids, $last_read_id, $service_id, $my_id);
} else if ($type == 2) {
    $engine->setPostField(array(
        'recipient_id' => true,
        'message_id' => true,
        'service_id' => true,
        'text' => true,
        'image' => false,
        'my_id' => true
    ));
    $recipient_id = $engine->getPOSTField('recipient_id');
    $message_id = $engine->getPOSTField('message_id');
    $service_id = $engine->getPOSTField('service_id');
    $text = $engine->getPOSTField('text');
    $image = $engine->getPOSTField('image');
    $my_id = $engine->getPOSTField('my_id');

    $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id",
        array(
            "user_id" => $recipient_id
        )
    );
    $ids = array();
    while ($row = $query->fetch(PDO::FETCH_ASSOC))
        $ids[] = array($row["gcm_regid"], $row["mobile"]);
    $gcm = new PushNotificationManager();
    //Send notification
    $gcm->sendMessageNotification(1, $ids, $message_id, $service_id, $my_id, $recipient_id, time(), $text, empty($image) ? "" : APP_SERVER_ROOT . $image);

    //Send Mail
    $query = $engine->executePrepared("SELECT email FROM users WHERE id = :user_id",
        array(
            "user_id" => $recipient_id
        )
    );
    $row = $query->fetchAll(PDO::FETCH_ASSOC);
    //Send Mail Notification
    sendMail($row[0]["email"], "Chat Notification for Listing " . $service_id, $text);

    //Send Response
    $engine->setSingleLineResponse($message_id);
}

$engine->sendResponse();