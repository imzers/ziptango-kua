<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();

//Get All POST Value
$engine->setPostField(
    array(
        "param_all" => true,
        "api_key" => true
    )
);

$param_all = $engine->getPOSTField("param_all");

//Check API Key
$engine->checkAPIKeyPair();

//Core Operation
//Get Category
$cat_filter = "";
if ($param_all == 0) {
    $cat_filter = " AND s.id != 6 ";
}

$img_path = APP_SERVER_ROOT;

$query = $engine->executePrepared("SELECT id, name, CONCAT('$img_path', icons) as icons FROM service_categories s WHERE s.enabled = 1 $cat_filter", null);
$result = $query->fetchAll(PDO::FETCH_ASSOC);
$engine->setDataResponse($result);

//Send Result
$engine->sendResponse();