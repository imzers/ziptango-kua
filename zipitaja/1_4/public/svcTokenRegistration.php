<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/FunctionList.php";

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField(
    array(
        'gcm_id' => false,
        'mobile' => true,
        'api_key' => true
    ));

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

$gcm_id = $engine->getPOSTField('gcm_id');
$mobile = $engine->getPOSTField('mobile');

// Push Notification ID
    $engine->executePrepared("REPLACE INTO gcm_users (gcm_regid, user_id, mobile) VALUES (:gcm_regid, :user_id, :mobile)", array(
        "gcm_regid" => $gcm_id,
        'user_id' => !$engine->isLogged() ? 0 :$_SESSION["id"],
        "mobile" => $mobile
    ));


    $engine->setSingleLineResponse(1);
    $engine->sendResponse();
