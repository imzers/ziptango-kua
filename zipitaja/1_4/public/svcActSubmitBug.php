<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/3/2016
 * Time: 12:02 AM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_email" => true,
        "param_title" => true,
        "param_description" => true,
        "param_picture" => false,
        "api_key" => true
    )
);

$param_email = $engine->getPOSTField("param_email");
$param_title = $engine->getPOSTField("param_title");
$param_description = $engine->getPOSTField("param_description");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
$pic = handleUploadFile("param_picture", APP_IMG_BUGS_PATH, false, true);

$query = $engine->executePrepared("INSERT INTO mobile_bugs (email, title, description, picture, date) VALUES (:email, :title, :description, :picture, UNIX_TIMESTAMP())",
    array(
        "email" => $param_email,
        "title" => $param_title,
        "description" => $param_description,
        "picture" => $pic
    )
);

if ($query) {
    $engine->setSingleLineResponse(1);

    $msg = "<h2>$param_title</h2></<br />";
    $msg .= "<p>Description:<br />$param_description</p><br />";
    if (isset($_FILES["param_picture"]))
        $msg .= "Picture Attachment:<br /><img src='" . APP_SERVER_ROOT . "$pic' height='250px' width='250px' alt=Picture Attachment' />";
    sendMail("support@ziptango.com", "Ziptango Bug", $msg);

} else $engine->setErrorResponse("Gagal submit bug, silakan coba kembali");

/*
 * Send Result
 */
$engine->sendResponse();

