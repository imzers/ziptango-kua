<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 6/24/2016
 * Time: 8:34 PM
 */

require_once "../core/Engine.php";
require_once "../core/SMTPMail.php";
require_once "../core/FunctionList.php";

$engine = new Engine();

//Get POST Value
$engine->setPostField(
    array(
        "param_email" => true,
        "api_key" => true
    )
);

$param_email = $engine->getPOSTField("param_email");

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Database Opration
 */
//Check Email is Exists
$query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email",
    array(
        "email" => $engine->getPOSTField("param_email")
    ));
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] <= 0)
    die($engine->createResponse(Engine::codError, "Email tidak terdaftar!"));

//Get User Detail
$query = $engine->executePrepared("
    SELECT
        *
    FROM
        users
    WHERE
	    email = :email",
    array(
        "email" => $param_email
    )
);
$rows = $query->fetchAll(PDO::FETCH_ASSOC);

//Change Temporary Password
$new_password = generateUniqueID(5, 1);
$query = $engine->executePrepared("
    UPDATE users SET
	    password = :password
    WHERE
	    email = :email",
    array(
        "password" => md5($new_password),
        "email" => $param_email
    )
);
//Send Mail
$mailC = getMailMessage("forgetpwd");
$mailC->subject;
$mailC->message = str_replace("#fullname#", $rows[0]["firstname"], $mailC->message);
$mailC->message = str_replace("#login#", $param_email, $mailC->message);
$mailC->message = str_replace("#pwd#", $new_password, $mailC->message);

$engine->setErrorResponse("Gagal reset password, silakan ulangi lagi!");
if ($query && sendMail($param_email, $mailC->subject, $mailC->message))
    $engine->setSingleLineResponse(1);

//Send Response
$engine->sendResponse();