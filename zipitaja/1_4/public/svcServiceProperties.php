<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(array(
    // 1 = Category, 2 = Brand, 3 = Color, 4 = Type, 5 = Condition
    'prop_type' => true,
    'api_key' => true
));
$prop_type = $engine->getPOSTField('prop_type');

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

// Create SQL
$sql = '';
if ($prop_type == 1)
    $sql = 'SELECT id AS `id`, name AS `label`, icons AS `extra` FROM service_categories WHERE enabled = 1 ORDER BY name ASC';
else if ($prop_type == 2)
    $sql = 'SELECT id AS `id`, name AS `label`, \'\' AS `extra` FROM brandtable ORDER BY name ASC';
else if ($prop_type == 3)
    $sql = 'SELECT id AS `id`, name AS `label`, \'\' AS `extra` FROM colortable ORDER BY name ASC';
else if ($prop_type == 4)
    $sql = 'SELECT id AS `id`, name AS `label`, \'\' AS `extra` FROM bagtypetable ORDER BY name ASC';
else if ($prop_type == 5)
    $sql = 'SELECT id AS `id`, name AS `label`, \'\' AS `extra` FROM conditionstable ORDER BY name ASC';
else
    die($engine->createResponse(Engine::codError, '[Dev] Wrong value for field `prop_type`'));

// Core Operation
$query = $engine->executePrepared($sql, array());
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));
$engine->sendResponse();