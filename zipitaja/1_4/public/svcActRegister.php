<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/MCrypt.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_firstname" => true,
        "param_lastname" => true,
        "param_username" => true,
        "param_email" => true,
        "param_hp" => true,
        "param_password" => true,
        "param_mobile" => true,
        "api_key" => true
    )
);

$param_username = $engine->getPOSTField("param_username");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
//Check Email is Exists
$query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email",
    array(
        "email" => $engine->getPOSTField("param_email")
    ));
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] >= 1)
    die($engine->createResponse(Engine::codError, "Email sudah terdaftar!"));

if (!isUsernameValid($param_username))
    die($engine->createResponse(Engine::codError, "Username hanya boleh terdiri dari huruf (A-z), nomor (0-9) dan dash (-)"));

//Check Username is Exists
$query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.username = :username",
    array(
        "username" => $engine->getPOSTField("param_username")
    ));
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] >= 1)
    die($engine->createResponse(Engine::codError, "Username sudah terdaftar!"));

//Check Phone is Exists
$query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.telephone = :telephone",
    array(
        "telephone" => $engine->getPOSTField("param_hp")
    ));
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] >= 1)
    die($engine->createResponse(Engine::codError, "Nomor HP sudah terdaftar!"));

//Register User
$param_mobile = $engine->getPOSTField("param_mobile");
$crypt = new MCrypt();
$pass = $engine->getPOSTField("param_password");
$pass = $param_mobile == 2 ? $pass : $crypt->decrypt($pass);
$ra_arr = array('A', 'b', 'C', 'D', 'y', 'Z', 'Q', 'q', '_', '0', '9', '1', '7', '8', '5');
$activation_code = $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)];
$permanent_link_code = substr(sha1(mt_rand()), 0, 16);

$query = $engine->executePrepared("
    INSERT INTO users
        (id_admin, id_users, firstname, lastname, username, icon, email, about, telephone, password, permanent_link_code, activate_code, active, mobile, username_slug, data_reg)
    VALUES
        (0, 0, :firstname, :lastname, :username, 'no_user_icons.png', :email, '-', :telephone, :password, :permanent_link_code, :activate_code, 0, :mobile, :username_slug, UNIX_TIMESTAMP())",
    array(
        "firstname" => $engine->getPOSTField("param_firstname"),
        "lastname" => $engine->getPOSTField("param_lastname"),
        "username" => $engine->getPOSTField("param_username"),
        "email" => $engine->getPOSTField("param_email"),
        "telephone" => $engine->getPOSTField("param_hp"),
        "permanent_link_code" => $permanent_link_code,
        "password" => md5($pass),
        "activate_code" => $activation_code,
        "mobile" => $param_mobile,
        "username_slug" => createUsernameSlug($engine->getPOSTField("param_username"))
    )
);

//Create Initial Status
$engine->setErrorResponse("Registrasi gagal, Silakan coba kembali!");
if ($query) {
    //Send Mail Activation Link
    $id = $engine->getLastID("id");
    $mailC = getMailMessage("signup");
    $mailC->message = str_replace("#fullname#", $engine->getPOSTField("param_firstname") . " " . $engine->getPOSTField("param_lastname"), $mailC->message);
    $mailC->message = str_replace("#veremailcode#", APP_SERVER_ROOT . "user/profile/verifyEmail/$activation_code/$id/1", $mailC->message);
    sendMail($engine->getPOSTField("param_email"), $mailC->subject, $mailC->message);

    //Send Status
    $engine->setSingleLineResponse("1");
}

//Send Result
$engine->sendResponse();