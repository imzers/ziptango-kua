<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/12/2016
 * Time: 6:44 PM
 */


function formatRp($angka)
{
    return "Rp " . number_format($angka, 2, ',', '.');
};

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/KebabCase.php";

$engine = new Engine();

/*
 * Get POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true,       // 1 = general, 2 = service, 2 = user
        "param_share_to" => true,   // 1 = fb, ELSE = non fb
        "param_id" => true,
        "param_icon" => true,
        "api_key" => true
    )
);

$param_mode = $engine->getPOSTField("param_mode");
$param_share_to = $engine->getPOSTField("param_share_to");
$param_id = $engine->getPOSTField("param_id");
$param_icon = $engine->getPOSTField("paramm_icon");

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Database Opration
 */
$param_title = "";
$param_text = "";
$param_url = "";
$param_image = "";

$my_id = $_SESSION["id"];
$my_fullname = $_SESSION["fullname"];
$my_permalink = $_SESSION["permalink"];

if ($param_mode == 1) { // GENERAL
    if ($engine->isLogged()) { // Logged
        if ($param_share_to == 1) { //FB
            $param_title = "Want to shop authentic branded bags without worries?";
            $param_text = $my_fullname . " invites you to join Ziptango, a fashion marketplace. Get IDR 100,000 just by inviting your friends!";
            $param_url = APP_SERVER_ROOT . "invite/index/" . $my_permalink;
            $param_image = APP_SERVER_ROOT . "assets/img/Facebook.jpg";
        } else { //Other
            $param_text = "Kini belanja barang branded authentic tidak sekhawatir dan seribet dulu! Buktikan segera di Ziptango! " . APP_SERVER_ROOT . "invite/index/" . $my_permalink . " @ziptango";
        }
    } else { // Not Logged
        if ($param_share_to == 1) { //FB
            $param_title = "Belanja barang branded authentic tanpa ribet";
            $param_text = "Kini menjual dan membeli produk fashion authentic tidak sekhawatir dan seribet dulu! Buktikan segera di Ziptango, Online Marketplace Pertama di Indonesia untuk Luxury Fashion Brand! Klik ini untuk membuktikannya!";
            $param_url = APP_SERVER_ROOT;
            $param_image = APP_SERVER_ROOT . "assets/img/Facebook.jpg";
        } else { //Other
            $param_text = "Kini jual beli produk high-end authentic tidak sekhawatir dan seribet dulu! Buktikan segera di Ziptango! " . APP_SERVER_ROOT . " @ziptango";
        }
    }
} elseif ($param_mode == 2) {//SERVICE
    $case = new KebabCase();

    $service = $engine->executePrepared("SELECT s.*, c.name category_name FROM service s INNER JOIN service_categories c ON (s.category_id = c.id) WHERE s.id = :id", array('id'=> $param_id));
    $data = $service->fetch(PDO::FETCH_ASSOC);

    if ($param_share_to == 1) { //FB
        $param_title = "Check out this great deal at Ziptango.com!!!";
        $param_text = "Wah aku dapat Best Price di Ziptango! Cek deh Product Listing-nya. Kalau aku suka authentic designer fashion yang ini nih!";
        $param_text .= "\n\n";
        $param_text .= $data['name'] . "\n";
        $param_text .= formatRp($data['price']);
        $param_url = APP_SERVER_ROOT . $case->kebabify($data['category_name']) .'/' . $param_id. '/' . $case->kebabify($data['name']);
        $param_image = APP_SERVER_ROOT . "assets/img/Facebook.jpg";
    } else { //Other
        $param_text = "Wah aku dapat Best Price di Ziptango! Cek deh Product Listing-nya. Kl aku suka produk authentic high-end yg ini nih! ";
        $param_text .= "\n\n";
        $param_text .= $data['name'] . "\n";
        $param_text .= formatRp($data['price']). "\n";
        $param_text .= APP_SERVER_ROOT . $case->kebabify($data['category_name']) .'/' . $param_id. '/' . $case->kebabify($data['name']);
    }
} else {
    if ($param_share_to == 1) { //FB
        $param_title = "Berjualan authentic branded fashion sangat mudah dan aman! Yuk klik ke toko saya di ziptango.com!";
        $param_text = "Punya tas branded jarang dipakai? Dijual yuk di ziptango!";
        $param_url = APP_SERVER_ROOT . "user/" . $param_id;
        $param_image = $param_icon;
    } else { //Other
        $param_text = "Berjualan authentic branded fashion sangat mudah dan aman! Yuk klik ke toko saya di ziptango.com! " . APP_SERVER_ROOT . "user/" . $param_id;
    }
}

$content = array(
    array(
        "title" => $param_title,
        "text" => $param_text,
        "url" => $param_url,
        "image" => $param_image
    )
);
$engine->setTranslateDataResponse($content);

/*
 * Send Response
 */
$engine->sendResponse();
