<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/3/2016
 * Time: 12:02 AM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_email" => true,
        "param_title" => true,
        "param_description" => true,
        "api_key" => true
    )
);

$param_email = $engine->getPOSTField("param_email");
$param_title = $engine->getPOSTField("param_title");
$param_description = $engine->getPOSTField("param_description");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Core Operation
 */
$engine->setSingleLineResponse(1);

$msg = "<h2>$param_title</h2></<br />";
$msg .= "<p>Description:<br />$param_description</p><br />";
sendMail("contact@ziptango.com", "Contact Ziptango (dari $param_email)", $msg);

/*
 * Send Result
 */
$engine->sendResponse();

