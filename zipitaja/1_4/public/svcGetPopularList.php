<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();
//Get POST Value
$engine->setPostField([]);

$page = isset($_GET['page']) ? $_GET['page'] : 0;
$page = is_numeric($page) ? $page : 0;
if ($page > 0) {
    $page--;
}
$limit = 25;

$time_limit = isset($_GET['time_limit']) ? $_GET['time_limit'] : 0; // 0 = All, 1 = week, 2 = month
$time_limit = is_numeric($time_limit) ? $time_limit : 0;

// Ambil limit hari ke belakang
$sql_date_interval = "";
switch ($time_limit) {
    case  1:
        $time_limit_val = 8;
		// 1 Minggu
		$sql_date_interval = " AND (service_datepost BETWEEN UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -1 WEEK)) AND UNIX_TIMESTAMP(NOW()))";
    break;
    case 2:
        $time_limit_val = 31;
		// 1 Bulan
		$sql_date_interval = " AND (service_datepost BETWEEN UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -1 MONTH)) AND UNIX_TIMESTAMP(NOW()))";
    break;
    case 0:
    default:
        $time_limit_val = 0;
		$sql_date_interval = "";
    break;
}
$time = 0;
if ($time_limit_val > 0) {
    $time = time() - ($time_limit_val * 24 * 60 * 60);
}
$base_sql = sprintf("SELECT * FROM %s WHERE (1 = 1) %s AND (CAST(enddatef AS UNSIGNED) = :enddatef)", 
	'view_public_svcgetpopularlist',
	$sql_date_interval
);
$base_param = array(
    'time'		=> $time,
	'enddatef'	=> 0,
);
//-------------
$sql_string =  sprintf("%s ORDER BY views DESC LIMIT %d OFFSET %d",
	$base_sql,
	$limit,
	($limit * $page)
);

//------------------------------
$pdoObj = $engine->getConn();
$sql_query = $pdoObj->prepare($sql_string);
$sql_query->bindParam(':enddatef', $base_param['enddatef']);
$sql_query->execute();
$rows = array();
$ct = 0;
clearstatcache();
while ($row = $sql_query->fetch(PDO::FETCH_ASSOC)) {
	if (isset($row["pictures"])) {
		$pic = unserialize($row["pictures"])[0];
	} else {
		$pic = '';
	}
	if (strlen($pic) > 0) {
		if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
			generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
		}
	}
    $cid = $row['id'];
    //$stock = '/var/www/html/ziptango.com/assets/service/stock/' . $cid . '.jpg';
	$stock = '/home/www/domains/jajan-domains/domains/sku/html/assets/service/stock/' . $cid . '.jpg';
    $rows[$ct] = array(
        "id" => $cid,
        //"has_stock" => file_exists($stock) ? 1 : 0,
		"has_stock" => $row['qty'],
        "count" => $row['views'],
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "picture" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "seller" => $row["seller"],
        "price" => $row["price"],
        "can_buy" => $row["can_buy"],
        "sold" => $row["sold"],
        "conditions" => $row["conditions"],
        "accepted_payment" => $row["accepted_payment"],
		'service_datepost'	=> $row['service_datepost'],
		'view_update_time'	=> $row['view_update_time'],
    );
    $ct++;
}


$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();
