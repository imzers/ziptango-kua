<?php

require '../../core/Engine.php';


$table = 'kp_view';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id',                        'dt' => 'id' ),
    array( 'db' => 'status',                    'dt' => 'status' ),
    array( 'db' => 'total',                     'dt' => 'total' ),
    array( 'db' => 'service_id',                'dt' => 'service_id' ),
    array( 'db' => 'service_name',              'dt' => 'service_name' ),
    array( 'db' => 'buyer_id',                  'dt' => 'buyer_id' ),
    array( 'db' => 'buyer_fullname',            'dt' => 'buyer_fullname' ),
    array( 'db' => 'buyer_username',            'dt' => 'buyer_username' ),
    array( 'db' => 'buyer_email',               'dt' => 'buyer_email' ),
    array( 'db' => 'kp_order_id',               'dt' => 'kp_order_id' ),
    array( 'db' => 'kp_approval_status',        'dt' => 'kp_approval_status' ),
    array( 'db' => 'kp_approval_date',          'dt' => 'kp_approval_date' ),
    array( 'db' => 'kp_po_number',              'dt' => 'kp_po_number' ),
    array( 'db' => 'kp_po_date',                'dt' => 'kp_po_date' ),
    array( 'db' => 'kp_awb_number',             'dt' => 'kp_awb_number' ),
    array( 'db' => 'kp_recipient_name',         'dt' => 'kp_recipient_name' ),
    array( 'db' => 'kp_shipping',               'dt' => 'kp_shipping' ),
    array( 'db' => 'kp_confirmation_status',    'dt' => 'kp_confirmation_status' ),
    array( 'db' => 'kp_delivery_date',          'dt' => 'kp_delivery_date' ),
    array( 'db' => 'kp_confirmation_notes',     'dt' => 'kp_confirmation_notes' ),
    array( 'db' => 'kp_confirmation_date',      'dt' => 'kp_confirmation_date' ),
    array( 'db' => 'kp_updated_at',             'dt' => 'kp_updated_at' )
);
// SQL server connection information
$sql_details = array(
    'user' => Engine::username,
    'pass' => Engine::password,
    'db'   => Engine::database,
    'host' => Engine::hostname
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require('ssp.class.php');
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
);
