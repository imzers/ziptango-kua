<?php

require '../../core/Engine.php';

$engine = new Engine();
$engine->setPostField([
  'email' => true,
  'password' => true
]);

$query = $engine->executePrepared(
    'SELECT * FROM kp_config WHERE admin_email = :email AND admin_password = :password',
[
  'email' => $engine->getPOSTField('email'),
  'password' => $engine->getPOSTField('password')
]
);
$row = $query->fetchAll(PDO::FETCH_ASSOC);

if ($row) {
    $engine->setSingleLineResponse(0);
} else {
    $engine->setErrorResponse('Email atau kata sandi salah!');
}

$engine->sendResponse();
