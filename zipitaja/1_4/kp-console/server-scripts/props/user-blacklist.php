<?php

require_once "../../../core/Engine.php";
require_once "../../../core/FunctionList.php";
require_once "../../../core/Consts.php";

$engine = new Engine();

$search = $_GET['search'];
if (!$search) {
    $search = '';
}
$search = '%'.$search.'%';

// Create SQL
$sql = "SELECT id AS `id`, CONCAT(firstname, ' ', lastname, ' (', username, ')') AS `text`, '' AS `extra` FROM users WHERE username LIKE :search ORDER BY firstname ASC";

// Core Operation
$query = $engine->executePrepared($sql, ['search'=> $search]);
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));
$engine->sendResponse();
