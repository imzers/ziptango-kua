<?php

require_once "../../../core/Engine.php";
require_once "../../../core/FunctionList.php";
require_once "../../../core/Consts.php";

$engine = new Engine();

$search = $_GET['search'];
if (!$search) {
    $search = '';
}
$search = '%'.$search.'%';

// Create SQL
$sql = 'SELECT 0 AS `id`, \'Semua\' AS `text`, \'-\' AS `extra` UNION (SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM brandtable WHERE name LIKE :search ORDER BY name ASC)';

// Core Operation
$query = $engine->executePrepared($sql, ['search'=> $search]);
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));
$engine->sendResponse();
