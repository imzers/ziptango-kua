<?php

require_once "../../core/Engine.php";

$engine = new Engine();

$engine->setPostField([
    'brand' => true,
    'type' => true,
    'condition' => true,
    'price_min' => true,
    'price_max' => true,
    'category' => true,
    'user-blacklist' => false,
    'payment_enabled' => true
]);

$brand = $engine->getPOSTField('brand');
if (strpos(',' . $brand . ',', ',0,')) {
    $brand = 0;
}
$type = $engine->getPOSTField('type');
if (strpos(',' . $type . ',', ',0,')) {
    $type = 0;
}
$condition = $engine->getPOSTField('condition');
if (strpos(',' . $condition . ',', ',-1,')) {
    $condition = -1;
}
$price_min = str_replace(',', '', $engine->getPOSTField('price_min'));
$price_max = str_replace(',', '', $engine->getPOSTField('price_max'));
$category = $engine->getPOSTField('category');
if (strpos(',' . $category . ',', ',0,')) {
    $category = 0;
}
$payment_enabled = $engine->getPOSTField('payment_enabled');
$blacklist = $engine->getPOSTField('user-blacklist');

// die($engine->createResponse(Engine::codError, $price_min));

$engine->executePrepared("
    UPDATE kp_config SET
    brands = :brands,
    types = :types,
    conditions = :conditions,
    min_price = :min_price,
    max_price = :max_price,
    categories = :categories,
    payment_enabled = :payment_enabled,
    seller_blacklist = :seller_blacklist
", [
    'brands' => $brand,
    'types' => $type,
    'conditions' => $condition,
    'min_price' => $price_min,
    'max_price' => $price_max,
    'categories' => $category,
    'payment_enabled' => $payment_enabled,
    'seller_blacklist' => $blacklist ? $blacklist : 0
]);

$engine->setSingleLineResponse(0);
$engine->sendResponse();
