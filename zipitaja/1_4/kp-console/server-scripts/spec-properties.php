<?php

require_once "../../core/Engine.php";
require_once "../../core/FunctionList.php";
require_once "../../core/Consts.php";

$engine = new Engine();
 
$type = $_GET['type'];
if (!$type) {
    $type = 'brand;';
}

// Create SQL
$sql = '';
if ($type == 'category') {
    $sql = 'SELECT id AS `id`, name AS `text`, icons AS `extra` FROM service_categories WHERE enabled = 1 ORDER BY name ASC';
} elseif ($type == 'brand') {
    $sql = 'SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM brandtable ORDER BY name ASC';
} elseif ($type == 'color') {
    $sql = 'SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM colortable ORDER BY name ASC';
} elseif ($type == 'type') {
    $sql = 'SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM bagtypetable ORDER BY name ASC';
} elseif ($type == 'condition') {
    $sql = 'SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM conditionstable ORDER BY name ASC';
} else {
    die($engine->createResponse(Engine::codError, '[Dev] Wrong value for field `type`'));
}

// Core Operation
$query = $engine->executePrepared($sql, array());
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));
$engine->sendResponse();
