<?php

require '../../core/Engine.php';
require '../../core/Consts.php';

$engine = new Engine();

$engine->setPostField([
    'order_id' => true,
    'resi' => true,
    'name' => true,
    'notes' => true,
    'date' => true
]);

// Ambil url eform
$params = [
    SupplierKey => KP_SUPPLIER_KEY,
    OrderID => $engine->getPOSTField('order_id'),
    AWBNumber => $engine->getPOSTField('resi'),
    ReceiverName => $engine->getPOSTField('notes'),
    DeliveryDate => gmdate("Y-m-d H:i:s", $engine->getPOSTField('date')/1000)
];

$params_string = '';
foreach ($params as $key => $value) {
    if ($params_string != '') {
        $params_string .= '&';
    }
    $params_string .= "$key=$value";
}

$url = KP_API_URL . "DeliveryOrder";
$options = array(
    CURLOPT_POST => 1,
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120,             // time-out on response
    CURLOPT_POSTFIELDS => $params_string
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$content = curl_exec($ch);
curl_close($ch);

// parse
$xml_snippet = @simplexml_load_string($content);
$json_convert = json_encode($xml_snippet);
$json = json_decode($json_convert, true);

// kirim hasil
if ($json['ResultStatus'] == 0) {
    $engine->executePrepared('
        UPDATE kp_payment_data SET 
        awb_number = :awb_number,
         delivery_date = :delivery_date,
         recipient_name = :recipient_name,
         shipping_detail = :shipping_detail,
          confirmation_date =  Unix_timestamp(),
          updated_at = Unix_timestamp() WHERE order_id = :id', [
            'awb_number' => $engine->getPOSTField('resi'),
            'delivery_date' =>  $engine->getPOSTField('date')/1000,
            'recipient_name' =>$engine->getPOSTField('name') ,
            'shipping_detail' =>$engine->getPOSTField('notes') ,
            'id' => $engine->getPOSTField('order_id')
          ]);
    $engine->setSingleLineResponse(0);
} else {
    $engine->setErrorResponse($json['Description']);
}
$engine->sendResponse();
