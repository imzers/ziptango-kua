<?php

require_once "../../core/Engine.php";

$engine = new Engine();

// Create SQL
$sql = 'SELECT * FROM kp_config';

// Core Operation
$query = $engine->executePrepared($sql, array());

// proses data
$rows = array();
$ct = 0;
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $rows[$ct] = $row;
    
    // brands
    if ($row['brands'] == 0 || $row['brands']) {
        $b = $engine->executePrepared('SELECT * FROM (SELECT 0 AS `id`, \'Semua\' AS `text`, \'-\' AS `extra` UNION (SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM brandtable ORDER BY name ASC)) t WHERE t.id IN ('.$row['brands'].')');
        $tmp = '';
        while ($b_row = $b->fetch(PDO::FETCH_ASSOC)) {
            if ($tmp) {
                $tmp .= ',';
            }
            $tmp .= $b_row['id'] . ';' . $b_row['text'];
        }
        $rows[$ct]['brands'] = $tmp;
    }

    // types
    if ($row['types'] == 0 || $row['types']) {
        $b = $engine->executePrepared('SELECT * FROM (SELECT 0 AS `id`, \'Semua\' AS `text`, \'-\' AS `extra` UNION (SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM bagtypetable ORDER BY name ASC)) t WHERE t.id IN ('.$row['types'].')');
        $tmp = '';
        while ($b_row = $b->fetch(PDO::FETCH_ASSOC)) {
            if ($tmp) {
                $tmp .= ',';
            }
            $tmp .= $b_row['id'] . ';' . $b_row['text'];
        }
        $rows[$ct]['types'] = $tmp;
    }

    // conditions
    if ($row['conditions'] == 0 || $row['conditions']) {
        $b = $engine->executePrepared('SELECT * FROM (SELECT -1 AS `id`, \'Semua\' AS `text`, \'-\' AS `extra` UNION (SELECT id AS `id`, name AS `text`, \'\' AS `extra` FROM conditionstable ORDER BY name ASC)) t WHERE t.id IN ('.$row['conditions'].')');
        $tmp = '';
        while ($b_row = $b->fetch(PDO::FETCH_ASSOC)) {
            if ($tmp) {
                $tmp .= ',';
            }
            $tmp .= $b_row['id'] . ';' . $b_row['text'];
        }
        $rows[$ct]['conditions'] = $tmp;
    }

    // categories
    if ($row['categories'] == 0 || $row['categories']) {
        $b = $engine->executePrepared('SELECT * FROM (SELECT 0 AS `id`, \'Semua\' AS `text`, \'-\' AS `extra` UNION (SELECT id AS `id`, name AS `text`, icons AS `extra` FROM service_categories WHERE enabled = 1 ORDER BY name ASC)) t WHERE t.id IN ('.$row['categories'].')');
        $tmp = '';
        while ($b_row = $b->fetch(PDO::FETCH_ASSOC)) {
            if ($tmp) {
                $tmp .= ',';
            }
            $tmp .= $b_row['id'] . ';' . $b_row['text'];
        }
        $rows[$ct]['categories'] = $tmp;
    }

    // user blacklist
    if ($row['seller_blacklist']) {
        $b = $engine->executePrepared("SELECT * FROM (SELECT id AS `id`, CONCAT(firstname, ' ', lastname, ' (', username, ')') AS `text`, '' AS `extra` FROM users ORDER BY firstname ASC) t WHERE t.id IN (".$row['seller_blacklist'].")");
        $tmp = '';
        while ($b_row = $b->fetch(PDO::FETCH_ASSOC)) {
            if ($tmp) {
                $tmp .= ',';
            }
            $tmp .= $b_row['id'] . ';' . $b_row['text'];
        }
        $rows[$ct]['user-blacklist'] = $tmp;
    }
    

    $ct++;
}
$engine->setTranslateDataResponse($rows);

$engine->sendResponse();
