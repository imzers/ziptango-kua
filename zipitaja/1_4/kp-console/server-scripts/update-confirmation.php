<?php

require '../../core/Engine.php';
require '../../core/Consts.php';

$engine = new Engine();

$engine->setPostField([
    'order_id' => true,
    'status' => true,
    'notes' => true,
    'date' => true
]);

// Ambil url eform
$params = [
    SupplierKey => KP_SUPPLIER_KEY,
    OrderID => $engine->getPOSTField('order_id'),
    OrderStatus => $engine->getPOSTField('status'),
    DeliveryDate => gmdate("Y-m-d H:i:s", $engine->getPOSTField('date')/1000),
    OrderNotes => $engine->getPOSTField('notes'),
    ConfirmationDate => gmdate("Y-m-d H:i:s", time())
];

$params_string = '';
foreach ($params as $key => $value) {
    if ($params_string != '') {
        $params_string .= '&';
    }
    $params_string .= "$key=$value";
}

$url = KP_API_URL . "ConfirmationOrder";
$options = array(
    CURLOPT_POST => 1,
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120,             // time-out on response
    CURLOPT_POSTFIELDS => $params_string
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$content = curl_exec($ch);
curl_close($ch);

// parse
$xml_snippet = @simplexml_load_string($content);
$json_convert = json_encode($xml_snippet);
$json = json_decode($json_convert, true);

// kirim hasil
if ($json['ResultStatus'] == 0) {
    $engine->executePrepared('
        UPDATE kp_payment_data SET 
        confirmation_status = :confirmation_status,
         delivery_date = :delivery_date,
          confirmation_notes = :confirmation_notes,
          confirmation_date =  Unix_timestamp(),
          updated_at = Unix_timestamp() WHERE order_id = :id', [
            'confirmation_status' => $engine->getPOSTField('status'),
            'delivery_date' =>  $engine->getPOSTField('date')/1000,
            'confirmation_notes' =>$engine->getPOSTField('notes') ,
            'id' => $engine->getPOSTField('order_id')
          ]);
    $engine->setSingleLineResponse(0);
} else {
    $engine->setErrorResponse($json['Description']);
}
$engine->sendResponse();
