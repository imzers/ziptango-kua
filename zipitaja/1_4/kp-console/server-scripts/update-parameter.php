<?php

require_once "../../core/Engine.php";

$engine = new Engine();

$engine->setPostField([
    'admin_fee' => true,
    'insurance' => true,
    'provicy_fee' => true,
    'shipping_fee' => true,
    'other_fee' => true,
]);

$admin_fee = str_replace(',', '', $engine->getPOSTField('admin_fee'));
$insurance = str_replace(',', '', $engine->getPOSTField('insurance'));
$provicy_fee = str_replace(',', '', $engine->getPOSTField('provicy_fee'));
$shipping_fee = str_replace(',', '', $engine->getPOSTField('shipping_fee'));
$other_fee =str_replace(',', '', $engine->getPOSTField('other_fee'));

$engine->executePrepared("
    UPDATE kp_config SET
    admin_fee = :admin_fee,
    insurance = :insurance,
    provicy_fee = :provicy_fee,
    shipping_fee = :shipping_fee,
    other_fee = :other_fee
", [
    'admin_fee' => $admin_fee,
    'insurance' => $insurance,
    'provicy_fee' => $provicy_fee,
    'shipping_fee' => $shipping_fee,
    'other_fee' => $other_fee
]);

$engine->setSingleLineResponse(0);
$engine->sendResponse();
