<?php

require_once "../../core/Engine.php";

$engine = new Engine();

// Create SQL
$sql = 'SELECT * FROM kp_config';

// Core Operation
$query = $engine->executePrepared($sql, array());
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));
$engine->sendResponse();
