<?php

require '../../../core/Engine.php';

$engine = new Engine();

$query = $engine->executePrepared(
    'SELECT * FROM kp_config',
[
  'email' => $engine->getPOSTField('email'),
  'password' => $engine->getPOSTField('password')
]
);
$row = $query->fetchAll(PDO::FETCH_ASSOC);

$engine->setDataResponse($row);
$engine->sendResponse();
