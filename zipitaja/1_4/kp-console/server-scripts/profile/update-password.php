<?php

require '../../../core/Engine.php';

$engine = new Engine();

$engine->setPostField([
    'old_password' => true,
    'new_password' => true
]);

$query = $engine->executePrepared(
    'SELECT * FROM kp_config WHERE admin_password = :password',
[
  'password' => $engine->getPOSTField('old_password')
]
);
$row = $query->fetchAll(PDO::FETCH_ASSOC);

if (!$row) {
    die($engine->createResponse(Engine::codError, 'Password lama salah!'));
}

$query = $engine->executePrepared(
    'UPDATE kp_config SET admin_password = :password',
[
  'password' => $engine->getPOSTField('new_password')
]
);

$engine->setSingleLineResponse(0);
$engine->sendResponse();
