<?php

require '../../../core/Engine.php';

$engine = new Engine();

$engine->setPostField([
    'name' => true,
    'email' => true
]);

$query = $engine->executePrepared(
    'UPDATE kp_config SET admin_name = :name, admin_email = :email',
[
    'name' => $engine->getPOSTField('name'),
    'email' => $engine->getPOSTField('email')
]
);

$engine->setSingleLineResponse(0);
$engine->sendResponse();
