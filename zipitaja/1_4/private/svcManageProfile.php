<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/1/2016
 * Time: 3:51 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true, // 1 = Pic, 2 = About, 3 = Name, 4 = Email, 5 = Address, 6 = Password
        "param_mobile" => false,
        "api_key" => true,
    ));

$mode = $engine->getPOSTField("param_mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

if ($mode == 1) {
    $engine->setPostField(array(
            "param_picture" => false
        )
    );

    //=============== Image
    $new_icon = handleUploadFile("param_picture", APP_IMG_USER_PATH, true, false);

    $query = $query = $engine->executePrepared("
    UPDATE users SET
        icon = :icon
    WHERE
        id = :id",
        array(
            "icon" => $new_icon,
            "id" => $my_id
        )
    );

    if (!$query) die($engine->createResponse(Engine::codError, "Foto gagal disimpan"));

} else if ($mode == 2) {
    $engine->setPostField(array(
            "param_about" => true
        )
    );
    $param_about = $engine->getPOSTField("param_about");

    //=============== About
    $query = $query = $engine->executePrepared("
    UPDATE users SET
        about = :about
    WHERE
        id = :id",
        array(
            "about" => $param_about,
            "id" => $my_id
        )
    );

    if (!$query) die($engine->createResponse(Engine::codError, "About gagal disimpan"));

} else if ($mode == 3) {
    $engine->setPostField(array(
            "param_firstname" => true,
            "param_lastname" => true,
            "param_username" => true,
            "param_hp" => true
        )
    );
    $param_firstname = $engine->getPOSTField("param_firstname");
    $param_lastname = $engine->getPOSTField("param_lastname");
    $param_username = $engine->getPOSTField("param_username");
    $param_hp = $engine->getPOSTField("param_hp");

    //=============== Name
    //Check Username is Exists
    $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.username = :username AND id != :id",
        array(
            "username" => $param_username,
            "id" => $my_id
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] >= 1) die($engine->createResponse(Engine::codError, "Username sudah terdaftar!"));

    //Check Username is Exists
    $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.telephone = :telephone AND id != :id",
        array(
            "telephone" => $param_hp,
            "id" => $my_id
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] >= 1) die($engine->createResponse(Engine::codError, "Nomor HP sudah terdaftar!"));

    //Update
    $query = $query = $engine->executePrepared("
    UPDATE users SET
        firstname = :firstname,
        lastname = :lastname,
        username = :username,
        telephone = :telephone,
        username_slug = :username_slug
    WHERE
        id = :id",
        array(
            "firstname" => $param_firstname,
            "lastname" => $param_lastname,
            "username" => $param_username,
            "telephone" => $param_hp,
            "username_slug" => createUsernameSlug($param_username),
            "id" => $my_id
        )
    );

    if (!$query) die($engine->createResponse(Engine::codError, "Data gagal disimpan"));

} else if ($mode == 4) {
    $engine->setPostField(array(
            "param_email" => true
        )
    );
    $param_email = $engine->getPOSTField("param_email");

    //Check email is exists
    $query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email AND id != :id",
        array(
            "email" => $param_email,
            "id" => $my_id
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] >= 1) die($engine->createResponse(Engine::codError, "Email sudah terdaftar!"));

    //=============== Email
    $ra_arr = array('A', 'b', 'C', 'D', 'y', 'Z', 'Q', 'q', '_', '0', '9', '1', '7', '8', '5');
    $activation_code = $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)] . $ra_arr[rand(0, 14)];

    $query = $query = $engine->executePrepared("
    UPDATE users SET
        email = :email,
        activate_code = :activate_code,
        active = 0
    WHERE
        id = :id",
        array(
            "email" => $param_email,
            "activate_code" => $activation_code,
            "id" => $my_id
        )
    );
    if (!$query) die($engine->createResponse(Engine::codError, "Email gagal disimpan"));

    //Send verification email
    $query = $engine->executePrepared("SELECT * FROM users WHERE id = :id",
        array(
            "id" => $my_id
        )
    );
    $row = $query->fetch(PDO::FETCH_ASSOC);

    $fullname = $row["firstname"] . " " . $row["lastname"];

    $mailC = getMailMessage("signup");
    $mailC->message = str_replace("#fullname#", $fullname, $mailC->message);
    $mailC->message = str_replace("#veremailcode#", APP_SERVER_ROOT . "user/profile/verifyEmail/$activation_code/$my_id/1", $mailC->message);
    sendMail($param_email, $mailC->subject, $mailC->message);

    $_SESSION["email"] = $param_email;

} else if ($mode == 5) {
    $engine->setPostField(array(
            "param_lat" => true,
            "param_long" => true,
            "param_address" => true,
            "param_city" => true,
            "param_state" => true,
            "param_country" => true,
            "param_unit_apt" => true,
            "param_address_notes" => true
        )
    );
    $param_lat = $engine->getPOSTField("param_lat");
    $param_long = $engine->getPOSTField("param_long");
    $param_address = $engine->getPOSTField("param_address");
    $param_city = $engine->getPOSTField("param_city");
    $param_state = $engine->getPOSTField("param_state");
    $param_country = $engine->getPOSTField("param_country");
    $param_unit_apt = $engine->getPOSTField("param_unit_apt");
    $param_address_notes = $engine->getPOSTField("param_address_notes");

    //=============== Address
    $query = $query = $engine->executePrepared("
    UPDATE users SET
        la = :la,
        lo = :lo,
        address = :address,
        city = :city,
        state = :state,
        unit_apt = :unit_apt,
        address_notes = :address_notes
    WHERE
        id = :id",
        array(
            "la" => $param_lat,
            "lo" => $param_long,
            "address" => $param_address,
            "city" => $param_city,
            "state" => $param_state,
            "unit_apt" => $param_unit_apt,
            "address_notes" => $param_address_notes,
            "id" => $my_id
        )
    );

    if (!$query) die($engine->createResponse(Engine::codError, "Alamat gagal disimpan"));

} else if ($mode == 6) {
    $engine->setPostField(array(
            "param_current" => true,
            "param_new" => true
        )
    );

    $crypt = new MCrypt();
    $param_current = $engine->getPOSTField("param_current");
    $param_new = $engine->getPOSTField("param_new");
    $param_mobile = $engine->getPOSTField("param_mobile");

    $param_current = $param_mobile == 2 ? $param_current : $crypt->decrypt($param_current);
    $param_new = $param_mobile == 2 ? $param_new : $crypt->decrypt($param_new);

    //=============== Password
    //Check is password match
    $query = $engine->executePrepared("SELECT count(u.id) AS result FROM users u WHERE u.id = :id AND u.password = :password",
        array(
            "id" => $my_id,
            "password" => md5($param_current)
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] < 1) die($engine->createResponse(Engine::codError, "Password lama salah!"));

    //Update
    $query = $query = $engine->executePrepared("
    UPDATE users SET
        password = :password
    WHERE
        id = :id",
        array(
            "password" => md5($param_new),
            "id" => $my_id
        )
    );

    if (!$query) die($engine->createResponse(Engine::codError, "Password gagal disimpan"));

}

$engine->setSingleLineResponse(1);

/*
 * Send Result
 */
$engine->sendResponse();

