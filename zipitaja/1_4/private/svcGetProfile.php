<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/2/2016
 * Time: 4:15 AM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "api_key" => true
    )
);

$mode = $engine->getPOSTField("param_mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

/*
 * Core Operation
 */
$url = APP_SERVER_ROOT . APP_IMG_USER_PATH;
$query = $engine->executePrepared("SELECT u.id, concat('$url', u.icon) as 'icon', u.about, u.firstname, u.lastname, u.username, u.telephone, u.email, u.la, u.lo, u.address, u.city, u.state, u.country, u.unit_apt, u.address_notes, u.password, IF(u.password = 'FB', 1, 0) as fb, u.active, t.credit as voucher FROM users u LEFT JOIN (SELECT
    IFNULL(SUM(value),0) AS credit,
    assigned_to as id
FROM
    zip_credits
WHERE
    deleted = 0 AND
    assigned_to = :assigned_to) t ON (t.id = u.id) WHERE u.id = :id",
    array(
        "assigned_to" => $my_id,
        "id" => $my_id
    )
);
$engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

/*
 * Send Response
 */
$engine->sendResponse();