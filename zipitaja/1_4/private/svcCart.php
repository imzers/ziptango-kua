<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');


$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_services"			=> true,
        "param_coupon"				=> false,
		'param_coupon_condition'	=> false,
        "api_key"					=> true
    )
);
$param_service = $engine->getPOSTField("param_services");
$param_coupon = $engine->getPOSTField("param_coupon");
if ($engine->getPOSTField("param_coupon_condition") != NULL) {
	$param_coupon_condition = $engine->getPOSTField("param_coupon_condition");
} else {
	$param_coupon_condition = 5;
}
$param_coupon_condition = (int)$param_coupon_condition;
/*
 * Check API Key
 */
$engine->checkAPIKeyPair();


// FORCE LOGIN AS 26

if (isset($_GET['pairingimzers'])) {
	$engine->setInitSession('268', 'sebanyam@gmail.com', 'Sebanyam', 'ss');
}

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

/*
 * Core Operation
 */

//Get Detail Selected Service
$services = explode(";;;", $param_service);
$s = "(";
for ($i = 0; $i < count($services) - 1; $i++) {
    if ($i > 0) {
        $s = $s . ", ";
    }
    $s = $s . $services[$i];
}
$s = $s . ")";
$sql_string = <<<SLQSTRING
SELECT
        b.id,
        b.category_id,
        c.name category_name,
        b.brand_id,
        IFNULL(bt.name, 'Not Assigned')  as    brand_name,
        b.conditions,
        IFNULL(cot.name, 'Not Assigned') as condition_name,
        b.name,
        b.id as listing,
        b.startdatef,
        b.pictures,
        b.street_1,
        b.city_1,
        b.lat_1,
        b.lng_1,
        s.id as id_seller,
        s.icon,
        s.username as seller,
        s.id as seller_id,
        b.price,
        b.details
    FROM
        service b
        INNER JOIN users s ON (b.id_user = s.id)
        INNER JOIN service_categories c ON (b.category_id = c.id)
        LEFT JOIN brandtable bt ON (b.brand_id = bt.id)
        LEFT JOIN conditionstable cot ON (b.conditions = cot.id)
    WHERE
        b.id_user = s.id 
		AND b.category_id = c.id 
		AND b.id IN {$s}
SLQSTRING;
switch ((int)$param_coupon_condition) {
	case 0:
		$sql_string .= sprintf(" AND b.conditions = '%d'", $param_coupon_condition);
	break;
	case 1:
	case 2:
	case 3:
	case 4:
		$sql_string .= sprintf(" AND b.conditions IN('%d', '%d', '%d', '%d')", 1, 2, 3, 4);
	break;
	case 5:
	default:
		$sql_string .= "";
	break;
}


$detService = $engine->executePrepared($sql_string, array());
$rows_service = array();
$ct = 0;
while ($row = $detService->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["pictures"])[0];
    if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
        generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
    }

    $rows_service[$ct] = array(
        "id" => $row["id"],
        "category_id" => $row["category_id"],
        "category_name" => $row["category_name"],
        "brand_id" => $row["brand_id"],
        "brand_name" => $row["brand_name"],
        "conditions" => $row["conditions"],
        "condition_name" => $row["condition_name"],
        "name" => $row["name"],
        "listing" => $row["listing"],
        "startdatef" => $row["startdatef"],
        "picture0" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "street_1" => $row["street_1"],
        "city_1" => $row["city_1"],
        "lat_1" => $row["lat_1"],
        "lng_1" => $row["lng_1"],
        "id_seller" => $row["id_seller"],
        "icon" => $row["icon"],
        "seller_id" => $row["seller_id"],
        "seller" => $row["seller"],
        "price" => $row["price"],
        "details" => $row["details"]
    );
    $ct++;
}

//Zip Voucher
$detVoucher = $engine->executePrepared(
    "
SELECT
    SUM(value) AS credit
FROM
    zip_credits
WHERE
    deleted = 0 AND
    assigned_to = :assigned_to",
    array(
        "assigned_to" => $_SESSION["id"]
    )
);

//Check Barang Tersedia
$detTersedia = $engine->executePrepared(
    "
SELECT
    *
FROM
    payment_list
WHERE
    id_service IN $s
",
    array()
);

//Cek Buyer Commision
$detCom = $engine->executePrepared(
    "
SELECT
    *
FROM
    common_admin
WHERE
    name = 'buyer_com'
",
    array()
);

//Cek Coupon
$sql_coupon = <<<SQLSTRING
SELECT
    (IF(UNIX_TIMESTAMP() BETWEEN start AND end, '', 'Kupon yang anda masukkan hanya bisa digunakan dalam waktu tertentu')) AS msg,
    type,
    id_user,
    value,
    min,
    max,
    `limit`,
    firsttimebuyer,
    mobile,
    (SELECT COUNT(id) FROM payment_list WHERE status IN (0, 3, 4, 5) AND id_buyer = :me) AS is_firstime_buy
FROM
    coupon
WHERE
    active = 0
	AND name = :name
SQLSTRING;
switch ((int)$param_coupon_condition) {
	case 0:
		$sql_coupon .= sprintf(" AND coupon_condition = '%d'", $param_coupon_condition);
	break;
	case 1:
	case 2:
	case 3:
	case 4:
		$sql_coupon .= sprintf(" AND coupon_condition IN('%d', '%d', '%d', '%d')", 1, 2, 3, 4);
	break;
	case 5:
	default:
		$sql_coupon .= "";
	break;
}
$coupon = $engine->executePrepared($sql_coupon,
    array(
        "name" => $param_coupon,
        "me" => $_SESSION["id"]
    )
);
try {
	$coupon_data = $coupon->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $ex) {
	throw $ex;
	$coupon_data = false;
}

//Get Address
$address = $engine->executePrepared(
    "
SELECT
    telephone,
    la,
    lo,
    address,
    city,
    state,
    country,
    unit_apt,
    address_notes
FROM
    users
WHERE
    id = :id 
",
    array(
        "id" => $my_id
    )
);

$kpq = $engine->executePrepared('SELECT * FROM kp_config', []);
$kp_config = $kpq->fetch(PDO::FETCH_ASSOC);

echo $engine->createResponse(
    Engine::codSingleLine,
    array(
        "service_details" => $rows_service,
        "user_voucher" => $detVoucher->fetchAll(PDO::FETCH_ASSOC),
        "service_available" => $detTersedia->fetchAll(PDO::FETCH_ASSOC),
        "buyer_com" => $detCom->fetchAll(PDO::FETCH_ASSOC),
        "zip_coupon" => $coupon_data,
        "address" => $address->fetchAll(PDO::FETCH_ASSOC),
        "accepted-payments" => 'bt,cc' . ($kp_config['payment_enabled'] ? ',kp' : '')
    )
);
