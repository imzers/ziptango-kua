<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/2/2016
 * Time: 1:05 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true, // 1 = Get, 2 = Cancel, 3 = Accept
        "param_as" => true, // 1 = Seller, 2 = Buyer
        "api_key" => true
    )
);

$mode = $engine->getPOSTField("param_mode");
$param_as = $engine->getPOSTField("param_as");
/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

if ($mode == 1) {
    $sql = "";
    if ($param_as == 1) {
        $sql = "
            -- AS Seller
            SELECT 
            	p.id, 
            	s.id AS id_service, 
            	s.name, 
            	s.city_1 AS location, 
            	s.startdatef AS end_date, 
            	s.enddatef AS delivery_date, 
            	u.username, 
            	u.id AS id_user, 
            	p.status
            FROM 
            	service s, payment_list p, users u
            WHERE 
	            s.id = p.id_service AND 
            	u.id = p.id_buyer AND 
            	p.id_buyer != :my_id AND
            	s.id_user = :my_id
            ORDER BY p.id DESC
        ";
	} else {
        $sql = "
            -- AS Buyer
            SELECT 
	            p.id, 
	            s.id AS id_service, 
            	s.name, 
            	s.city_1 AS location, 
            	s.startdatef AS end_date, 
            	s.enddatef AS delivery_date, 
            	u.username, 
            	u.id AS id_user, 
            	p.status
            FROM 
	            service s, payment_list p, users u
            WHERE 
	            s.id = p.id_service AND 
            	u.id = s.id_user AND 
            	p.id_buyer = :my_id
            ORDER BY p.id DESC
        ";
	}
    $query = $engine->executePrepared($sql,
        array(
            "my_id" => $my_id
        )
    );

    $engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

} else if ($mode == 2) {
    $engine->setPostField(
        array(
            "param_order" => true,
            "param_service" => true
        )
    );

    $param_order = $engine->getPOSTField("param_order");
    $param_service = $engine->getPOSTField("param_service");

    $status = "2";
    if ($param_as == 1) {
        //$status = 2;
        $status = 6;
    } else {
        $status = 6;
	}

    $query = $engine->executePrepared("
            UPDATE payment_list SET
            	status = :status
            WHERE 
	            id = :id",
        array(
            "status" => $status,
            "id" => $param_order
        )
    );

    $engine->executePrepared("
            UPDATE site_commission SET
            	status = 6
            WHERE 
	            id_order = :id",
        array(
            "id" => $param_order
        )
    );

    if ($query) {
        $engine->setSingleLineResponse(1);

        //Send mail to me
        $mailC = getMailMessage("orderdeniad");
        $mailC->message = str_replace("#fullname#", $_SESSION["fullname"], $mailC->message); //Fullname
        $mailC->message = str_replace("#idlisting#", $param_service, $mailC->message); //ID Listing
        sendMail($_SESSION["email"], $mailC->subject, $mailC->message);

        //Send mail to other
        $query = $engine->executePrepared("SELECT u.firstname, u.lastname, u.email FROM payment_list p, users u WHERE p.id_buyer = u.id AND p.id = :id",
            array(
                "id" => $param_order
            )
        );
        $res = $query->fetchAll(PDO::FETCH_ASSOC);

        $full_name = $res[0]["firstname"] . " " . $res[0]["lastname"];
        $email = $res[0]["email"];

        $mailC = getMailMessage("orderdeniad");
        $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
        $mailC->message = str_replace("#idlisting#", $param_service, $mailC->message); //ID Listing
        sendMail($email, $mailC->subject, $mailC->message);
    } else {
        $engine->setErrorResponse("Gagal membatalkan order");
	}

} else if ($mode == 3) {
    $engine->setPostField(
        array(
            "param_order" => true,
            "param_service" => true
        )
    );

    $param_order = $engine->getPOSTField("param_order");
    $param_service = $engine->getPOSTField("param_service");

    $status = "2";
    if ($param_as == 1) {
        $status = 3;
		$query = $engine->executePrepared("
				UPDATE payment_list SET
					status = :status
				WHERE 
					id = :id",
			array(
				"status" => $status,
				"id" => $param_order
			)
		);
		$engine->executePrepared("
				UPDATE site_commission SET
					status = 3
				WHERE 
					id_order = :id",
			array(
				"id" => $param_order
			)
		);
	
		if ($query) {
			$engine->setSingleLineResponse(1);

			//Send mail to me
			$mailC = getMailMessage("orderdeniad");
			$mailC->message = str_replace("#fullname#", $_SESSION["fullname"], $mailC->message); //Fullname
			$mailC->message = str_replace("#idlisting#", $param_service, $mailC->message); //ID Listing
			sendMail($_SESSION["email"], $mailC->subject, $mailC->message);

			//Send mail to other
			$query = $engine->executePrepared("SELECT u.firstname, u.lastname, u.email FROM payment_list p, users u WHERE p.id_buyer = u.id AND p.id = :id",
				array(
					"id" => $param_order
				)
			);
			$res = $query->fetchAll(PDO::FETCH_ASSOC);

			$full_name = $res[0]["firstname"] . " " . $res[0]["lastname"];
			$email = $res[0]["email"];

			$mailC = getMailMessage("orderdeniad");
			$mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
			$mailC->message = str_replace("#idlisting#", $param_service, $mailC->message); //ID Listing
			sendMail($email, $mailC->subject, $mailC->message);
		} else {
			$engine->setErrorResponse("Gagal membatalkan order");
		}
	
	} else {
		$engine->setErrorResponse("Accept order only available for seller");
	}
} else {
	$engine->setErrorResponse("Undefined param_mode");
}

/*
 * Send Response
 */
$engine->sendResponse();