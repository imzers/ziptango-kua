<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/FunctionList.php";

$engine = new Engine();
/*
 * Get POST Value
 */
$engine->setPostField(array(
    "api_key" => true
));

/*
 * Check API key
 */
$engine->checkAPIKeyPair();

/*
 * Check Session
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
//$old_sql = "SELECT l.id,
//       id_service,
//       s.name     AS produk,
//       s.pictures AS image,
//       me,
//       recipient,
//       u.username,
//       unread,
//       time_send,
//       text,
//       img
//FROM   (SELECT id,
//               id_service,
//               me,
//               recipient,
//               id_first_message,
//               isread,
//               Sum(unread) AS unread,
//               time_send,
//               text,
//               img,
//               mobile
//        FROM   (SELECT *
//                FROM   (SELECT id,
//                               id_service,
//                               id_user_send                           AS me,
//                               id_user_get                            AS
//                               recipient,
//                               id_first_message,
//                               isread,
//                               Sum(IF(isread = 0
//                                      AND id_user_send != :my_id, 1, 0)) AS unread,
//                               time_send,
//                               text,
//                               img,
//                               mobile
//                        FROM   (SELECT *
//                                FROM   message
//                                ORDER  BY id DESC) as_sender
//                        WHERE  id_user_send = :my_id
//                        GROUP  BY id_service
//                        UNION ALL
//                        SELECT id,
//                               id_service,
//                               id_user_get                            AS me,
//                               id_user_send                           AS
//                               recipient,
//                               id_first_message,
//                               isread,
//                               Sum(IF(isread = 0
//                                      AND id_user_send != :my_id, 1, 0)) AS unread,
//                               time_send,
//                               text,
//                               img,
//                               mobile
//                        FROM   (SELECT *
//                                FROM   message
//                                ORDER  BY id DESC) as_recipient
//                        WHERE  id_user_get = :my_id
//                        GROUP  BY id_service)a
//                ORDER  BY id DESC) main
//        GROUP  BY recipient,
//                  id_service
//        ORDER  BY time_send DESC) l,
//       users u,
//       service s
//WHERE  u.id = l.recipient
//       AND s.id = l.id_service;";

$my_id = $_SESSION["id"];

$where_pattern = ' AND ';
if ($my_id == 1)
    $where_pattern = 'OR';

$query = $engine->executePrepared("
SELECT * FROM (
SELECT 
    m.id,
    m.id_service,
    s.name AS 'produk',
    s.pictures AS 'image',
    u.id AS 'recipient',
    u.firstname AS 'recipient_username',
    u_send.id AS 'sender',
    u_send.firstname AS 'sender_username',
    SUM(IF(isread = 1, 0, 1)) AS 'unread',
    m.time_send,
    m.text,
    m.img
FROM
    message m
        LEFT JOIN
    service s ON (m.id_service = s.id)
        LEFT JOIN
    users u ON (m.id_user_get = u.id)
    LEFT JOIN
    users u_send ON (m.id_user_send = u_send.id)
     INNER JOIN
    (SELECT 
            MAX(a.id) as id
        FROM
            message a
        GROUP BY a.id_service, id_user_send, id_user_get ORDER BY time_send DESC)dd ON (dd.id = m.id) 
WHERE
    id_user_get = :my_id OR id_user_send = :my_id
GROUP BY id_service, id_user_send, id_user_get ORDER BY m.time_send DESC
) oo ORDER BY oo.id DESC, oo.time_send ASC
",
    array(
        "my_id" => $my_id
    ));

//die($engine->createResponse(Engine::codError, $query->queryString));

$rows = array();
$ct = 0;
$last_rec = 0;
$last_sen = 0;

$exc = array();
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["image"])[0];
    if (!file_exists(serverRoot() . $pic . thumb_prefix()))
        generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);

    // Recipient
    $r_id = $row["recipient"];
    $r_name = $row["recipient_username"];
    // Sender
    $s_id = $row["sender"];
    $s_name = $row["sender_username"];

    // Revert Reverse
    if (in_array($row["id_service"] .';'.$s_id . ';' . $r_id, $exc))
        continue;
    array_push($exc, $row["id_service"] .';'.$r_id . ';' . $s_id);
    $last_rec = $r_id;
    $last_sen = $s_id;

    $rows[$ct] = array(
        "id" => $row["id"],
        "id_service" => $row["id_service"],
        "produk" => is_null($row["produk"]) ? 'Deleted Product' :$row["produk"],
        "image" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "unread" => $row["unread"],
        "time_send" => $row["time_send"],
        "text" => $row["text"],
        "img" => $row["img"],
        'sender' => $s_id
    );

    if ($my_id == $r_id) {
        $rows[$ct]['recipient'] = $s_id;
        $rows[$ct]['username'] = $s_name;
    } else {
        $rows[$ct]['recipient'] = $r_id;
        $rows[$ct]['username'] = $r_name;
    }

    $ct++;
}
$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();