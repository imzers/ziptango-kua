<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();
//Get POST Value
$engine->setPostField(
    array(
        'api_key'				=> true,
		'item_post_date'		=> false,
		'item_end_date'			=> false,
		
    )
);
$date_interval = array(
	'datepost'			=> $engine->getPOSTField('item_post_date'),
	'startdatef'		=> $engine->getPOSTField('item_end_date'),
	'date'				=> array(
		'starting'				=> false,
		'stopping'				=> false,
	),
);
if (is_string($date_interval['datepost']) || is_numeric($date_interval['datepost'])) {
	$date_interval['datepost'] = sprintf('%s', $date_interval['datepost']);
	try {
		$date_interval['date']['starting'] = DateTime::createFromFormat('Y-m-d', date('Y-m-d', $date_interval['datepost']));
	} catch (Exception $ex) {
		throw $ex;
		$date_interval['date']['starting'] = false;
	}
}
if (is_string($date_interval['startdatef']) || is_numeric($date_interval['startdatef'])) {
	$date_interval['startdatef'] = sprintf('%s', $date_interval['startdatef']);
	try {
		$date_interval['date']['stopping'] = DateTime::createFromFormat('Y-m-d', date('Y-m-d', $date_interval['startdatef']));
	} catch (Exception $ex) {
		throw $ex;
		$date_interval['date']['stopping'] = false;
	}
}
//Check API key
$engine->checkAPIKeyPair();

/*Check session is valid*/
$engine->checkCurrentSession();


// Set Date Filter
$sql_where_date = '';
if ($date_interval['date']['starting'] != FALSE) {
	$sql_where_date .= sprintf(" AND (DATE(FROM_UNIXTIME(j.datepost)) >= '%s')", $date_interval['date']['starting']->format('Y-m-d'));
}
if ($date_interval['date']['stopping'] != FALSE) {
	$sql_where_date .= sprintf(" AND (DATE(FROM_UNIXTIME(j.startdatef)) <= '%s')", $date_interval['date']['stopping']->format('Y-m-d'));
}
$sql_string = "SELECT j.*, IF(p.id_service IS NULL, 1, 0) AS available FROM view_public_svcmyproduct AS j LEFT JOIN payment_list AS p ON p.id_service = j.id WHERE j.b_id_user = :id_user";
$sql_string .= $sql_where_date;
$sql_string .= " ORDER BY j.sold ASC";
//Database Operation
$query = $engine->executePrepared($sql_string, array('id_user' => $_SESSION['id']));
/*
$query = $engine->executePrepared("
SELECT j.*,
       IF(p.id_service IS NULL, 1, 0) AS available
FROM   (SELECT t.*,
               IF(p.id_service IS NULL, 0, 1) AS sold
        FROM   (SELECT b.id,
                       b.category_id,
                       b.name,
                       b.activate,
                       b.id        AS listing,
                       b.pictures,
                       b.details,
                       s.firstname AS seller,
                       b.price,
                       b.force_sold
                FROM   service b,
                       users s
                WHERE  b.id_user = s.id
                       AND b.id_user = :id_user) t
               LEFT JOIN payment_list p
                      ON ( p.id_service = t.id AND p.id_buyer != :id_user)) j
       LEFT JOIN payment_list p
              ON ( p.id_service = j.id
                   AND p.id_buyer = :id_user ) ORDER BY sold ASC ",
    array(
        "id_user" => $_SESSION["id"]
    ));
*/
$rows = array();
$ct = 0;
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $pic = unserialize($row["pictures"])[0];
    if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
        generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
	}
    $rows[$ct] = array(
        "id" => $row["id"],
        "category_id" => $row["category_id"],
        "name" => $row["name"],
        "activate" => $row["activate"],
        "listing" => $row["listing"],
        "picture" => APP_SERVER_ROOT . $pic . thumb_prefix(),
        "seller" => $row["seller"],
        "details" => utf8_encode($row["details"]),
        "price" => $row["price"],
        "sold" => $row["sold"],
        "available" => $row["available"],
        "force_sold" => $row["force_sold"],
		'datepost'			=> (isset($row['datepost']) ? $row['datepost'] : ''),
		'startdatef'		=> (isset($row['startdatef']) ? $row['startdatef'] : ''),
		'item_post_date'	=> (isset($row['datepost']) ? $row['datepost'] : ''),
		'item_end_date'		=> (isset($row['startdatef']) ? $row['startdatef'] : ''),
    );
    $ct++;
}
$engine->setTranslateDataResponse($rows);

//Send Response
$engine->sendResponse();