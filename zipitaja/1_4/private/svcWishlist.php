<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true, // 1 = Get, 2 = Add, 3 = Remove
        "api_key" => true
    )
);

$mode = $engine->getPOSTField("param_mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

if ($mode == 1) {
	$sql = "SELECT t.*, IF (p.id_service IS NULL, 0, 1) AS sold
		FROM view_private_svcwishlist AS t
		LEFT JOIN payment_list AS p ON p.id_service = t.id
		WHERE t.f_id_user = :id_user";
    $query = $engine->executePrepared($sql, array("id_user" => $my_id));
    //Fetch Result
    $rows = array();
    $ct = 0;
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $pic = unserialize($row["pictures"])[0];
        if (!file_exists(serverRoot() . $pic . thumb_prefix())) {
            generateThumbnail(serverRoot() . $pic, APP_IMG_SERVICE_PATH);
        }

        $rows[$ct] = array(
            "id" => $row["id"],
            "category_id" => $row["category_id"],
            "category_name" => $row["category_name"],
            "name" => $row["name"],
            "listing" => $row["listing"],
            "picture" => APP_SERVER_ROOT . $pic . thumb_prefix(),
            "seller" => $row["seller"],
            "price" => $row["price"],
            "can_buy" => $row["can_buy"],
            "sold" => $row["sold"],
            "conditions" => $row["conditions"],
            "accepted_payment" => $row["accepted_payment"]
        );
        $ct++;
    }
    $engine->setTranslateDataResponse($rows);
} elseif ($mode == 2) {
    $engine->setPostField(
        array(
            "param_id" => true
        )
    );

    $param_id = $engine->getPOSTField("param_id");

    //======== ADD
    //Check is service in wishlist
    $query = $engine->executePrepared(
        "SELECT count(id) AS result FROM favorite f WHERE f.id_service = :id_service AND f.id_user = :id_user",
        array(
            "id_service" => $param_id,
            "id_user" => $my_id
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    if ($res["result"] < 1) {
        //Insert
        $query = $query = $engine->executePrepared(
            "
            INSERT INTO favorite (id_service, id_user)
            VALUES (:id_service, :id_user);",
            array(
                "id_service" => $param_id,
                "id_user" => $my_id
            )
        );
    }
    $engine->setSingleLineResponse(1);
} elseif ($mode == 3) {
    $engine->setPostField(
        array(
            "param_id" => true
        )
    );
    $param_id = $engine->getPOSTField("param_id");

    //==== DELETE
    $query = $query = $engine->executePrepared(
        "
        DELETE FROM favorite WHERE id_service = :id_service AND id_user = :id_user",
        array(
            "id_service" => $param_id,
            "id_user" => $my_id
        )
    );

    $engine->setSingleLineResponse(1);
}

/*
 * Send Result
 */
$engine->sendResponse();
