<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 11:45 PM
 */
require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";
require_once "../core/SMTPMail.php";
require_once "../core/PushNotificationManager.php";
require_once "cc_util.php";
require_once "checkout_util.php";

$engine = new Engine();

date_default_timezone_set('Asia/Jakarta');
header("Content-Type: text/html");

if ($_POST['ERR_CODE'] == 0) {
    $crypt = new MCrypt();
    $param = $_POST['MPARAM1'] . $_POST['MPARAM2'];
    $param = $crypt->decrypt($param);
    $param = json_decode($param, true);

    $trans_id = $_POST['TRANSACTIONID'];

    $current_id = $param['usr_id'];
    $param_services = $param['services'];
    $param_use_voucher = $param['use_voucher'];
    $param_coupon = $param['coupon'];
    $param_mobile = $param['mobile'];

    //Change Address
    $param_hp = $param['ex_adr'][5];
    $param_lat = $param['ex_adr'][0];
    $param_long = $param['ex_adr'][1];
    $param_address = $_POST['SHIPPING_ADDRESS'];
    $param_city = $_POST['SHIPPING_ADDRESS_CITY'];
    $param_state = $_POST['SHIPPING_ADDRESS_STATE'];
    $param_country = $param['ex_adr'][2];
    $param_unit_apt = $param['ex_adr'][3];
    $param_address_notes = $param['ex_adr'][4];

    $co = new CO(CO::PAYMENT_CC, $current_id, str_replace(';', ';;;', $param_services), $param_use_voucher, $param_coupon, $param_mobile, new CustAddress(
        $param_address,
        $param_city,
        $param_state,
        $param_country,
        $param_hp,
        $param_lat,
        $param_long,
        $param_unit_apt,
        $param_address_notes
    ));

    // Menyimpan pesanan
    $co->placeOrder($trans_id);

    header("Content-Type: text/html"); ?>

	<!DOCTYPE html>
	<html>

	<head>
	</head>

	<body>
		<script>
			window.JSIface.sayThanks();
		</script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
	</body>

	</html>

	<?php
} else {
        ?>

		<!DOCTYPE html>
		<html>

		<head>
			<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7'
			 crossorigin='anonymous'>
			<title>Pembayaran Gagal</title>
		</head>

		<body>

			<div class="container">
				<h3 class="text-center text-danger">PEMBAYARAN GAGAL</h3>
				<p class="text-center">Pembayarang gagal karena beberapa alasan.<br/><br/>Error Code :
					<?php echo $_POST['ERR_CODE']; ?><br/>Error Description :
					<?php echo $_POST['ERR_DESC']; ?>
				</p>
			</div>

			<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
		</body>

		</html>

		<?php
    } ?>