<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/1/2016
 * Time: 1:29 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_name" => true,
        "param_email" => true,
        "param_pesan" => true,
        "api_key" => true
    )
);

$param_name = $engine->getPOSTField("param_name");
$param_email = $engine->getPOSTField("param_email");
$param_pesan = $engine->getPOSTField("param_pesan");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
//User tidak boleh invite dirinya sendiri
if ($param_email == $_SESSION["email"])
    die($engine->createResponse(Engine::codError, "Anda tidak boleh mengundang diri anda sendiri!"));

//Check apakah email sudah ada
$query = $engine->executePrepared("SELECT count(id) AS result FROM users u WHERE u.email = :email",
    array(
        "email" => $engine->getPOSTField("param_email")
    ));
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] >= 1)
    die($engine->createResponse(Engine::codError, "Email sudah terdaftar. Sialakan undang teman anda yang belum terdaftar di Ziptango."));

//Get Username and invite Link
$query = $engine->executePrepared("SELECT username, permanent_link_code FROM users u WHERE u.id = :id",
    array(
        "id" => $_SESSION["id"]
    )
);
$rows = $query->fetchAll(PDO::FETCH_ASSOC);

$username = $rows[0]["username"];
$link = APP_SERVER_ROOT . "invite/index/" . $rows[0]["permanent_link_code"];

//Send Invitation Mail
$mailC = getMailMessage("invite");
$mailC->message = str_replace("#whosent#", $username, $mailC->message);
$mailC->message = str_replace("#invitelink#", $link, $mailC->message);

$final_msg = $param_pesan == "-" ? $mailC->message : $param_pesan;

if (sendMail($engine->getPOSTField("param_email"), $mailC->subject, htmlspecialchars_decode($final_msg, ENT_NOQUOTES)))
    $engine->setSingleLineResponse(1);
else
    $engine->setErrorResponse("Undangan gagal dikirim, silakan coba kembali!");

/*
 * Send Result
 */
$engine->sendResponse();
