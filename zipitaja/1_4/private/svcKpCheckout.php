<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";
require_once "../core/PushNotificationManager.php";
require_once "cc_util.php";
require_once "checkout_util.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
  array(
        'param_services' => true,
        'param_use_voucher' => true,
        'param_coupon' => false,
        'param_hp' => true,
        'param_lat' => true,
        'param_long' => true,
        'param_address' => true,
        'param_city' => true,
        'param_state' => true,
        'param_country' => true,
        'param_unit_apt' => true,
        'param_address_notes' => true,
        'param_mobile' => true,
        'api_key' => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

$param_services = $engine->getPOSTField('param_services');
$param_use_voucher = $engine->getPOSTField('param_use_voucher');
$param_coupon = $engine->getPOSTField('param_coupon');
$param_mobile = $engine->getPOSTField('param_mobile');

$param_hp = $engine->getPOSTField('param_hp');
$param_lat = $engine->getPOSTField('param_lat');
$param_long = $engine->getPOSTField('param_long');
$param_address = $engine->getPOSTField('param_address');
$param_city = $engine->getPOSTField('param_city');
$param_state = $engine->getPOSTField('param_state');
$param_country = $engine->getPOSTField('param_country');
$param_unit_apt = $engine->getPOSTField('param_unit_apt');
$param_address_notes = $engine->getPOSTField('param_address_notes');

//die($engine->createResponse(Engine::codError, preg_replace("/\;;;$/", "", $param_services)));

$co = new CO(CO::PAYMENT_KP, null, $param_services, $param_use_voucher, $param_coupon, $param_mobile, new CustAddress(
    $param_address,
    $param_city,
    $param_state,
    $param_country,
    $param_hp,
    $param_lat,
    $param_long,
    $param_unit_apt,
    $param_address_notes
));

$error = '';
if (!$co->validateOrder($error)) {
    die($engine->createResponse(Engine::codError, $error));
}

if ($co->serviceCount() > 1) {
    die($engine->createResponse(Engine::codError, 'Hanya 1 barang yang diperbolehkan untuk Pengajuan Kredit Plus.'));
}

$token = $co->placeAsTemporary();

// Get service info
$service = $co->getServices()[0];

// Check payment info, apakah support KreditPlus
if (strpos($service['accepted_payment'], 'kp') === false) {
    die($engine->createResponse(Engine::codError, 'Kredit Plus hanya dapat dipakai untuk produk baru senilai Rp.1.250.000,- sampai Rp.10.000.000,-'));
}

$kpq = $engine->executePrepared('SELECT * FROM kp_config', []);
$kp_config = $kpq->fetch(PDO::FETCH_ASSOC);

// cek total
function rp($angka)
{
    $jadi = "Rp " . number_format($angka, 2, ',', '.');
    return $jadi;
}

$total = $co->getGrandTotal();
if ($total < $kp_config['min_price'] || $total > $kp_config['max_price']) {
    die($engine->createResponse(Engine::codError, 'Grand total pembelian tidak sesuai dengan kriteria pembayaran melalui Kredit Plus (' . rp($kp_config['min_price']) . ' - ' . rp($kp_config['max_price']) . ').'));
}

// Get user info
$userQ = $engine->executePrepared(
    "SELECT email, firstname, lastname, telephone FROM users u WHERE u.id = :id",
    ["id" => $_SESSION['id']]
);
$user = $userQ->fetch(PDO::FETCH_ASSOC);

// Ambil url eform
$params = [
    SupplierKey => KP_SUPPLIER_KEY,
    OrderID => $co->getUniqIdFromToken($token) ,
    CustomerName => $_SESSION["fullname"],
    MobilePhone => $user['telephone'] ? $user['telephone'] : '-',
    Email => $user["email"],
    ItemDescription => $service['name'],
    CategoryID => $service['category_id'],
    OTR => $co->getGrandTotal(),
    DP => 0,
    AdminFee => $kp_config['admin_fee'],
    ShippingCost => $kp_config['shipping_fee'],
    OtherFee => $kp_config['other_fee'],
    Insurance => $kp_config['insurance'],
    Provicy => $kp_config['provicy_fee'],
    CallBackUrl=> APP_KP_CALLBACK . '?token=' . $token
];

$params_string = '';
foreach ($params as $key => $value) {
    if ($params_string != '') {
        $params_string .= '&';
    }
    $params_string .= "$key=$value";
}

$url = KP_API_URL . "UrlReq";
$options = array(
    CURLOPT_POST => 1,
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER => false,            // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
    CURLOPT_AUTOREFERER => true,        // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
    CURLOPT_TIMEOUT => 120,             // time-out on response
    CURLOPT_POSTFIELDS => $params_string
);
$ch = curl_init($url);
curl_setopt_array($ch, $options);
$content = curl_exec($ch);
curl_close($ch);

// Parse KP Response
$xml_snippet = @simplexml_load_string($content);
$json_convert = json_encode($xml_snippet);
$json = json_decode($json_convert, true);

// kirim hasil
if ($json['ResultStatus'] == 0) {
    $engine->setSingleLineResponse($json['Link_URL']);
} else {
    $engine->setErrorResponse("Url eForm Gagal Dibuat");
}
$engine->sendResponse();
