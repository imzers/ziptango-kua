<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 12:27 AM
 */
require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../private/cc_util.php";

$engine = new Engine();

$param_order = $_POST['TRANSACTIONID'];

if (isset($param_order)) {
    $query = $engine->executePrepared("UPDATE payment_list SET `status` = :status WHERE ccid = :ccid", array(
        'status' => 6,
        'ccid' => $param_order
    ));
    $engine->setSingleLineResponse(1);
} else
    $engine->setSingleLineResponse(0);

$engine->sendResponse();
