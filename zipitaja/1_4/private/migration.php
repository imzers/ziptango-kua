<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 2:26 AM
 */

require_once '../core/PushNotificationManager.php';
require_once '../core/Engine.php';

/*
             * SEND Notification
             */
$engine = new  Engine();

$engine->executePrepared("
DROP function IF EXISTS `kp_qualify`;
", []);
$engine->executePrepared("
CREATE FUNCTION `kp_qualify`(brand int, s_type int, s_condition int, price float, category int, seller_id int, accept_kp int) RETURNS int(11)
BEGIN

DECLARE p_brands TEXT;
DECLARE p_types TEXT;
DECLARE p_conditions TEXT;
DECLARE p_min_price DECIMAL;
DECLARE p_max_price DECIMAL;
DECLARE p_categories TEXT;
DECLARE p_seller_blacklist TEXT;
DECLARE p_enabled INT;

SELECT
    `brands`,
    `types`,
    `conditions`,
    `min_price`,
    `max_price`,
    `categories`,
	`seller_blacklist`,
    `payment_enabled`
INTO p_brands , p_types , p_conditions , p_min_price , p_max_price , p_categories , p_seller_blacklist , p_enabled FROM
    kp_config
WHERE
    id = 1;

if (p_brands is null OR p_brands = '0') then SET brand = 0; end if;
if (p_types is null OR p_types = '0') then SET s_type = 0;end if;
if (p_conditions is null OR p_conditions = '-1') then SET s_condition = -1;end if;
if (p_categories is null OR p_categories = '0') then SET category = 0;end if;

-- return 1;

if (
-- check payment is enabled
p_enabled != 1 OR
-- check brand
LOCATE(CONCAT(',', brand, ','), CONCAT(',', p_brands, ',')) = 0 OR
-- check type
LOCATE(CONCAT(',', s_type, ','), CONCAT(',', p_types, ',')) = 0 OR
-- check condition
LOCATE(CONCAT(',', s_condition, ','), CONCAT(',', p_conditions, ',')) = 0 OR
-- check price
price < p_min_price OR
price > p_max_price OR
-- check categories
LOCATE(CONCAT(',', category, ','), CONCAT(',', p_categories, ',')) = 0 OR
-- check seller blacklist
LOCATE(CONCAT(',', seller_id, ','), CONCAT(',', p_seller_blacklist, ',')) > 0 OR
-- check accep KP
accept_kp != 1
)then return 0;
else
RETURN 1;
end if;

END
", []);
die("Migration not required...");

// =================================
// tb_tmp_checkout
// =================================
$engine->executePrepared("DROP TABLE IF EXISTS `tb_tmp_checkout`");
$engine->executePrepared("
CREATE TABLE IF NOT EXISTS `tb_tmp_checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) DEFAULT NULL,
  `payment_type` varchar(45) DEFAULT NULL,
  `token` text,
  `services` varchar(500) DEFAULT NULL,
  `use_voucher` int(11) DEFAULT NULL,
  `coupon` varchar(500) DEFAULT NULL,
  `hp` varchar(500) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `long` float DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `unit_apt` varchar(500) DEFAULT NULL,
  `address_notes` varchar(500) DEFAULT NULL,
  `platform` int(11) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
", []);

// =================================
// kp_payment_data
// =================================
$engine->executePrepared("DROP TABLE IF EXISTS `kp_payment_data`");
$engine->executePrepared("
CREATE TABLE IF NOT EXISTS `kp_payment_data` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `order_id` varchar(100) DEFAULT NULL,
    `approval_date` bigint(20) DEFAULT NULL,
    `po_number` varchar(100) DEFAULT NULL,
    `po_date` bigint(20) DEFAULT NULL,
    `awb_number` varchar(100) DEFAULT NULL,
    `recipient_name` varchar(45) DEFAULT NULL,
    `updated_at` bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
", []);

$engine->executePrepared("
ALTER TABLE `kp_payment_data`
ADD COLUMN `approval_status` VARCHAR(45) NULL AFTER `order_id`;
", []);

$engine->executePrepared("
ALTER TABLE `kp_payment_data`
ADD COLUMN `confirmation_status` VARCHAR(100) NULL,
ADD COLUMN `delivery_date` BIGINT(20) NULL,
ADD COLUMN `confirmation_notes` TEXT NULL,
ADD COLUMN `confirmation_date` BIGINT(20) NULL,
ADD COLUMN `shipping_detail` TEXT NULL,
ADD COLUMN `kp_shipping_detail` TEXT NULL;
", []);

// =================================
// kp_config
// =================================
$engine->executePrepared("DROP TABLE IF EXISTS `kp_config`");
$engine->executePrepared("
CREATE TABLE `kp_config` (
  `id` INT NOT NULL,
  `brands` TEXT NULL,
  `types` TEXT NULL,
  `conditions` TEXT NULL,
  `min_price` FLOAT NULL,
  `max_price` FLOAT NULL,
  `categories` TEXT NULL,
  `admin_fee` FLOAT NULL DEFAULT 0,
  `insurance` FLOAT NULL DEFAULT 0,
  `provicy_fee` FLOAT NULL DEFAULT 0,
  `shipping_fee` FLOAT NULL DEFAULT 0,
  `other_fee` FLOAT NULL DEFAULT 0,
  `update_time` BIGINT NULL,
PRIMARY KEY (`id`));
", []);

$engine->executePrepared("
ALTER TABLE `kp_config`
ADD COLUMN `seller_blacklist` TEXT NULL AFTER `categories`,
ADD COLUMN `payment_enabled` INT NULL DEFAULT 1,
ADD COLUMN `admin_name` TEXT NULL AFTER `update_time`,
ADD COLUMN `admin_email` TEXT NULL AFTER `admin_name`,
ADD COLUMN `admin_password` TEXT NULL AFTER `admin_email`;
", []);

$engine->executePrepared("
INSERT INTO `kp_config`
(`id`,
`brands`,
`types`,
`conditions`,
`min_price`,
`max_price`,
`categories`,
`update_time`)
VALUES
(1,
'0',
'0',
'0',
1250000,
10000000,
'0',
UNIX_TIMESTAMP());
", []);

$engine->executePrepared('UPDATE kp_config SET admin_name = \'Administrator\', admin_email = \'admin@ziptango.com\', admin_password = \'1234\' WHERE 1 = 1', []);

// =================================
// kp_qualify
// =================================
$engine->executePrepared("
DROP function IF EXISTS `kp_qualify`;
", []);
$engine->executePrepared("
CREATE FUNCTION `kp_qualify`(brand int, s_type int, s_condition int, price float, category int, seller_id int, accept_kp int) RETURNS int(11)
BEGIN

DECLARE p_brands TEXT;
DECLARE p_types TEXT;
DECLARE p_conditions TEXT;
DECLARE p_min_price DECIMAL;
DECLARE p_max_price DECIMAL;
DECLARE p_categories TEXT;
DECLARE p_seller_blacklist TEXT;
DECLARE p_enabled INT;

SELECT 
    `brands`,
    `types`,
    `conditions`,
    `min_price`,
    `max_price`,
    `categories`,
	`seller_blacklist`,
    `payment_enabled`
INTO p_brands , p_types , p_conditions , p_min_price , p_max_price , p_categories , p_seller_blacklist , p_enabled FROM
    kp_config
WHERE
    id = 1;

if (p_brands is not null OR p_brands = '0') then SET brand = 0; end if;
if (p_types is not null OR p_types = '0') then SET s_type = 0;end if;
if (p_conditions is not null OR p_conditions = '-1') then SET s_condition = -1;end if;
if (p_categories is not null OR p_categories = '0') then SET category = 0;end if;

if (
-- check payment is enabled
p_enabled != 1 OR
-- check brand
LOCATE(CONCAT(',', brand, ','), CONCAT(',', p_brands, ',')) = 0 OR 
-- check type
LOCATE(CONCAT(',', s_type, ','), CONCAT(',', p_types, ',')) = 0 OR
-- check condition
LOCATE(CONCAT(',', s_condition, ','), CONCAT(',', p_conditions, ',')) = 0 OR
-- check price
price < p_min_price OR
price > p_max_price OR
-- check categories
LOCATE(CONCAT(',', category, ','), CONCAT(',', p_categories, ',')) = 0 OR
-- check seller blacklist
LOCATE(CONCAT(',', seller_id, ','), CONCAT(',', p_seller_blacklist, ',')) > 0
)then return 0;
else 
RETURN 1;
end if;

END
", []);


// =================================
// service
// =================================
$engine->executePrepared("ALTER TABLE `service` 
ADD COLUMN `payment_kp_accepted` INT NULL DEFAULT 1 AFTER `qty`;
");


// =================================
// kp_view
// =================================
$query = $engine->executePrepared('
CREATE
OR REPLACE VIEW `kp_view` AS
SELECT
   pl.id,
   pl.status,
   pl.total AS total,
   s.id AS service_id,
   s.name AS service_name,
   u.id AS buyer_id,
   CONCAT(u.firstname, \' \', u.lastname) AS buyer_fullname,
   u.username AS buyer_username,
   u.email AS buyer_email,
   kpd.order_id AS kp_order_id,
   kpd.approval_status AS kp_approval_status,
   kpd.approval_date AS kp_approval_date,
   kpd.po_number AS kp_po_number,
   kpd.po_date AS kp_po_date,
   kpd.awb_number AS kp_awb_number,
   kpd.recipient_name AS kp_recipient_name,
   kp_shipping_detail AS kp_shipping,
   shipping_detail AS shipping,

   kpd.confirmation_status as kp_confirmation_status,
   kpd.delivery_date as kp_delivery_date,
   kpd.confirmation_notes as kp_confirmation_notes,
   kpd.confirmation_date as kp_confirmation_date,

   kpd.updated_at AS kp_updated_at
FROM
   (((`payment_list` `pl`
   JOIN `kp_payment_data` `kpd` ON ((CONVERT( `kpd`.`order_id` USING UTF8) = `pl`.`ccid`)))
   LEFT JOIN `service` `s` ON ((`s`.`id` = `pl`.`id_service`)))
   LEFT JOIN `users` `u` ON ((`u`.`id` = `pl`.`id_buyer`)));
');


echo 'Migration success..';
