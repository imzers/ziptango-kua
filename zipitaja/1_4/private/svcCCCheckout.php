<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";
require_once "../core/PushNotificationManager.php";
require_once "cc_util.php";
require_once "checkout_util.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'param_services' => true,
        'param_use_voucher' => true,
        'param_coupon' => false,
        'param_hp' => true,
        'param_lat' => true,
        'param_long' => true,
        'param_address' => true,
        'param_city' => true,
        'param_state' => true,
        'param_country' => true,
        'param_unit_apt' => true,
        'param_address_notes' => true,
        'param_mobile' => true,
        'api_key' => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

$param_services = $engine->getPOSTField('param_services');
$param_use_voucher = $engine->getPOSTField('param_use_voucher');
$param_coupon = $engine->getPOSTField('param_coupon');
$param_mobile = $engine->getPOSTField('param_mobile');

$param_hp = $engine->getPOSTField('param_hp');
$param_lat = $engine->getPOSTField('param_lat');
$param_long = $engine->getPOSTField('param_long');
$param_address = $engine->getPOSTField('param_address');
$param_city = $engine->getPOSTField('param_city');
$param_state = $engine->getPOSTField('param_state');
$param_country = $engine->getPOSTField('param_country');
$param_unit_apt = $engine->getPOSTField('param_unit_apt');
$param_address_notes = $engine->getPOSTField('param_address_notes');

// Hanya untuk memvalidasi pesanan
// pesanan akan disimpan di callback credit card
$co = new CO(CO::PAYMENT_CC, null, $param_services, $param_use_voucher, $param_coupon, $param_mobile, new CustAddress(
    $param_address,
    $param_city,
    $param_state,
    $param_country,
    $param_hp,
    $param_lat,
    $param_long,
    $param_unit_apt,
    $param_address_notes
));

// Validasi Order
$error = '';
if (!$co->validateOrder($error)) {
    die($engine->createResponse(Engine::codError, $error));
}

// Update alamat
$co->updateAddress();

// Parameter CC
$data = json_encode(array(
    'usr_id' => $co->currentUserId(),
    'services' => str_replace(';;;', ';', $param_services),
    'use_voucher' => $param_use_voucher,
    'coupon' => $param_coupon,
    'mobile' => $param_mobile,
    'ex_adr' => array(
        $param_lat,
        $param_long,
        $param_country,
        $param_unit_apt,
        $param_address_notes,
        substr($param_hp, 0, 12)
    )
));

// Membuat eform cc
$engine->setSingleLineResponse(
    generateCCForm(
        uniqid(),
        $_SESSION['fullname'],
        $_SESSION['email'],
        $co->getGrandTotal(),
        new CustAddress($param_address, $param_city, $param_state, $param_country, $param_hp, $param_lat, $param_long, $param_unit_apt, $param_address_notes),
        $co->getServicesName(),
        $data
    )
);
$engine->sendResponse();
