<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/PushNotificationManager.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

define("MODE_READ", "1");
define("MODE_SEND", "2");

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(array(
        "param_mode" => true,
        "api_key" => true
    )
);
$param_mode = $engine->getPOSTField("param_mode");

$my_id = $_SESSION["id"];
if ($param_mode == MODE_READ) {
    $engine->setPostField(array(
            "param_service" => true,
            "param_recipient" => true,
            "param_page" => true,
            "api_key" => true
        )
    );
    $param_service = $engine->getPOSTField("param_service");
    // Change to ziptango ID, for centralizing support
    $param_recipient = $engine->getPOSTField("param_recipient");
    $param_page = $engine->getPOSTField("param_page");

} else {
    $engine->setPostField(array(
        "param_service" => true,
        "param_recipient" => true,
        "param_text" => false,
        "param_img" => false,
        "param_mobile" => true,
        "api_key" => true));
    $param_service = $engine->getPOSTField("param_service");

    if ($my_id == 1)
        $param_recipient = $engine->getPOSTField("param_recipient");
    else
        $param_recipient = 1;
    $param_text = $engine->getPOSTField("param_text");
    $param_img = $engine->getPOSTField("param_img");
    $param_mobile = $engine->getPOSTField("param_mobile");

}

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core operation
 */
if ($param_mode == MODE_READ) {
    $page = $engine->getPOSTField("param_page");
    $offset = $page * 200;
    $srv = APP_SERVER_ROOT;
    //die($engine->createResponse(Engine::codError, $param_recipient));
    $query = $engine->executePrepared("
    SELECT
        m.id,
        m.id_service,
        m.id_user_send,
        m.id_user_get,
        m.id_first_message,
        m.isread,
        m.time_send,
        m.text,
        (CASE m.img
            WHEN '' THEN ''
            ELSE concat('$srv', m.img) END)  as img
    FROM
        message m
    WHERE
        ((m.id_user_send = :sender OR m.id_user_send = :recipient) AND
        (m.id_user_get = :sender OR m.id_user_get = :recipient)) AND
        m.id_service = :service
    ORDER BY m.id DESC
    LIMIT 200 OFFSET $offset",
        array(
            "sender" => $my_id,
            "recipient" => $param_recipient,
            "service" => $param_service
        )
    );
    $engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

    //UPDATE ISREAD
    $query = $engine->executePrepared("UPDATE message SET isread = 1 WHERE id_user_get = :my_id AND id_service = :id_service",
        array(
            "my_id" => $my_id,
            "id_service" => $param_service
        ));

    if ($query->rowCount() > 0) {
        //GET LAST MESSAGE UD
        $query = $engine->executePrepared("SELECT id FROM message WHERE id_user_get = :my_id AND id_service = :id_service ORDER BY id DESC LIMIT 1",
            array(
                "my_id" => $my_id,
                "id_service" => $param_service
            ));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $last_id = $row["id"];

        $gcm = new PushNotificationManager();
        //Get list registered id
        $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id",
            array(
                "user_id" => $param_recipient
            )
        );
        $ids = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
            $ids[] = array($row["gcm_regid"], $row["mobile"]);

        //Send notification
        $gcm->sendReadNotification(2, $ids, $last_id, $param_service, $_SESSION["id"]);
    }

} else {
    //Handle Upload File
    $param_img = handleUploadFile("param_img", APP_IMG_MESSAGE_PATH, true, true);

    //Insert Message
    $query = $engine->executePrepared("
    INSERT INTO message
        (id_service, id_user_send, id_user_get, id_first_message, isread, time_send, text, img, mobile)
    VALUES
        (:id_service, :id_user_send, :id_user_get, 0, 0, UNIX_TIMESTAMP(), :text, :img, :mobile)",
        array(
            "id_service" => $param_service,
            "id_user_send" => $my_id,
            "id_user_get" => $param_recipient,
            "text" => $param_text,
            "img" => $param_img,
            "mobile" => $param_mobile
        )
    );

    //Create Response
    $engine->setErrorResponse("Pesan gagal dikirim, Silakan coba kembali!");
    if ($query) {
        $id = $engine->getLastID("id");
        //SEND Notification
        $gcm = new PushNotificationManager();
        //Get list registered id
        $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id",
            array(
                "user_id" => $param_recipient
            )
        );
        $ids = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC))
            $ids[] = array($row["gcm_regid"], $row["mobile"]);

        //Send notification
        $gcm->sendMessageNotification(1, $ids, $id, $param_service, $my_id, $param_recipient, time(), $param_text, empty($param_img) ? "" : APP_SERVER_ROOT . $param_img);

        //Send Mail
        $query = $engine->executePrepared("SELECT email FROM users WHERE id = :user_id",
            array(
                "user_id" => $param_recipient
            )
        );
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        //Send Mail Notification
        sendMail($row[0]["email"], "Chat Notification for Listing " . $param_service, $param_text);

        //Send Response
        $engine->setSingleLineResponse($id);
    }

}

/*
 * Send Result
 */
$engine->sendResponse();