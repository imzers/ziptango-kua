<?php

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__FILE__) .  DIRECTORY_SEPARATOR . 'cc_util.php');
        
// Mengambil object checkout dari tabel temporary sesuai token
function getCoByTemporaryToken($token)
{
    $engine = new Engine();
    $query = $engine->executePrepared('SELECT * FROM tb_tmp_checkout WHERE token = :token', ['token'=> $token]);
    $row = $query->fetch(PDO::FETCH_ASSOC);

    if ($row) {
        $address = new CustAddress(
            $row['address'],
            $row['city'],
            $row['state'],
            $row['country'],
            $row['hp'],
            $row['lat'],
            $row['long'],
            $row['unit_apt'],
            $row['address_notes']
        );
        return new CO($row['payment_type'], $row['usr_id'], $row['services'], $row['use_voucher'], $row['coupon'], $row['platform'], $address, $token);
    } else {
        return null;
    }
}

class CO {
    // Untuk membuat token (base64 tahap 2)
    const SALT = 'salt:892341-2-2394-23:salt';

    const PAYMENT_BT = 'bt';
    const PAYMENT_CC = 'cc';
    const PAYMENT_KP = 'kp';
	const PAYMENT_GOPAY = 'gopay';

    private $payment_type;
    private $user_id;
    private $services;
    private $use_voucher;
    private $coupon_code;
    private $mobile_code;
    private $address;
    private $token;
	
	protected $DateObject;
    /**
     * CO constructor.
     * @param $payment_type
     * @param $user_id // User ID bisa null agar bisa dipakai oleh callback Credit Card (jika null akan diisei dengan id yang tersimpan di session)
     * @param $services
     * @param $use_voucher
     * @param $coupon_code
     * @param $mobile_code
     * @param CustAddress $address
     */
    public function __construct($payment_type, $user_id, $services, $use_voucher, $coupon_code, $mobile_code, $address, $token = null) {
        $this->payment_type = $payment_type;
        $this->user_id = $user_id;
        $this->services = preg_replace("/\;;;$/", "", $services); // antisipasi client request dengan delimiter (;;;) di akhir
        $this->use_voucher = $use_voucher;
        $this->coupon_code = $coupon_code;
        $this->mobile_code = $mobile_code;
        $this->address = $address;
        $this->token = $token;
		
		$this->DateObject = $this->create_dateobject('Asia/Bangkok', 'Y-m-d H:i:s', date('Y-m-d H:i:s'));
    }
	function create_dateobject($timezone, $format, $date) {
		$microtime = microtime(true);
		$micro = sprintf("%06d",($microtime - floor($microtime)) * 1000000);
		$DateObject = DateTime::createFromFormat($format, $date);
		if ($DateObject != false) {
			$DateObject->setTimezone(new DateTimeZone($timezone));
		}
		return $DateObject;
	}
	public function get_services() {
		return $this->services;
	}
    // Engine
    public function ce() {
        require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . "Engine.php");
        return new Engine();
    }

    public function currentUserId()
    {
        return ($this->user_id != null) ? $this->user_id : $_SESSION['id'];
    }

    /**
     * @return array [firstname, lastname, email]
     */
    public function currentUserInfo()
    {
        $query_user = $this->ce()->executePrepared("SELECT id, firstname, lastname, concat(firstname, lastname) as fullname, email, telephone FROM users WHERE id = :id", array(
            'id' => $this->currentUserId()
        ));
        return $query_user->fetch(PDO::FETCH_ASSOC);
    }

    public function IsNullOrEmptyString($question)
    {
        return (!isset($question) || trim($question)==='');
    }

    // Menyatukan service dengan delimiter koma,
    // untuk kebutuhan select in di mysql e.g in (2134, 2423)
    public function commaServices()
    {
        $services = explode(';;;', $this->services);
        for ($i = 0; $i < count($services); $i++) {
            if (!$this->IsNullOrEmptyString($services[$i])) {
                if ($i > 0) {
                    $s = $s . ', ';
                }
                $s = $s . $services[$i];
            }
        }

        return "($s)";
    }

    public function serviceCount()
    {
        $services = explode(';;;', $this->services);
        return count($services);
    }

    public function isServicesAvailable_Original() {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT COUNT(id) as count FROM payment_list WHERE id_service IN $s", array());
        return $query->fetch(PDO::FETCH_ASSOC)['count'] <= 0;
    }
	public function isServicesAvailable() {
        $s = $this->commaServices();
		$is_available = TRUE;
		$sql_string = sprintf("SELECT COUNT(id) AS value FROM service WHERE id IN %s", $s);
		$query = $this->ce()->executePrepared($sql_string, array());
		$row = array(
			'services'		=> $query->fetch(PDO::FETCH_ASSOC),
		);
		$sql_string = sprintf("SELECT COUNT(id) AS value FROM service WHERE id IN %s AND (qty > :qty)", $s);
		$query = $this->ce()->executePrepared($sql_string, array('qty' => 0));
		$row['quantity'] = $query->fetch(PDO::FETCH_ASSOC);
		if ($row['quantity']['value'] < $row['services']['value']) {
			$is_available = FALSE;
		}
		return $is_available;
    }

    public function getSubTotal()
    {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT SUM(price) as subtotal FROM service WHERE id IN $s", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row['subtotal'];
    }

    public function getServices()
    {
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT u.id as seller_id, s.name, s.category_id, CONCAT('bt,cc', IF(KP_QUALIFY(s.brand_id, s.bagtype_id, s.conditions, CAST(s.price AS DECIMAL), s.category_id, s.id_user, s.payment_kp_accepted) = 1, ',kp', '')) as accepted_payment, CAST(s.price AS DECIMAL) AS price FROM service s, users u WHERE s.id_user = u.id AND s.id IN $s", array());
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCoupon(&$error)
    {
        $error = '';
        $coupon_code = $this->coupon_code;
        $mobile_code = $this->mobile_code;
        $subtotal = $this->getSubTotal();

        if (!$coupon_code) {
            return 0;
        }

        // Ambil Data Kupon
        $query = $this->ce()->executePrepared("SELECT ( IF(Unix_timestamp() BETWEEN start AND end, '', 'Kupon yang anda masukkan hanya bisa digunakan dalam waktu tertentu')) AS msg, id_user as user_id, type,  value, min, max, `limit`, firsttimebuyer, mobile, (SELECT Count(id) FROM payment_list WHERE  status IN ( 0, 3, 4, 5 ) AND id_buyer = :me) AS is_firstime_buy FROM   coupon WHERE  active = 0 AND name = :name", array(
            'name' => $coupon_code,
            'me' => $this->currentUserId()
        ));
        $coupon = $query->fetch(PDO::FETCH_ASSOC);

        // Temporary perhitungan
        $tmp = 0;

        // Memulai Validasi Kupon
        // ====================================
        if (!empty($coupon['msg'])) {
            // Check apakah ada pesan error
            $error = $coupon['msg'];
            return 0;
        } else {
            $type = $coupon['type'];
            $value = $coupon['value'];
            if ($type == 1) {
                $tmp = $value / 100 * $subtotal;
            } else {
                $tmp = $value;
            }
        }

        // Cek kupon untuk seller tertentu
        $service = $this->getServices();
        for ($i = 0; $i < count($service) - 1; $i++) {
            if ($service[$i]['seller_id'] != $coupon['user_id']) {
                $error = 'Kode kupon hanya berlaku untuk seller tertentu!';
            }
        }

        //Check Limit
        $limit = $coupon['limit'];
        if ($limit > 0 && $subtotal < $limit) {
            $error = "Kode kupon tidak bisa digunakan untuk subtotal pembelian kurang dari Rp. $limit";
        }

        //Check First Time Buy
        $firstTimeBuy = $coupon['firsttimebuyer'];
        $isFirstTimeBuy = $coupon['is_firstime_buy'];
        if ($firstTimeBuy == 1 && $isFirstTimeBuy > 0) {
            $error = "Coupon ini khusus untuk pembelian pertama";
        }

        //Check Platform
        $mobileCode = $coupon['mobile'];

        if ($mobileCode != 4) {
            if ($mobile_code != $mobileCode) {
                if ($mobileCode == 3) {
                    if ($mobile_code != 1 && $mobile_code != 2) {
                        switch ($mobileCode) {
                            case 0:
                                $error = "Coupon berlaku hanya untuk pembelian di Web";
                                return 0;
                            case 1:
                                $error = "Coupon berlaku hanya untuk pembelian di Android";
                                return 0;
                            case 2:
                                $error = "Coupon berlaku hanya untuk pembelian di iOS";
                                return 0;
                            case 3:
                                $error = "Coupon berlaku hanya untuk pembelian di aplikasi mobile";
                                return 0;
                        }
                    }
                }
            }
        }

        //Validate min & max
        $c_min = $coupon['min'];
        $c_max = $coupon['max'];
        if ($c_min > 0 && $tmp < $c_min) {
            $tmp = $c_min;
        }
        if ($c_max > 0 && $tmp > $c_max) {
            $tmp = $c_max;
        }

        return $tmp ? $tmp : 0;
    }

    public function getVoucher()
    {
        if (!$this->use_voucher) {
            return 0;
        }

        $err = '';
        $coupon = $this->getCoupon($err);
        $subtotal = $this->getSubTotal();

        // Ambil total voucher yang dimiliki
        $query = $this->ce()->executePrepared("SELECT IFNULL(SUM(value), 0) AS credit FROM zip_credits WHERE deleted = 0 AND assigned_to = :assigned_to", array(
            'assigned_to' => $this->currentUserId()
        ));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $voucher = $row['credit'];
        if ($subtotal - $coupon <= $voucher) {
            $voucher = $subtotal - $coupon;
        }

        return $voucher;
    }

    public function getBuyerCommission()
    {
        $subtotal = $this->getSubTotal();

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'buyer_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $komisi_buyer = $row["value_2"] / 100 * $subtotal;

        return $komisi_buyer;
    }

    public function getSellerCommission()
    {
        $subtotal = $this->getSubTotal();

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $komisi_seller = $row['value_2'] / 100 * $subtotal;

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_min_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $min_komisi_seller = $row['value_2'];

        $query = $this->ce()->executePrepared("SELECT * FROM common_admin WHERE name = 'seller_max_com'", array());
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $max_komisi_seller = $row['value_2'];

        if ($komisi_seller < $min_komisi_seller) {
            $komisi_seller = $min_komisi_seller;
        }

        if ($max_komisi_seller > 0) {
            if ($max_komisi_seller < $komisi_seller) {
                $komisi_seller = $max_komisi_seller;
            }
        }

        return $komisi_seller;
    }

    public function isPhoneExists()
    {
        $query = $this->ce()->executePrepared("SELECT count(id) AS result FROM users u WHERE u.telephone = :telephone AND u.id != :id", array(
            'telephone' => $this->address->phone,
            'id' => $this->currentUserId()
        ));
        $res = $query->fetch(PDO::FETCH_ASSOC);

        return $res['result'] >= 1;
    }

    public function updateAddress()
    {
        $query = $this->ce()->executePrepared(
            "
            UPDATE users SET
                telephone = :telephone,
                la = :la,
                lo = :lo,
                address = :address,
                city = :city,
                state = :state,
                unit_apt = :unit_apt,
                address_notes = :address_notes
            WHERE id = :id",
            [
                'telephone' => $this->address->phone,
                'la' => $this->address->lat,
                'lo' => $this->address->lng,
                'address' => $this->address->address,
                'city' => $this->address->city,
                'state' => $this->address->state,
                'unit_apt' => $this->address->unit_apt,
                'address_notes' => $this->address->address_notes,
                'id' => $this->currentUserId()
            ]
        );
        return $query;
    }

    public function getGrandTotal()
    {
        $grandTotal = 0;

        $services = explode(';;;', $this->services);
        $voucher = $this->getVoucher();
        $err = '';
        $coupon = $this->getCoupon($err);
        $komisi_buyer = $this->getBuyerCommission();

        $serviceCount = $this->serviceCount();
        //Checkout
        for ($i = 0; $i < $serviceCount; $i++) {
            //Get Service Info
            $query = $this->ce()->executePrepared("SELECT * FROM service WHERE id = :id", array(
                'id' => $services[$i]
            ));
            $row = $query->fetch(PDO::FETCH_ASSOC);

            $s_price = $row['price'];
            $s_total = ($s_price - ($voucher / $serviceCount) - ($coupon / $serviceCount)) + $komisi_buyer;
            $grandTotal += $s_total;
        }

        return $grandTotal;
    }

    public function getServicesName()
    {
        $services_detail = array();
        $s = $this->commaServices();
        $query = $this->ce()->executePrepared("SELECT `name` FROM service s WHERE s.id IN $s", array());
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        for ($i = 0; $i < count($rows); $i++) {
            $services_detail[$i] = $rows[$i]['name'];
        }

        return $services_detail;
    }

    public function validateOrder(&$error) {
		$service_is_available = $this->isServicesAvailable();
		if ($service_is_available === FALSE) {
			$error = "Semua atau sebagian produk yang anda pilih telah terjual.";
            return false;
		}
        
        $this->getCoupon($error);
        if ($error) {
            return false;
        }

        if ($this->isPhoneExists()) {
            $error = "Nomor HP sudah terdaftar!";
            return false;
        }

        return true;
    }

    public function getGUID()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid, 12, 4).$hyphen
                .substr($charid, 16, 4).$hyphen
                .substr($charid, 20, 12)
                .chr(125);// "}"
            return $uuid;
        }
    }

    public function generateToken()
    {
        $a = strtoupper(uniqid()) . '|' . $this->getGUID() . '|'. time();
        $b = base64_encode($a);
        $c = base64_encode(Self::SALT.$b.Self::SALT);
        return $c;
    }

    public function getUniqIdFromToken($token)
    {
        $a = base64_decode($token);
        $b = base64_decode(str_replace(Self::SALT, '', $a));
        return explode('|', $b)[0];
    }

    public function placeAsTemporary() {
        $token = $this->generateToken();
        $this->ce()->executePrepared("INSERT INTO tb_tmp_checkout (`usr_id`, `payment_type`, `token`, `services`, 
                `use_voucher`, `coupon`, `hp`, `lat`, `long`, `address`, `city`, `state`, `country`, `unit_apt`, `address_notes`,
                `platform`, `create_time`, `deleted`
            ) 
            VALUES (:usr_id, :payment_type, :token, :services, :use_voucher, :coupon, :hp, :lat, :long, :address,
                :city, :state, :country, :unit_apt, :address_notes, :platform, :create_time, :deleted
            )", ['usr_id' =>$this->currentUserId(),
                    'payment_type' => $this->payment_type,
                    'token' => $token,
                    'services' =>$this->services,
                    'use_voucher' => $this->use_voucher,
                    'coupon' =>$this->coupon_code,
                    'hp' => $this->address->phone,
                    'lat' =>$this->address->lat,
                    'long' =>$this->address->lng,
                    'address' =>$this->address->address,
                    'city' =>$this->address->city,
                    'state' =>$this->address->state,
                    'country' =>$this->address->country,
                    'unit_apt' =>$this->address->unit_apt,
                    'address_notes' =>$this->address->address_notes,
                    'platform'=> $this->mobile_code,
                    'create_time' => time(),
                    'deleted' => 0
		]);

        return $token;
    }
	
	
	
    public function deleteFromTemporary($token) {
        $this->ce()->executePrepared('DELETE FROM tb_tmp_checkout WHERE token = :token', ['token' => $token]);
    }

    public function placeOrder($cc_id, $payment_params = array()) {
        require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
        require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
		$service_datas = array();
        $engine = $this->ce();

        $services = explode(';;;', $this->services);
        $serviceCount = $this->serviceCount();

        $voucher = $this->getVoucher();
        $err = '';
        $coupon = $this->getCoupon($err);
        $komisi_buyer = $this->getBuyerCommission();
        $komisi_seller = $this->getSellerCommission();
        $grand_total = $this->getGrandTotal();

        $res_user = $this->currentUserInfo();
        $full_name = $res_user['firstname'] . " " . $res_user['lastname'];
        $email = $res_user['email'];
		
		
		$collectData = array(
			'services'					=> $services,
			'serviceCount'				=> $serviceCount,
			'voucher'					=> $voucher,
			'coupon'					=> $coupon,
			'komisi_buyer'				=> $komisi_buyer,
			'komisi_seller'				=> $komisi_seller,
			'grand_total'				=> $grand_total,
			'res_user'					=> $res_user,
			'full_name'					=> $full_name,
			'email'						=> $email,
			
		);
		
		
		
        //Checkout
		$mailC = getMailMessage('orderplacementmanual');
        for ($i = 0; $i < $serviceCount; $i++) {
            //Get Service Info
            $query = $engine->executePrepared("SELECT * FROM service WHERE id = :id", array(
                'id' => $services[$i]
            ));
            $row = $query->fetch(PDO::FETCH_ASSOC);

            $s_price = $row['price'];
            $s_total = ($s_price - ($voucher / $serviceCount) - ($coupon / $serviceCount)) + $komisi_buyer;
            $s_seller = $row['id_user'];
			$cc_id = (is_string($cc_id) || is_numeric($cc_id)) ? sprintf("%s", $cc_id) : '';
			$payment_list_params = array(
				'id_buyer'					=> $this->currentUserId(),
				'id_service' 				=> $services[$i],
				'total'						=> $s_total,
				'price' 					=> $s_price,
                'status' 					=> 0,
				'payment_type' 				=> '0',
				'paid' 						=> '0',
				'zip_credit' 				=> ($voucher / $serviceCount),
				'mobile' 					=> $this->mobile_code,
				'ccid' 						=> $cc_id,
				'date_app' 					=> time(),
				'transfer_key'				=> '',
			);
			$payment_list_params['log_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
			switch (strtolower($this->payment_type)) {
				case 'cc':
					$payment_list_params['payment_type'] = 6;
				break;
				case 'wallet':
					$payment_list_params['payment_type'] = 5;
				break;
				case 'point':
					$payment_list_params['payment_type'] = 4;
				break;
				case 'cash':
					$payment_list_params['payment_type'] = 3;
				break;
				case 'ovo':
					$payment_list_params['payment_type'] = 2;
				break;
				case 'gopay':
					$payment_list_params['payment_type'] = 1;
				break;
				case 'bt':
				default:
					$payment_list_params['payment_type'] = 0;
				break;
			}
			if (isset($payment_params['transaction_id'])) {
				$payment_list_params['transfer_key'] = $payment_params['transaction_id'];
			}
			$payment_list_params['payment_comment'] = '';
			$payment_list_params['refund_comment'] = '';
			
			if (isset($payment_params['payment_code'])) {
				$payment_params['payment_code'] = (is_string($payment_params['payment_code']) ? strtolower($payment_params['payment_code']) : '');
				switch (strtolower($payment_params['payment_code'])) {
					case 'cc':
					
					break;
					case 'gopay':
					case 'ovo':
						$payment_list_params['status'] = 5;
					break;
					case 'bt':
					case 'wallet':
					case 'point':
					default:
						$payment_list_params['status'] = 0;
					break;
				}
			}
			
            //Insert checkout and get order id
            $engine->executePrepared(
                "INSERT INTO payment_list (id_buyer, id_service, total, price, status, payment_type, date, paid, zip_credit, mobile, ccid, qty, date_app, transfer_key, payment_comment, refund_comment, log_datetime)
				VALUES (:id_buyer, :id_service, :total, :price, :status, :payment_type, UNIX_TIMESTAMP(), :paid, :zip_credit, :mobile, :ccid, 1, :date_app, :transfer_key, :payment_comment, :refund_comment, :log_datetime)",
                $payment_list_params 
            );
            //Last id
            $last_id = $engine->getLastID('id');
			
			# UPDATE USER ziprewards
			if (strtolower($payment_params['payment_code']) === strtolower('point')) {
				$sql_string = sprintf("UPDATE %s SET ziprewards = (IFNULL(ziprewards, 0) - :ziprewards) WHERE id = :id", 'users');
				$sql_params = array(
					'ziprewards'	=> (int)$payment_list_params['price'],
					'id'			=> $payment_list_params['id_buyer'],
				);
			} else {
				$sql_string = sprintf("UPDATE %s SET ziprewards = (IFNULL(ziprewards, 0) + :ziprewards) WHERE id = :id", 'users');
				$sql_params = array(
					'ziprewards'	=> (int)$payment_list_params['price'],
					'id'			=> $payment_list_params['id_buyer'],
				);
			}
			$engine->executePrepared($sql_string, $sql_params);
			
            $engine->executePrepared("UPDATE service SET qty = qty-1 WHERE id = :id AND qty > 0", array('id' => $services[$i]));

            //Update End Date F
            $engine->executePrepared("UPDATE service SET enddatef = UNIX_TIMESTAMP(DATE_ADD(CURDATE(), INTERVAL 15 DAY)) WHERE id = :id", array(
                'id' => $services[$i]
            ));

            //Insert Commission
            $engine->executePrepared(
                "INSERT INTO site_commission (id_buyer, id_seller, id_service, id_order, total, price, com_s, com_b, date_c, status) VALUES (:id_buyer, :id_seller, :id_service, :id_order, :total, :price, :com_s, :com_b, UNIX_TIMESTAMP(), status);",
                array(
                    'id_buyer' 					=> $this->currentUserId(),
                    'id_seller' 				=> $s_seller,
                    'id_service' 				=> $services[$i],
                    'id_order' 					=> $last_id,
                    'total' 					=> $s_total,
                    'price' 					=> $s_price,
                    'com_s' 					=> $komisi_seller / $serviceCount,
                    'com_b' 					=> $komisi_buyer / $serviceCount
                )
            );

            //Insert coupon log
            if (!empty($this->coupon_code)) {
                $engine->executePrepared(
                    "INSERT INTO coupon_log (id_buyer, id_order, price, coupon_sum, data, name) VALUES (:id_buyer, :id_order, :price, :coupon_sum, UNIX_TIMESTAMP(), :name);",
                    array(
                        'id_buyer' => $this->currentUserId(),
                        'id_order' => $last_id,
                        'price' => $s_price,
                        'coupon_sum' => $coupon,
                        'name' => $this->coupon_code
                    )
                );
            }

            //insert used zip voucher
            if ($voucher > 0) {
                $engine->executePrepared(
                    "INSERT INTO zip_credits (value, details, created, assigned_to) VALUES (:value, :details, UNIX_TIMESTAMP(), :assigned_to)",
                    array(
                        'value' => -($voucher / $serviceCount),
                        'details' => 'Voucher for order #' . $last_id,
                        'assigned_to' => $this->currentUserId()
                    )
                );
            }

            // Update alamat
            $this->updateAddress();

            //Send Mail per Item

            $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
            $mailC->message = str_replace("#idlisting#", $services[$i], $mailC->message); //Listing
            $mailC->message = str_replace("#buyerpayment#", number_format($s_total, 2, ",", "."), $mailC->message); //Total
            sendMail($email, $mailC->subject, $mailC->message);
			
			# Add to services datas
			array_push($service_datas, $payment_list_params);
        }

        // Insert KP Data
        if ($this->payment_type == Self::PAYMENT_KP) {
            $engine->executePrepared('INSERT INTO kp_payment_data (`order_id`, `updated_at`) VALUES (:order_id, :updated_at)', [
                'order_id' => $this->getUniqIdFromToken($this->token),
                'updated_at' => time()
            ]);
        }
		#############################
		# Insert Payment Data
		#############################
		
		#############################

        if ($this->serviceCount() > 1) {
            $mailC = getMailMessage("orderplacementtotal");

            $mail_listing = "";
            for ($i = 0; $i < count($services) - 1; $i++) {
                if ($i > 0) {
                    $mail_listing = $mail_listing . ", ";
                }
                $mail_listing = $mail_listing . "<a href='" . APP_SERVER_ROOT . "service/viewlisting/" . $services[$i] . "'>Listing " . $services[$i] . "</a>";
            }

            $mailC->message = str_replace("#fullname#", $full_name, $mailC->message); //Fullname
            $mailC->message = str_replace("#totallisting#", $mail_listing, $mailC->message); //Listing
            $mailC->message = str_replace("#totalprice#", "Rp. " . number_format($grand_total, 2, ",", "."), $mailC->message); //Total
            sendMail($email, $mailC->subject, $mailC->message);
        }
		
		# Get service items seller data
		$service_seller_datas = array();
		for ($i = 0; $i < $serviceCount; $i++) {
			//== Get Seller Profile
            $query = $engine->executePrepared("SELECT u.id, u.firstname, u.lastname, u.email, s.price FROM users u, service s WHERE s.id_user = u.id AND s.id = :id", array(
                "id" => $services[$i]
            ));
			
			$res = $query->fetchAll(PDO::FETCH_ASSOC);
			
			$service_seller_datas[] = array(
				'id'						=> $res[0]["id"],
				'full_name'					=> sprintf("%s %s", $res[0]["firstname"], $res[0]["lastname"]),
				'email'						=> $res[0]["email"],
				'price'						=> $res[0]["price"],
				'service_id'				=> $services[$i],
			);
	
		}
		/*
         * Send Mail to Seller
         */
		if (count($service_seller_datas) > 0) {
			$mailC = getMailMessage("orderapprovalmanual");
			foreach ($service_seller_datas as $service_seller_data) {
				$mailC->message = str_replace("#fullname#", $service_seller_data['full_name'], $mailC->message); //Fullname
				$mailC->message = str_replace("#idlisting#", $service_seller_data['service_id'], $mailC->message); //ID Listing
				$mailC->message = str_replace("#price#", number_format($service_seller_data['price'], 2, ",", "."), $mailC->message); //Price Total
				sendMail($service_seller_data['email'], $mailC->subject, $mailC->message);
			}
		}
		// SEND Notification
		/*
		if (count($service_seller_datas) > 0) {
			$gcm = new PushNotificationManager();
			foreach ($service_seller_datas as $service_seller_data) {
				//Get list registered id
				$query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id", array(
					"user_id" => $service_seller_data['id']
				));
				$ids = array();
				$ct = 0;
				while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
					$ids[$ct] = array($row['gcm_regid'], $row['mobile']);
					$ct++;
				}
				$gcm->sendSoldNotification(3, $ids);
			}
		}
        */
		
		return $service_datas;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//----------------------------------------------------------------------
	// Another Utils by imzers@gmail.com
	//----------------------------------------------------------------------
	public function create_payment_with_midtrans($server_mode = 'dev', $transaction_data = array(), $MIDTRANS_APP_SERVER = '') {
		$server_mode = (is_string($server_mode) ? strtolower($server_mode) : 'dev');
		switch ($server_mode) {
			case 'production':
			case 'live':
				$url = 'https://api.midtrans.com/v2';
			break;
			case 'dev':
			case 'sandbox':
			default:
				$url = 'https://api.sandbox.midtrans.com/v2';
			break;
		}
		if (!is_array($transaction_data)) {
			return false;
		}
		
		$transaction_data = json_encode($transaction_data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
		$ch = curl_init();
		$url .= '/charge';
		$headers = array(
			'Content-Type: application/json',
			'Accept: application/json',
			'Authorization: Basic ' . base64_encode($MIDTRANS_APP_SERVER) . ':',
			'X-Request-By: Api.Context',
		);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		
		curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $transaction_data);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseBody = curl_exec($ch);
		if ($responseBody === false) {
			if (strpos('not resolve host', curl_error($ch)) !== FALSE) {
				$responseInt = 0;
			}
		} else {
			$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($responseCode != 504) {
				$responseInt = $responseCode;
			} else {
				$responseInt = 0;
			}
		}
		curl_close($ch);
		return array(
			'code'			=> $responseInt,
			'body'			=> $responseBody,
		);
	}
	
	//-------------------------------
	// Get TMP Data 
	//-------------------------------
	public function get_payment_information_from_tmp_table_by_token($token) {
		$token = (is_string($token) ? sprintf("%s", $token) : '');
		$sql_string = sprintf("SELECT t.* FROM %s AS t WHERE t.token = :token ORDER BY t.create_time DESC LIMIT 1", 'tb_tmp_checkout');
		$sql_params = array(
			'token'				=> $token,
		);
		$sql_query = $this->ce()->executePrepared($sql_string, $sql_params);
		return $sql_query->fetch(PDO::FETCH_ASSOC);
	}
	public function get_payment_information_from_tmp_table_by_pgtrans_pgorder($pg_trans, $pg_order) {
		$pg_trans = ((is_string($pg_trans) || is_numeric($pg_trans)) ? sprintf("%s", $pg_trans) : '');
		$pg_order = ((is_string($pg_order) || is_numeric($pg_order)) ? sprintf("%s", $pg_order) : '');
		
		$sql_string = "SELECT t.* FROM tb_tmp_checkout AS t WHERE (t.pg_transaction_id = :trans_id AND t.pg_order_id = :order_id) ORDER BY t.create_time DESC LIMIT 1";
		$sql_params = array(
			'trans_id'				=> $pg_trans,
			'order_id'				=> $pg_order,
		);
		$sql_query = $this->ce()->executePrepared($sql_string, $sql_params);
		return $sql_query->fetch(PDO::FETCH_ASSOC);
	}
	
	
	
	
	
	
	
	
	
}














