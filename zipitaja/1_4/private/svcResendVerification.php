<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/15/2016
 * Time: 5:41 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "api_key" => true,
    )
);

$mode = $engine->getPOSTField("param_mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];
$my_fullname = $_SESSION["fullname"];
$my_mail = $_SESSION["email"];

//Send verification email
$query = $engine->executePrepared("SELECT * FROM users WHERE id = :id",
    array(
        "id" => $my_id
    )
);
$row = $query->fetch(PDO::FETCH_ASSOC);

$activation_code = $row["activate_code"];

$mailC = getMailMessage("signup");
$mailC->message = str_replace("#fullname#", $my_fullname, $mailC->message);
$mailC->message = str_replace("#veremailcode#", APP_SERVER_ROOT . "user/profile/verifyEmail/$activation_code/$my_id/1", $mailC->message);
if (sendMail($my_mail, $mailC->subject, $mailC->message))
    $engine->setSingleLineResponse(1);
else
    $engine->setErrorResponse("Email verifikasi gagal dikirim, silakan coba kembali");

/*
 * Send Response
 */
$engine->sendResponse();