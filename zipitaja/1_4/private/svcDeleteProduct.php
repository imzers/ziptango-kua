<?php
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";

$engine = new Engine();
//Get All POST Value
$engine->setPostField(
    array(
        "param_service" => true,
        "api_key" => true,
    )
);

$param_service = $engine->getPOSTField("param_service");

//Check API Key
$engine->checkAPIKeyPair();

//Check session is valid
$engine->checkCurrentSession();

//Core Operation
//DELETE Product                        
$query = $engine->executePrepared("DELETE FROM service WHERE id = :id",
    array(
        "id" => $param_service,
    ));

//Create Response
$engine->setErrorResponse("Produk gagal dihapus, Silakan coba kembali!");
if ($query) {
    $engine->setSingleLineResponse("1");
}

//Send Result
$engine->sendResponse();
