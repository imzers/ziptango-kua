<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/20/2016
 * Time: 1:48 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "api_key" => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
$detUser = $engine->executePrepared("SELECT * FROM users WHERE id = :id", array("id" => $_SESSION["id"]));
$result = $detUser->fetchAll(PDO::FETCH_ASSOC);

$icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH;

$image = $icon_path . $result[0]["icon"];
$username = $result[0]["username"];
$email = $result[0]["email"];

$detVoucher = $engine->executePrepared("
SELECT
    IFNULL(SUM(value),0) AS credit
FROM
    zip_credits
WHERE
    deleted = 0 AND
    assigned_to = :assigned_to",
    array(
        "assigned_to" => $_SESSION["id"]
    )
);
$result = $detVoucher->fetchAll(PDO::FETCH_ASSOC);

$my_id = $_SESSION["id"];
$e_filter = '';
if ($my_id > 1)
    $e_filter = 'AND id_user_send = 1';

$chatUnread = $engine->executePrepared("
SELECT
    SUM(IF(isread = 1, 0, 1)) AS count
FROM
    message
WHERE
    id_user_get = :my_id $e_filter",
    array(
        "my_id" => $my_id
    ));
$unread = $chatUnread->fetch(PDO::FETCH_ASSOC);

$voucher = $result[0]["credit"];

$active = $engine->executePrepared("SELECT active FROM users WHERE id = :id", array(
    "id" => $_SESSION["id"]
));
$row = $active->fetch(PDO::FETCH_ASSOC);

$engine->setTranslateDataResponse(array(
    array(
        "icon" => $image,
        "username" => $username,
        "email" => $email,
        "unread_chat" => $unread['count'],
        "credit" => $voucher,
        "verified" => $row['active']
    )
));

/*
 * Send Result
 */
$engine->sendResponse();