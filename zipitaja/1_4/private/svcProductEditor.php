<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

define("MODE_INSERT", "1");
define("MODE_UPDATE", "2");
$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true,
        "param_service" => true,
        "param_category" => true,
        "param_brand" => true,
        "param_color" => true,
        "param_type" => true,
        "param_name" => true,
        "param_date" => true,
        "param_price" => true,
        "param_details" => true,
        "param_lat" => true,
        "param_long" => true,
        "param_location_name" => true,
        "param_address" => true,
        "param_condition" => true,
        "param_payment_kp" => false,
        "param_pic0" => false, // pic_n SET FILE for add or replace, '-' for DELETE, else IGNORE
        "param_pic1" => false,
        "param_pic2" => false,
        "param_pic3" => false,
        "param_pic4" => false,
        "param_pic5" => false,
        "param_pic6" => false,
        "param_pic7" => false,
        "param_pic8" => false,
        "param_pic9" => false,
        "param_pic10" => false,
        "param_mobile" => true,
        "api_key" => true
    )
);
$param_mode = $engine->getPOSTField("param_mode");
$service = $engine->getPOSTField("param_service");
$param_name = $engine->getPOSTField("param_name");
$param_category = $engine->getPOSTField("param_category");
$param_brand = $engine->getPOSTField("param_brand");
if (!$param_brand) {
    $param_brand = 0;
}
$param_color = $engine->getPOSTField("param_color");
if (!$param_color) {
    $param_color = 0;
}
$param_type = $engine->getPOSTField("param_type");
if (!$param_type) {
    $param_type = 0;
}
$param_date = $engine->getPOSTField("param_date");
$param_price = $engine->getPOSTField("param_price");
$param_details = $engine->getPOSTField("param_details");
$param_lat = $engine->getPOSTField("param_lat");
$param_long = $engine->getPOSTField("param_long");
$param_location_name = $engine->getPOSTField("param_location_name");
$param_address = $engine->getPOSTField("param_address");
$param_condition = $engine->getPOSTField("param_condition");
$param_payment_kp = $engine->getPOSTField("param_payment_kp");
if (!$param_payment_kp) {
    $param_payment_kp = 0;
}
$param_mobile = $engine->getPOSTField("param_mobile");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
/*
 * Core Operation
 */
//Check Name is Exists
if ($param_mode == MODE_INSERT) {
    $query = $engine->executePrepared(
        "
        SELECT 
            count(id) AS result 
        FROM 
            service s 
        WHERE 
            s.name = :name AND 
            id_user = :id_user",
        array(
            "name" => $param_name,
            "id_user" => $engine->getPOSTField("param_user")
        )
    );
} else {
    $query = $engine->executePrepared(
        "
        SELECT 
            count(id) AS result 
        FROM 
            service s 
        WHERE 
            s.name = :name AND 
            id_user = :id_user AND 
            id != :id",
        array(
            "name" => $param_name,
            "id_user" => $engine->getPOSTField("param_user"),
            "id" => $service
        )
    );
}
$res = $query->fetch(PDO::FETCH_ASSOC);
if ($res["result"] >= 1) {
    die($engine->createResponse(Engine::codError, "Anda sudah memiliki produk dengan nama $param_name!"));
}

/** Uplopad Picture */
$pictures = array();
if ($param_mode == MODE_INSERT) {
    $pictures = array(
        0 => handleUploadFile("param_pic0", APP_IMG_SERVICE_PATH, true, true),
        1 => handleUploadFile("param_pic1", APP_IMG_SERVICE_PATH, true, true),
        2 => handleUploadFile("param_pic2", APP_IMG_SERVICE_PATH, true, true),
        3 => handleUploadFile("param_pic3", APP_IMG_SERVICE_PATH, true, true),
        4 => handleUploadFile("param_pic4", APP_IMG_SERVICE_PATH, true, true),
        5 => handleUploadFile("param_pic5", APP_IMG_SERVICE_PATH, true, true),
        6 => handleUploadFile("param_pic6", APP_IMG_SERVICE_PATH, true, true),
        7 => handleUploadFile("param_pic7", APP_IMG_SERVICE_PATH, true, true),
        8 => handleUploadFile("param_pic8", APP_IMG_SERVICE_PATH, true, true),
        9 => handleUploadFile("param_pic9", APP_IMG_SERVICE_PATH, true, true)
    );
} else {
    $query = $engine->executePrepared(
        "
    SELECT 
        pictures 
    FROM 
        service s 
    WHERE 
        s.id = :id",
        array(
            "id" => $service
        )
    );
    $res = $query->fetch(PDO::FETCH_ASSOC);
    $pictures = unserialize($res["pictures"]);

    $pictures = array(
        0 => $_POST["param_pic0"] == "-" ? "" : (!isset($_FILES["param_pic0"]) ? $pictures[0] : handleUploadFile("param_pic0", APP_IMG_SERVICE_PATH, true, true)),
        1 => $_POST["param_pic1"] == "-" ? "" : (!isset($_FILES["param_pic1"]) ? $pictures[1] : handleUploadFile("param_pic1", APP_IMG_SERVICE_PATH, true, true)),
        2 => $_POST["param_pic2"] == "-" ? "" : (!isset($_FILES["param_pic2"]) ? $pictures[2] : handleUploadFile("param_pic2", APP_IMG_SERVICE_PATH, true, true)),
        3 => $_POST["param_pic3"] == "-" ? "" : (!isset($_FILES["param_pic3"]) ? $pictures[3] : handleUploadFile("param_pic3", APP_IMG_SERVICE_PATH, true, true)),
        4 => $_POST["param_pic4"] == "-" ? "" : (!isset($_FILES["param_pic4"]) ? $pictures[4] : handleUploadFile("param_pic4", APP_IMG_SERVICE_PATH, true, true)),
        5 => $_POST["param_pic5"] == "-" ? "" : (!isset($_FILES["param_pic5"]) ? $pictures[5] : handleUploadFile("param_pic5", APP_IMG_SERVICE_PATH, true, true)),
        6 => $_POST["param_pic6"] == "-" ? "" : (!isset($_FILES["param_pic6"]) ? $pictures[6] : handleUploadFile("param_pic6", APP_IMG_SERVICE_PATH, true, true)),
        7 => $_POST["param_pic7"] == "-" ? "" : (!isset($_FILES["param_pic7"]) ? $pictures[7] : handleUploadFile("param_pic7", APP_IMG_SERVICE_PATH, true, true)),
        8 => $_POST["param_pic8"] == "-" ? "" : (!isset($_FILES["param_pic8"]) ? $pictures[8] : handleUploadFile("param_pic8", APP_IMG_SERVICE_PATH, true, true)),
        9 => $_POST["param_pic9"] == "-" ? "" : (!isset($_FILES["param_pic9"]) ? $pictures[9] : handleUploadFile("param_pic9", APP_IMG_SERVICE_PATH, true, true))
    );
}

//Remove empty image
$tmpPictures = array();
for ($i = 0; $i < count($pictures); $i++) {
    if (!empty($pictures[$i])) {
        $tmpPictures[count($tmpPictures)] = $pictures[$i];
    }
}
$pictures = $tmpPictures;

/** Write to Database */
if ($param_mode == MODE_INSERT) {
    //I    nsert Product
    $query = $engine->executePrepared(
        "
    INSERT INTO service
        (id_user, category_id, brand_id, color_id, bagtype_id, name, activate, startdatef, price, datepost, details, lat_1, lng_1, street_1, city_1, pictures, for_wanted_id, can_carry, can_buy, conditions, mobile, qty, payment_kp_accepted)
    VALUES
        (:id_user, :category_id, :brand_id, :color_id, :bagtype_id, :name, 1, :startdatef, :price, UNIX_TIMESTAMP(), :details, :lat_1, :lng_1, :street_1, :city_1, :pictures, NULL, 0, 0, :conditions, :mobile, 1, :payment_kp_accepted)",
        array(
            "id_user" => $_SESSION["id"],
            "category_id" => $param_category,
            'brand_id' => $param_brand,
            'color_id' => $param_color,
            'bagtype_id' => $param_type,
            "name" => $param_name,
            "startdatef" => $param_date,
            "price" => $param_price,
            "details" => $param_details,
            "lat_1" => $param_lat,
            "lng_1" => $param_long,
            "city_1" => $param_location_name,
            "street_1" => $param_address,
            "conditions" => $param_condition,
            "payment_kp_accepted" => $param_payment_kp,
            "pictures" => serialize($pictures),
            "mobile" => $param_mobile
        )
    );

    //Send Mail
    $id = $engine->getLastID("id");
    $mailC = getMailMessage("livelist");
    $mailC->message = str_replace("#fullname#", $_SESSION["fullname"], $mailC->message);
    $mailC->message = str_replace("#idlisting#", $id, $mailC->message);
    sendMail($_SESSION["email"], $mailC->subject, $mailC->message);
} else {
    //Update Product
    $query = $engine->executePrepared(
        "
    UPDATE service SET
        category_id = :category_id,
        brand_id = :brand_id,
        color_id = :color_id,
        bagtype_id = :bagtype_id,
        name = :name,
        startdatef = :startdatef,
        price = :price,
        activate = 1,
        details = :details,
        lat_1 = :lat_1,
        lng_1 = :lng_1,
        city_1 = :city_1,
        street_1 = :street_1,
        conditions = :conditions,
        payment_kp_accepted = :payment_kp_accepted,
        pictures = :pictures,
        datepost = UNIX_TIMESTAMP()
    WHERE
        id = :id",
        array(
            "id" => $service,
            "category_id" => $param_category,
            'brand_id' => $param_brand,
            'color_id' => $param_color,
            'bagtype_id' => $param_type,
            "name" => $param_name,
            "startdatef" => $param_date,
            "price" => $param_price,
            "details" => $param_details,
            "lat_1" => $param_lat,
            "lng_1" => $param_long,
            "city_1" => $param_location_name,
            "street_1" => $param_address,
            "conditions" => $param_condition,
            "payment_kp_accepted" => $param_payment_kp,
            "pictures" => serialize($pictures)
        )
    );
}

//Create Response
$engine->setErrorResponse("Produk gagal disimpan, Silakan coba kembali!");
if ($query) {
    $engine->setSingleLineResponse("1");
}

//Send Result
$engine->sendResponse();
