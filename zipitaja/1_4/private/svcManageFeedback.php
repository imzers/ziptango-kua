<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/MCrypt.php');

defined('AS_BUYER') OR define('AS_BUYER', 1);
defined('AS_SELLER') OR define('AS_SELLER', 2);




$engine = new Engine();

$view_feedback = "view_private_svcgetmanagefeedback";

function isFeedbackInserted($vf, $payment_id) {
	global $engine;
	
    $query = $engine->executePrepared("SELECT * FROM {$vf} AS t WHERE t.payment_id = :payment_id", array(
            "payment_id" => $payment_id
        )
    );
    $row = $query->fetch(PDO::FETCH_ASSOC);
    if ($row["inserted"] == 1) {
        return (int)$row["id"];
    } else {
        return 0;
	}
}

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_mode" => true, // 1 = leave, 2 = view, 3 = insert
        "api_key" => true
    )
);

$my_id = (isset($_SESSION["id"]) ? $_SESSION["id"] : '');
$mode = $engine->getPOSTField("param_mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

if (isset($_GET['pairingimzers'])) {
	$engine->setInitSession('268', 'sebanyam@gmail.com', 'Sebanyam', 'ss');
}


/*
 * Check session is valid
 */
$engine->checkCurrentSession();

if ($mode == 1) {
    $to_seller = array();
    $to_buyer = array();

    //To Seller
    $query = $engine->executePrepared("SELECT * FROM view_private_svcgetmanagefeedback AS t WHERE t.buyer_write = 0 AND t.id_buyer = :me",
        array(
            "me" => $my_id
        )
    );
    $to_seller = $query->fetchAll(PDO::FETCH_ASSOC);
	foreach ($to_seller as &$seller_feedback_data) {
		///////////////////////////////////////////////////////
		/////////////// BEGIN - sellers rating ////////////////
		///////////////////////////////////////////////////////
		$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
		$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
		$pointsPerCompleteOrder = $result['value'];

		$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
		$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
		$pointsPerCanceledOrder = $result['value'];

		// Number of complete orders for this year
		$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
		$noOfCompleteOrdersThisYear = $engine->executePrepared("
						SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
						FROM service
						INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
						WHERE service.id_user = :id
							AND date <> ''
							AND date >= :beginningOfYear",
						array(
							'id' => $seller_feedback_data['id_seller'],
							'beginningOfYear' => $beginningOfYear
						)
					);
		$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
		$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

		// Number of canceled orders for this year
		$noOfCanceledOrdersThisYear = $engine->executePrepared("
						SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
						FROM service
						INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
						WHERE service.id_user = :id
							AND date <> ''
							AND date >= :beginningOfYear",
						array(
							'id' => $seller_feedback_data['id_seller'],
							'beginningOfYear' => $beginningOfYear
						)
					);
		$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
		$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

		$rating = ($noOfCompleteOrdersThisYear * $pointsPerCompleteOrder) + ($noOfCanceledOrdersThisYear * $pointsPerCanceledOrder);

		// Star rating
		$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
		$starRatingLastYear = $engine->executePrepared("
						SELECT last_year_star_rating
						FROM users
						WHERE id = :id",
						array(
							'id' => $seller_feedback_data['id_seller']
						)
					);
		$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
		$starRatingLastYear = $result['last_year_star_rating'];

		if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
		$starRatingThisYear = 'Bronze';
		if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
		if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
		if ($rating > 400) $starRatingThisYear = 'Diamond';

		if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) {
			$starRating = $starRatingThisYear;
		} else {
			$starRating = $starRatingLastYear;
		}
		///////////////////////////////////////////////////////
		///////////////////////// END /////////////////////////
		///////////////////////////////////////////////////////
		// Push Rating and medalRating
		$seller_feedback_data['rating'] = $rating;
		$seller_feedback_data['medalRating'] = $starRating;
	}
    //To Buyer
    $query = $engine->executePrepared("SELECT * FROM view_private_svcgetmanagefeedback AS t WHERE t.seller_write = 0 AND t.id_seller = :me",
        array(
            "me" => $my_id
        )
    );
    $to_buyer = $query->fetchAll(PDO::FETCH_ASSOC);
	foreach ($to_buyer as &$buyer_feedback_data) {
		///////////////////////////////////////////////////////
		/////////////// BEGIN - sellers rating ////////////////
		///////////////////////////////////////////////////////
		$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
		$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
		$pointsPerCompleteOrder = $result['value'];

		$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
		$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
		$pointsPerCanceledOrder = $result['value'];

		// Number of complete orders for this year
		$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
		$noOfCompleteOrdersThisYear = $engine->executePrepared("
						SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
						FROM service
						INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
						WHERE service.id_user = :id
							AND date <> ''
							AND date >= :beginningOfYear",
						array(
							'id' => $buyer_feedback_data['id_buyer'],
							'beginningOfYear' => $beginningOfYear
						)
					);
		$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
		$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

		// Number of canceled orders for this year
		$noOfCanceledOrdersThisYear = $engine->executePrepared("
						SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
						FROM service
						INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
						WHERE service.id_user = :id
							AND date <> ''
							AND date >= :beginningOfYear",
						array(
							'id' => $buyer_feedback_data['id_buyer'],
							'beginningOfYear' => $beginningOfYear
						)
					);
		$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
		$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

		$rating = ($noOfCompleteOrdersThisYear * $pointsPerCompleteOrder) + ($noOfCanceledOrdersThisYear * $pointsPerCanceledOrder);

		// Star rating
		$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
		$starRatingLastYear = $engine->executePrepared("
						SELECT last_year_star_rating
						FROM users
						WHERE id = :id",
						array(
							'id' => $buyer_feedback_data['id_buyer']
						)
					);
		$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
		$starRatingLastYear = $result['last_year_star_rating'];

		if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
		$starRatingThisYear = 'Bronze';
		if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
		if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
		if ($rating > 400) $starRatingThisYear = 'Diamond';

		if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) {
			$starRating = $starRatingThisYear;
		} else {
			$starRating = $starRatingLastYear;
		}
		///////////////////////////////////////////////////////
		///////////////////////// END /////////////////////////
		///////////////////////////////////////////////////////
		// Push Rating and medalRating
		$buyer_feedback_data['rating'] = $rating;
		$buyer_feedback_data['medalRating'] = $starRating;
	}
    //Send Response
    $engine->setSingleLineResponse(array(
            "to_seller" => $to_seller,
            "to_buyer" => $to_buyer
        )
    );

} else if ($mode == 2) {
    $as_seller = array();
    $as_buyer = array();

    //As Seller
    $query = $engine->executePrepared("SELECT * FROM view_private_svcgetmanagefeedback AS t WHERE t.seller_write = 1 AND t.id_seller = :me ORDER BY t.id DESC",
        array(
            "me" => $my_id
        )
    );
    $as_seller = $query->fetchAll(PDO::FETCH_ASSOC);

    //As Buyer
    $query = $engine->executePrepared("SELECT * FROM view_private_svcgetmanagefeedback AS t WHERE t.buyer_write = 1 AND t.id_buyer = :me ORDER BY t.id DESC",
        array(
            "me" => $my_id
        )
    );
    $as_buyer = $query->fetchAll(PDO::FETCH_ASSOC);
	
	if (is_array($as_buyer) && (count($as_buyer) > 0)) {
		foreach ($as_buyer as &$feedback_data) {
			///////////////////////////////////////////////////////
			/////////////// BEGIN - sellers rating ////////////////
			///////////////////////////////////////////////////////
			$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
			$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
			$pointsPerCompleteOrder = $result['value'];

			$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
			$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
			$pointsPerCanceledOrder = $result['value'];

			// Number of complete orders for this year
			$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
			$noOfCompleteOrdersThisYear = $engine->executePrepared("
							SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
							FROM service
							INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
							WHERE service.id_user = :id
								AND date <> ''
								AND date >= :beginningOfYear",
							array(
								'id' => $feedback_data['id_seller'],
								'beginningOfYear' => $beginningOfYear
							)
						);
			$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
			$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

			// Number of canceled orders for this year
			$noOfCanceledOrdersThisYear = $engine->executePrepared("
							SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
							FROM service
							INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
							WHERE service.id_user = :id
								AND date <> ''
								AND date >= :beginningOfYear",
							array(
								'id' => $feedback_data['id_seller'],
								'beginningOfYear' => $beginningOfYear
							)
						);
			$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
			$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

			$rating = ($noOfCompleteOrdersThisYear * $pointsPerCompleteOrder) + ($noOfCanceledOrdersThisYear * $pointsPerCanceledOrder);

			// Star rating
			$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
			$starRatingLastYear = $engine->executePrepared("
							SELECT last_year_star_rating
							FROM users
							WHERE id = :id",
							array(
								'id' => $feedback_data['id_seller']
							)
						);
			$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
			$starRatingLastYear = $result['last_year_star_rating'];

			if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
			$starRatingThisYear = 'Bronze';
			if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
			if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
			if ($rating > 400) $starRatingThisYear = 'Diamond';

			if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) {
				$starRating = $starRatingThisYear;
			} else {
				$starRating = $starRatingLastYear;
			}
			///////////////////////////////////////////////////////
			///////////////////////// END /////////////////////////
			///////////////////////////////////////////////////////
			// Push Rating and medalRating
			$feedback_data['rating'] = $rating;
			$feedback_data['medalRating'] = $starRating;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
    //Send Response
    $engine->setSingleLineResponse(array(
            "as_seller" => $as_seller,
            "as_buyer" => $as_buyer
        )
    );

} else if ($mode == 3) {
    $engine->setPostField(
        array(
            "param_payment_id" => true,
            "param_as" => true, // 1 = buyer, 2 = seller
            "param_id_lawan" => true,
            "param_service" => true,
            "param_rate" => true,
            "param_text" => true
        )
    );

    $param_payment_id = $engine->getPOSTField("param_payment_id");
    $param_as = $engine->getPOSTField("param_as");
    $param_id_lawan = $engine->getPOSTField("param_id_lawan");
    $param_service = $engine->getPOSTField("param_service");
    $param_rate = $engine->getPOSTField("param_rate");
    $param_text = $engine->getPOSTField("param_text");

    $f_rate = "rep_s";
    $f_text = "text_s";
    if ($param_as == AS_BUYER) {
        $f_rate = "rep_b";
        $f_text = "text_b";
    }

    $id_feedback = isFeedbackInserted($view_feedback, $param_payment_id);
    if ($id_feedback > 0) {
		//Update
		$sql = sprintf("UPDATE feedback SET id_seller = :id_seller, id_buyer = :id_buyer, %s = :rate, %s =:text WHERE id = :id",
			$f_rate,
			$f_text
		);
		$sql_params = array(
			"id_seller" => $param_as == AS_SELLER ? $my_id : $param_id_lawan,
			"id_buyer" => $param_as == AS_BUYER ? $my_id : $param_id_lawan,
			"rate" => $param_rate,
			"text" => $param_text,
			"id" => $id_feedback
		);
    } else {
        //Insert
		$sql = sprintf("INSERT INTO feedback (id_seller, id_buyer, id_service, id_payment, %s, %s, data_create) VALUES (:id_seller, :id_buyer, :id_service, :id_payment, :rate, :text, UNIX_TIMESTAMP())",
			$f_rate,
			$f_text
		);
		$sql_params = array(
			"id_seller" => $param_as == AS_SELLER ? $my_id : $param_id_lawan,
			"id_buyer" => $param_as == AS_BUYER ? $my_id : $param_id_lawan,
			"id_service" => $param_service,
			"id_payment" => $param_payment_id,
			"rate" => $param_rate,
			"text" => $param_text
		);
    }
	$query = $engine->executePrepared($sql, $sql_params);
    $engine->setSingleLineResponse(1);
}

/*
 * Send Response
 */
$engine->sendResponse();