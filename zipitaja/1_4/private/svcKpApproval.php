<?php

require_once "../core/Engine.php";
require_once "../core/SMTPMail.php";

$engine = new Engine();

// Get Posted Parameters
$orderId = $_POST['OrderID'];
$approvalResult = $_POST['ApprovalResult'];
$approvalDate = $_POST['ApprovalDate'];
$poNo = $_POST['PONo'];
$poDate = $_POST['PODate'];
$shippingDetail = $_POST['ShippingDetail'];

$status = 0;

$failedDetail = "";

// check param
if (!isset($orderId) || !isset($approvalResult) || !isset($approvalDate) || !isset($poNo) || !isset($poDate)) {
    $status = 2;
    $failedDetail = "Bad Request";
}

// Get order data
if ($status == 0) {
    $query = $engine->executePrepared('SELECT * FROM kp_payment_data WHERE order_id = :order_id', ['order_id' => $orderId]);
    $order = $query->fetch(PDO::FETCH_ASSOC);

    // Check is order available
    if (!$order) {
        $status = 1;
    }
}

// Write data if order is available
if ($status == 0) {
    $d = new DateTime($approvalDate);
    $query = $engine->executePrepared('UPDATE kp_payment_data SET approval_status = :approval_status, approval_date = :approval_date, po_number = :po_number, po_date = :po_date, kp_shipping_detail = :kp_shipping_detail WHERE order_id = :order_id', [
        'approval_status' => $approvalResult,
        'approval_date' => $d->getTimestamp(),
        'order_id' => $orderId,
        'po_number' => $poNo,
        'po_date' => $poDate,
        'kp_shipping_detail' => $shippingDetail
    ]);
    if (!$query) {
        $status = 2;
        $failedDetail = "Query Error";
    }
}

// Return Response
$description = 'Failed';
if ($status == 0) {
    $description = 'Success';
} elseif ($status == 1) {
    $description = 'ID not Found';
}

$response_params = [
    'OrderId' => $orderId ? $orderId : 'null',
    'Status'  => $status,
    'ApprovalDate' => $approvalDate ? $approvalDate : date('Y-m-d H:i:s'),
    'Description' => $description . ($failedDetail ? " (" . $failedDetail . ")" : '')
];

// Send notification to admin
if ($status == 0) {
    $conf = $engine->executePrepared('SELECT * FROM kp_config', [])->fetch(PDO::FETCH_ASSOC);
    if ($approvalResult == 'A') {
        sendMail($conf['admin_email'], 'Approval Kredit Plus', 'Pengajuan Kredit Plus dengan nomor order ' . $orderId . ' diterima.');
    } else {
        sendMail($conf['admin_email'], 'Approval Kredit Plus', 'Pengajuan Kredit Plus dengan nomor order ' . $orderId . ' ditolak.');
    }
}

//generate response
$response = '';
foreach ($response_params as $key => $value) {
    $response .= "<$key>$value</$key>";
}

$template = '<?xml version="1.0" encoding="utf-8"?>
	<response>' . $response . '</response>'; header("Content-Type: text/xml"); echo $template;
