<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/3/2016
 * Time: 2:32 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "param_service" => true,
        "param_available" => true, //1 = Avalibale, 0 = Not Avalibale
        "api_key" => true,
    )
);

$param_service = $engine->getPOSTField("param_service");
$param_available = $engine->getPOSTField("param_available");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

/*
 * Core Operation
 */
if ($param_available == 1) {
    //BUY TO SOLD
    // $query = $engine->executePrepared("DELETE FROM payment_list WHERE id_service = :id_service AND id_buyer = :id_buyer",
    //    array(
    //        "id_service" => $param_service,
    //        "id_buyer" => $my_id
    //    ));
    $query = $engine->executePrepared("UPDATE service SET force_sold = 1 WHERE id = :id", array(
        'id' => $param_service
    ));

    //Create Response
    $engine->setErrorResponse("Produk gagal disimpan!");
    if ($query) {
        $engine->setSingleLineResponse("1");
    }
    $engine->setErrorResponse($param_available);

} else {
    //BUY TO SOLD
    // $query = $engine->executePrepared("INSERT INTO payment_list (id_service, id_buyer, payment_type, date) VALUES (:id_service, :id_buyer, 1, UNIX_TIMESTAMP())",
    //     array(
    //         "id_service" => $param_service,
    //         "id_buyer" => $my_id
    //     ));
    $query = $engine->executePrepared("UPDATE service SET force_sold = 0 WHERE id = :id", array(
        'id' => $param_service
    ));

    //Create Response
    $engine->setErrorResponse("Produk gagal disimpan!");
    if ($query) {
        $engine->setSingleLineResponse("1");
    }

}

/*
 * Send Response
 */
$engine->sendResponse();
