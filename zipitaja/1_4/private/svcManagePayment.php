<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 12:01 AM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "mode" => true, // 1 = Withdrawal, 2 = ZIP Bucks, 2 = Payment as Seller, 4 = Payment as Buyer
        "api_key" => true
    )
);
$mode = $engine->getPOSTField("mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

//SQL Payment
$sql_payment = "
            SELECT 
            	p.id, 
            	s.id AS id_service, 
            	s.name, 
            	s.city_1 AS location, 
            	s.startdatef AS end_date, 
            	s.enddatef AS delivery_date, 
            	p.date,
            	u.username, 
            	u.id AS id_user, 
            	p.status,
            	p.total AS price,
            	p.id_buyer,
            	s.id_user AS id_seller
            FROM 
            	service s, payment_list p, users u
            WHERE 
	            s.id = p.id_service AND 
            	u.id = p.id_buyer AND
            	p.status IN (4, 5)
            ORDER BY p.id DESC";

if ($mode == 1) {

} else if ($mode == 2) {
    $query = $engine->executePrepared("
        SELECT
            id,
            value,
            details,
            created AS date
        FROM
            zip_credits
        WHERE
            assigned_to = :my_id
    ",
        array(
            "my_id" => $my_id
        )
    );

    $engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

} else if ($mode == 3) {
    $query = $engine->executePrepared("SELECT * FROM ($sql_payment) t WHERE t.id_buyer != :my_id AND t.id_seller = :my_id",
        array(
            "my_id" => $my_id
        )
    );

    $engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

} else if ($mode == 4) {
    $query = $engine->executePrepared("
            SELECT 
            	p.id, 
            	s.id AS id_service, 
            	s.name, 
            	s.city_1 AS location, 
            	s.startdatef AS end_date, 
            	s.enddatef AS delivery_date, 
            	p.date,
            	u.username, 
            	u.id AS id_user, 
            	p.status,
            	p.total AS price,
            	p.id_buyer,
            	s.id_user AS id_seller
            FROM 
            	service s, payment_list p, users u
            WHERE 
	            s.id = p.id_service AND 
            	u.id = s.id_user AND 
            	p.id_buyer = :my_id AND
            	p.status IN (4, 5)
            ORDER BY p.id DESC
    ",
        array(
            "my_id" => $my_id
        )
    );

    $engine->setTranslateDataResponse($query->fetchAll(PDO::FETCH_ASSOC));

}

/*
 * Send Response
 */
$engine->sendResponse();