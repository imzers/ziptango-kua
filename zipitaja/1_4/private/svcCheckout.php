<?php

require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";
require_once "../core/PushNotificationManager.php";
require_once "cc_util.php";
require_once "checkout_util.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'param_services' => true,
        'param_use_voucher' => true,
        'param_coupon' => false,
        'param_hp' => true,
        'param_lat' => true,
        'param_long' => true,
        'param_address' => true,
        'param_city' => true,
        'param_state' => true,
        'param_country' => true,
        'param_unit_apt' => true,
        'param_address_notes' => true,
        'param_mobile' => true,
        'api_key' => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

$param_services = $engine->getPOSTField('param_services');
$param_use_voucher = $engine->getPOSTField('param_use_voucher');
$param_coupon = $engine->getPOSTField('param_coupon');
$param_mobile = $engine->getPOSTField('param_mobile');

$param_hp = $engine->getPOSTField('param_hp');
$param_lat = $engine->getPOSTField('param_lat');
$param_long = $engine->getPOSTField('param_long');
$param_address = $engine->getPOSTField('param_address');
$param_address = (is_string($param_address) ? substr($param_address, 0, 128) : '');
$param_city = $engine->getPOSTField('param_city');
$param_state = $engine->getPOSTField('param_state');
$param_country = $engine->getPOSTField('param_country');
$param_unit_apt = $engine->getPOSTField('param_unit_apt');
$param_address_notes = $engine->getPOSTField('param_address_notes');



$co = new CO(CO::PAYMENT_BT, null, $param_services, $param_use_voucher, $param_coupon, $param_mobile, new CustAddress(
    $param_address,
    $param_city,
    $param_state,
    $param_country,
    $param_hp,
    $param_lat,
    $param_long,
    $param_unit_apt,
    $param_address_notes
));




$error = '';
if (!$co->validateOrder($error)) {
    die($engine->createResponse(Engine::codError, $error));
}

$co->updateAddress();
$co->placeOrder(0);

$engine->setSingleLineResponse(0);
$engine->sendResponse();