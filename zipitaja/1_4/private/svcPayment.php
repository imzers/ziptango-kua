<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 21-Jul-16
 * Time: 12:05 AM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/SMTPMail.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "mode" => true, // 1 = Info, 2 = Transfer
        "api_key" => true
    )
);
$mode = $engine->getPOSTField("mode");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();
$my_id = $_SESSION["id"];

if ($mode == 1) {
    //Get Voucher
    $query = $engine->executePrepared("
SELECT
    SUM(value) AS credit
FROM
    zip_credits
WHERE
    deleted = 0 AND
    assigned_to = :assigned_to",
        array(
            "assigned_to" => $my_id
        )
    );
    $row = $query->fetch(PDO::FETCH_ASSOC);
    $credit = $row["credit"];

    //Get Balance
    $balance = getBalance($my_id);

    $engine->setSingleLineResponse(json_encode(array(
                "credit" => $credit,
                "balance" => $balance
            )
        )
    );
} else if ($mode == 2) {
    $balance = getBalance($my_id);
    if ($balance <= 0)
        die($engine->createResponse(Engine::codError, "Balance akun anda Rp0"));

    $engine->executePrepared("
INSERT INTO manual_payout_request 
    (user_id, amount, method, date_requested, status)
VALUES 
    (:user_id, :amount, 1, unix_timestamp(), 0)", array(
            "user_id" => $my_id,
            "amount" => $balance
        )
    );

    $engine->executePrepared("
INSERT INTO payout_info 
    (id_user, paypal_email)
VALUES
    (:id_user, :paypal_email);", array(
            "id_user" => $my_id,
            "paypal_email" => $_SESSION["email"]
        )
    );

    $engine->setSingleLineResponse(1);

}

/*
 * Send Response
 */
$engine->sendResponse();

function getBalance($id)
{
    $engine = new Engine();

    $total_num = 0;

    $timecheck = 0;
    $query = $engine->executePrepared("SELECT * FROM common_admin WHERE name = 'timecheck'", array());
    if ($query->rowCount()) {
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $timecheck = $row["value_2"];
    }

    $now_date = time();
    $the_days_time = $timecheck * 60 * 60;

    $query = $engine->executePrepared("
SELECT Coalesce(payment_list.total, 0)    AS tprice,
       Coalesce(site_commission.com_s, 0) AS coms,
       Coalesce(site_commission.com_b, 0) AS comb,
       Coalesce(website_pay.pay, 0)       AS apay,
       service.price,
       Coalesce(dispute.status, 3)        AS dispstatus
FROM   service
       INNER JOIN payment_list
               ON payment_list.id_service = service.id
                  AND payment_list.status = '4'
       LEFT JOIN site_commission
              ON site_commission.id_order = payment_list.id
       LEFT JOIN website_pay
              ON website_pay.id_order = payment_list.id
       LEFT JOIN dispute
              ON dispute.id_order = payment_list.id
WHERE  service.id_user = :id_user
       AND service.enddatef + :the_days_time < :now_date",
        array(
            "id_user" => $id,
            "the_days_time" => $the_days_time,
            "now_date" => $now_date
        )
    );
    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
        if (isset($row["dispstatus"])) {
            if ($row["dispstatus"] == '3') {
                $total_num += $row["price"] - $row["coms"];
            }
        } else {
            $total_num += $row["price"] - $row["coms"];
        }
    }

    return $total_num;
}
