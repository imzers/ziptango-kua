<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 7/1/2016
 * Time: 1:45 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/Consts.php";
require_once "../core/SMTPMail.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "api_key" => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
//Get invite Link
$query = $engine->executePrepared("SELECT username, permanent_link_code FROM users u WHERE u.id = :id",
    array(
        "id" => $_SESSION["id"]
    )
);
$rows = $query->fetchAll(PDO::FETCH_ASSOC);

$username = $rows[0]["username"];
$link = APP_SERVER_ROOT . "invite/index/" . $rows[0]["permanent_link_code"];

//Send Invitation Mail
$mailC = getMailMessage("invite");
$mailC->message = str_replace("#whosent#", $username, $mailC->message);
$mailC->message = str_replace("#invitelink#", $link, $mailC->message);

//Send Invitation Mail
$engine->setSingleLineResponse($link . ";;;;;" . $mailC->message);

/*
 * Send Result
 */
$engine->sendResponse();