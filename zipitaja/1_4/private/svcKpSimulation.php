<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";

$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
  array(
    'amount' => true,
    'category_id' => true,
        'api_key' => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

$amount = $engine->getPOSTField('amount');
if (!is_numeric($amount)) {
    die($engine->createResponse(Engine::codError, "Nilai produk harus numerik"));
}
$cat_id = $engine->getPOSTField('category_id');

function simulation($a, $category_id, $tenor)
{
    $params_string = 'SupplierKey='.KP_SUPPLIER_KEY.'&CategoryID='.$category_id.'&AmountFinance='.$a.'&Tenor='.$tenor;
    $url = KP_API_URL . "CalculateReq";
    $options = array(
        CURLOPT_POST => 1,
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER => false,            // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_MAXREDIRS => 10,            // stop after 10 redirects
        CURLOPT_AUTOREFERER => true,        // set referrer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // time-out on connect
        CURLOPT_TIMEOUT => 120,             // time-out on response
        CURLOPT_POSTFIELDS => $params_string
    );
    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    curl_close($ch);
    
    // Parse KP Response
    $xml_snippet = @simplexml_load_string($content);
    $json_convert = json_encode($xml_snippet);
    $json = json_decode($json_convert, true);

    if ($json['Status'] == 0) {
        return $json['InstallmentAmount'];
    } else {
        return 0;
    }
}

$result = [
    'amount' => $amount,
    't3' => simulation($amount, $cat_id, 3),
    't6' => simulation($amount, $cat_id, 6),
    't12' => simulation($amount, $cat_id, 12)
];

$engine->setSingleLineResponse(json_encode($result));
$engine->sendResponse();
