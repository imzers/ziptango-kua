<?php
/**
 * Created by PhpStorm.
 * User: Riad
 * Date: 6/28/2016
 * Time: 2:29 PM
 */
require_once "../core/Engine.php";
require_once "../core/FunctionList.php";
require_once "../core/PushNotificationManager.php";
require_once "../core/Consts.php";

$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(array(
    "param_id_msg" => true,
    "param_sender" => true,
    "param_service" => true,
    "api_key" => true
));
$param_id_msg = $engine->getPOSTField("param_id_msg");
$param_sender = $engine->getPOSTField("param_sender");
$param_service = $engine->getPOSTField("param_service");

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();

/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
//UPDATE ISREAD
$query = $engine->executePrepared("UPDATE message SET isread = 1 WHERE id = :id",
    array(
        "id" => $param_id_msg
    )
);

$engine->setErrorResponse("Pesan gagal dikirim, Silakan coba kembali!");
if ($query) {
    //SEND Notification
    $gcm = new PushNotificationManager();
    //Get list registered id
    $query = $engine->executePrepared("SELECT gcm_regid, mobile FROM gcm_users WHERE user_id = :user_id",
        array(
            "user_id" => $param_sender
        )
    );
    $ids = array();
    while ($row = $query->fetch(PDO::FETCH_ASSOC))
        $ids[] = array($row["gcm_regid"], $row["mobile"]);

    //Send notification
    $gcm->sendReadNotification(2, $ids, $param_id_msg, $param_service, $_SESSION["id"]);

    //Send Response
    $engine->setSingleLineResponse(1);
}

//Send Response
$engine->sendResponse();