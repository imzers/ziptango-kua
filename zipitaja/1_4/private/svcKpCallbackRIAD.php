<?php

require_once "../core/Engine.php";
require_once "../core/Consts.php";
require_once "../core/MCrypt.php";
require_once "../core/SMTPMail.php";
require_once "../core/PushNotificationManager.php";
require_once "cc_util.php";
require_once "checkout_util.php";

$engine = new Engine();
$token = $_GET['token'];

date_default_timezone_set('Asia/Jakarta');
header("Content-Type: text/html");

if (isset($_GET['token'])) {
    $co = getCoByTemporaryToken($token);

    if ($co) {
        $co->placeOrder($co->getUniqIdFromToken($token));
        $co->deleteFromTemporary();

        header("Content-Type: text/html"); ?>
	<!DOCTYPE html>
	<html>

	<head>
	</head>

	<body>
		<script>
			window.JSIface.sayThanks();
		</script>
	</body>

	</html>
	<?php

        die('');
    }
}

header("Content-Type: text/html");
?>

		<!DOCTYPE html>
		<html>

		<head>
			<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7'
			 crossorigin='anonymous'>
			<title>Pembayaran Gagal</title>
		</head>

		<body>

			<div class="container">
				<h3 class="text-center text-danger">PENGAJUAN GAGAL</h3>
				<p class="text-center">Pembayaran gagal karena beberapa alasan.</p>
			</div>

			<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
		</body>

		</html>