<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core/FunctionList.php');


$engine = new Engine();
/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        "api_key" => true
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();


/*
 * Check session is valid
 */
$engine->checkCurrentSession();

/*
 * Core Operation
 */
$detUser = $engine->executePrepared("SELECT * FROM users WHERE id = :id", array("id" => $_SESSION["id"]));
$rowsuser = $detUser->fetchAll(PDO::FETCH_ASSOC);

/* $icon_path = APP_SERVER_ROOT . APP_IMG_USER_PATH; */
$icon_path = APP_IMG_USER_PATH;
$image = $icon_path . $rowsuser[0]["icon"];
$username = $rowsuser[0]["username"];
$email = $rowsuser[0]["email"];
$ziprewards = (isset($rowsuser[0]['ziprewards']) ? $rowsuser[0]['ziprewards'] : '');
// Make rating data
if (is_array($rowsuser) && (count($rowsuser) > 0)) {
	foreach ($rowsuser as &$row) {
		if (isset($row['id'])) {
			$row['reputation_seller'] = 0;
			$row['reputation_buyer'] = 0;
			$sql = "SELECT COALESCE(SUM(vf.rep), 0) AS sum_reputation, COUNT(vf.id) AS count_reputation FROM view_public_svcgetuserfeedback AS vf WHERE vf.id_seller = :id_seller";
			$sql_params = array('id_seller' => $row['id']);
			$sql_query = $engine->executePrepared($sql, $sql_params);
			$rate_row = $sql_query->fetch(PDO::FETCH_ASSOC);
			if (isset($rate_row['sum_reputation']) && isset($rate_row['count_reputation'])) {
				if (($rate_row['sum_reputation'] > 0) && ($rate_row['count_reputation'] > 0)) {
					$row['reputation_seller'] = ceil($rate_row['sum_reputation']/$rate_row['count_reputation']);
				}
			}
		}
	}
}
$reputation_seller = $rowsuser[0]["reputation_seller"];
$reputation_buyer = $rowsuser[0]["reputation_buyer"];
$detVoucher = $engine->executePrepared("
SELECT
    IFNULL(SUM(value),0) AS credit
FROM
    zip_credits
WHERE
    deleted = 0 AND
    assigned_to = :assigned_to",
    array(
        "assigned_to" => $_SESSION["id"]
    )
);
$result = $detVoucher->fetchAll(PDO::FETCH_ASSOC);

$my_id = $_SESSION["id"];
$e_filter = '';
if ($my_id > 1)
    $e_filter = 'AND id_user_send = 1';

$chatUnread = $engine->executePrepared("
SELECT
    SUM(IF(isread = 1, 0, 1)) AS count
FROM
    message
WHERE
    id_user_get = :my_id $e_filter",
    array(
        "my_id" => $my_id
    ));
$unread = $chatUnread->fetch(PDO::FETCH_ASSOC);

$voucher = $result[0]["credit"];

$active = $engine->executePrepared("SELECT active FROM users WHERE id = :id", array(
    "id" => $_SESSION["id"]
));
$row = $active->fetch(PDO::FETCH_ASSOC);

///////////////////////////////////////////////////////
/////////////// BEGIN - sellers rating ////////////////
///////////////////////////////////////////////////////
$pointsPerCompleteOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'complete_order_points'");
$result = $pointsPerCompleteOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCompleteOrder = $result['value'];

$pointsPerCanceledOrder = $engine->executePrepared("SELECT * FROM site_options WHERE name = 'canceled_order_points'");
$result = $pointsPerCanceledOrder->fetch(PDO::FETCH_ASSOC);
$pointsPerCanceledOrder = $result['value'];

// Number of complete orders for this year
$beginningOfYear = gmmktime(0, 0, 0, 1, 1);
$noOfCompleteOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCompleteOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND (payment_list.status='4' OR payment_list.status='5' OR payment_list.status='7')
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCompleteOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCompleteOrdersThisYear = $result['noOfCompleteOrdersThisYear'];

// Number of canceled orders for this year
$noOfCanceledOrdersThisYear = $engine->executePrepared("
                SELECT COUNT(service.id) as noOfCanceledOrdersThisYear
                FROM service
                INNER JOIN payment_list ON payment_list.id_service = service.id AND payment_list.status='6'
                WHERE service.id_user = :id
                    AND date <> ''
                    AND date >= :beginningOfYear",
                array(
                    'id' => $my_id,
                    'beginningOfYear' => $beginningOfYear
                )
            );
$result = $noOfCanceledOrdersThisYear->fetch(PDO::FETCH_ASSOC);
$noOfCanceledOrdersThisYear = $result['noOfCanceledOrdersThisYear'];

$rating = $noOfCompleteOrdersThisYear*$pointsPerCompleteOrder + $noOfCanceledOrdersThisYear*$pointsPerCanceledOrder;

// Star rating
$starValue = array('Bronze' => 1, 'Silver' => 2, 'Gold' => 3, 'Diamond' => 4);
$starRatingLastYear = $engine->executePrepared("
                SELECT last_year_star_rating
                FROM users
                WHERE id = :id",
                array(
                    'id' => $my_id
                )
            );
$result = $starRatingLastYear->fetch(PDO::FETCH_ASSOC);
$starRatingLastYear = $result['last_year_star_rating'];

if (!isset($starRatingLastYear) || trim($starRatingLastYear) == '') $starRatingLastYear = 'Bronze';
$starRatingThisYear = 'Bronze';
if ($rating > 100 && $rating <= 250) $starRatingThisYear = 'Silver';
if ($rating > 250 && $rating <= 400) $starRatingThisYear = 'Gold';
if ($rating > 400) $starRatingThisYear = 'Diamond';

if ($starValue[$starRatingThisYear] > $starValue[$starRatingLastYear]) $starRating = $starRatingThisYear;
else $starRating = $starRatingLastYear;
///////////////////////////////////////////////////////
///////////////////////// END /////////////////////////
///////////////////////////////////////////////////////

$engine->setTranslateDataResponse(array(
    array(
        "icon" => $image,
        "username" => $username,
        "email" => $email,
        "unread_chat" => $unread['count'],
        "credit" => $voucher,
        "verified" => $row['active'],
		'ziprewards'	=> $ziprewards,
        "rating" => $rating,
        "medalRating" => $starRating,
		'reputation_seller' => $reputation_seller,
		'reputation_buyer'	=> $reputation_buyer,
		
    )
));

/*
 * Send Result
 */
$engine->sendResponse();