<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'MCrypt.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'cc_util.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'checkout_util.php');



$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'param_services' 		=> false,
        'param_use_voucher' 	=> false,
        'param_coupon' 			=> false,
        'param_unit_apt' 		=> false,
        'param_address_notes' 	=> false,
        'param_mobile' 			=> false,
        'api_key' 				=> false,
		'param_payment_code'	=> false,
    )
);
/*
Total Payment option
1. Go Pay
2. OVO
3. Cash
4. Point
5. Wallet
6. CC
*/
$collectData['payment_options'] = array(
	'bt',
	'kp',
	'cc',
	'gopay',
	'ovo',
	'wallet',
	'point',
	'cash',
);
$input_params = array();
$input_params['param_payment_code_numeric'] = 0;
if (isset($_GET['payment'])) {
	if (is_numeric($_GET['payment'])) {
		$input_params['param_payment_code_numeric'] = intval($_GET['payment']);
	}
}
/*
 * Check API Key
 */
//$engine->checkAPIKeyPair();
//$engine->checkCurrentSession();

$error = false;
$error_msg = array();
switch ((int)$input_params['param_payment_code_numeric']) {
	case 6:
		$input_params['param_payment_code'] = 'cc';
	break;
	case 5:
		$input_params['param_payment_code'] = 'wallet';
	break;
	case 4:
		$input_params['param_payment_code'] = 'point';
	break;
	case 3:
		$input_params['param_payment_code'] = 'bt';
	break;
	case 2:
		$input_params['param_payment_code'] = 'ovo';
	break;
	case 1:
		$input_params['param_payment_code'] = 'gopay';
	break;
	case 0:
	default:
		$input_params['param_payment_code'] = 'bt';
	break;
}
if (!in_array($input_params['param_payment_code'], $collectData['payment_options'])) {
	$input_params['param_payment_code'] = 'bt';
}

$input_params['http_request_host'] = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
if (isset($_SERVER['HTTP_HOST'])) {
	$input_params['http_request_host'] .= $_SERVER['HTTP_HOST'];
}
if (isset($_SERVER['REQUEST_URI'])) {
	$input_params['http_request_host'] .= $_SERVER['REQUEST_URI'];
}
$input_params['http_request_ip'] = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0');
$input_params['http_request_ip'] = (is_string($input_params['http_request_ip']) ? sprintf("%s", $input_params['http_request_ip']) : '0.0.0.0');
$input_params['notification_raw'] = @file_get_contents("php://input");
try {
	$input_params['notification_params'] = json_decode($input_params['notification_raw']);
} catch (Exception $ex) {
	$error = true;
	$error_msg[] = "Cannot json-decoded raw input data with exception: {$ex->getMessage()}.";
}

# Save Every Request
if (!$error) {
	if ($input_params['http_request_ip'] != '52.221.212.221') {
		$sql_string = "INSERT INTO weblog_requests(request_host, request_header, request_body, request_datetime, request_ip, response_body) VALUES(:request_host, :request_header, :request_body, NOW(), :request_ip, :response_body)";
		$sql_params = array(
			'request_host'					=> $input_params['http_request_host'],
			'request_header'				=> '',
			'request_body'					=> json_encode($input_params, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),
			'request_ip'					=> $input_params['http_request_ip'],
			'response_body'					=> '',
			
		);
		$engine->executePrepared($sql_string, $sql_params);
	}
}
$collectData = array();
$collectData['pg_params'] = array(
	'pg_trans'						=> false,
	'pg_order'						=> false,
);
if (!$error) {
	switch (strtolower($input_params['param_payment_code'])) {
		case 'gopay':
			if (isset($input_params['notification_params']->transaction_id)) {
				if (is_string($input_params['notification_params']->transaction_id) || is_numeric($input_params['notification_params']->transaction_id)) {
					$collectData['pg_params']['pg_trans'] = sprintf("%s", $input_params['notification_params']->transaction_id);
				}
			}
			if (isset($input_params['notification_params']->order_id)) {
				if (is_string($input_params['notification_params']->order_id) || is_numeric($input_params['notification_params']->order_id)) {
					$collectData['pg_params']['pg_order'] = sprintf("%s", $input_params['notification_params']->order_id);
				}
			}
		break;
		case 'ovo':
			
		break;
	}
}
# Get Payment Config
if (!$error) {
	$Payment_configs = new Payment_configs('dev');
	$Payment_configs->set_payment_environment($Payment_configs->get_payment_environment());
}
# Get Data from Temporary
if (!$error) {
	$collectData['sql_string'] = "SELECT t.* FROM tb_tmp_checkout AS t WHERE (t.pg_transaction_id = :trans_id AND t.pg_order_id = :order_id) ORDER BY t.create_time DESC LIMIT 1";
	$collectData['sql_params'] = array(
		'trans_id'				=> $collectData['pg_params']['pg_trans'],
		'order_id'				=> $collectData['pg_params']['pg_order'],
	);
	$sql_query = $engine->executePrepared($collectData['sql_string'], $collectData['sql_params']);
	$collectData['payment_data'] = $sql_query->fetch(PDO::FETCH_OBJ);
	if (!isset($collectData['payment_data']->id)) {
		$error = true;
		$error_msg[] = "Cannot get payment-data details from tmp-table.";
	}
}
if (!$error) {
	switch (strtolower($input_params['param_payment_code'])) {
		case 'gopay':
			if (isset($input_params['notification_params']->status_code) && isset($input_params['notification_params']->gross_amount) && isset($input_params['notification_params']->signature_key)) {
				$collectData['signature_string'] = sprintf("%s%s%s%s",
					$collectData['pg_params']['pg_order'],
					(is_string($input_params['notification_params']->status_code) ? $input_params['notification_params']->status_code : ''),
					(is_string($input_params['notification_params']->gross_amount) || is_numeric($input_params['notification_params']->gross_amount)) ? $input_params['notification_params']->gross_amount : '',
					Payment_configs::$MIDTRANS_APP_SERVER
				);
				$collectData['signature'] = openssl_digest($collectData['signature_string'], 'sha512');
				if ($input_params['notification_params']->signature_key != $collectData['signature']) {
					$error = true;
					$error_msg[] = "Signature not match";
				}
			} else {
				$error = true;
				$error_msg[] = "Not have signature-key";
			}
		break;
		case 'ovo':
			
		break;
		
	}
}
if (!$error) {
	$collectData['collect']['payment_gateway_params'] = array(
		'payment_code'			=> strtolower($input_params['param_payment_code']),
		'services'				=> $collectData['payment_data']->services,
		'mobile'				=> $collectData['payment_data']->hp,
		'transaction_id'		=> $collectData['payment_data']->pg_transaction_id,
		'payment_invoice_id'	=> $collectData['payment_data']->pg_order_id,
	);
	$collectData['customer_address'] = new CustAddress(
		$collectData['payment_data']->address,
		$collectData['payment_data']->city,
		$collectData['payment_data']->state,
		$collectData['payment_data']->country,
		$collectData['payment_data']->hp,
		$collectData['payment_data']->lat,
		$collectData['payment_data']->long,
		$collectData['payment_data']->unit_apt,
		$collectData['payment_data']->address_notes
	);
	try {
		$collectData['checkout'] = new CO($collectData['collect']['payment_gateway_params']['payment_code'], $collectData['payment_data']->usr_id, $collectData['payment_data']->services, $collectData['payment_data']->coupon, $collectData['payment_data']->coupon, 0, $collectData['customer_address']);
	} catch (Exception $ex) {
		$error = true;
		$error_msg[] = "Cannot make checkout object class with exception: {$ex->getMessage()}.";
	}
}


//-----------------------------------
// Make Payment
//-----------------------------------
if (!$error) {
	$collectData['checkout']->updateAddress();
	switch (strtolower($input_params['param_payment_code'])) {
		case 'cc':
			
			
		break;
		case 'gopay':
			if (!isset($input_params['notification_params']->transaction_status)) {
				$error = true;
				$error_msg[] = "No transaction status from Gopay IPN by Midtrans.";
			} else {
				if (is_string($input_params['notification_params']->transaction_status)) {
					$input_params['notification_params']->transaction_status = strtolower($input_params['notification_params']->transaction_status);
				} else {
					$error = true;
					$error_msg[] = "Transaction status should be in string datatype.";
				}
			}
		break;
		case 'ovo':
			
			
		break;
	}
}
if (!$error) {
	$collectData['order_error'] = '';
	if (!$collectData['checkout']->validateOrder($collectData['order_error'])) {
		die($engine->createResponse(Engine::codError, $collectData['order_error']));
	}
	try {
		$collectData['update_address'] = $collectData['checkout']->updateAddress();
	} catch (Exception $ex) {
		$error = true;
		$error_msg[] = "Cannot running checkout update address with exception: {$ex->getMessage()}.";
	}
}
if (!$error) {
	$collectData['sql_services'] = explode(';;;', $collectData['checkout']->get_services());
	if (count($collectData['sql_services']) > 0) {
		foreach ($collectData['sql_services'] as $service_id) {
			$collectData['sql_string'] = sprintf("SELECT COUNT(id) AS value FROM %s WHERE (id_service = :id_service) AND (transfer_key = :transfer_key AND ccid = :ccid)", 'payment_list');
			$collectData['sql_params'] = array(
				'id_service'				=> $service_id,
				'transfer_key'				=> (isset($collectData['collect']['payment_gateway_params']['transaction_id']) ? $collectData['collect']['payment_gateway_params']['transaction_id'] : ''),
				'ccid'						=> (isset($collectData['collect']['payment_gateway_params']['payment_invoice_id']) ? $collectData['collect']['payment_gateway_params']['payment_invoice_id'] : ''),
			);
			$collectData['sql_query'] = $engine->executePrepared($collectData['sql_string'], $collectData['sql_params']);
			$collectData['sql_result'] = $collectData['sql_query']->fetch(PDO::FETCH_OBJ);
			if (isset($collectData['sql_result']->value)) {
				if ((int)$collectData['sql_result']->value > 0) {
					$error = true;
					$error_msg[] = "Payment already confirmed, maybe duplicated IPN.";
				}
			}
		}
	}
	
}


if (!$error) {
	
	switch (strtolower($input_params['param_payment_code'])) {
		case 'cc':
			
			
		break;
		case 'gopay':
			if (strtolower($input_params['notification_params']->transaction_status) === strtolower('settlement')) {
				try {
					$collectData['collect']['payment_place_order'] = $collectData['checkout']->placeOrder($collectData['collect']['payment_gateway_params']['payment_invoice_id'], $collectData['collect']['payment_gateway_params']);
				} catch (Exception $ex) {
					$error = true;
					$error_msg[] = "Tidak bisa place order untuk gopay dengan exception: {$ex->getMessage()}.";
				}
			}
		break;
		case 'ovo':
		
		break;
	}
}


/*

*/



if (!$error) {
	$engine->setSingleLineResponse($collectData['collect']['payment_gateway_params']);
} else {
	$engine->setSingleLineResponse($error_msg);
}
$engine->sendResponse();



