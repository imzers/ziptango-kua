<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'MCrypt.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'cc_util.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'checkout_util.php');



$engine = new Engine();

if (php_sapi_name() != 'cli') {
	exit("Cli command only.");
}
$limit = (isset($argv[1]) ? $argv[1] : 10);
$limit = (is_numeric($limit) ? (int)$limit : 10);

$sql_collecteds = array();
$sql_string = sprintf("SELECT wl.* FROM %s AS wl WHERE wl.request_completed = :request_completed ORDER BY request_datetime ASC LIMIT %d OFFSET 0",
	'weblog_requests',
	$limit
);
$sql_params = array(
	'request_completed'			=> 'N',
);
$sql_query = $engine->executePrepared($sql_string, $sql_params);
$ch = curl_init();
curl_setopt($ch. CURLOPT_HTTPHEADER, array('Accept: */*', 'Content-Type: application/json'));
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_NOBODY, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 


while ($row = $sql_query->fetch(PDO::FETCH_OBJ)) {
	$httpCode = 0;
	$sql_update = array(
		'string'			=> sprintf("UPDATE %s SET request_completed = :request_completed, response_body = :response_body WHERE seq = :seq", 'weblog_requests'),
		'params'			=> array(
			'request_completed'			=> 'Y',
			'response_body'				=> '',
			'seq'						=> $row->seq,
		),
	);
	
	if (isset($row->request_body)) {
		$request_body = json_decode($row->request_body);
		if (isset($request_body->notification_params)) {
			$notification_params = json_encode($notification_params);
			if (strlen((string)$row->request_host) > 0) {
				curl_setopt($ch, CURLOPT_URL, $row->request_host);
			} else {
				curl_setopt($ch, CURLOPT_URL, 'https://api.jajandijalan.com/zipitaja/1_4/payment/svcIPN.php?payment=1&mode=notify');
			}
			
			if (strlen($notification_params) > 0) {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_params);
			}
			try {
				$sql_update['params']['response_body'] = curl_exec($ch);
			} catch (Exception $ex) {
				throw $ex;
				$sql_update['params']['response_body'] = $ex->getMessage();
			}
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		}
		
		
		
		echo sprintf("%d\r\n", $httpCode);
	}
	array_push($sql_collecteds, $sql_update);
}
curl_close($ch);

if (count($sql_collecteds) > 0) {
	foreach ($sql_collecteds as $sql_update) {
		$engine->executePrepared($sql_update['string'], $sql_update['params']);
	}
}















