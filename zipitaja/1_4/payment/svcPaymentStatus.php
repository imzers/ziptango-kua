<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'MCrypt.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'cc_util.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'checkout_util.php');



$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'param_services' 		=> false,
        'param_order_id' 		=> true,
        'api_key' 				=> true,
		'param_payment_code'	=> true,
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();
$engine->checkCurrentSession();

$input_params = array();
$input_params['param_services'] = $engine->getPOSTField('param_services');
$input_params['param_order_id'] = $engine->getPOSTField('param_order_id');
$input_params['param_payment_code_numeric'] = $engine->getPOSTField('param_payment_code');

$input_params['param_payment_code'] = 'bt';



/*
Total Payment option
1. Go Pay
2. OVO
3. Cash
4. Point
5. Wallet
6. CC
*/
$collectData['payment_options'] = array(
	'bt',
	'kp',
	'cc',
	'gopay',
	'ovo',
	'wallet',
	'point',
	'cash',
);
$input_params['param_payment_code_numeric'] = (is_numeric($input_params['param_payment_code_numeric']) ? intval($input_params['param_payment_code_numeric']) : 0);
$error = false;
$error_msg = array();
switch ((int)$input_params['param_payment_code_numeric']) {
	case 6:
		$input_params['param_payment_code'] = 'cc';
	break;
	case 5:
		$input_params['param_payment_code'] = 'wallet';
	break;
	case 4:
		$input_params['param_payment_code'] = 'point';
	break;
	case 3:
		$input_params['param_payment_code'] = 'bt';
	break;
	case 2:
		$input_params['param_payment_code'] = 'ovo';
	break;
	case 1:
		$input_params['param_payment_code'] = 'gopay';
	break;
	case 0:
	default:
		$input_params['param_payment_code'] = 'bt';
	break;
}
if (!in_array($input_params['param_payment_code'], $collectData['payment_options'])) {
	$input_params['param_payment_code'] = 'bt';
}
$collectData = array();
$collectData['pg_params'] = array(
	'pg_trans'						=> false,
	'pg_order'						=> false,
);
$input_params['http_request_host'] = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
if (isset($_SERVER['HTTP_HOST'])) {
	$input_params['http_request_host'] .= $_SERVER['HTTP_HOST'];
}
if (isset($_SERVER['REQUEST_URI'])) {
	$input_params['http_request_host'] .= $_SERVER['REQUEST_URI'];
}
$input_params['http_request_ip'] = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0');
$input_params['http_request_ip'] = (is_string($input_params['http_request_ip']) ? sprintf("%s", $input_params['http_request_ip']) : '0.0.0.0');
$input_params['notification_raw'] = @file_get_contents("php://input");
try {
	$input_params['notification_params'] = json_decode($input_params['notification_raw']);
} catch (Exception $ex) {
	$error = true;
	$error_msg[] = "Cannot json-decoded raw input data with exception: {$ex->getMessage()}.";
}
$collectData['payment_data'] = array();
# Get Payment Config
if (!$error) {
	$Payment_configs = new Payment_configs('dev');
	$Payment_configs->set_payment_environment($Payment_configs->get_payment_environment());
}
if (!$error) {
	$collectData['sql_actions'] = array(
		'sql_params'						=> array(
			'ccid'				=> (is_string($input_params['param_order_id']) ? $input_params['param_order_id'] : ''),
			'payment_type'		=> (int)$input_params['param_payment_code_numeric'],
		),
	);
	$collectData['sql_actions']['sql_string'] = sprintf("SELECT * FROM %s WHERE (ccid = :ccid AND payment_type = :payment_type) LIMIT 1", 'payment_list');
	$collectData['sql_actions']['sql_query'] = $engine->executePrepared($collectData['sql_actions']['sql_string'], $collectData['sql_actions']['sql_params']);
	$collectData['sql_actions']['sql_result'] = $collectData['sql_actions']['sql_query']->fetch(PDO::FETCH_OBJ);
}
if (!$error) {
	$collectData['payment_data'] = array();
	if (!isset($collectData['sql_actions']['sql_result']->id)) {
		$error = true;
		$error_msg[] = "Payment data not exists or still in pending.";
		$collectData['payment_data']['payment_status'] = 'pending';
	} else {
		$collectData['payment_data']['order_id'] = $collectData['sql_actions']['sql_result']->ccid;
		$collectData['payment_data']['transaction_id'] = $collectData['sql_actions']['sql_result']->transfer_key;
		$collectData['payment_data']['payment_type'] = $collectData['sql_actions']['sql_result']->payment_type;
		switch ((int)$collectData['sql_actions']['sql_result']->status) {
			case 5:
				$collectData['payment_data']['payment_status'] = 'success';
			break;
			case 1:
			default:
				$collectData['payment_data']['payment_status'] = 'pending';
			break;
		}
		$collectData['payment_data']['payment_code'] = $input_params['param_payment_code'];
	}	
}




$engine->setSingleLineResponse($collectData['payment_data']);

$engine->sendResponse();



