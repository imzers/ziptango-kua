<?php
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Engine.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'FunctionList.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'Consts.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'MCrypt.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'SMTPMail.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'PushNotificationManager.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'cc_util.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'private' . DIRECTORY_SEPARATOR . 'checkout_util.php');



$engine = new Engine();

/*
 * Get All POST Value
 */
$engine->setPostField(
    array(
        'param_services' 		=> true,
        'param_use_voucher' 	=> true,
        'param_coupon' 			=> false,
        'param_hp' 				=> true,
        'param_lat' 			=> true,
        'param_long' 			=> true,
        'param_address' 		=> true,
        'param_city' 			=> true,
        'param_state' 			=> true,
        'param_country' 		=> true,
        'param_unit_apt' 		=> true,
        'param_address_notes' 	=> true,
        'param_mobile' 			=> true,
        'api_key' 				=> true,
		'param_payment_code'	=> true,
    )
);

/*
 * Check API Key
 */
$engine->checkAPIKeyPair();
$engine->checkCurrentSession();

$input_params = array();
$input_params['param_services'] = $engine->getPOSTField('param_services');
$input_params['param_use_voucher'] = $engine->getPOSTField('param_use_voucher');
$input_params['param_coupon'] = $engine->getPOSTField('param_coupon');
$input_params['param_mobile'] = $engine->getPOSTField('param_mobile');

$input_params['param_hp'] = $engine->getPOSTField('param_hp');
$input_params['param_lat'] = $engine->getPOSTField('param_lat');
$input_params['param_long'] = $engine->getPOSTField('param_long');
$input_params['param_address'] = $engine->getPOSTField('param_address');
$input_params['param_address'] = substr($input_params['param_address'], 0, 128);
$input_params['param_city'] = $engine->getPOSTField('param_city');
$input_params['param_state'] = $engine->getPOSTField('param_state');
$input_params['param_country'] = $engine->getPOSTField('param_country');
$input_params['param_unit_apt'] = $engine->getPOSTField('param_unit_apt');
$input_params['param_address_notes'] = $engine->getPOSTField('param_address_notes');

$input_params['param_payment_code_numeric'] = $engine->getPOSTField('param_payment_code');
$input_params['param_payment_code'] = 'bt';



/*
Total Payment option
1. Go Pay
2. OVO
3. Cash
4. Point
5. Wallet
6. CC
*/
$collectData['payment_options'] = array(
	'bt',
	'kp',
	'cc',
	'gopay',
	'ovo',
	'wallet',
	'point',
	'cash',
);
$input_params['param_payment_code_numeric'] = (is_numeric($input_params['param_payment_code_numeric']) ? intval($input_params['param_payment_code_numeric']) : 0);
switch ((int)$input_params['param_payment_code_numeric']) {
	case 6:
		$input_params['param_payment_code'] = 'cc';
	break;
	case 5:
		$input_params['param_payment_code'] = 'wallet';
	break;
	case 4:
		$input_params['param_payment_code'] = 'point';
	break;
	case 3:
		$input_params['param_payment_code'] = 'bt';
	break;
	case 2:
		$input_params['param_payment_code'] = 'ovo';
	break;
	case 1:
		$input_params['param_payment_code'] = 'gopay';
	break;
	case 0:
	default:
		$input_params['param_payment_code'] = 'bt';
	break;
}
if (!in_array($input_params['param_payment_code'], $collectData['payment_options'])) {
	$input_params['param_payment_code'] = 'bt';
}


# Save Every Request
/*
$sql_string = "INSERT INTO weblog_requests(request_header, request_body, request_datetime) VALUES(:request_header, :request_body, NOW())";
$sql_params = array(
	'request_header'				=> '',
	'request_body'					=> json_encode($input_params, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),
);
$engine->executePrepared($sql_string, $sql_params);
*/


$customer_address = new CustAddress(
    $input_params['param_address'],
    $input_params['param_city'],
    $input_params['param_state'],
    $input_params['param_country'],
    $input_params['param_hp'],
    $input_params['param_lat'],
    $input_params['param_long'],
    $input_params['param_unit_apt'],
    $input_params['param_address_notes']
);
$co = new CO($input_params['param_payment_code'], null, $input_params['param_services'], $input_params['param_use_voucher'], $input_params['param_coupon'], $input_params['param_mobile'], $customer_address);

$error = '';
if (!$co->validateOrder($error)) {
    die($engine->createResponse(Engine::codError, $error));
}
$co->updateAddress();
$collectData = array(
	'collect'				=> array(),
	'payment_code'			=> '',
	'payment_create'		=> false,
);
$collectData['collect']['payment_userinfo'] = $co->currentUserInfo();
$collectData['collect']['payment_token'] = $co->placeAsTemporary();
$collectData['collect']['payment_invoice_id'] = $co->getUniqIdFromToken($collectData['collect']['payment_token']);
$error = false;
$error_msg = array();
try {
	$collectData['collect']['payment_information'] = $co->get_payment_information_from_tmp_table_by_token($collectData['collect']['payment_token']);
} catch (Exception $ex) {
	$error = false;
	$error_msg[] = "Exception error while fetching temporary payment data.";
}
if (!$error) {
	if (!isset($collectData['collect']['payment_information']['id'])) {
		$error = true;
		$error_msg[] = "Not have id after insert new temporary payment data.";
	}
}
if (!$error) {
	$collectData['collect']['payment_data'] = array(
		'items'			=> $co->getServices(),
		'names'			=> $co->getServicesName(),
	);
	$collectData['collect']['payment_data']['transactions'] = array();

	$Payment_configs = new Payment_configs('dev');
	$Payment_configs->set_payment_environment($Payment_configs->get_payment_environment());
}

if (!$error) {
	switch (strtolower($input_params['param_payment_code'])) {
		case 'cc':
			// Parameter CC
			$collectData['collect']['payment_data']['transactions'] = array(
				'usr_id' 					=> $co->currentUserId(),
				'services' 					=> str_replace(';;;', ';', $input_params['param_services']),
				'use_voucher' 				=> $input_params['param_use_voucher'],
				'coupon' 					=> $input_params['param_coupon'],
				'mobile' 					=> $input_params['param_mobile'],
				'ex_adr' 					=> array(
					$input_params['param_lat'],
					$input_params['param_long'],
					$input_params['param_country'],
					$input_params['param_unit_apt'],
					$input_params['param_address_notes'],
					substr($input_params['param_hp'], 0, 12)
				)
			);
			try {
				$collectData['collect']['payment_create'] = generateCCForm(
					$collectData['collect']['payment_invoice_id'],
					sprintf("%s %s", (isset($collectData['collect']['payment_userinfo']['firstname']) ? $collectData['collect']['payment_userinfo']['firstname'] : ''), (isset($collectData['collect']['payment_userinfo']['lastname']) ? $collectData['collect']['payment_userinfo']['lastname'] : '')),
					(isset($collectData['collect']['payment_userinfo']['email']) ? $collectData['collect']['payment_userinfo']['email'] : ''),
					$co->getGrandTotal(),
					$customer_address,
					$co->getServicesName(),
					json_encode($collectData['collect']['payment_data']['transactions'])
				);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot generate CC Form.";
				exit($engine->createResponse(Engine::codError, $ex->getMessage()));
			}
		break;
		case 'ovo':
			
			include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'payment' . DIRECTORY_SEPARATOR . 'faspay' . DIRECTORY_SEPARATOR . 'Faspay.php');
			//include_once;
			$collectData['Faspay'] = new Faspay(Payment_configs::$FASPAY_APP);
			
			try {
				$collectData['collect']['payment_create'] = $collectData['Faspay']->create_faspay_payment_structure($input_params, '812', $collectData, $co);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot get Faspay response: {$ex->getMessage()}.";
			}
		break;
		case 'gopay':
			$collectData['collect']['payment_data']['transactions'] = array(
				'payment_type' 				=> 'gopay',
				'transaction_details'		=> array(
					'order_id'						=> $collectData['collect']['payment_invoice_id'],
					'gross_amount'					=> $co->getGrandTotal(),
				),
				'item_details'				=> array(),
				'gopay'						=> array(
					'enable_callback'				=> false,
					'callback_url'					=> 'android://jajandijalan',
				),
				'customer_details'			=> array(
					'first_name'					=> (isset($collectData['collect']['payment_userinfo']['firstname']) ? $collectData['collect']['payment_userinfo']['firstname'] : ''),
					'last_name'						=> (isset($collectData['collect']['payment_userinfo']['lastname']) ? $collectData['collect']['payment_userinfo']['lastname'] : ''),
					'email'							=> (isset($collectData['collect']['payment_userinfo']['email']) ? $collectData['collect']['payment_userinfo']['email'] : ''),
					'phone'							=> (isset($collectData['collect']['payment_userinfo']['telephone']) ? $collectData['collect']['payment_userinfo']['telephone'] : ''),
				),
			);
			if (is_array($collectData['collect']['payment_data']['items']) && (count($collectData['collect']['payment_data']['items']) > 0)) {
				$for_i = 1;
				foreach ($collectData['collect']['payment_data']['items'] as $item) {
					$collectData['collect']['payment_data']['transactions']['item_details'][] = array(
						'id'				=> sprintf("%d%d", $item['seller_id'], $for_i),
						'price'				=> $item['price'],
						'quantity'			=> 1,
						'name'				=> $item['name'],
					);
					$for_i++;
				}
			}
			try {
				$collectData['collect']['payment_create'] = $co->create_payment_with_midtrans($Payment_configs->get_payment_environment(), $collectData['collect']['payment_data']['transactions'], Payment_configs::$MIDTRANS_APP_SERVER);
			} catch (Exception $ex) {
				exit($engine->createResponse(Engine::codError, $ex->getMessage()));
			}
			if (isset($collectData['collect']['payment_create']['body'])) {
				try {
					$collectData['collect']['payment_create'] = json_decode($collectData['collect']['payment_create']['body']);
				} catch (Exception $ex) {
					$error = true;
					$error_msg = sprintf("Cannot json-decoded: %s", $ex->getMessage());
				}
			} else {
				$error = true;
				$error_msg[] = "Tidak ada body response dari api midtrans.";
			}
		break;

		case 'bt':
		default:
			$collectData['collect']['payment_create'] = array(
				'payment_invoice_id'				=> $collectData['collect']['payment_invoice_id'],
			);
		break;
	}
}



if (!$error) {
	$collectData['collect']['payment_gateway_params'] = array();
	switch (strtolower($input_params['param_payment_code'])) {
		case 'cc':
			
			
		break;
		case 'gopay':
			if (!isset($collectData['collect']['payment_create']->transaction_id) || !isset($collectData['collect']['payment_create']->order_id)) {
				$error = true;
				$error_msg[] = "Tidak ada transaction id maupun order-id.";
			} else {
				$sql_string = "UPDATE tb_tmp_checkout SET pg_transaction_id = :transaction_id, pg_order_id = :order_id WHERE id = :token_id";
				$sql_params = array(
					'transaction_id'				=> ((is_string($collectData['collect']['payment_create']->transaction_id) || is_numeric($collectData['collect']['payment_create']->transaction_id)) ? sprintf("%s", $collectData['collect']['payment_create']->transaction_id) : ''),
					'order_id'						=> ((is_string($collectData['collect']['payment_create']->order_id) || is_numeric($collectData['collect']['payment_create']->order_id)) ? sprintf("%s", $collectData['collect']['payment_create']->order_id) : ''),
					'token_id'						=> $collectData['collect']['payment_information']['id'],
				);
				$engine->executePrepared($sql_string, $sql_params);
			}
		break;
		case 'ovo':
			$collectData['collect']['create_response'] = array(
				'status'						=> true,
				'url'							=> '',
				"order_id"						=> '',
				"gross_amount"					=> $co->getGrandTotal(),
				"currency"						=> "IDR",
				"payment_type"					=> "ovo",
				"transaction_status"			=> "created",
				'actions'						=> array(),
			);
			if (isset($collectData['collect']['payment_create']['response']['faspay']['trx_id']['value']) && isset($collectData['collect']['payment_create']['response']['faspay']['bill_no']['value'])) {
				$collectData['collect']['create_response']['order_id'] = $collectData['collect']['payment_create']['response']['faspay']['bill_no']['value'];
				$collectData['collect']['create_response']['transaction_id'] = $collectData['collect']['payment_create']['response']['faspay']['trx_id']['value'];
				$sql_string = "UPDATE tb_tmp_checkout SET pg_transaction_id = :transaction_id, pg_order_id = :order_id WHERE id = :token_id";
				$sql_params = array(
					'transaction_id'				=> ((is_string($collectData['collect']['payment_create']['response']['faspay']['trx_id']['value']) || is_numeric($collectData['collect']['payment_create']['response']['faspay']['trx_id']['value'])) ? sprintf("%s", $collectData['collect']['payment_create']['response']['faspay']['trx_id']['value']) : ''),
					'order_id'						=> ((is_string($collectData['collect']['payment_create']['response']['faspay']['bill_no']['value']) || is_numeric($collectData['collect']['payment_create']['response']['faspay']['bill_no']['value'])) ? sprintf("%s", $collectData['collect']['payment_create']['response']['faspay']['bill_no']['value']) : ''),
					'token_id'						=> $collectData['collect']['payment_information']['id'],
				);
				$engine->executePrepared($sql_string, $sql_params);
			}
			if (isset($collectData['collect']['payment_create']['url'])) {
				$collectData['collect']['create_response']['url'] = $collectData['collect']['payment_create']['url'];
				array_push($collectData['collect']['create_response']['actions'], array('deeplink_redirect' => array('url' => $collectData['collect']['payment_create']['url'])));
			}
		break;
		case 'bt':
		case 'point':
		default:
			$sql_string = "UPDATE tb_tmp_checkout SET pg_transaction_id = :transaction_id, pg_order_id = :order_id WHERE id = :token_id";
			$sql_params = array(
				'transaction_id'				=> uniqid(),
				'order_id'						=> $collectData['collect']['payment_invoice_id'],
				'token_id'						=> $collectData['collect']['payment_information']['id'],
			);
			$engine->executePrepared($sql_string, $sql_params);
		break;
	}
}















if (!$error) {
	$collectData['collect']['payment_gateway_params']['payment_code'] = strtolower($input_params['param_payment_code']);
	if (in_array(strtolower($input_params['param_payment_code']), array('bt', 'point'))) {
		try {
			$collectData['collect']['payment_place_order'] = $co->placeOrder($collectData['collect']['payment_invoice_id'], $collectData['collect']['payment_gateway_params']);
		} catch (Exception $ex) {
			$error = true;
			$error_msg[] = "Tidak bisa place order dengan exception: {$ex->getMessage()}.";
		}
	}
}
		





if (!$error) {
	switch (strtolower($input_params['param_payment_code'])) {
		case 'cc':
		
		break;
		case 'gopay':
		default:
			$engine->setSingleLineResponse($collectData['collect']['payment_create']);
		break;
		case 'ovo':
			$engine->setSingleLineResponse($collectData['collect']['create_response']);
		break;
		case 'point':
		case 'bt':
			$engine->setSingleLineResponse($collectData['collect']['payment_place_order']);
		break;
	}
} else {
	$engine->setSingleLineResponse($error_msg);
}











$engine->sendResponse();



