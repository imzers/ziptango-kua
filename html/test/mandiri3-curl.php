<?php
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://ibank.bankmandiri.co.id/retail3/loginfo/loginRequest');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
#curl_setopt($ch, CURLOPT_COOKIEJAR, (dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mandiri3.txt'));
curl_setopt($ch, CURLOPT_COOKIEFILE, (dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mandiri3.txt'));
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
curl_setopt($ch, CURLOPT_SSLVERSION, 6);
//curl_setopt($ch, CURLOPT_CAPATH, dirname(__FILE__));
//curl_setopt($ch, CURLOPT_CAINFO, (dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cacert.pem'));

$headers = array();
$headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/539.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36';
$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
$headers[] = 'Accept-Encoding: ';
$headers[] = 'Accept-Language: id-ID,en-ID,en-US,en;q=0.5';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_AUTOREFERER, true);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);

echo $result;
