<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<!DOCTYPE html>
	<html lang="en" class="no-js">
	<head>
		<title><?= (isset($title) ? $title : '');?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta name="MobileOptimized" content="320">
		
		
		<link href="<?= base_url('assets/bootstrap/bootstrap-3.1.0/css/bootstrap.min.css');?>" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css?v=4.7.0');?>" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/plugins/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker3.min.css');?>" />
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/datatables/DataTables-1.10.20/datatables.min.css');?>" />
		
	</head>
	<body class="container-fluid">