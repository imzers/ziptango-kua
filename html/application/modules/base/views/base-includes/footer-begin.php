<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<script type="text/javascript" src="<?= base_url('assets/js/jquery-1.11.1.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/bootstrap/bootstrap-3.1.0/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/datatables/DataTables-1.10.20/datatables.min.js');?>"></script>


