<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<script src="<?= base_static_url('vendor/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_static_url('vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_static_url('vendor/jquery-easing/jquery.easing.min.js');?>"></script>
<!-- Custom scripts for all pages-->
<script src="<?php echo base_static_url('js/sb-admin-2.min.js');?>"></script>
<!-- Page level plugins -->
<script src="<?php echo base_static_url('vendor/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_static_url('vendor/datatables/dataTables.bootstrap4.min.js');?>"></script>
<!-- Sweet Alert -->
<script type="text/javascript" src="<?= base_static_url('vendor/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- Date Picker -->
<script type="text/javascript" src="<?= base_static_url('vendor/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.min.js');?>"></script>
  
<script type="text/javascript">
	$(function() {
		function reposition() {
			var modal = $(this), dialog = modal.find('.modal-dialog');
			modal.css('display', 'block');

			// Dividing by two centers the modal exactly, but dividing by three 
			// or four works better for larger screens.
			//alert($(window).width() + " -  " + dialog.find('.modal-content').width() + " / 2  = " + );
			dialog.css("margin-left", Math.max(0, ($(window).width() - dialog.find('.modal-content').width()) / 2));
			dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
		}
		// Reposition when a modal is shown
		$('.modal').on('show.bs.modal', reposition);
		// Reposition when the window is resized
		$(window).on('resize', function() {
			$('.modal:visible').each(reposition);
		});
	});

	function loadForm(el, target){
		$(el).empty();
		$(el).html('<span class="please-wait">Please wait...</span>');
		$(el).load(target, function(html){
			//$(".please-wait").remove();
		});
	}
	function closeForm(target){
		$(target).remove();
	}

	function handleFileSelect(evt, target) {
		var files = evt.files;

		if(files == undefined || files.length == 0) return;
		
		var file = files[0];

		var reader = new FileReader();
		reader.onload = function(event) {
			$(target).attr("src", event.target.result);
		};
		reader.onerror = function(event) {
			alert("I AM ERROR: " + event.target.error.code);
		};
		reader.readAsDataURL(file);
	  }

</script>
