<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

$page = (isset($page) ? $page : 'dashboard');
$page = (is_string($page) ? strtolower($page) : 'dashboard');
switch ($page) {
	case 'kua-vendors-add':
	case 'kua-vendors-index':
		$section = 'vendor';
	break;
	case 'kua-shio-index':
	case 'kua-shio-add':
	case 'kua-shio-edit':
		$section = 'shio';
	break;
	case 'profile-index':
	case 'profile-password':
	case 'profile-edit':
		$section = 'profile';
	break;
	case 'dashboard':
	default:
		$section = 'dashboard';
	break;
}
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-left" href="<?= base_url('dashboard');?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-cog"></i>
		</div>
		<div class="sidebar-brand-text mx-3">Admin <sup>panel</sup></div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?=(($section == 'dashboard') ? 'active' : '');?>">
		<a class="nav-link" href="<?= base_url('dashboard');?>">
			<i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
		</a>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider">
	<!-- Heading -->
	<div class="sidebar-heading">
		Profile
	</div>

	<!-- Nav Item - Charts -->
	<li class="nav-item <?=(($section === 'profile') ? 'active' : '');?>">
		<a class="nav-link" href="<?= base_url('dashboard/account/profile/password');?>">
			<i class="fas fa-fw fa-key"></i> <span>Password</span>
		</a>
	</li>
	<br />
	<!-- Heading -->
	<div class="sidebar-heading">
		KUA
	</div>
	<!-- Nav Item - Tables -->
	<li class="nav-item <?=(($section === 'vendor') ? 'active' : '');?>">
		<a class="nav-link" href="<?= base_url('shio/vendors');?>">
			<i class="fas fa-fw fa-highlighter"></i> <span>Vendor</span>
		</a>
	</li>
	<!-- Nav Item - Tables -->
	<li class="nav-item <?=(($section === 'shio') ? 'active' : '');?>">
		<a class="nav-link" href="<?= base_url('shio/shio');?>">
			<i class="fas fa-fw fa-leaf"></i> <span>Shio</span>
		</a>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<!-- End of Sidebar -->