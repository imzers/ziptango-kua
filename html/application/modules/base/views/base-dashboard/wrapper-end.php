<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
		</div>
		<!-- Footer -->
		<footer class="sticky-footer bg-white">
			<div class="container my-auto">
				<div class="copyright text-center my-auto">
					<!--<span>Copyright &copy; Your Website 2019</span>-->
				</div>
			</div>
		</footer>
		<!-- End of Footer -->

    </div>
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>