<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title><?= (isset($title) ? $title : 'Admin Panel - Dashboard');?></title>
		<!-- Custom fonts for this template-->
		<link href="<?= base_static_url('vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<!-- Custom styles for this template-->
		<link href="<?= base_static_url('css/sb-admin-2.min.css');?>" rel="stylesheet" />
		<!-- Custom styles for this page -->
		<link href="<?php echo base_static_url('vendor/datatables/dataTables.bootstrap4.min.css');?>" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?= base_static_url('vendor/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker.min.css');?>"/>
		<style type="text/css">
			.padding-10{
				padding:10px;
			}
			.no-padding{
				padding:0px;
			}
			.hidden{
				display:none;
			}
			.align-right{
				text-align:right;
			}
			.fa.fa-star{
				color:#ccc;
			}
			.fa.fa-star.active{
				color:#e3e806;
			}
			.input-group span.input-group-addon{
				border:1px solid #ccc;
				border-right:0px;
				padding:5px 10px 5px 10px;
				
				-webkit-border-top-left-radius: 5px;
			-webkit-border-bottom-left-radius: 5px;
			-moz-border-radius-topleft: 5px;
			-moz-border-radius-bottomleft: 5px;
			border-top-left-radius: 5px;
			border-bottom-left-radius: 5px;
			}
			.sidebar{
				background:#333;
			}
			.nav-item .nav-link{
				padding-top:10px !important; 
				padding-bottom:10px !important;
			}
			.table-responsive{
				width:100%;
			}
			.table-responsive .table{
				width:100%;
			}
			.table-responsive .table{
				*color:#333;
			}
			.table-responsive .table td.table-col-action{
				white-space:nowrap;
			}
			.table-responsive .table td.table-col-action a{
				white-space:nowrap;
			}
			.table-responsive .table td.table-col-action i{
				margin-right:5px;
			}

			.checkbox {
			  display: inline-block;
			  position: relative;
			  padding-left: 20px;
			  margin-right:15px;
			  cursor: pointer;
			  -webkit-user-select: none;
			  -moz-user-select: none;
			  -ms-user-select: none;
			  user-select: none;
			  
			}

			/* Hide the browser's default checkbox */
			.checkbox input {
			  position: absolute;
			  opacity: 0;
			  cursor: pointer;
			  height: 0;
			  width: 0;
			}

			/* Create a custom checkbox */
			.checkbox .checkmark {
			  position: absolute;
			  top: 0;
			  left: 0;
			  height: 15px;
			  width: 15px;
			  margin-top:5px;
			  background-color: #ccc;
			}

			/* On mouse-over, add a grey background color */
			.checkbox:hover input ~ .checkmark {
			  background-color: #aaa;
			}

			/* When the checkbox is checked, add a blue background */
			.checkbox input:checked ~ .checkmark {
			  background-color: #337ab7;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark:after {
			  content: "";
			  position: absolute;
			  display: none;
			}

			/* Show the checkmark when checked */
			.checkbox input:checked ~ .checkmark:after {
			  display: block;
			}

			/* Style the checkmark/indicator */
			.checkbox .checkmark:after {
			  left: 5px;
			  top: 1px;
			  width: 5px;
			  height: 10px;
			  border: solid white;
			  border-width: 0 3px 3px 0;
			  -webkit-transform: rotate(45deg);
			  -ms-transform: rotate(45deg);
			  transform: rotate(45deg);
			}

		</style>
		<!-- Sweet Alert -->
		<link rel="stylesheet" href="<?= base_static_url('vendor/sweetalert2/sweetalert2.min.css');?>" />
	</head>
	<body id="page-top">
		<div id="wrapper">
	
	
	
	
	
