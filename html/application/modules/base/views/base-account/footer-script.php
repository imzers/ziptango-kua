<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_static_url('vendor/jquery/jquery.min.js');?>"></script>
	<script src="<?= base_static_url('vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_static_url('vendor/jquery-easing/jquery.easing.min.js');?>"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_static_url('js/sb-admin-2.min.js');?>"></script>