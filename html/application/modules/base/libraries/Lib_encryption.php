<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
---------
Source
---------
- https://secure.php.net/openssl_encrypt
*/
class Lib_encryption {
	protected $auth;
	protected $CI;
	protected $base_encrypt = array();
	private static $SESS_AES = 'AES-128-CBC';
	private static $SESS_IV = '';
	private static $SESS_HASH, $SESS_CIPHER;
	private static $CIPHER_BINARY = TRUE;
	function __construct($base_encrypt = null) {
		$this->CI = &get_instance();
		if (!isset($base_encrypt)) {
			$this->CI->load->config('base/base_encrypt');
			$base_encrypt = $this->CI->config->item('base_encrypt');
		}
		$this->base_encrypt = $base_encrypt;
		if (isset($this->base_encrypt['sess_aes'])) {
			self::$SESS_AES = strtoupper($this->base_encrypt['sess_aes']);
			$this->set_sess_cipher($this->base_encrypt['sess_aes']);
		}
		if (isset($this->base_encrypt['sess_iv'])) {
			$this->set_sess_iv($this->base_encrypt['sess_iv']);
		}
	}
	//--------------------------------------------------
	public function set_sess_aes($sess_aes = null) {
		if (!isset($sess_aes)) {
			$sess_aes = 'AES-128-CBC';
		}
		self::$SESS_AES = strtoupper($sess_aes);
		return $this;
	}
	public function get_sess_aes() {
		return self::$SESS_AES;
	}
	private function set_sess_iv($sess_iv) {
		self::$SESS_IV = $sess_iv;
		return $this;
	}
	private function set_sess_hash($hash) {
		self::$SESS_HASH = $hash;
	}
	private function set_sess_cipher($cipher) {
		self::$SESS_CIPHER = $cipher;
	}
	private function unpad_by_pkcs7($data = '') {
		if (is_string($data) && (strlen($data) > 0)) {
			return substr($data, 0, -ord($data[strlen($data) - 1]));
		} else {
			return false;
		}
	}
	//--------------------------------------------------
	public function string_encrypt($input_text = null) {
		$key = $this->base_encrypt['key'];
		if (!isset($input_text)) {
			return false;
		}
		$input_text = (is_string($input_text) || is_numeric($input_text)) ? sprintf("%s", $input_text) : '';
		if (strlen($input_text) == 0) {
			return false;
		}
		$collectData = array(
			'key'				=> $key,
			'collect'			=> array(),
		);
		try {
			$collectData['iv_length'] = openssl_cipher_iv_length(self::$SESS_AES);
			$collectData['iv'] = openssl_random_pseudo_bytes($collectData['iv_length']);
			$collectData['collect']['raw'] = openssl_encrypt($input_text, self::$SESS_AES, $collectData['key'], OPENSSL_RAW_DATA, $collectData['iv']);
			$collectData['collect']['hmac'] = hash_hmac($this->base_encrypt['encrypt']['hash_hmac'], $collectData['collect']['raw'], $collectData['key'], self::$CIPHER_BINARY);
			$collectData['collect']['string'] = sprintf("%s%s%s", 
				$collectData['iv'],
				$collectData['collect']['hmac'],
				$collectData['collect']['raw']
			);
			$collectData['collect']['base64'] = base64_encode($collectData['collect']['string']);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $collectData;
	}
	public function string_descrypt($input_base64 = null) {
		if (!isset($input_base64)) {
			return false;
		}
		$collectData = array(
			'key'				=> $this->base_encrypt['key'],
			'is_match'			=> FALSE,
			'input'				=> $input_base64,
			'collect'			=> array(),
			'iv_length'			=> openssl_cipher_iv_length(self::$SESS_CIPHER),
		);
		try {
			$collectData['collect']['text'] = base64_decode($collectData['input']);
			$collectData['iv'] = substr($collectData['collect']['text'], 0, $collectData['iv_length']);
			$collectData['collect']['hmac'] = substr($collectData['collect']['text'], $collectData['iv_length'], $this->base_encrypt['encrypt']['length']);
			$collectData['collect']['raw'] = substr($collectData['collect']['text'], ($collectData['iv_length'] + $this->base_encrypt['encrypt']['length']));
			$collectData['collect']['string'] = openssl_decrypt($collectData['collect']['raw'], self::$SESS_CIPHER, $collectData['key'], OPENSSL_RAW_DATA, $collectData['iv']);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		$collectData['hash_hmac'] = hash_hmac($this->base_encrypt['encrypt']['hash_hmac'], $collectData['collect']['raw'], $collectData['key'], self::$CIPHER_BINARY);
		if (hash_equals($collectData['collect']['hmac'], $collectData['hash_hmac'])) {
			$collectData['is_match'] = TRUE;
		}
		return $collectData;
	}
	
}



