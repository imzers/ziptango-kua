<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

if (!function_exists('base_permalink')) {
	function base_permalink($url) {
		$url = strtolower($url);
		$url = preg_replace('/&.+?;/', '', $url);
		$url = preg_replace('/\s+/', '_', $url);
		$url = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '_', $url);
		$url = preg_replace('|%|', '_', $url);
		$url = preg_replace('/&#?[a-z0-9]+;/i', '', $url);
		$url = preg_replace('/[^%A-Za-z0-9 \_\-]/', '_', $url);
		$url = preg_replace('|_+|', '-', $url);
		$url = preg_replace('|-+|', '-', $url);
		$url = trim($url, '-');
		$url = (strlen($url) > 128) ? substr($url, 0, 128) : $url;
		return $url;
	}
}
if (!function_exists('base_config')) {
	function base_config($config_name = '') {
		$CI = &get_instance();
		$CI->load->config('base/base_config');
		$base_config = $CI->config->item('base_config');
		if (isset($base_config[$config_name])) {
			return $base_config[$config_name];
		}
		return "";
	}
}
if (!function_exists('base_static_url')) {
	function base_static_url($url_path) {
		$CI = &get_instance();
		$CI->load->config('base/base_config');
		$base_config = $CI->config->item('base_config');
		return sprintf("%s/%s",
			$base_config['static_url'],
			$url_path
		);
	}
}
if (!function_exists('base_safe_text')) {
	function base_safe_text($text, $length, $allow_nl = false) {
		$text = htmlspecialchars($text, ENT_QUOTES);
		$text = trim(chop($text));
		$text = $allow_nl ? $text : preg_replace("/[\r|\n]/", "", $text);
		$text = substr($text, 0, $length);
		return $text;
	}
}

