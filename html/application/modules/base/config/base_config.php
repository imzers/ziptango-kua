<?php
if (!defined('BASEPATH')) { exit("Cannot load script directly."); }

$config['base_config'] = array(
	'base_path'					=> 'base',
	'base_tables'				> array(),
);

$config['base_config']['static_url'] = 'http://kua.ziptango.com/assets/sb-admin';
$config['base_config']['per_page'] = 10;

$config['base_config']['base_tables']['kua_tables'] = array(
	'kua_vendor'				=> 'kua_vendor',
	'kua_zodiac'				=> 'kua_zodiac',
);