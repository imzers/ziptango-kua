<?php
if (!defined('BASEPATH')) { exit("Cannot load script directly."); }

$config['base_encrypt'] = array(
	'key'				=> '12345ABCDE72cda1', 
	'encrypt'			=> array(
		'type'					=> 'CBC',
		'hash_hmac'				=> 'sha256',
		'length'				=> 32,
	),
	
);
$config['base_encrypt']['descrypt_cipher'] = 'AES-256-CBC';
$config['base_encrypt']['sess_iv'] = md5(time());
$config['base_encrypt']['sess_aes'] = 'AES-128-CBC';
$config['base_encrypt']['sess_iv_length'] = 16;

