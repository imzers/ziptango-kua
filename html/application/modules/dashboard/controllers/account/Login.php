<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config;
	protected $userdata = null;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		
		
		$this->base_config = $this->mod_account->get_base_config();
	}
	private function get_userdata() {
		$this->userdata = $this->mod_account->get_session_userdata();
	}
	public function index() {		
		$data = array(
			'page'				=> 'login',
			'collect'			=> array(),
		);
		if (!$this->error) {
			if ($this->userdata) {
				$this->error = true;
				$this->error_msg[] = "User already logged in.";
			}
		}
		
		if (!$this->error) {
			$this->load->view('dashboard/account/account.php', $data);
		} else {
			redirect(base_url('dashboard'));
		}
	}
	
	
	
	
	public function action() {
		$this->get_userdata();
		if ($this->userdata) {
			redirect(base_url('dashboard'));
		}
		$data = array(
			'page'				=> 'login-action',
			'collect'			=> array(),
		);
		$this->form_validation->set_rules('input_username', 'Account Username', 'required|max_length[128]');
		$this->form_validation->set_rules('input_password', 'Account Password', 'required|max_length[128]');
		if ($this->form_validation->run() == FALSE) {
			$this->error = true;
			$this->error_msg[] = validation_errors('-', '<br/>');
		} else {
			$data['collect']['query_params'] = array();
			$data['collect']['input_params'] = array(
				'account_username'		=> $this->input->post('input_username', TRUE),
				'account_password'		=> $this->input->post('input_password', TRUE),
			);
			if (!is_string($data['collect']['input_params']['account_username'])) {
				$this->error = true;
				$this->error_msg[] = "Account username should be in string datatype";
			} else {
				$data['collect']['query_params']['account_username'] = sprintf("%s", $data['collect']['input_params']['account_username']);
			}
			if (!is_string($data['collect']['input_params']['account_password'])) {
				$this->error = true;
				$this->error_msg[] = "Account password should be in string datatype";
			} else {
				$data['collect']['query_params']['account_password'] = sprintf("%s", $data['collect']['input_params']['account_password']);
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['account_data'] = $this->mod_account->get_user_account_data_by('username', $data['collect']['query_params']['account_username']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get user account data by username with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['account_data']->account_password)) {
				$this->error = true;
				$this->error_msg[] = "Input account username is not valid.";
			} else {
				try {
					$data['collect']['encrypted_password_input'] = $this->mod_account->decrypt_input($data['collect']['account_data']->account_password);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot encrypt input password with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['encrypted_password_input']['collect']['string'])) {
				$this->error = true;
				$this->error_msg[] = "Failed to descripting current existing password with exception: {$ex->getMessage()}.";
			} else {
				$data['collect']['encrypted_password_input']['collect']['string'] = sprintf("%s", $data['collect']['encrypted_password_input']['collect']['string']);
				if ($data['collect']['query_params']['account_password'] !== $data['collect']['encrypted_password_input']['collect']['string']) {
					$this->error = true;
					$this->error_msg[] = "Password do not match, please try again.";
				}
			}
		}
		if (!$this->error) {
			$data['collect']['session_params'] = array(
				'account_seq'				=> (isset($data['collect']['account_data']->seq) ? $data['collect']['account_data']->seq : 0),
			);
			try {
				$data['collect']['logged_in'] = $this->mod_account->set_userdata_session($data['collect']['session_params']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot logged in with user session with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if ($data['collect']['logged_in'] !== TRUE) {
				$this->error = true;
				$this->error_msg[] = "Cannot logged-in, something wrong with system.";
			}
		}
		if (!$this->error) {
			redirect(base_url('dashboard'));
			exit;
		} else {
			$this->session->set_flashdata('error_msg', json_encode($this->error_msg));
			redirect(base_url('dashboard/account/login/index'));
			exit;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
