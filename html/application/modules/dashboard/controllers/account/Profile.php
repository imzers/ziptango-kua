<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config, $DateObject;
	protected $userdata = null;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		$this->userdata = $this->mod_account->get_userdata();
		
		$this->base_config = $this->mod_account->get_base_config();
		$this->DateObject = $this->mod_account->get_dateobject();
	}
	
	public function index() {		
		$data = array(
			'page'				=> 'profile-index',
			'collect'			=> array(
				'userdata'			=> $this->userdata,
			),
		);
		
		
		redirect(base_url('dashboard'));
	}
	
	function password() {
		$data = array(
			'page'				=> 'profile-password',
			'collect'			=> array(
				'userdata'			=> $this->userdata,
			),
		);

		if (!$this->error) {
			$this->load->view('dashboard/dashboard/dashboard.php', $data);
		}
	}
	function password_change() {
		$data = array(
			'page'				=> 'profile-password',
			'collect'			=> array(
				'userdata'			=> $this->userdata,
			),
		);
		$this->form_validation->set_rules('input_password', 'Account Password', 'required|max_length[128]');
		if ($this->form_validation->run() == FALSE) {
			$this->error = true;
			$this->error_msg[] = validation_errors('-', '<br/>');
		} else {
			$data['collect']['query_params'] = array();
			$data['collect']['input_params'] = array(
				'account_password'		=> $this->input->post('input_password', TRUE),
			);
			if (!is_string($data['collect']['input_params']['account_password'])) {
				$this->error = true;
				$this->error_msg[] = "Account password should be in string datatype";
			} else {
				$data['collect']['query_params']['account_password'] = sprintf("%s", $data['collect']['input_params']['account_password']);
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['account_data'] = $this->mod_account->get_user_account_data_by('seq', $this->userdata->seq);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get user account data by sequence with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['account_data']->account_password)) {
				$this->error = true;
				$this->error_msg[] = "Userdata account username is not exists.";
			} else {
				try {
					$data['collect']['encrypted_password_input'] = $this->mod_account->encrypt_input($data['collect']['query_params']['account_password']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot encrypt input password with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['encrypted_password_input']['collect']['base64'])) {
				$this->error = true;
				$this->error_msg[] = "Failed to encrypt new input password with exception: {$ex->getMessage()}.";
			} else {
				$data['collect']['sql_query_params'] = array(
					'account_password'			=> sprintf("%s", $data['collect']['encrypted_password_input']['collect']['base64']),
					'account_datetime_update'	=> $this->DateObject->format('Y-m-d H:i:s'),
				);
				try {
					$data['collect']['update_password'] = $this->mod_account->set_user_account_data_by('seq', $data['collect']['account_data']->seq, $data['collect']['sql_query_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot update new password with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if ($data['collect']['update_password'] > 0) {
				try {
					$data['collect']['logged_out'] = $this->mod_account->unset_session_userdata();
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot logged out after change new password with exception: {$ex->getMessage()}.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Failed during update new password, no affected rows.";
			}
		}
		if (!$this->error) {
			$this->session->set_flashdata('error_msg', '["Success update new password"]');
		} else {
			$this->session->set_flashdata('error_msg', $this->error_msg);
		}
		
		redirect(base_url('dashboard'));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}