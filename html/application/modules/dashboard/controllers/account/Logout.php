<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config;
	protected $userdata = null;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		$this->get_userdata();
		
		$this->base_config = $this->mod_account->get_base_config();
	}
	private function get_userdata() {
		$this->userdata = $this->mod_account->get_session_userdata();
	}
	public function index() {		
		$data = array(
			'page'				=> 'logout',
			'collect'			=> array(),
		);
		if (!$this->error) {
			if (!$this->userdata) {
				$this->error = true;
				$this->error_msg[] = "User is not logged-in.";
			}
		}
		
		if (!$this->error) {
			try {
				$data['collect']['logged_out'] = $this->mod_account->unset_session_userdata();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot execute process logout with exception: {$ex->getMessage()}.";
			}
		}
		
		redirect(base_url('dashboard'));
	}
	
	
	
	
	
	
	
	
	
	
}