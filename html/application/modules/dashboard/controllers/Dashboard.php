<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrypt;
	protected $userdata = null;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		$this->userdata = $this->mod_account->get_userdata();
		
		$this->base_config = $this->mod_account->get_base_config();
		$this->base_encrypt = $this->mod_account->get_base_encrypt();
	}

	function index() {
		$data = array(
			'page'					=> 'dashboard',
			'collect'				=> array(),
		);
		$data['collect']['userdata'] = $this->userdata;
		
		
		
		
		
		if (!$this->error) {
			$this->load->view('dashboard/dashboard/dashboard.php', $data);
		}
		
	}
	
	function debug() {
		print_r($this);
	}
}
