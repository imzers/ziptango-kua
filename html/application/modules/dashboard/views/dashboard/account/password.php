<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>



<!-- Outer Row -->

	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-12">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-info-900 mb-4">
										<i class="fas fa-key"></i> Change <sup>Password</sup>
									</h1>
								</div>
								<div class="text-center">
									<span class="mb-4">
									
									</span>
								</div>
								<form class="user" method="post" action="<?=base_url('dashboard/account/profile/password-change');?>">
									<div class="form-group">
										<input type="text" class="form-control form-control-user" required="required" name="input_password" placeholder="Type New Password" />
									</div>
									<br />
									<div class="row">
										<a href="<?= base_url('dashboard'); ?>" class="btn btn-success m-1">
											<i class="fa fa-home"></i> Back to HOME
										</a>
										<button type="submit" class="btn btn-primary m-1">Change Password</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

