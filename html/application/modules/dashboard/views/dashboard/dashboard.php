<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

$page = (isset($page) ? strtolower($page) : 'dashboard');

$this->load->view('base/base-dashboard/header-begin.php');
$this->load->view('base/base-dashboard/top-sidebar.php');

$this->load->view('base/base-dashboard/wrapper-begin.php');

$this->load->view('base/base-dashboard/top-topbar.php');
switch (strtolower($page)) {
	case 'profile':
	case 'profile-password':
		$file_view = 'dashboard/dashboard/account/password.php';
	break;
	
	case 'dashboard':
	default:
		$file_view = 'dashboard/dashboard/dashboard/dashboard.php';
	break;
}
$this->load->view($file_view);
$this->load->view('base/base-dashboard/wrapper-end.php');


$this->load->view('base/base-dashboard/footer-begin.php');
$this->load->view('base/base-dashboard/footer-script.php');
switch (strtolower($page)) {
	
	
	case 'dashboard':
	default:
		
	break;
}

$this->load->view('base/base-dashboard/footer-end.php');

