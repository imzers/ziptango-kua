<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<div class="container">
	<!-- Outer Row -->
	<div class="row justify-content-center">

		<div class="col-md-6">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
				<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-12">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-info-900 mb-4">
										<i class="fas fa-cog"></i> Admin <sup>Panel</sup>
									</h1>
								</div>
								<div class="text-center">
									<span class="mb-4" style="color:red">
										<?= (isset($collect['status']) ? $collect['status'] : '');?>
									</span>
								</div>
								<form class="user" method="post" action="<?= base_url('dashboard/account/login/action');?>">
									<div class="form-group">
										<input type="text" class="form-control form-control-user" name="input_username" placeholder="Username" />
									</div>
									<div class="form-group">
										<input type="text" class="form-control form-control-user" name="input_password" placeholder="Password" />
									</div>
									<br />
									<button class="btn btn-primary btn-user btn-block">
										Login
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






