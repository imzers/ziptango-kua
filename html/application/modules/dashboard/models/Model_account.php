<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Imzers\Utils\Datezone;
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
class Model_account extends CI_Model {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrypt, $DateObject;
	protected $userdata = null;
	protected $db_api;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->load->config('base/base_encrypt');
		$this->base_config = $this->config->item('base_config');
		$this->base_encrypt = $this->config->item('base_encrypt');
		
		$this->load->helper('base/base_config');
		$this->load->library('base/Lib_encryption', $this->base_encrypt, 'lib_encrypt');
		
		$this->db_api = $this->load->database('api', TRUE);
		
		# Load Codeigniter helpers
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		# Session start
		$this->load->library('session');
		$this->start_userdata();
		
		# DateObject
		$this->DateObject = Imzers\Utils\Datezone::create_time_zone('Asia/Bangkok', date('Y-m-d H:i:s'));
	}
	public function get_base_config() {
		return $this->base_config;
	}
	public function get_base_encrypt() {
		return $this->base_encrypt;
	}
	public function get_dateobject() {
		return $this->DateObject;
	}
	
	
	private function start_userdata() {
		if ($this->session->userdata('account_seq') != FALSE) {
			try {
				$this->userdata = $this->set_userdata($this->session->userdata('account_seq'));
			} catch (Exception $ex) {
				throw $ex;
				return false;
			}
		}
	}
	public function get_userdata() {
		if (!$this->userdata) {
			redirect(base_url('dashboard/account/login/index'));
			exit;
		}
		return $this->userdata;
	}
	public function set_userdata($account_seq) {
		if (!is_numeric($account_seq)) {
			return false;
		}
		$this->db_api->select('*')->from('dashboard_account')
			->where('seq', $account_seq);
		try {
			$sql_query = $this->db_api->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	public function set_userdata_session($sesion_params) {
		if (!isset($sesion_params['account_seq'])) {
			return false;
		}
		if (!is_numeric($sesion_params['account_seq'])) {
			return false;
		}
		$this->session->set_userdata('account_seq', $sesion_params['account_seq']);
		return true;
	}
	public function get_session_userdata() {
		return $this->userdata;
	}
	//----
	public function unset_session_userdata() {
		$this->userdata = null;
		$this->session->unset_userdata('account_seq');
		return true;
	}
	//-----------------------------------
	public function get_user_account_data_by(String $by_type, $by_value) {
		if (!is_string($by_type)) {
			return false;
		}
		$value = '';
		if (is_string($by_value)) {
			$value = sprintf("%s", $by_value);
		} else if (is_numeric($by_value)) {
			$value = sprintf("%d", $by_value);
		} else {
			$value = sprintf("%s", $by_value);
		}
		if (strlen(sprintf("%s", $value)) > 0) {
			$this->db_api->select('*')->from('dashboard_account');
			switch (strtolower($by_type)) {
				case 'seq':
					$this->db_api->where('seq', $value);
				break;
				case 'username':
				default:
					$this->db_api->where('account_username', $value);
				break;
			}
			$this->db_api->limit(1);
			try {
				$sql_query = $this->db_api->get();
			} catch (Exception $ex) {
				throw $ex;
				return false;
			}
			return $sql_query->row();
		}
		return false;
	}
	public function set_user_account_data_by($by_type, $by_value, $input_params) {
		if (!is_string($by_type)) {
			return false;
		}
		$value = '';
		if (is_string($by_value)) {
			$value = sprintf("%s", $by_value);
		} else if (is_numeric($by_value)) {
			$value = sprintf("%d", $by_value);
		} else {
			$value = sprintf("%s", $by_value);
		}
		
		switch (strtolower($by_type)) {
			case 'seq':
				$this->db_api->where('seq', $value);
			break;
			case 'username':
			default:
				$this->db_api->where('account_username', $value);
			break;
		}
		
		$this->db_api->update('dashboard_account', $input_params);
		return $this->db_api->affected_rows();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function encrypt_input($input_string) {
		if (!is_string($input_string)) {
			return false;
		}
		try {
			$encrypted = $this->lib_encrypt->string_encrypt($input_string);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $encrypted;
	}
	function decrypt_input($input_string) {
		if (!is_string($input_string)) {
			return false;
		}
		try {
			$decrypted = $this->lib_encrypt->string_descrypt($input_string);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $decrypted;
	}
	public function get_pagination_start($page, $per_page, $rows_count) {
		$end_page = ceil($rows_count / $per_page);
		if ($end_page < 2) { $end_page = 1; }
		$page = isset($page) ? intval($page) : 1;
		//if ($page < 1 || $page > $end_page) { $page = 1; }
		$start = ceil(($page * $per_page) - $per_page);
		if ($start > 0) {
			return $start;
		}
		return 0;
	}
	public function generate_pagination($self, &$page, $per_page, $rows_count, &$start) {
		$sum_pages = ceil($rows_count / $per_page);
		if ($sum_pages < 2) { $sum_pages = 1; }
		$page = isset($page) ? intval($page) : 1;
		if ($page < 1 || $page > $sum_pages) { $page = 1; }
		$start = ceil(($page * $per_page) - $per_page);
				

		$page_display = '<nav aria-label="...">';
		$page_display .= '<ul class="pagination">';
		if ($page > 1) {
			$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, ($sum_pages / $sum_pages)) . '"><i class="fa fa-angle-double-left"></i></a></li>';
		} else {
			$page_display .= '<li class="page-item disabled"><a class="page-link" href="' . sprintf($self, ($sum_pages / $sum_pages)) . '" tabindex="-1"><i class="fa fa-angle-double-left"></i></a></li>';
		}
		if ($sum_pages <= 0) {
			if ($page > 1) {
				$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, ($page - 1)) . '" tabindex="-1">Previous</a></li>';
			} else {
				$page_display .= '<li class="page-item disabled"><a class="page-link" href="' . sprintf($self, ($page - 1)) . '">Previous</a></li>';
			}
			$i = 1;
			while ($i <= $sum_pages) {
				$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				$i++;
			}
			$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, ($page + 1)) . '">Next</a></li>';
		} else {
			if ($page > 1) {
				$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, ($page - 1)) . '">Previous</a></li>';
			} else {
				$page_display .= '<li class="page-item disabled"><a class="page-link" href="' . sprintf($self, ($page - 1)) . '" tabindex="-1">Previous</a></li>';
			}
			for ($i = ($page - 2); $i < $page; $i++) {
				if ($i > 0) {
					$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			$page_display .= '<li class="page-item active"><a class="page-link" href="#">' . $page . '<span class="sr-only">(current)</span></a></li>';
			for ($i = ($page + 1); $i <= ($page + 2); $i++) {
				if ($i <= $sum_pages) {
					$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			if (($page + 1) > $sum_pages) {
				$page_display .= '<li class="page-item disabled"><a class="page-link" href="' . sprintf($self, ($page + 1)) . '" tabindex="-1">Next</a></li>';
			} else {
				$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, ($page + 1)) . '">Next</a></li>';
			}
		}
		if ($page >= $sum_pages) {
			$page_display .= '<li class="page-item disabled"><a class="page-link" href="' . sprintf($self, $sum_pages) . '" tabindex="-1"><i class="fa fa-angle-double-right"></i></a></li>';
		} else {
			$page_display .= '<li class="page-item"><a class="page-link" href="' . sprintf($self, $sum_pages) . '"><i class="fa fa-angle-double-right"></i></a></li>';
		}
		$page_display .= '</ul>';
		$page_display .= '</nav>';
		return $page_display;
	}
	
}