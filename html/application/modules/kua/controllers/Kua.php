<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}


class Kua extends MY_Controller {
	public $error = FALSE, $error_msg = array();
	protected $DateObject;
	function __construct() {
		parent::__construct();
		
		$this->DateObject = Imzers\Utils\Datezone::create_time_zone('Asia/Bangkok', date('Y-m-d H:i:s'));
		# Load CI Helpers
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	
	function index() {
		phpinfo();
	}
	
	function cek_kua() {
		$collectData = array(
			'base_path'				=> 'kua',
			'collect'				=> array(),
		);
		if (!$this->error) {
			$this->form_validation->set_rules('input_gender', 'Person Gender', 'required|numeric|max_length[1]|xss_clean');
			$this->form_validation->set_rules('input_datebirth', 'Person Birthday', 'required|max_length[16]|xss_clean');
			$this->form_validation->set_rules('input_action', 'KUA Action', 'required|min_length[8]|max_length[64]|xss_clean');
		}
		if (!$this->error) {
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', "\r\n");
			} else {
				$collectData['collect']['input_params'] = array(
					'input_gender'				=> $this->input->post('input_gender'),
					'input_datebirth'			=> $this->input->post('input_datebirth'),
					'input_action'				=> $this->input->post('input_action'),
				);
			}
		}
		if (!$this->error) {
			
		}
		
	}
}