<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Model_shio_data extends CI_Model {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrypt, $DateObject;
	protected $userdata = null;
	protected $db_kua, $kua_tables;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->load->config('base/base_encrypt');
		$this->base_config = $this->config->item('base_config');
		$this->base_encrypt = $this->config->item('base_encrypt');
		
		$this->kua_tables = (isset($this->base_config['base_tables']['kua_tables']) ? $this->base_config['base_tables']['kua_tables'] : array());
		# Load Database
		$this->db_kua = $this->load->database('kua', TRUE);
		
		# DateObject
		$this->DateObject = Imzers\Utils\Datezone::create_time_zone('Asia/Bangkok', date('Y-m-d H:i:s'));
	}
	
	
	
	function get_kua_date_from_database($paito_vendor_data, $by_type, $by_value) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'day':
			case 'month':
			case 'year':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		if (!isset($paito_vendor_data->seq)) {
			return false;
		}
		$this->db_kua->where('pd.vendor_seq', $paito_vendor_data->seq);
		$this->db_kua->from('paito_all AS pd');
		switch (strtolower($collectData['by_type'])) {
			case 'month':
				$this->db_paito->select("MIN(DATE_FORMAT(pd.tgl_rilis, '%Ym')) AS date_min, MAX(DATE_FORMAT(pd.tgl_rilis, '%Ym')) AS date_max");
			break;
			case 'day':
				$this->db_paito->select('MIN(pd.tgl_rilis) AS date_min, MAX(pd.tgl_rilis) AS date_max');
			break;
			case 'year':
			default:
				$this->db_paito->select('MIN(pd.tahun) AS date_min, MAX(pd.tahun) AS date_max');
			break;
		}
		try {
			$sql_query = $this->db_paito->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	function get_paito_all_by_vendor_seq_year($vendor_seq, $vendor_year) {
		$collectData = array(
			'vendor_seq'				=> (is_numeric($vendor_seq) ? intval($vendor_seq) : 0),
			'vendor_year'				=> (is_numeric($vendor_year) ? intval($vendor_year) : 0),
		);
		if (!preg_match('/^[1-9][0-9]*$/', $collectData['vendor_seq'])) {
			return false;
		}
		if (!preg_match('/^[1-9][0-9]*$/', $collectData['vendor_year'])) {
			return false;
		}
		$this->db_paito->select('pd.id, pd.vendor_seq, pd.nilai, pd.tgl_rilis, pd.harinya, pd.tahun, pd.vendor, WEEK(pd.tgl_rilis) AS week_number')->from('paito_all AS pd');
		$this->db_paito->where('pd.vendor_seq', $collectData['vendor_seq']);
		$this->db_paito->where('pd.tahun', sprintf("%s", $collectData['vendor_year']));
		
		$this->db_paito->order_by('pd.tgl_rilis', 'DESC');
		
		try {
			$sql_query = $this->db_paito->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	function get_paito_dayname_by_vendor_seq($vendor_seq) {
		$collectData = array(
			'vendor_seq'				=> (is_numeric($vendor_seq) ? intval($vendor_seq) : 0),
		);
		if (!preg_match('/^[1-9][0-9]*$/', $collectData['vendor_seq'])) {
			return false;
		}
		$sql = sprintf("SELECT paa.* FROM(SELECT pd.paito_dayname, pd.vendor_seq, pv.vendor_name AS paito_vendor_name, CASE WHEN LOWER(pd.paito_dayname) = 'monday' THEN 1 WHEN LOWER(pd.paito_dayname) = 'tuesday' THEN 2 WHEN LOWER(pd.paito_dayname) = 'wednesday' THEN 3 WHEN LOWER(pd.paito_dayname) = 'thursday' THEN 4 WHEN LOWER(pd.paito_dayname) = 'friday' THEN 5 WHEN LOWER(pd.paito_dayname) = 'saturday' THEN 6 WHEN LOWER(pd.paito_dayname) = 'sunday' THEN 7 ELSE 7 END AS paito_dayindex FROM (SELECT harinya AS paito_dayname, vendor_seq FROM %s WHERE vendor_seq = '%d' GROUP BY paito_dayname, vendor_seq) AS pd LEFT JOIN %s AS pv ON pv.seq = pd.vendor_seq) AS paa ORDER BY paa.paito_dayindex ASC",
			'paito_all',
			$this->db_paito->escape_str($collectData['vendor_seq']),
			'paito_vendor'
		);
		try {
			$sql_query = $this->db_paito->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	function get_paito_dayname_on_wholeweek() {
		return false;
	}
	function get_paito_digit_shio($digit_number) {
		$digit_number = (is_numeric($digit_number) ? (int)$digit_number : '');
		if (!preg_match('/^[0-9]*$/', $digit_number)) {
			return false;
		}
		$this->db_paito->where('shio_angka', $digit_number);
		$this->db_paito->limit(1);
		try {
			$sql_query = $this->db_paito->get('paito_shio');
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	private function get_paito_shio_by_value(String $digit_number) {
		if (!preg_match('/^[0-9]*$/', $digit_number)) {
			return false;
		}
		$digit_number = sprintf("%02s", $digit_number);
		$this->db_paito->where('shio_value', $digit_number);
		$this->db_paito->order_by('shio_angka', 'ASC');
		try {
			$sql_query = $this->db_paito->get('paito_shio');
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	public function get_paito_shio_values() {
		$paito_shios = array();
		for ($s = 1; $s <= (int)$this->base_config['paito_jumlah']['shio']; $s++) {
			$string_index = sprintf("%02s", $s);
			$paito_shios_string = $this->get_paito_shio_by_value($string_index);
			if (is_array($paito_shios_string) && (count($paito_shios_string) > 0)) {
				foreach ($paito_shios_string as $paito_shio) {
					$paito_shios[sprintf("%02s", $paito_shio->shio_angka)] = sprintf("%02s", $paito_shio->shio_value);
				}
			}
		}
		return $paito_shios;
	}
	
	
	// Tools
	public function get_week_number_maximum($data, $column_key = 'week_number') {
		$column_key = (is_string($column_key) ? sprintf("%s", $column_key) : FALSE);
		if (($column_key !== FALSE) && (strlen($column_key) > 0)) {
			return max(array_column($data, $column_key));
		}
		return 0;
	}
	public function add_week_number_to_paito_data($paito_data) {
		if (is_array($paito_data) && (count($paito_data) > 0)) {
			foreach ($paito_data as &$pd) {
				$pd_date = new DateTime($pd->tgl_rilis);
				$pd->week_number = sprintf("%d", $pd_date->format('W'));
			}
		}
		return $paito_data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}