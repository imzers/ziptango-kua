<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Model_shio_vendor extends CI_Model {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrypt, $DateObject;
	protected $userdata = null;
	protected $db_kua, $kua_tables;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->load->config('base/base_encrypt');
		$this->base_config = $this->config->item('base_config');
		$this->base_encrypt = $this->config->item('base_encrypt');
		
		$this->kua_tables = (isset($this->base_config['base_tables']['kua_tables']) ? $this->base_config['base_tables']['kua_tables'] : array());
		# Load Database
		$this->db_kua = $this->load->database('kua', TRUE);
		
		# DateObject
		$this->DateObject = Imzers\Utils\Datezone::create_time_zone('Asia/Bangkok', date('Y-m-d H:i:s'));
	}
	public function get_kua_vendor_default() {
		$vendor_data = new stdClass();
		$vendor_data->seq = 0;
		$vendor_data->vendor_code = 'all';
		$vendor_data->vendor_name = 'All';
		$vendor_data->vendor_datetime_insert = $this->DateObject->format('Y-m-d H:i:s');
		$vendor_data->vendor_datetime_update = $this->DateObject->format('Y-m-d H:i:s');
		return $vendor_data;
	}
	public function get_kua_vendors() {
		$this->db_kua->select('*');
		$this->db_kua->from($this->kua_tables['kua_vendor']);
		$this->db_kua->order_by('vendor_code', 'ASC');
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	
	public function get_kua_vendor_count_by($by_type, $by_value, $search_text = '') {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		$collectData['search_text'] = (is_string($search_text) ? sprintf("%s", $search_text) : '');
		switch (strtolower($collectData['by_type'])) {
			case 'all':
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('COUNT(seq) AS value');
		$this->db_kua->from($this->kua_tables['kua_vendor']);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			case 'seq':
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
			case 'all':
			default:
				$this->db_kua->where('1 = 1', NULL, FALSE);
			break;
		}
		if (strlen($collectData['search_text']) > 0) {
			$sql_search_strings = '(';
			$collectData['search_array'] = explode("-", $collectData['search_text']);
			if (count($collectData['search_array']) > 0) {
				$foreach_i = 0;
				foreach ($collectData['search_array'] as $search_str) {
					if ($foreach_i > 0) {
						$sql_search_strings .= sprintf(" AND (CONCAT('', vendor_name, '') LIKE '%%%s%%')", $search_str);
					} else {
						$sql_search_strings .= sprintf("(CONCAT('', vendor_name, '') LIKE '%%%s%%')", $search_str);
					}
					$foreach_i++;
				}
			} else {
				$sql_search_strings .= "1 = 1";
			}
			$sql_search_strings .= ')';
			$this->db_kua->where($sql_search_strings, NULL, FALSE);
		}
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	function get_kua_vendor_data_by($by_type, $by_value, $search_text = '', $start = 0, $per_page = 10) {
		$start = (is_numeric($start) ? (int)$start : 0);
		$per_page = (is_numeric($per_page) ? (int)$per_page : 0);
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		$collectData['search_text'] = (is_string($search_text) ? sprintf("%s", $search_text) : '');
		switch (strtolower($collectData['by_type'])) {
			case 'all':
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('*');
		$this->db_kua->from($this->kua_tables['kua_vendor']);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			case 'seq':
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
			case 'all':
			default:
				$this->db_kua->where('1 = 1', NULL, FALSE);
			break;
		}
		if (strlen($collectData['search_text']) > 0) {
			$sql_search_strings = '(';
			$collectData['search_array'] = explode("-", $collectData['search_text']);
			if (count($collectData['search_array']) > 0) {
				$foreach_i = 0;
				foreach ($collectData['search_array'] as $search_str) {
					if ($foreach_i > 0) {
						$sql_search_strings .= sprintf(" AND (CONCAT('', vendor_name, '') LIKE '%%%s%%')", $search_str);
					} else {
						$sql_search_strings .= sprintf("(CONCAT('', vendor_name, '') LIKE '%%%s%%')", $search_str);
					}
					$foreach_i++;
				}
			} else {
				$sql_search_strings .= "1 = 1";
			}
			$sql_search_strings .= ')';
			$this->db_kua->where($sql_search_strings, NULL, FALSE);
		}
		$this->db_kua->order_by('vendor_datetime_update', 'DESC');
		$this->db_kua->limit($per_page, $start);
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	public function get_kua_vendor_single_data_by($by_type, $by_value) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('*')->from($this->kua_tables['kua_vendor']);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
		}
		
		
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	public function get_kua_vendor_single_data_withoutme_by($vendor_seq, $by_type, $by_value) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
			'vendor_seq'			=> (is_numeric($vendor_seq) ? (int)$vendor_seq : 0),
		);
		try {
			$collectData['vendor_data'] = $this->get_kua_vendor_single_data_by('seq', $collectData['vendor_seq']);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		if (!isset($collectData['vendor_data']->seq)) {
			return false;
		}
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('*')->from($this->kua_tables['kua_vendor']);
		$this->db_kua->where('seq !=', $collectData['vendor_data']->seq);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
		}
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	function insert_kua_vendor($input_params) {
		$this->db_kua->insert($this->kua_tables['kua_vendor'], $input_params);
		return $this->db_kua->insert_id();
	}
	function set_kua_vendor_by($by_type, $by_value, $input_params) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$query_params = array();
		if (isset($input_params['vendor_name'])) {
			$query_params['vendor_name'] = $input_params['vendor_name'];
		}
		if (isset($input_params['vendor_code'])) {
			$query_params['vendor_code'] = $input_params['vendor_code'];
		}
		if (isset($input_params['vendor_datetime_insert'])) {
			$query_params['vendor_datetime_insert'] = $input_params['vendor_datetime_insert'];
		}
		if (isset($input_params['vendor_datetime_update'])) {
			$query_params['vendor_datetime_update'] = $input_params['vendor_datetime_update'];
		}
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
		}
		$this->db_kua->update($this->kua_tables['kua_vendor'], $query_params);
		return $this->db_kua->affected_rows();
	}
	function delete_kua_vendor_by($by_type, $by_value, $input_params = array()) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$query_params = 'seq';
		if (isset($input_params['vendor_code'])) {
			$query_params = $input_params['vendor_code'];
		}
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				try {
					$collectData['vendor_data'] = $this->get_kua_vendor_single_data_by('code', $collectData['by_value']);
				} catch (Exception $ex) {
					throw $ex;
					return false;
				}
			break;
			case 'name':
				try {
					$collectData['vendor_data'] = $this->get_kua_vendor_single_data_by('name', $collectData['by_value']);
				} catch (Exception $ex) {
					throw $ex;
					return false;
				}
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				try {
					$collectData['vendor_data'] = $this->get_kua_vendor_single_data_by('seq', $collectData['by_value']);
				} catch (Exception $ex) {
					throw $ex;
					return false;
				}
			break;
		}
		if (!isset($collectData['vendor_data']->seq)) {
			return false;
		}
		# Start Delete
		$this->db_kua->where('seq', $collectData['vendor_data']->seq)->delete($this->kua_tables['kua_vendor']);
		$this->db_kua->where('vendor_seq', $collectData['vendor_data']->seq)->delete($this->kua_tables['kua_zodiac']);
		
		return $this->db_kua->affected_rows();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}