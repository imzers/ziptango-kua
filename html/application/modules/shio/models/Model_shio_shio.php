<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Model_shio_shio extends CI_Model {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrypt, $DateObject;
	protected $userdata = null;
	protected $db_kua, $kua_tables;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->load->config('base/base_encrypt');
		$this->base_config = $this->config->item('base_config');
		$this->base_encrypt = $this->config->item('base_encrypt');
		
		$this->kua_tables = (isset($this->base_config['base_tables']['kua_tables']) ? $this->base_config['base_tables']['kua_tables'] : array());
		# Load Database
		$this->db_kua = $this->load->database('kua', TRUE);
		
		# DateObject
		$this->DateObject = Imzers\Utils\Datezone::create_time_zone('Asia/Bangkok', date('Y-m-d H:i:s'));
	}
	
	
	public function get_vendor_kua_count_by($by_type, $by_value, $condition_params = array(), $search_text = '') {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		$collectData['search_text'] = (is_string($search_text) ? sprintf("%s", $search_text) : '');
		switch (strtolower($collectData['by_type'])) {
			case 'all':
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('COUNT(pa.seq) AS value');
		$this->db_kua->from("{$this->kua_tables['kua_zodiac']} AS pa");
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->join("{$this->kua_tables['kua_vendor']} AS pv", 'pv.seq = pa.vendor_seq', 'LEFT');
				$this->db_kua->where('pv.vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->join("{$this->kua_tables['kua_vendor']} AS pv", 'pv.seq = pa.vendor_seq', 'LEFT');
				$this->db_kua->where('pv.vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
				$this->db_kua->where('pa.vendor_seq', $collectData['by_value']);
			break;
			case 'seq':
			case 'id':
				$this->db_kua('pa.seq', $collectData['by_value']);
			break;
			case 'all':
			default:
				$this->db_kua->where('1 = 1', NULL, FALSE);
			break;
		}
		if (strlen($collectData['search_text']) > 0) {
			$sql_search_strings = '(';
			$collectData['search_array'] = explode("-", $collectData['search_text']);
			if (count($collectData['search_array']) > 0) {
				$foreach_i = 0;
				foreach ($collectData['search_array'] as $search_str) {
					if ($foreach_i > 0) {
						$sql_search_strings .= sprintf(" AND (CONCAT('', pa.zodiac_name_english, '') LIKE '%%%s%%' OR CONCAT('', pa.zodiac_name_indonesia, '') LIKE '%%%s%%')", $search_str, $search_str);
					} else {
						$sql_search_strings .= sprintf("(CONCAT('', pa.zodiac_name_english, '') LIKE '%%%s%%' OR CONCAT('', pa.zodiac_name_indonesia, '') LIKE '%%%s%%')", $search_str, $search_str);
					}
					$foreach_i++;
				}
			} else {
				$sql_search_strings .= "1 = 1";
			}
			$sql_search_strings .= ')';
			$this->db_kua->where($sql_search_strings, NULL, FALSE);
		}
		// Condition params
		if (isset($condition_params['kua_date']['starting']) && isset($condition_params['kua_date']['stopping'])) {
			$this->db_kua->where("pa.zodiac_year BETWEEN '{$condition_params['kua_date']['starting']}' AND '{$condition_params['kua_date']['stopping']}'", NULL, FALSE);
		}
		
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	function get_vendor_kua_data_by($by_type, $by_value, $condition_params = array(), $search_text = '', $start = 0, $per_page = 10) {
		$start = (is_numeric($start) ? (int)$start : 0);
		$per_page = (is_numeric($per_page) ? (int)$per_page : 0);
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		$collectData['search_text'] = (is_string($search_text) ? sprintf("%s", $search_text) : '');
		switch (strtolower($collectData['by_type'])) {
			case 'all':
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('pa.*, pv.seq AS pv_seq, pv.vendor_code, pv.vendor_name');
		$this->db_kua->from("{$this->kua_tables['kua_zodiac']} AS pa");
		$this->db_kua->join("{$this->kua_tables['kua_vendor']} AS pv", 'pv.seq = pa.vendor_seq', 'LEFT');
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->where('pv.vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->where('pv.vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
				$this->db_kua->where('pa.vendor_seq', $collectData['by_value']);
			break;
			case 'seq':
			case 'id':
				$this->db_kua('pa.id', $collectData['by_value']);
			break;
			case 'all':
			default:
				$this->db_kua->where('1 = 1', NULL, FALSE);
			break;
		}
		if (strlen($collectData['search_text']) > 0) {
			$sql_search_strings = '(';
			$collectData['search_array'] = explode("-", $collectData['search_text']);
			if (count($collectData['search_array']) > 0) {
				$foreach_i = 0;
				foreach ($collectData['search_array'] as $search_str) {
					if ($foreach_i > 0) {
						$sql_search_strings .= sprintf(" AND (CONCAT('', pa.zodiac_name_english, '') LIKE '%%%s%%' OR CONCAT('', pa.zodiac_name_indonesia, '') LIKE '%%%s%%')", $search_str, $search_str);
					} else {
						$sql_search_strings .= sprintf("(CONCAT('', pa.zodiac_name_english, '') LIKE '%%%s%%' OR CONCAT('', pa.zodiac_name_indonesia, '') LIKE '%%%s%%')", $search_str, $search_str);
					}
					$foreach_i++;
				}
			} else {
				$sql_search_strings .= "1 = 1";
			}
			$sql_search_strings .= ')';
			$this->db_kua->where($sql_search_strings, NULL, FALSE);
		}
		// Condition params
		if (isset($condition_params['kua_date']['starting']) && isset($condition_params['kua_date']['stopping'])) {
			$this->db_kua->where("pa.zodiac_year BETWEEN '{$condition_params['kua_date']['starting']}' AND '{$condition_params['kua_date']['stopping']}'", NULL, FALSE);
		}
		
		
		if (isset($condition_params['order_by']['key']) && isset($condition_params['order_by']['sort'])) {
			$this->db_kua->order_by($condition_params['order_by']['key'], $condition_params['order_by']['sort']);
		} else {
			$this->db_kua->order_by('pa.zodiac_year', 'DESC');
		}
		$this->db_kua->limit($per_page, $start);
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->result();
	}
	public function get_kua_single_data_by($by_type, $by_value, $condition_params = array()) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'hari':
			case 'code':
			case 'name':
			case 'nilai':
			case 'zodiac':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'paito_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('*')->from("{$this->kua_tables['kua_zodiac']}");
		switch (strtolower($collectData['by_type'])) {
			case 'hari':
				$this->db_kua->where('harinya', $collectData['by_value']);
			break;
			case 'nilai':
			case 'zodiac':
				$this->db_kua->where('zodiac_name_english', $collectData['by_value']);
				$this->db_kua->or_where('zodiac_name_indonesia', $collectData['by_value']);
			break;
			case 'vendor_seq':
				$this->db_kua->where('vendor_seq', $collectData['by_value']);
			break;
			case 'seq':
			case 'id':
			default:
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
		}
		if (isset($condition_params['vendor_seq'])) {
			$this->db_kua->where('vendor_seq', $condition_params['vendor_seq']);
		}
		if (isset($condition_params['zodiac_year'])) {
			$this->db_kua->where('zodiac_year', $condition_params['zodiac_year']);
		}
		if (isset($condition_params['tahun'])) {
			$this->db_kua->where('zodiac_year', $condition_params['zodiac_year']);
		}
		
		
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	public function get_kua_single_data_withoutme_by($kua_seq, $by_type, $by_value, $condition_params = array()) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
			'kua_seq'				=> (is_numeric($kua_seq) ? (int)$kua_seq : 0),
		);
		try {
			$collectData['kua_data'] = $this->get_kua_single_data_by('seq', $collectData['kua_seq']);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		if (!isset($collectData['kua_data']->seq)) {
			return false;
		}
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'kua_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$this->db_kua->select('pa.*')->from("{$this->kua_tables['kua_zodiac']} AS pa");
		$this->db_kua->where('pa.seq !=', $collectData['kua_data']->seq);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
				$this->db_kua->join("{$this->kua_tables['kua_vendor']} AS pv", 'pv.seq = pa.vendor_seq', 'LEFT');
				$this->db_kua->where('pv.vendor_code', $collectData['by_value']);
			break;
			case 'name':
				$this->db_kua->join("{$this->kua_tables['kua_vendor']} AS pv", 'pv.seq = pa.vendor_seq', 'LEFT');
				$this->db_kua->where('pv.vendor_name', $collectData['by_value']);
			break;
			case 'vendor_seq':
			default:
				$this->db_kua->where('pa.vendor_seq', $collectData['by_value']);
			break;
			case 'kua_seq':
			case 'seq':
			case 'id':
				$this->db_kua->where('pa.seq', $collectData['by_value']);
			break;
		}
		if (isset($condition_params['zodiac_year'])) {
			$this->db_kua->where('pa.zodiac_year', $condition_params['zodiac_year']);
		}
		
		
		try {
			$sql_query = $this->db_kua->get();
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $sql_query->row();
	}
	function set_kua_data_by($by_type, $by_value, $input_params) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'all'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'code':
			case 'name':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			case 'id':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$query_params = array();
		if (isset($input_params['vendor_seq'])) {
			$query_params['vendor_seq'] = $input_params['vendor_seq'];
		}
		if (isset($input_params['zodiac_name_english'])) {
			$query_params['zodiac_name_english'] = $input_params['zodiac_name_english'];
		}
		if (isset($input_params['zodiac_name_indonesia'])) {
			$query_params['zodiac_name_indonesia'] = $input_params['zodiac_name_indonesia'];
		}
		if (isset($input_params['zodiac_year'])) {
			$query_params['zodiac_year'] = $input_params['zodiac_year'];
		}
		if (isset($input_params['zodiac_type'])) {
			$query_params['zodiac_type'] = $input_params['zodiac_type'];
		}
		if (isset($input_params['vendor_seq'])) {
			$query_params['vendor_seq'] = $input_params['vendor_seq'];
		}
		switch (strtolower($collectData['by_type'])) {
			case 'vendor_seq':
				$this->db_kua->where('vendor_seq', $collectData['by_value']);
			break;
			case 'seq':
			case 'id':
			default:
				$this->db_kua->where('seq', $collectData['by_value']);
			break;
		}
		$this->db_kua->update($this->kua_tables['kua_zodiac'], $query_params);
		return $this->db_kua->affected_rows();
	}
	function delete_kua_data_by($by_type, $by_value, $input_params = null) {
		$collectData = array(
			'by_type'				=> (is_string($by_type) ? strtolower($by_type) : 'seq'),
		);
		switch (strtolower($collectData['by_type'])) {
			case 'nilai':
			case 'tgl_rilis':
			case 'zodiac_year':
			case 'zodiac_name_english':
				$collectData['by_value'] = sprintf("%s", $by_value);
			break;
			case 'vendor_seq':
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$collectData['by_value'] = 0;
				} else {
					$collectData['by_value'] = sprintf('%d', $by_value);
				}
			break;
		}
		$query_params = 'seq';
		if (isset($input_params->vendor_seq)) {
			$query_params = $input_params->vendor_seq;
		}
		switch (strtolower($collectData['by_type'])) {
			case 'zodiac_name_english':
				$this->db_kua->where('zodiac_name_english', $collectData['by_value']);
			break;
			case 'zodiac_year':
				$this->db_kua->where('zodiac_year', $collectData['by_value']);
			break;
			case 'vendor_seq':
				$this->db_kua->where('vendor_seq', $collectData['by_value']);
			break;
			case 'seq':
			case 'id':
			default:
				$this->db_kua->where('id', $collectData['by_value']);
			break;
		}
		$this->db_kua->delete($this->kua_tables['kua_zodiac']);
		return $this->db_kua->affected_rows();
	}
	function insert_kua_data($input_params) {
		$this->db_kua->insert($this->kua_tables['kua_zodiac'], $input_params);
		return $this->db_kua->insert_id();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}