<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shio extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrpt;
	protected $userdata = null;
	protected $DateObject;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		$this->userdata = $this->mod_account->get_userdata();
		
		$this->base_config = $this->mod_account->get_base_config();
		$this->base_encrpt = $this->mod_account->get_base_encrypt();
		
		$this->load->model('shio/Model_shio_vendor', 'mod_vendor');
		$this->load->model('shio/Model_shio_shio', 'mod_shio');
		$this->DateObject = $this->mod_account->get_dateobject();
	}
	
	function index($vendor_seq = 0) {
		$data = array(
			'page'					=> 'kua-shio-index',
			'collect'				=> array(),
			'title'					=> 'Shio Index',
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		$data['vendor_seq'] = (is_numeric($vendor_seq) ? (int)$vendor_seq : 0);
		//-------------------------------------------------------------
		$this->view($data['vendor_seq']);
	}
	
	function view($vendor_seq = 0, $pgnumber = 0) {
		$data = array(
			'page'					=> 'kua-shio-index',
			'collect'				=> array(),
			'title'					=> 'Shio Index',
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		$data['vendor_seq'] = (is_numeric($vendor_seq) ? (int)$vendor_seq : 0);
		//-------------------------------------------------------------
		if (!$this->error) {
			if ($data['vendor_seq'] > 0) {
				try {
					$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['vendor_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
				}
			} else {
				try {
					$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_default();
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get default vendor data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['vendor_data']->vendor_code)) {
				$this->error = true;
				$this->error_msg[] = "Vendor data not exist on database.";
			} else {
				$data['title'] .= sprintf(' | %s', $data['collect']['vendor_data']->vendor_name);
				try {
					$data['collect']['current_paito_vendors'] = $this->mod_vendor->get_kua_vendors();
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get all available vendors with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			$data['collect']['paito_vendors'] = array();
			array_push($data['collect']['paito_vendors'], $this->mod_vendor->get_kua_vendor_default());
			if (is_array($data['collect']['current_paito_vendors']) && (count($data['collect']['current_paito_vendors']) > 0)) {
				foreach ($data['collect']['current_paito_vendors'] as $paito_vendor) {
					array_push($data['collect']['paito_vendors'], $paito_vendor);
				}
			}
			
			$this->form_validation->set_rules('search_text', 'Search Text', 'max_length[128]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$data['search_text'] = '';
			} else {
				$data['search_text'] = $this->input->post('search_text');
			}
			$data['condition_params'] = array();
		}
		# GET Count Paito by Vendor
		if (!$this->error) {
			if (strtolower($data['collect']['vendor_data']->vendor_code) === 'all') {
				try {
					$data['collect']['vendor_paito_count'] = $this->mod_shio->get_vendor_kua_count_by('all', 'all', $data['condition_params'], $data['search_text']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get vendor paito count with exception: {$ex->getMessage()}.";
				}
			} else {
				try {
					$data['collect']['vendor_paito_count'] = $this->mod_shio->get_vendor_kua_count_by('vendor_seq', $data['collect']['vendor_data']->seq, $data['condition_params'], $data['search_text']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get vendor paito count with exception: {$ex->getMessage()}.";
				}
			}
		}
		# Make Paging
		if (!$this->error) {
			if (!isset($data['collect']['vendor_paito_count']->value)) {
				$this->error = true;
				$this->error_msg[] = "Not have value during count vendor paito.";
			} else {
				$data['collect']['pagination'] = array(
					'page'		=> $data['pgnumber'],
					'start'		=> 0,
					'perpage'	=> base_config('per_page'),
				);
				if ($data['collect']['pagination']['page'] > 0) {
					$data['collect']['pagination']['page'] = (int)$data['collect']['pagination']['page'];
				} else {
					$data['collect']['pagination']['page'] = 1;
				}
				$data['collect']['pagination']['start'] = $this->mod_account->get_pagination_start($data['collect']['pagination']['page'], $data['collect']['pagination']['perpage'], $data['collect']['vendor_paito_count']->value);
				try {
					$data['collect']['paging_display'] = $this->mod_account->generate_pagination(base_url("paito/paito/view/{$data['collect']['vendor_data']->seq}/%d"), $data['collect']['pagination']['page'], $data['collect']['pagination']['perpage'], $data['collect']['vendor_paito_count']->value, $data['collect']['pagination']['start']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot generate paging display with exception: {$ex->getMessage()}.";
				}
			}
		}
		# GET Data Paito by Vendor
		if (!$this->error) {
			if (strtolower($data['collect']['vendor_data']->vendor_code) === 'all') {
				try {
					$data['collect']['vendor_paito_data'] = $this->mod_shio->get_vendor_kua_data_by('all', 'all', $data['condition_params'], $data['search_text'], $data['collect']['pagination']['start'], $data['collect']['pagination']['perpage']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get vendor paito data with exception: {$ex->getMessage()}.";
				}
			} else {
				try {
					$data['collect']['vendor_paito_data'] = $this->mod_shio->get_vendor_kua_data_by('vendor_seq', $data['collect']['vendor_data']->seq, $data['condition_params'], $data['search_text'], $data['collect']['pagination']['start'], $data['collect']['pagination']['perpage']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get vendor paito data with exception: {$ex->getMessage()}.";
				}
			}
		}
		
		
		if (!$this->error) {
			$this->load->view('shio/shio.php', $data);
		}
	}
	function edit($paito_seq, $page_type = 'view') {
		$data = array(
			'page'					=> 'kua-shio-edit',
			'collect'				=> array(),
			'title'					=> 'Edit Shio Data',
			'paito_seq'				=> (is_numeric($paito_seq) ? (int)$paito_seq : 0),
			'page_type'				=> (is_string($page_type) ? strtolower($page_type) : 'view'),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			try {
				$data['collect']['paito_data'] = $this->mod_shio->get_kua_single_data_by('seq', $data['paito_seq']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get paito data by id with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['paito_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Zodiac data by given sequence not exists.";
			} else {
				$data['title'] .= sprintf(' %s | %s | %s', $data['collect']['paito_data']->zodiac_year, $data['collect']['paito_data']->zodiac_name_english, $data['collect']['paito_data']->zodiac_name_indonesia);
				try {
					$data['collect']['current_vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['collect']['paito_data']->vendor_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['current_vendor_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Current paito vendor data not exists.";
			} else {
				try {
					$data['collect']['paito_vendors'] = $this->mod_vendor->get_kua_vendors();
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get all available paito vendors with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				$this->form_validation->set_rules('vendor_seq', 'Zodiac Vendor Seq', 'numeric|required|max_length[11]|xss_clean');
				$this->form_validation->set_rules('zodiac_year', 'Zodiac Year', 'numeric|required|max_length[4]|xss_clean');
				$this->form_validation->set_rules('zodiac_type', 'Zodiac Type', 'max_length[32]|xss_clean');
				$this->form_validation->set_rules('zodiac_name_english', 'Zodiac Name (English)', 'required|max_length[32]|xss_clean');
				$this->form_validation->set_rules('zodiac_name_indonesia', 'Zodiac Name (Indonesia)', 'required|max_length[32]|xss_clean');
				if ($this->form_validation->run() == FALSE) {
					$this->error = true;
					$this->error_msg[] = validation_errors('-', "\r\n");
				} else {
					$data['collect']['input_params'] = array(
						'vendor_seq'								=> $this->input->post('vendor_seq'),
						'zodiac_name_english'						=> $this->input->post('zodiac_name_english'),
						'zodiac_name_indonesia'						=> $this->input->post('zodiac_name_indonesia'),
						'zodiac_year'								=> $this->input->post('zodiac_year'),
					);
					if (!is_numeric($data['collect']['input_params']['vendor_seq'])) {
						$this->error = true;
						$this->error_msg[] = "Zodiac vendor should be in numeric datatype.";
					}
					if (!is_string($data['collect']['input_params']['zodiac_name_english'])) {
						$this->error = true;
						$this->error_msg[] = "Zodiac name should be in string datatype.";
					}
					if (!is_string($data['collect']['input_params']['zodiac_name_indonesia'])) {
						$this->error = true;
						$this->error_msg[] = "Zodiac name should be in string datatype.";
					}
					if (!preg_match('/^[0-9]*$/', $data['collect']['input_params']['zodiac_year'])) {
						$this->error = true;
						$this->error_msg[] = "Zodiac Year should be in digit value.";
					}
					try {
						$data['collect']['input_date_object'] = DateTime::createFromFormat('Y-m-d', sprintf("%s-01-01", $data['collect']['input_params']['zodiac_year']), new DateTimeZone('Asia/Bangkok'));
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot create date object from given date with exception: {$ex->getMessage()}.";
					}
				}
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				if ($data['collect']['input_date_object'] == FALSE) {
					$this->error = true;
					$this->error_msg[] = "False response while create date object from given date while edit.";
				} else {
					try {
						$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['collect']['input_params']['vendor_seq']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
					}
				}
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				if (!isset($data['collect']['vendor_data']->seq)) {
					$this->error = true;
					$this->error_msg[] = "Vendor data of given vendor-seq not exists on database.";
				}
			}
			$data['collect']['select_years'] = array(
				'current'			=> sprintf("%d", $this->DateObject->format('Y')),
				'before'			=> sprintf("%d", $this->DateObject->format('Y')) - 100,
				'future'			=> sprintf("%d", $this->DateObject->format('Y')) + 100,
			);
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				$data['collect']['query_params'] = array(
					'vendor_seq'					=> $data['collect']['vendor_data']->seq,
					'zodiac_name_english'			=> sprintf("%s", $data['collect']['input_params']['zodiac_name_english']),
					'zodiac_name_indonesia'			=> sprintf("%s", $data['collect']['input_params']['zodiac_name_indonesia']),
					'zodiac_year'					=> $data['collect']['input_date_object']->format('Y'),
					'zodiac_insert_datetime'		=> $this->DateObject->format('Y-m-d H:i:s'),
				);
				$data['collect']['condition_params'] = array(
					'zodiac_year'					=> $data['collect']['input_date_object']->format('Y'),
				);
				try {
					$data['collect']['current_shio_data'] = $this->mod_shio->get_kua_single_data_withoutme_by($data['collect']['paito_data']->seq, 'vendor_seq', $data['collect']['vendor_data']->seq, $data['collect']['condition_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get to check if vendor code already exists or not.";
				}
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {		
				if (isset($data['collect']['current_shio_data']->seq)) {
					$this->error = true;
					$this->error_msg[] = "Paito data with tgl rilis already exists, please check tgl rilis.";
				}
			}
		}
		# Edit Data
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				try {
					$data['collect']['edit_paito_data'] = $this->mod_shio->set_kua_data_by('seq', $data['collect']['paito_data']->seq, $data['collect']['query_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot edit current shio data with exception: {$ex->getMessage()}.";
				}
			}
		}
		
		if (!$this->error) {
			if ($data['page_type'] === strtolower('edit')) {
				redirect(base_url("shio/shio/view/{$data['collect']['vendor_data']->seq}"));
			} else {
				$this->load->view('shio/shio-loads/shio-shio-edit.php', $data);
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('error_msg', json_encode($this->error_msg));
			redirect(base_url("shio/shio/index"));
		}
	}
	function add($vendor_code = 'all') {
		$data = array(
			'page'					=> 'kua-shio-add',
			'collect'				=> array(),
			'title'					=> 'Add Shio Data',
			'vendor_code'			=> (is_string($vendor_code) ? strtolower($vendor_code) : 'all'),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		if (!$this->error) {
			$data['vendor_code'] = str_replace('_', '-', $data['vendor_code']);
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		if (!$this->error) {
			$data['collect']['select_years'] = array(
				'current'			=> sprintf("%d", $this->DateObject->format('Y')),
				'before'			=> sprintf("%d", $this->DateObject->format('Y')) - 100,
				'future'			=> sprintf("%d", $this->DateObject->format('Y')) + 100,
			);
			try {
				$data['collect']['paito_vendors'] = $this->mod_vendor->get_kua_vendors();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get all available shio vendors with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('code', $data['vendor_code']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
			}
		}
		
		
		if (!$this->error) {
			$this->load->view('shio/shio-loads/shio-shio-add.php', $data);
		}
	}
	function addaction() {
		$data = array(
			'page'					=> 'shio-shio-add',
			'collect'				=> array(),
			'title'					=> 'Add Shio Data',
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			$this->form_validation->set_rules('vendor_seq', 'Paito Vendor', 'numeric|required|max_length[11]|xss_clean');
			$this->form_validation->set_rules('zodiac_name_english', 'Zodica Name (en_UK)', 'required|max_length[64]|xss_clean');
			$this->form_validation->set_rules('zodiac_name_indonesia', 'Zodica Name (id_ID)', 'required|max_length[64]|xss_clean');
			$this->form_validation->set_rules('zodiac_year', 'Zodiac Year', 'required|max_length[4]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', "\r\n");
			} else {
				$data['collect']['input_params'] = array(
					'vendor_seq'								=> $this->input->post('vendor_seq'),
					'zodiac_name_english'						=> $this->input->post('zodiac_name_english'),
					'zodiac_name_indonesia'						=> $this->input->post('zodiac_name_indonesia'),
					'zodiac_year'								=> $this->input->post('zodiac_year'),
				);
				if (!is_numeric($data['collect']['input_params']['vendor_seq'])) {
					$this->error = true;
					$this->error_msg[] = "Zodiac vendor should be in numeric datatype.";
				}
				if (!is_string($data['collect']['input_params']['zodiac_name_english'])) {
					$this->error = true;
					$this->error_msg[] = "Zodiac name should be in string datatype.";
				}
				if (!is_string($data['collect']['input_params']['zodiac_name_indonesia'])) {
					$this->error = true;
					$this->error_msg[] = "Zodiac name should be in string datatype.";
				}
				if (!preg_match('/^[0-9]*$/', $data['collect']['input_params']['zodiac_year'])) {
					$this->error = true;
					$this->error_msg[] = "Zodiac Year should be in digit value.";
				}
				try {
					$data['collect']['input_date_object'] = DateTime::createFromFormat('Y-m-d', sprintf("%s-01-01", $data['collect']['input_params']['zodiac_year']), new DateTimeZone('Asia/Bangkok'));
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot create date object from given year date with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if ($data['collect']['input_date_object'] == FALSE) {
				$this->error = true;
				$this->error_msg[] = "False response while create date object from given date while add.";
			} else {
				try {
					$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['collect']['input_params']['vendor_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['vendor_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Vendor data of given vendor-seq not exists on database.";
			}
		}
		if (!$this->error) {
			$data['collect']['query_params'] = array(
				'vendor_seq'					=> $data['collect']['vendor_data']->seq,
				'zodiac_name_english'			=> sprintf("%s", $data['collect']['input_params']['zodiac_name_english']),
				'zodiac_name_indonesia'			=> sprintf("%s", $data['collect']['input_params']['zodiac_name_indonesia']),
				'zodiac_year'					=> $data['collect']['input_date_object']->format('Y'),
				'zodiac_insert_datetime'		=> $this->DateObject->format('Y-m-d H:i:s'),
			);
			$data['collect']['condition_params'] = array(
				'vendor_seq'				=> $data['collect']['vendor_data']->seq,
				'zodiac_year'				=> $data['collect']['input_date_object']->format('Y'),
			);
			try {
				$data['collect']['current_shio_data'] = $this->mod_shio->get_kua_single_data_by('vendor_seq', $data['collect']['vendor_data']->seq, $data['collect']['condition_params']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get to check if vendo code already exists or not.";
			}
		}
		# Add Data
		if (!$this->error) {
			if (isset($data['collect']['current_shio_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Zodia data with year rilis already exists, please check year rilis.";
			} else {
				try {
					$data['collect']['insert_zodiac_data_seq'] = $this->mod_shio->insert_kua_data($data['collect']['query_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot insert new shio data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if ((int)$data['collect']['insert_zodiac_data_seq'] == 0) {
				$this->error = true;
				$this->error_msg[] = "Failed insert new paito data, return false response.";
			} else {
				try {
					$data['collect']['insert_shio_data'] = $this->mod_shio->get_kua_single_data_by('seq', $data['collect']['insert_zodiac_data_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get new inserted shio data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['insert_shio_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Data of new inserted shio data not exists on database.";
			}
		}
		if (!$this->error) {
			redirect(base_url("shio/shio/view/{$data['collect']['vendor_data']->seq}"));
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('error_msg', json_encode($this->error_msg));
			redirect(base_url("shio/shio/index"));
		}
	}
	function shio_delete($paito_seq) {
		$data = array(
			'page'					=> 'shio-shio-delete',
			'collect'				=> array(),
			'title'					=> 'Delete Shio Data',
			'paito_seq'				=> (is_numeric($paito_seq) ? (int)$paito_seq : 0),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			try {
				$data['collect']['paito_data'] = $this->mod_shio->get_kua_single_data_by('seq', $data['paito_seq']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get shio data by id with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['paito_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Shio data by given sequence not exists.";
			} else {
				$data['title'] .= sprintf(' %s | %s | %s', $data['collect']['paito_data']->zodiac_year, $data['collect']['paito_data']->zodiac_name_english, $data['collect']['paito_data']->zodiac_name_indonesia);
				try {
					$data['collect']['current_vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['collect']['paito_data']->vendor_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get selected vendor data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			$this->form_validation->set_rules('shio_seq', 'Shio Sequence', 'required|numeric|max_length[11]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', "\r\n");
			} else {
				$data['collect']['input_params'] = array(
					'shio_seq'						=> $this->input->post('shio_seq'),
				);
				if (!is_numeric($data['collect']['input_params']['shio_seq'])) {
					$this->error = true;
					$this->error_msg[] = "Shio sequence should a numeric datatype.";
				} else {
					$data['collect']['query_params'] = array(
						'shio_seq'						=> (int)$data['collect']['input_params']['shio_seq'],
					);
					if ($data['collect']['query_params']['shio_seq'] !== (int)$data['collect']['paito_data']->seq) {
						$this->error = true;
						$this->error_msg[] = "Input data sequence should be same with shio data sequence.";
					}
				}
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['delete_paito'] = $this->mod_shio->delete_kua_data_by('seq', $data['collect']['paito_data']->seq, $data['collect']['paito_data']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot delete given shio data with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			$data['json_response'] = array(
				'status'				=> true,
				'data'					=> $data['collect']['paito_data'],
				'redirect_url'			=> base_url("shio/shio/view/{$data['collect']['current_vendor_data']->seq}"),
				'error'					=> false,
			);
		} else {
			$data['json_response'] = array(
				'status'				=> false,
				'data'					=> false,
				'redirect_url'			=> base_url('shio/vendors/index'),
				'error'					=> $this->error_msg,
			);
		}
		header('Content-Type: application/json');
		echo json_encode($data['json_response'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}