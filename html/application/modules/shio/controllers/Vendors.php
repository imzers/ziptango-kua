<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends MY_Controller {
	protected $error = false, $error_msg = [];
	protected $base_config, $base_encrpt;
	protected $userdata = null;
	protected $DateObject;
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/Model_account', 'mod_account');
		$this->userdata = $this->mod_account->get_userdata();
		
		$this->base_config = $this->mod_account->get_base_config();
		$this->base_encrpt = $this->mod_account->get_base_encrypt();
		
		$this->load->model('shio/Model_shio_vendor', 'mod_vendor');
		$this->DateObject = $this->mod_account->get_dateobject();
	}

	function index($pgnumber = 0) {
		$data = array(
			'page'					=> 'kua-vendors-index',
			'collect'				=> array(),
			'title'					=> 'Shio Vendors',
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		if (!$this->error) {
			$this->form_validation->set_rules('search_text', 'Search Text', 'max_length[128]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$data['search_text'] = '';
			} else {
				$data['search_text'] = $this->input->post('search_text');
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['shio_vendor_count'] = $this->mod_vendor->get_kua_vendor_count_by('all', 'all', $data['search_text']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get count of all shio vendor with exception: {$ex->getMessage()}.";
			}
		}
		# Make Paging
		if (!$this->error) {
			if (!isset($data['collect']['shio_vendor_count']->value)) {
				$this->error = true;
				$this->error_msg[] = "Not have value of return response after count";
			} else {
				$data['collect']['pagination'] = array(
					'page'		=> $data['pgnumber'],
					'start'		=> 0,
					'perpage'	=> base_config('per_page'),
				);
				if ($data['collect']['pagination']['page'] > 0) {
					$data['collect']['pagination']['page'] = (int)$data['collect']['pagination']['page'];
				} else {
					$data['collect']['pagination']['page'] = 1;
				}
				$data['collect']['pagination']['start'] = $this->mod_account->get_pagination_start($data['collect']['pagination']['page'], $data['collect']['pagination']['perpage'], $data['collect']['shio_vendor_count']->value);
				try {
					$data['collect']['paging_display'] = $this->mod_account->generate_pagination(base_url("shio/vendors/index/%d"), $data['collect']['pagination']['page'], $data['collect']['pagination']['perpage'], $data['collect']['shio_vendor_count']->value, $data['collect']['pagination']['start']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot generate paging display with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['shio_vendor_data'] = $this->mod_vendor->get_kua_vendor_data_by('all', 'all', $data['search_text'], $data['collect']['pagination']['start'], $data['collect']['pagination']['perpage']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get all shio vendor on this page with exception: {$ex->getMessage()}.";
			}
		}
		
		
		
		
		
		
		if (!$this->error) {
			$this->load->view('shio/shio.php', $data);
		}
		
	}
	function add() {
		$data = array(
			'page'					=> 'kua-vendors-add',
			'collect'				=> array(),
			'title'					=> 'Add Shio Vendors',
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		if (!$this->error) {
			$this->load->view('shio/shio-loads/shio-vendor-add.php', $data);
		}
	}
	function addaction() {
		$data = array(
			'page'					=> 'kua-vendors-add',
			'collect'				=> array(),
			'title'					=> 'Add Shio Vendors',
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			$this->form_validation->set_rules('vendor_name', 'Vendor Name', 'required|max_length[64]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', "\r\n");
			} else {
				$data['collect']['input_params'] = array(
					'vendor_name'			=> $this->input->post('vendor_name'),
				);
				if (!is_string($data['collect']['input_params']['vendor_name'])) {
					$this->error = true;
					$this->error_msg[] = "Vendor name should a string datatype.";
				} else {
					$data['collect']['query_params'] = array(
						'vendor_name'						=> base_safe_text($data['collect']['input_params']['vendor_name'], 64),
						'vendor_datetime_insert'			=> $this->DateObject->format('Y-m-d H:i:s'),
						'vendor_datetime_update'			=> $this->DateObject->format('Y-m-d H:i:s'),
					);
					$data['collect']['query_params']['vendor_code'] = base_permalink($data['collect']['query_params']['vendor_name']);
				}
				try {
					$data['collect']['current_vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('code', $data['collect']['query_params']['vendor_code']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get to check if vendo code already exists or not.";
				}
			}
		}
		if (!$this->error) {
			if (isset($data['collect']['current_vendor_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Vendor code already exist on database, please use another vendor name.";
			} else {
				try {
					$data['collect']['insert_vendor'] = $this->mod_vendor->insert_kua_vendor($data['collect']['query_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot insert new vendor data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (is_numeric($data['collect']['insert_vendor']) && ((int)$data['collect']['insert_vendor'] > 0)) {
				try {
					$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['collect']['insert_vendor']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get new inserted vendor data with exception: {$ex->getMessage()}.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Failed insert new vendor data, return false response.";
			}
		}
		
		if (!$this->error) {
			redirect(base_url('shio/vendors/index'));
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('error_msg', json_encode($this->error_msg));
			redirect(base_url("shio/shio/index"));
		}
	}
	function edit($vendor_seq, $page_type = 'view') {
		$data = array(
			'page'					=> 'kua-vendors-add',
			'collect'				=> array(),
			'title'					=> 'Edit Shio Vendors',
			'vendor_seq'			=> (is_numeric($vendor_seq) ? (int)$vendor_seq : 0),
			'page_type'				=> (is_string($page_type) ? strtolower($page_type) : 'view'),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			try {
				$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['vendor_seq']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get new inserted vendor data with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['vendor_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Vendor data by given sequence not exists.";
			} else {
				$data['title'] .= sprintf(' | %s', $data['collect']['vendor_data']->vendor_name);
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				$this->form_validation->set_rules('vendor_name', 'Vendor Name', 'required|max_length[64]|xss_clean');
				if ($this->form_validation->run() == FALSE) {
					$this->error = true;
					$this->error_msg[] = validation_errors('-', "\r\n");
				} else {
					$data['collect']['input_params'] = array(
						'vendor_name'						=> $this->input->post('vendor_name'),
					);
					if (!is_string($data['collect']['input_params']['vendor_name'])) {
						$this->error = true;
						$this->error_msg[] = "Vendor name should a string datatype.";
					} else {
						$data['collect']['query_params'] = array(
							'vendor_name'						=> base_safe_text($data['collect']['input_params']['vendor_name'], 64),
							'vendor_datetime_update'			=> $this->DateObject->format('Y-m-d H:i:s'),
						);
						$data['collect']['query_params']['vendor_code'] = base_permalink($data['collect']['query_params']['vendor_name']);
						try {
							$data['collect']['current_vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_withoutme_by($data['collect']['vendor_data']->seq, 'code', $data['collect']['query_params']['vendor_code']);
						} catch (Exception $ex) {
							$this->error = true;
							$this->error_msg[] = "Cannot get to check if vendo code already exists or not.";
						}
					}
				}
			}
		}
		if (!$this->error) {
			if ($data['page_type'] === 'edit') {
				if (isset($data['collect']['current_vendor_data']->seq)) {
					$this->error = true;
					$this->error_msg[] = "Vendor code already exist on database, please use another vendor name.";
				} else {
					try {
						$data['collect']['edit_vendor'] = $this->mod_vendor->set_kua_vendor_by('seq', $data['collect']['vendor_data']->seq, $data['collect']['query_params']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot edit vendor data with exception: {$ex->getMessage()}.";
					}
				}
			}
		}
		
		
		
		
		
		if (!$this->error) {
			if ($data['page_type'] === strtolower('edit')) {
				redirect(base_url('shio/vendors/index'));
			} else {
				$this->load->view('shio/shio-loads/shio-vendor-edit.php', $data);
			}
		} else {
			$this->session->set_flashdata('error', true);
			$this->session->set_flashdata('error_msg', json_encode($this->error_msg));
			redirect(base_url("shio/shio/index"));
		}
	}
	function vendor_delete($vendor_seq) {
		$data = array(
			'page'					=> 'kua-vendors-delete',
			'collect'				=> array(),
			'title'					=> 'Delete Shio Vendors',
			'vendor_seq'			=> (is_numeric($vendor_seq) ? (int)$vendor_seq : 0),
		);
		$data['collect']['userdata'] = $this->userdata;
		if (!$this->error) {
			if (!isset($data['collect']['userdata']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No userdata available.";
			}
		}
		//-------------------------------------------------------------
		if (!$this->error) {
			try {
				$data['collect']['vendor_data'] = $this->mod_vendor->get_kua_vendor_single_data_by('seq', $data['vendor_seq']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get new inserted vendor data with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			if (!isset($data['collect']['vendor_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "Vendor data by given sequence not exists.";
			} else {
				$data['title'] .= sprintf(' | %s', $data['collect']['vendor_data']->vendor_name);
			}
		}
		if (!$this->error) {
			$this->form_validation->set_rules('vendor_seq', 'Vendor Sequence', 'required|numeric|max_length[11]|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', "\r\n");
			} else {
				$data['collect']['input_params'] = array(
					'vendor_seq'						=> $this->input->post('vendor_seq'),
				);
				if (!is_numeric($data['collect']['input_params']['vendor_seq'])) {
					$this->error = true;
					$this->error_msg[] = "Vendor name should a numeric datatype.";
				} else {
					$data['collect']['query_params'] = array(
						'vendor_seq'						=> (int)$data['collect']['input_params']['vendor_seq'],
					);
					if ($data['collect']['query_params']['vendor_seq'] !== (int)$data['collect']['vendor_data']->seq) {
						$this->error = true;
						$this->error_msg[] = "Input data sequence should be same with vendor data seq.";
					}
				}
			}
		}
		if (!$this->error) {
			try {
				$data['collect']['delete_vendor'] = $this->mod_vendor->delete_kua_vendor_by('seq', $data['collect']['vendor_data']->seq);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot delete given vendor data with exception: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			$data['json_response'] = array(
				'status'				=> true,
				'data'					=> $data['collect']['vendor_data'],
				'redirect_url'			=> base_url('shio/vendors/index'),
				'error'					=> false,
			);
		} else {
			$data['json_response'] = array(
				'status'				=> false,
				'data'					=> false,
				'redirect_url'			=> base_url('shio/vendors/index'),
				'error'					=> $this->error_msg,
			);
		}
		header('Content-Type: application/json');
		echo json_encode($data['json_response'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
