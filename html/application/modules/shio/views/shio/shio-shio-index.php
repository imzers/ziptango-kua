<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Paito<?=(isset($collect['vendor_data']->vendor_name) ? sprintf(" | %s", $collect['vendor_data']->vendor_name) : '');?></h1>
		<a id="paito-add-button" href="<?= base_url('shio/shio/add');?>" class="d-none d-sm-inline-block btn btn-primary shadow" data-load-url="shio/shio/add" data-load-placeholder="paito-loader-placeholder">
			<i class="fa fa-plus"></i> Add
		</a>
	</div>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="collapse navbar-collapse" id="nav-paito-vendor">
			<ul class="navbar-nav nav-pills">
				<?php
				if (isset($collect['paito_vendors'])) {
					if (is_array($collect['paito_vendors']) && (count($collect['paito_vendors']) > 0)) {
						foreach ($collect['paito_vendors'] as $paito_vendor) {
							if ($paito_vendor->seq == $collect['vendor_data']->seq) {
								$nav_li_active = ' active';
							} else {
								$nav_li_active = '';
							}
							?>
							<li class="nav-item<?=$nav_li_active;?>">
								<a class="nav-link" href="<?=base_url("shio/shio/view/{$paito_vendor->seq}");?>">
									<i class="fa fa-list"></i> <?=$paito_vendor->vendor_name;?>
								</a>
							</li>
							<?php
						}
					}
				}
				?>
				
			</ul>
		</div>
	</nav>
	
	<div id="paito-loader-placeholder"></div>
	
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Shio');?></h6>
		</div>
		<div class="card-body">
			
			<div class="table-responsive">
                <table class="table table-bordered" id="tbl-paito-paito" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Vendor</th>
							<th>Tahun</th>
							<th>Zodiac Name (en_UK)</th>
							<th>Zodiac Name (id_ID)</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if (isset($collect['vendor_paito_data'])) {
							if (is_array($collect['vendor_paito_data']) && (count($collect['vendor_paito_data']) > 0)) {
								$for_i = 1;
								foreach ($collect['vendor_paito_data'] as $paito_data) {
									?>
									<tr>
										<td><?=$for_i;?></td>
										<td><?=$paito_data->vendor_name;?></td>
										<td><?=$paito_data->zodiac_year;?></td>
										<td><?=$paito_data->zodiac_name_english;?></td>
										<td><?=$paito_data->zodiac_name_indonesia;?></td>
										<td>
											<button type="button" class="btn-sm btn-info paito-paito-edit" data-paito-seq="<?=$paito_data->seq;?>" data-load-placeholder="paito-add-loader">
												<i class="fa fa-pencil-alt"></i>
											</button>
										</td>
									</tr>
									<?php
									$for_i++;
								}
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Content Row -->
	<div class="row">
		<div class="col-md-4 pull-left">
			<?= (isset($collect['paging_display']) ? $collect['paging_display'] : '');?>
		</div>
	</div>
</div>

		
		