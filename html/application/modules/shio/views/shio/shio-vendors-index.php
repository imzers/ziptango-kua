<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Vendors</h1>
		<a id="vendors-add-button" href="<?= base_url('shio/vendors/add');?>" class="d-none d-sm-inline-block btn btn-primary shadow" data-load-url="shio/vendors/add" data-load-placeholder="vendors-loader-placeholder">
			<i class="fa fa-plus"></i> Add
		</a>
	</div>
	
	<div id="vendors-loader-placeholder"></div>
	
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Vendors');?></h6>
		</div>
		<div class="card-body">
			
			<div class="table-responsive">
                <table class="table table-bordered" id="tbl-paito-vendors" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Vendor Code</th>
							<th>Vendor Name</th>
							<th>Vendor Insert</th>
							<th>Vendor Update</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if (isset($collect['shio_vendor_data'])) {
							if (is_array($collect['shio_vendor_data']) && (count($collect['shio_vendor_data']) > 0)) {
								$for_i = 1;
								foreach ($collect['shio_vendor_data'] as $vendor_data) {
									?>
									<tr>
										<td><?=$for_i;?></td>
										<td><?=$vendor_data->vendor_code;?></td>
										<td>
											<a href="<?= base_url('shio/shio/view/' . $vendor_data->seq);?>">
												<?=$vendor_data->vendor_name;?>
											</a>
										</td>
										<td><?= $vendor_data->vendor_datetime_insert;?></td>
										<td><?= $vendor_data->vendor_datetime_update;?></td>
										<td>
											<button type="button" class="btn-sm btn-info paito-vendor-edit" data-paito-vendor-seq="<?=$vendor_data->seq;?>" data-load-placeholder="vendor-add-loader">
												<i class="fa fa-pencil-alt"></i>
											</button>
											
										</td>
									</tr>
									<?php
									$for_i++;
								}
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Content Row -->
	<div class="row">
		<div class="col-md-4 pull-left">
			<?= (isset($collect['paging_display']) ? $collect['paging_display'] : '');?>
		</div>
	</div>
	
</div>

		
		