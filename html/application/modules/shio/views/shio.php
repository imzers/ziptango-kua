<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

$page = (isset($page) ? strtolower($page) : 'index');

$this->load->view('base/base-dashboard/header-begin.php');
$this->load->view('base/base-dashboard/top-sidebar.php');

$this->load->view('base/base-dashboard/wrapper-begin.php');

$this->load->view('base/base-dashboard/top-topbar.php');
switch (strtolower($page)) {
	case 'kua-vendors-index':
		$file_view = 'shio/shio/shio-vendors-index.php';
	break;
	case 'kua-shio-index':
		$file_view = 'shio/shio/shio-shio-index.php';
	break;
	case 'index':
	default:
		$file_view = 'shio/shio/shio-shio-index.php';
	break;
}
$this->load->view($file_view);
$this->load->view('base/base-dashboard/wrapper-end.php');


$this->load->view('base/base-dashboard/footer-begin.php');
$this->load->view('base/base-dashboard/footer-script.php');
switch (strtolower($page)) {
	
	case 'kua-vendors-index':
		$this->load->view('shio/shio-includes/vendors/index.php');
	break;
	case 'kua-shio-index':
		$this->load->view('shio/shio-includes/shio/index.php');
	break;
	case 'index':
	default:
		
	break;
}

$this->load->view('base/base-dashboard/footer-end.php');

