<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<script type="text/javascript">

	
	$(document).ready(function() {
		const load_placeholder = $('#vendors-loader-placeholder');
		$('#vendors-add-button').click(function(el) {
			el.preventDefault();
			$.ajax({
				'type'				: 'GET',
				'url'				: $(this).attr('href'),
				'data'				: false,
				'success'			: function(response) {
					load_placeholder.html(response);
					load_placeholder.show();
				}
			});
		});
		$('.paito-vendor-edit').click(function(elm) {
			elm.preventDefault();
			var vendor_seq = $(this).attr('data-paito-vendor-seq');
			$.ajax({
				'type'				: 'POST',
				'url'				: '<?= base_url('shio/vendors/edit');?>' + '/' + vendor_seq + '/view',
				'data'				: {
					'vendor_seq'		: vendor_seq
				},
				'success'			: function(response) {
					load_placeholder.html(response);
					load_placeholder.show();
				}
			});
		});
		
		
		if ($('#tbl-paito-vendors').length) {
			$("#tbl-paito-vendors").DataTable();
		}
		$(document).on('click', '#paito-close-button', function(e) {
			e.preventDefault();
			load_placeholder.hide();
		});
		<?php
		if ($this->session->flashdata('error')) {
			if ($this->session->flashdata('error_msg')) {
				?>
				var error_msgs = <?=$this->session->flashdata('error_msg');?>;
				if (error_msgs.length > 0) {
					var error_msg_html = '<div class="row"><div class="col-md-8"><ul class="list-group">';
					for (var i = 0; i < error_msgs.length; i++) {
						error_msg_html += '<li class="list-group-item">' + error_msgs[i] + '</li>';
					}
					error_msg_html += '</ul></div></div>';
					load_placeholder.html('<div class="alert alert-danger alert-dismissable">' + 
						'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + 
						error_msg_html + 
					'</div>');
					load_placeholder.show();
				}
				<?php 
			}
		}
		?>
	});
</script>