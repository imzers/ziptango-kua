<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
<script type="text/javascript">

	
	$(document).ready(function() {
		const load_placeholder = $('#paito-loader-placeholder');
		
		$('#paito-add-button').click(function(el) {
			el.preventDefault();
			$.ajax({
				'type'				: 'GET',
				'url'				: $(this).attr('href') + '/' + '<?=(isset($collect['vendor_data']->vendor_code) ? $collect['vendor_data']->vendor_code : '');?>',
				'data'				: false,
				'success'			: function(response) {
					load_placeholder.html(response);
					load_placeholder.show();
				}
			});
		});
		$('.paito-paito-edit').click(function(elm) {
			elm.preventDefault();
			var paito_seq = $(this).attr('data-paito-seq');
			$.ajax({
				'type'				: 'POST',
				'url'				: '<?= base_url('shio/shio/edit');?>' + '/' + paito_seq + '/view',
				'data'				: {
					'paito_seq'		: paito_seq
				},
				'success'			: function(response) {
					load_placeholder.html(response);
					load_placeholder.show();
				}
			});
		});
		
		
		if ($('#tbl-paito-paito').length) {
			$("#tbl-paito-paito").DataTable();
		}
		
		$(document).on('click', '#paito-close-button', function(e) {
			e.preventDefault();
			load_placeholder.hide();
		});
		$(document).on('click', "#btn-wrapper-begin-paito-form-reset", function(e) {
			e.preventDefault();
		});
		$(document).on('click', "#btn-wrapper-begin-paito-form-tools", function(e) {
			e.preventDefault();
		});
		
		
		<?php
		if ($this->session->flashdata('error')) {
			if ($this->session->flashdata('error_msg')) {
				?>
				var error_msgs = <?=$this->session->flashdata('error_msg');?>;
				if (error_msgs.length > 0) {
					var error_msg_html = '<div class="row"><div class="col-md-8"><ul class="list-group">';
					for (var i = 0; i < error_msgs.length; i++) {
						error_msg_html += '<li class="list-group-item">' + error_msgs[i] + '</li>';
					}
					error_msg_html += '</ul></div></div>';
					load_placeholder.html('<div class="alert alert-danger alert-dismissable">' + 
						'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + 
						error_msg_html + 
					'</div>');
                    load_placeholder.show();
				}
				<?php 
			}
		}
		?>
	});
</script>