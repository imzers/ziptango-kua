<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-md-11">
				<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Edit Shio');?></h6>
			</div>
			<div class="col-md-1 align-right">
				<button id="paito-close-button" class="d-none d-sm-inline-block btn btn-danger btn-sm shadow">
					<i class="fa fa-window-close"></i> Close
				</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-4">
				<form id="form-edit-paito-paito" class="form-horizontal" method="post" action="<?= base_url("shio/shio/edit/{$collect['paito_data']->seq}/edit");?>" enctype="multipart/form-data">
					<div class="form-group">
						<label for="vendor-name" class="control-label col-md-4 d-none d-sm-inline-block">Nama Vendor</label>
						<select id="vendor-name" name="vendor_seq" class="form-control">
							<?php
							foreach ($collect['paito_vendors'] as $vendor_data) {
								if ($vendor_data->seq == $collect['current_vendor_data']->seq) {
									$option_is_selected = ' selected="selected"';
								} else {
									$option_is_selected = '';
								}
								?>
								<option value="<?=$vendor_data->seq;?>"<?=$option_is_selected;?>><?=$vendor_data->vendor_name;?></option>
								<?php
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="zodiac-year" class="control-label">Zodiac Year</label>
						<select id="zodiac-year" class="form-control" name="zodiac_year">
							<?php
							for ($i = $collect['select_years']['before']; $i < $collect['select_years']['future']; $i++) {
								if ($i == $collect['paito_data']->zodiac_year) {
									$option_year_selected = ' selected="selected"';
								} else {
									$option_year_selected = '';
								}
								?>
								<option value="<?=$i;?>"<?=$option_year_selected;?>><?=$i;?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					<div class="form-group">
						<label for="zodiac-name-en" class="control-label">Zodiac Name (en_UK)</label>
						<input type="text" class="form-control" name="zodiac_name_english" id="zodiac-name-en" value="<?=$collect['paito_data']->zodiac_name_english;?>" />
					</div>
					<div class="form-group">
						<label for="zodiac-name-id" class="control-label">Zodiac Name (id_ID)</label>
						<input type="text" class="form-control" name="zodiac_name_indonesia" id="zodiac-name-id" value="<?=$collect['paito_data']->zodiac_name_indonesia;?>" />
					</div>
					
					
					
					<div class="align-right">
						<a href="javascript:void(0)" id="btn-delete-paito-paito" class="btn btn-sm btn-danger mb-4" data-paito-seq="<?=$collect['paito_data']->seq;?>">
							<i class="fa fa-ban"></i> Delete
						</a>
						<a href="javascript:void(0)" id="btn-edit-paito-paito" class="btn btn-sm btn-warning mb-4">
							<i class="fa fa-pencil-alt"></i> Edit
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	function edit_paito_paito() {
		var paito_nilai = $('#zodiac-name-id');
		var paito_form = $('#form-edit-paito-paito');
		if ($.trim(paito_nilai.val()) == '') {
			var fire_html = '<ul class="list-group">' +
				'<li class="list-group-item">' + 'Zodiac Name Required' + '</li>' +
				'<li class="list-group-item">' + 'Plese insert zodiac name' + '</li>' +
			'</ul>';
			Swal.fire({
				'title'				: "ERROR",
				'html'				: fire_html,  
				'confirmButtonText'	: 'OK', 
			});
			return false;
		} else {
			paito_form.submit();
		}
	}
	function delete_paito_data(swalert, paito_seq) {
		swalert.fire({
			title				: "Are you sure?",
			text				: "You will lost this data.",
			type				: "warning",
			showCancelButton	: true,
			confirmButtonColor	: "#DD6B55",
			confirmButtonText	: "Yes, delete it!",
			cancelButtonText	: "No, cancel please!"
		}).then((result) => {
			if (result.value) {
				$.ajax({
					'type'				: 'POST',
					'url'				: '<?= base_url('shio/shio/shio-delete');?>' + '/' + paito_seq,
					'data'				: {
						'shio_seq'		: paito_seq
					},
					'dataType'			: 'json',
					'success'			: function(response) {
						if (response.status == true) {
							window.location.href = response.redirect_url;
						} else {
							var fire_html = '<ul class="list-group">';
							if (response.error.length > 0) {
								for (var i = 0; i < response.error.length; i++) {
									fire_html += '<li class="list-group-item">' + response.error[i].toString() + '</li>';
								}
							}
							fire_html += '</ul>';
							swalert.fire({
								'title'				: "ERROR",
								'html'				: fire_html,  
								'confirmButtonText'	: 'OK', 
							});
						}
					}
				});
			} else {
				swalert.fire({
					'title'				: 'Canceled',
					'html'				: '<p>You are canceling to delete data</p>',
					'confirmButtonText'	: 'OK'
				});
			}
		});
	}
	$(document).ready(function() {
		$('#btn-edit-paito-paito').click(function(el) {
			el.preventDefault();
			edit_paito_paito();
		});
		
		$('#btn-delete-paito-paito').click(function(e) {
			e.preventDefault();
			var paito_seq = $(this).attr('data-paito-seq');
			paito_seq = parseInt(paito_seq);
			delete_paito_data(Swal, paito_seq);
		});
		
		$('#form-edit-paito-paito').submit(function(e) {
			var paito_nilai = $('#zodiac-name-id');
			if ($.trim(paito_nilai.val()) == '') {
				var fire_html = '<ul class="list-group">' +
					'<li class="list-group-item">' + 'Zodiac Name Required' + '</li>' +
					'<li class="list-group-item">' + 'Plese insert zodiac name' + '</li>' +
				'</ul>';
				Swal.fire({
					'title'				: "ERROR",
					'html'				: fire_html,  
					'confirmButtonText'	: 'OK', 
				});
				return false;
			}
		});
		
		

		
		$('#paito-tanggal-container').datepicker({
			format				: "yyyy-mm-dd",
			viewMode			: "days", 
			minViewMode			: "days",
			showButtonPanel		: true,
			autoclose			: true,
			todayHighlight		: true
		});
		
	});
</script>
	
		