<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-md-11">
				<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Add Shio');?></h6>
			</div>
			<div class="col-md-1 align-right">
				<button id="paito-close-button" class="d-none d-sm-inline-block btn btn-danger btn-sm shadow">
					<i class="fa fa-window-close"></i> Close
				</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-4">
				<form id="form-add-paito-paito" class="form-horizontal" method="post" action="<?= base_url("shio/shio/addaction");?>" enctype="multipart/form-data">
					<div class="form-group">
						<label for="vendor-name" class="control-label col-md-4 d-none d-sm-inline-block">Nama Vendor</label>
						<select id="vendor-name" name="vendor_seq" class="form-control">
							<?php
							$not_all = FALSE;
							if (isset($collect['vendor_data']->seq)) {
								$not_all = TRUE;
							}
							if (is_array($collect['paito_vendors']) && (count($collect['paito_vendors']) > 0)) {
								foreach ($collect['paito_vendors'] as $vendor_data) {
									if ($not_all === TRUE) {
										if ($vendor_data->seq == $collect['vendor_data']->seq) {
											$option_is_selected = ' selected="selected"';
										} else {
											$option_is_selected = '';
										}
									} else {
										$option_is_selected = '';
									}
									?>
									<option value="<?=$vendor_data->seq;?>"<?=$option_is_selected;?>><?=$vendor_data->vendor_name;?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="zodiac-year" class="control-label">Zodiac Year</label>
						<select id="zodiac-year" class="form-control" name="zodiac_year">
							<?php
							for ($i = $collect['select_years']['before']; $i < $collect['select_years']['future']; $i++) {
								if ($i == $collect['select_years']['current']) {
									$option_year_selected = ' selected="selected"';
								} else {
									$option_year_selected = '';
								}
								?>
								<option value="<?=$i;?>"<?=$option_year_selected;?>><?=$i;?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					<div class="form-group">
						<label for="zodiac-name-en" class="control-label">Zodiac Name (en_UK)</label>
						<input type="text" class="form-control" name="zodiac_name_english" id="zodiac-name-en" value="" />
					</div>
					<div class="form-group">
						<label for="zodiac-name-id" class="control-label">Zodiac Name (id_ID)</label>
						<input type="text" class="form-control" name="zodiac_name_indonesia" id="zodiac-name-id" value="" />
					</div>
					
					
					
					<div class="align-right">
						<a href="javascript:void(0)" id="btn-add-paito-paito" class="btn btn-sm btn-primary mb-4">
							<i class="fa fa-plus"></i> Add
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	function add_paito_data() {
		var error = 0;
		var zodiac_name_english = $('#zodiac-name-en');
		var zodiac_name_indonesia = $('#zodiac-name-id');
		var zodiac_year = $('#zodiac-year');
		var zodiac_vendor = $('#vendor-name');
		var paito_form = $('#form-add-paito-paito');
		var fire_html = '<ul class="list-group">';
		if ($.trim(zodiac_name_english.val()) == '') {
			error++;
			fire_html += '<li class="list-group-item">' + 'Zodiac Name (en_UK) Required' + '</li>';
		}
		if ($.trim(zodiac_name_indonesia.val()) == '') {
			error++;
			fire_html += '<li class="list-group-item">' + 'Zodiac Name (id_ID) Required' + '</li>';
		}
		if ($.trim(zodiac_year.val()) == '') {
			error++;
			fire_html += '<li class="list-group-item">' + 'Zodiac Year Required' + '</li>';
		}
		if ($.trim(zodiac_vendor.val()) == '') {
			error++;
			fire_html += '<li class="list-group-item">' + 'Shio Vendor Required' + '</li>';
		}
		fire_html += '</ul>';
		if (error > 0) {
			Swal.fire({
				'title'				: "ERROR",
				'html'				: fire_html,  
				'confirmButtonText'	: 'OK', 
			});
		} else {
			paito_form.submit();
		}
	}

	$(document).ready(function() {
		$('#btn-add-paito-paito').click(function(el) {
			el.preventDefault();
			add_paito_data();
		});
		
		/*
		$('#form-add-paito-paito').submit(function(e) {
			var paito_nilai = $('#paito-nilai');
			if ($.trim(paito_nilai.val()) == '') {
				var fire_html = '<ul class="list-group">' +
					'<li class="list-group-item">' + 'Paito Nilai Required' + '</li>' +
					'<li class="list-group-item">' + 'Plese insert paito nilai' + '</li>' +
				'</ul>';
				Swal.fire({
					'title'				: "ERROR",
					'html'				: fire_html,  
					'confirmButtonText'	: 'OK', 
				});
				return false;
			}
		});
		*/
		
		

		
		$('#paito-tanggal-container').datepicker({
			format				: "yyyy-mm-dd",
			viewMode			: "days", 
			minViewMode			: "days",
			showButtonPanel		: true,
			autoclose			: true,
			todayHighlight		: true
		});
		
	});
</script>
	
		