<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-md-11">
				<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Edit Vendors');?></h6>
			</div>
			<div class="col-md-1 align-right">
				<button id="paito-close-button" class="d-none d-sm-inline-block btn btn-danger btn-sm shadow">
					<i class="fa fa-window-close"></i> Close
				</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<form id="form-edit-paito-vendor" class="form-horizontal" method="post" action="<?= base_url("shio/vendors/edit/{$collect['vendor_data']->seq}/edit");?>" enctype="multipart/form-data">
					<div class="form-row mb-1">
						<label for="vendor-name" class="control-label col-md-4 d-none d-sm-inline-block">Nama Vendor</label>
						<div class="col-md-8 d-none d-sm-inline-block">
							<input type="text" class="form-control" name="vendor_name" id="vendor-name" placeholder="Nama Vendor" value="<?=$collect['vendor_data']->vendor_name;?>" />
						</div>
					</div>
					<div class="align-right">
						<a href="javascript:void(0)" id="btn-delete-paito-vendor" class="btn btn-sm btn-danger mb-4" data-paito-vendor-seq="<?=$collect['vendor_data']->seq;?>">
							<i class="fa fa-ban"></i> Delete
						</a>
						<a href="javascript:void(0)" id="btn-edit-paito-vendor" class="btn btn-sm btn-warning mb-4">
							<i class="fa fa-pencil-alt"></i> Edit
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	function edit_paito_vendor() {
		var vendor_name = $('#vendor-name');
		var vendor_form = $('#form-edit-paito-vendor');
		if (vendor_name.val().length == 0) {
			var fire_html = '<ul class="list-group">' +
				'<li class="list-group-item">' + 'Vendor Name Required' + '</li>' +
				'<li class="list-group-item">' + 'Plese insert vendor name' + '</li>' +
			'</ul>';
			Swal.fire({
				'title'				: "ERROR",
				'html'				: fire_html,  
				'confirmButtonText'	: 'OK', 
			});
			return false;
		} else {
			vendor_form.submit();
		}
	}
	function delete_paito_vendor(swalert, vendor_seq) {
		swalert.fire({
			title				: "Are you sure?",
			text				: "You will lost this data.",
			type				: "warning",
			showCancelButton	: true,
			confirmButtonColor	: "#DD6B55",
			confirmButtonText	: "Yes, delete it!",
			cancelButtonText	: "No, cancel please!"
		}).then((result) => {
			if (result.value) {
				$.ajax({
					'type'				: 'POST',
					'url'				: '<?= base_url('shio/vendors/vendor-delete');?>' + '/' + vendor_seq,
					'data'				: {
						'vendor_seq'		: vendor_seq
					},
					'dataType'			: 'json',
					'success'			: function(response) {
						if (response.status == true) {
							window.location.href = response.redirect_url;
						} else {
							var fire_html = '<ul class="list-group">';
							if (response.error.length > 0) {
								for (var i = 0; i < response.error.length; i++) {
									fire_html += '<li class="list-group-item">' + response.error[i].toString() + '</li>';
								}
							}
							fire_html += '</ul>';
							swalert.fire({
								'title'				: "ERROR",
								'html'				: fire_html,  
								'confirmButtonText'	: 'OK', 
							});
						}
					}
				});
			} else {
				swalert.fire({
					'title'				: 'Canceled',
					'html'				: '<p>You are canceling to delete data</p>',
					'confirmButtonText'	: 'OK'
				});
			}
		});
	}
	$(document).ready(function() {
		$('#btn-edit-paito-vendor').click(function(el) {
			el.preventDefault();
			edit_paito_vendor();
		});
		$('#btn-delete-paito-vendor').click(function(e) {
			e.preventDefault();
			var paito_vendor_seq = $(this).attr('data-paito-vendor-seq');
			paito_vendor_seq = parseInt(paito_vendor_seq);
			delete_paito_vendor(Swal, paito_vendor_seq);
		});
		
		
		
		
		
		
		document.getElementById("vendor-name").addEventListener("keydown", function(e) {
			if (!e) { 
				var e = window.event; 
			} 
			if (e.keyCode == 13) { 
				edit_paito_vendor();
			}
		}, false);

		
		
	});
</script>
	
		