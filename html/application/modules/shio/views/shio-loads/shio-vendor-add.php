<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-md-11">
				<h6 class="m-0 font-weight-bold text-primary"><?= (isset($title) ? $title : 'Add Vendors');?></h6>
			</div>
			<div class="col-md-1 align-right">
				<button id="paito-close-button" class="d-none d-sm-inline-block btn btn-danger btn-sm shadow">
					<i class="fa fa-window-close"></i> Close
				</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<form id="form-add-paito-vendor" class="form-horizontal" method="post" action="#" enctype="multipart/form-data">
					<div class="form-row mb-1">
						<label for="vendor-name" class="control-label col-md-4 d-none d-sm-inline-block">Nama Vendor</label>
						<div class="col-md-8 d-none d-sm-inline-block">
							<input type="text" class="form-control" name="vendor_name" id="vendor-name" placeholder="Nama Vendor" value="" />
						</div>
					</div>
					<div class="align-right">
						<a href="javascript:void(0)" id="btn-save-paito-vendor" class="btn btn-sm btn-success mb-4">
							<i class="fa fa-check"></i> Submit
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
		$('#btn-save-paito-vendor').click(function(el) {
			el.preventDefault();
			var vendor_name = $('#vendor-name');
			var vendor_form = $('#form-add-paito-vendor');
			if ($.trim(vendor_name.val()) == '') {
				var fire_html = '<ul class="list-group">' +
					'<li class="list-group-item">' + 'Vendor Name Required' + '</li>' +
					'<li class="list-group-item">' + 'Plese insert vendor name' + '</li>' +
				'</ul>';
				Swal.fire({
					'title'				: "ERROR",
					'html'				: fire_html,  
					'confirmButtonText'	: 'OK', 
				});
				return false;
			} else {
				vendor_form.attr('action', '<?= base_url('shio/vendors/addaction');?>');
				vendor_form.submit();
			}
		});
		
		$('#form-add-paito-vendor').submit(function(e) {
			var vendor_name = $('#vendor-name');
			$(this).attr('action', '<?= base_url('shio/vendors/addaction');?>');
			if ($.trim(vendor_name.val()) == '') {
				var fire_html = '<ul class="list-group">' +
					'<li class="list-group-item">' + 'Vendor Name Required' + '</li>' +
					'<li class="list-group-item">' + 'Plese insert vendor name' + '</li>' +
				'</ul>';
				Swal.fire({
					'title'				: "ERROR",
					'html'				: fire_html,  
					'confirmButtonText'	: 'OK', 
				});
				return false;
			}
		});
	});
</script>
	
		